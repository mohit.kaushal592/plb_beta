<?php
/**
 * @package	PLB tool
 * @module	Common Functions
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
function strip_zeros_from_date( $marked_string="" ) {
  // first remove the marked zeros
  $no_zeros = str_replace('*0', '', $marked_string);
  // then remove any remaining marks
  $cleaned_string = str_replace('*', '', $no_zeros);
  return $cleaned_string;
}

function redirect_to( $location = null ) {
  if ($location != null) {
    header("location: $location");
    exit;
  }
}

function output_message($message="") {
  if (!empty($message)) {
    $out = '<div class="alert">
              <button class="close" data-dismiss="alert">�</button>
              <strong>Warning!</strong> '.$message.'
              </div>';
    return $out;
  } else {
    return "";
  }
}

function __autoload($class_name) {
	$class_name = strtolower($class_name);
  $path = LIB_PATH.DS."{$class_name}.php";
  if(file_exists($path)) {
    require_once($path);
  } else {
		!preg_match('/^phpexcel/i', $class_name) ? die("The file {$class_name}.php could not be found.") : null;
	}
}

function include_layout_template($template="") {
	include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}

function include_view_template($template="") {
	include(SITE_ROOT.DS.'public'.DS.'views'.DS.$template);
}

function log_action($action, $message="") {
	$logfile = SITE_ROOT.DS.'logs'.DS.'log.txt';
	$new = file_exists($logfile) ? false : true;
  if($handle = fopen($logfile, 'a')) { // append
    $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
		$content = "{$timestamp} | {$action}: {$message}\n";
    fwrite($handle, $content);
    fclose($handle);
    if($new) { chmod($logfile, 0755); }
  } else {
    echo "Could not open log file for writing.";
  }
}

function datetime_to_text($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%B %d, %Y at %I:%M %p", $unixdatetime);
}

function date_to_text($datetime="") {
  $unixdatetime = strtotime($datetime);
  return date("M j, Y", $unixdatetime);
}

function pr($array=array()){
  echo "<pre>";
  print_r($array);
  echo "</pre>";
}


function object_to_array($obj){
  if(is_object($obj)){
    $array = array();
    foreach($obj as $key=>$val){
      $array[$key] = is_object($val) ? object_to_array($val) : $val;
    }
    return $array;
  }
  return $obj;
}

function is_active_menu($path=null){
  if(isset($path)){
    $curUrl = $_SERVER['REQUEST_URI'];
    $curUrlSplt = explode('?',$curUrl);
    if(count($curUrlSplt)>1){
      $curUrl = $curUrlSplt[0];
      $curUrlSplt = explode('&', $curUrlSplt[1]);
      foreach($curUrlSplt as $_curUrl){
        if(preg_match('/^act\=/', $_curUrl)){
          $curUrl .= '?'.$_curUrl;
        }
      }
    }
    $path = str_replace('?', '-qst-', $path);
    $path = str_replace('&', '-mpc-', $path);
    $path = str_replace('=', '-eql-', $path);
    
    $curUrl = str_replace('?', '-qst-', $curUrl);
    $curUrl = str_replace('&', '-mpc-', $curUrl);
    $curUrl = str_replace('=', '-eql-', $curUrl);
    $curUrl =  preg_match('/^\/$/', $curUrl)? 'index.php' : $curUrl;
    $path = addslashes($path);
    if(preg_match('/'.$path.'$/i', $curUrl)){
      return true;
    }
  }
  return false;
}

function current_url(){
  return preg_replace('/^\//', '', $_SERVER['REQUEST_URI']);
}

 function get_base_url($skipPublic=false)
  {
      /* First we need to get the protocol the website is using */
      $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https://' : 'http://';

      /* returns /myproject/index.php */
      $path = $_SERVER['PHP_SELF'];

      /*
       * returns an array with:
       * Array (
       *  [dirname] => /myproject/
       *  [basename] => index.php
       *  [extension] => php
       *  [filename] => index
       * )
       */
      $path_parts = pathinfo($path);
      $directory = $path_parts['dirname'];
      /*
       * If we are visiting a page off the base URL, the dirname would just be a "/",
       * If it is, we would want to remove this
       */
      $directory = ($directory == "/") ? "" : $directory;
      /*
       * if $skipPublic is true than remove public from url
       * otherwise don't made any change
       */
      $directory = $skipPublic===true? preg_replace('/\/public$/', '', $directory) : $directory;
      
      /* Returns localhost OR mysite.com */
      $host = $_SERVER['HTTP_HOST'];

      /*
       * Returns:
       * http://localhost/mysite
       * OR
       * https://mysite.com
       */
      return $protocol . $host . $directory;
  }
  
  function get_domain_name($url)
  {
    $url=str_replace(array("https://","http://","www."),"",$url);
  
      preg_match( "/([a-zA-Z0-9.-]*)\/?.*/i", $url, $domain );
      $domain = explode( ".", $domain[1] );
  
      if ( strlen( end($domain) ) == 2 && ( strlen($domain[count($domain)-2]) == 3 || strlen($domain[count($domain)-2]) == 2 )  )
      {
          # special case domains -- ex: co.uk .in .ca
          return strtolower( $domain[count($domain)-3] . "." . $domain[count($domain)-2] . "." . end( $domain ) );
      }elseif(count($domain)==1){
	return strtolower(end( $domain ));
      }
      else
      {
          # regular .com type domains -- three or more letters
          return strtolower( $domain[count($domain)-2] . "." . end( $domain ) );
      }
  }
  
  if(!function_exists('get_called_class')) {
    function get_called_class($bt = false,$l = 1) { 
      if (!$bt) $bt = debug_backtrace();
      if (!isset($bt[$l])) throw new Exception("Cannot find called class -> stack level too deep.");
      if (!isset($bt[$l]['type'])) {
        throw new Exception ('type not set');
      }
      else switch ($bt[$l]['type']) {
        case '::':
            $lines = file($bt[$l]['file']);
            $i = 0;
            $callerLine = '';
            do {
                $i++;
                $callerLine = $lines[$bt[$l]['line']-$i] . $callerLine;
            } while (stripos($callerLine,$bt[$l]['function']) === false);
            preg_match('/([a-zA-Z0-9\_]+)::'.$bt[$l]['function'].'/',
                        $callerLine,
                        $matches);
            if (!isset($matches[1])) {
                // must be an edge case.
                throw new Exception ("Could not find caller class: originating method call is obscured.");
            }
            switch ($matches[1]) {
              case 'self':
              case 'parent':
                  return get_called_class($bt,$l+1);
              default:
                  return $matches[1];
            }
            // won't get here.
        case '->': switch ($bt[$l]['function']) {
              case '__get':
                  // edge case -> get class of calling object
                  if (!is_object($bt[$l]['object'])) throw new Exception ("Edge case fail. __get called on non object.");
                  return get_class($bt[$l]['object']);
              default: return get_class($bt[$l]['object']);
            }

        default: throw new Exception ("Unknown backtrace method type");
      }
    }
  }
  
  function sqlDebugLogs(){
    global $database;
    $sqlLogs = $database->get_sql_logs();
    if(DB_LOGS == 1 and !empty($sqlLogs)){
      echo "<br clear='all' />";
      echo "<div style='clear:both;width:100%;display:block;background:#FFF;'>";
      echo "<h3>Sql Queries</h3>";
      echo "<table style='width:100%;'>";
      echo "<tr><th>Sr. No</th><th style='text-align:left;'>Sql</th><th style='text-align:left;'>Error</th><th style='text-align:left;'>Execution Time</th></tr>";
      foreach($sqlLogs as $k=>$_logs){
        echo "<tr><td>". ++$k ."</td><td>{$_logs['sql']}</td><td>{$_logs['error']}</td><td>{$_logs['time']}</td></tr>";
      }
      echo "</table>";
      echo "</div>";
    }
  }
  
  function currency_format($amount=0, $currency='USD'){
    $amount = !empty($amount) ? $amount : 0;
    $currency = !empty($currency) ? $currency : 'USD';
    
    switch(strtolower($currency)){
        case 'dollars':
        case 'usd':
            $currency = '$';
            break;
        case 'pounds':
        case 'gbp':
            $currency = '&pound;';
            break;
        case 'euro':
        case 'eur':
            $currency = '&euro;';
            break;
    }
    
    $amount = preg_replace('/.00$/', '', number_format($amount, 2));
    return $currency.' '.$amount;
  }
  function getFormOptions($options=array(), $selected=array()){
    if(!empty($options)){
      $selected = is_array($selected) ? $selected : (array)$selected;
      $_html = '';
      foreach($options as $k=>$v){
        $_selected = in_array($k, $selected)? "selected='selected'" : '';
        $_html .= '<option value="'.$k.'" '.$_selected.'>'.$v.'</option>';
      }
      return $_html;
    }
    return false;
  }
  
  function getCampaignIcons($appLink=array(), &$viewData, $allowLink=true){
    global $session;
    $icons = '';
    if(!empty($appLink)){
      if(!($_rejectReasons = $viewData->get('_rejectReasons'))){
	$RejectReason = new RejectReason;
        $_rejectReasons = $RejectReason->find_list(array('where'=>'RejectReason.status=1', 'fields'=>array('id','data'), 'order'=>'RejectReason.data ASC'));
        $viewData->set('_rejectReasons', $_rejectReasons);
      }
      $_reasonIds = explode(',', $appLink['r_data_i']);
      $_reasonTxt = array();
      if(!empty($_reasonIds)){
        foreach($_reasonIds as $_rId){
          $_reasonTxt[] = $_rejectReasons[$_rId];
        }
      }
      $userCanDo = ($appLink['user_id']==$session->read('User.id') and $allowLink) ? true :false;
      $submitTitle = $userCanDo ? 'Link not Submitted, Click to Submit.' : 'Link not Submitted';
      if($appLink['approve_status']==1){
        
        // phase II approval
        if($appLink['is_secondary_approval']==1){
          // phase I approval
          $icons .= '<span class="icon tip-top status-accept" title="Approved Phase I"><i class="icon-ok icon-large"></i><sup class="icontxt">1</sup></span>';
          
          if($appLink['secondary_approval']==1){
            $icons .= '<span class="icon tip-top status-accept" title="Approved Phase II"><i class="icon-ok icon-large"></i><sup class="icontxt">2</sup></span>';
            // submit link
            $icons .= $appLink['final_link_submit_status']==1 ? '<span class="icon tip-top status-accept" title="Link Submitted"><i class="icon-link icon-large"></i></span>' : '<span class="icon tip-top '.($userCanDo ? '_submitLink' : '').'" title="'.$submitTitle.'" '.($userCanDo ? 'rel="'.$appLink['id'].'"' : '').'><i class="icon-link icon-large"></i></span>';
          }else if($appLink['secondary_approval']==0){
            // phase II approval
            $icons .= '<span class="icon tip-top status-reject" title="'. join(', ', $_reasonTxt).'"><i class="icon-ban-circle icon-large"></i><sup class="icontxt">2</sup></span>';
          }else if($appLink['secondary_approval']==-1){
            // phase II approval
            $icons .= '<span class="icon tip-top" title="Approval Pending Phase II"><i class="icon-ok icon-large"></i><sup class="icontxt">2</sup></span>';
          }
        }else{
          // phase I approval
          $icons .= '<span class="icon tip-top status-accept" title="Approved"><i class="icon-ok icon-large"></i></span>';
          // submit link
          $icons .= $appLink['final_link_submit_status']==1 ? '<span class="icon tip-top status-accept" title="Link Submitted"><i class="icon-link icon-large"></i></span>' : '<span class="icon tip-top '.($userCanDo ? '_submitLink' : '').'" title="'.$submitTitle.'" '.($userCanDo ? 'rel="'.$appLink['id'].'"' : '').'><i class="icon-link icon-large"></i></span>';
        }
        
        
      }else if($appLink['approve_status']==0){
        // phase I approval
        $icons .= '<span class="icon tip-top status-reject" title="'.join(", ", $_reasonTxt).'"><i class="icon-ban-circle icon-large"></i></span>';
      }else if($appLink['approve_status']==-1){
        // phase II approval
        if($appLink['is_secondary_approval']==1){
          // phase I approval
          $icons .= '<span class="icon tip-top" title="Approval Pending Phase I"><i class="icon-ok icon-large"></i><sup class="icontxt">1</sup></span>';
        }else{
          // phase I approval
          $icons .= '<span class="icon tip-top" title="Approval Pending"><i class="icon-ok icon-large"></i></span>';
        }
      }
      
    }
    return $icons;
  }
  
  function canUserDoThis($action, $data=array(), $appChkBx=false){
    global $session;
    $_acts = is_array($action)?$action : (array)$action;
    $role = $session->read('User.user_type');
    $userId = $session->read('User.id');
    //Code by Arvind 
    $supper = $session->read('User.supper_user');
    if($role == 'user' && $supper == 1)
    {
      $role =  'supperuser'; 
    }
    //end 
    $_actions = array(
      'superadmin' => array(
        'approve_phase_1',
        'approve_phase_2',
        'approval_delete',
	'link_delete',
	'payment',
	'paypal',
	'client',
	'admin_comment',
	'payment_edit'
      ),
      'admin' => array(
        //'forward',
       // 'approve_phase_1',
        //'approve_phase_2',
	'link_delete'
	//'paypal',
	//'client',
	//'admin_comment',
      ),
      'user' => array(
	'link_delete'
      ),
      //Code by Arvind 
        'supperuser' => array(
        'approve_phase_1',
        'approve_phase_2',
	'approval_delete',
      ),
        //end
    );
    if(!empty($role) and !empty($_acts)){
      $role = strtolower($role);
      foreach($_acts as $action){
	if(in_array($action, array('approve_phase_2'))){
	  if(!empty($data) and $appChkBx==true){
	    if($data['approve_status']==1 and $data['is_secondary_approval']==1){
	      return (
		($data['secondary_app_user']==$userId AND in_array('approve_phase_2', $_actions[$role]))
		OR
		($role=='superadmin' AND in_array('approve_phase_2', $_actions['superadmin']))
	      ) ? true : false;
	    }
	  }
	  //return !empty($_actions[$role]) ? true : false;
	}
	if(in_array($action, array('payment_edit')) and $userId==$data['user_id']){
	  return true;
	}
	if(in_array($action, $_actions[$role])){
	  return true;
	}
      }
    }
    return false;
  }
  
  function canDeleteLink($status, $userId){
    global $session;
    $role = $session->read('User.user_type');
    $_userId = $session->read('User.id');
    if(!empty($status) and !empty($userId)){
      if(canUserDoThis('link_delete')){
	if($role=='superadmin' || ($status=='Underprocess' && $userId==$_userId)){
	  return true;
	}
      }
    }
    return false;
  }
  
  function getCurrencyRate($from_Currency,$to_Currency='USD',$amount=1) {
    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);
    $url = "http://www.google.com/ig/calculator?hl=en&q=$amount+$from_Currency=?$to_Currency";
    $ch = curl_init();
    $timeout = 0;
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode('"', $rawdata);
    $data = explode(' ', $data['3']);
    $var = $data['0'];
    return round($var,3);
}

function array_avg($values=array()) {
  if(!empty($values)){
    $values = is_array($values) ? $values : (array) $values;
    return round(array_sum($values)/count($values), 2);
  }
  return $values;
}

function getIconHtml($status=null, $titles=array()){
    $titles = is_array($titles) ? $titles : (array)$titles;
    $_titles = array('0'=>'Rejected', '1'=>'Accepted', '-1'=>'Pending');
    if(!empty($titles)){
      foreach($titles as $k=>$v){
	$_titles[$k]= $v;
      }
    }
    
    $html = '';
    switch($status){
      case '1':
	$html .= "<span class='icon tip-top status-accept' title='".$_titles[$status]."'><i class='icon-ok icon-large'></i></span>";
	break;
      case '0':
	$html .= "<span class='icon tip-top status-reject' title='".$_titles[$status]."'><i class='icon-ban-circle icon-large'></i></span>";
	break;
      case '-1':
	$html .= "<span class='icon tip-top' title='".$_titles[$status]."'><i class='icon-ok icon-large'></i></span>";
	break;
    }
    return $html;
}

function monthsList(){
  return array('01'=>'January', '02'=>'February', '03'=>'March', '04'=>'April', '05'=>'May', '06'=>'June', '07'=>'July', '08'=>'August', '09'=>'September', '10'=>'October', '11'=>'November', '12'=>'December');
}

function yearsList($options=array()){
  $options = is_array($options) ? $options : (array)$options;
  $_options = array_merge(array('min'=>date('Y'), 'max'=>(date('Y')+10)), $options);
  $years=array();
  for($i=$_options['min']; $i<=$_options['max']; $i++){
    $years[$i] = $i;
  }
  return $years;
}

function select2DataFormat($data){
  if(!empty($data)){
    $data = is_array($data)?$data : (array)$data;
    $result=array();
    foreach($data as $k=>$v){
      $result[]=array('id'=>$k,'text'=>$v);
    }
    return $result;
  }
  return $data;
}

function generatePassword($length) {
      $lowercase = "qwertyuiopasdfghjkzxcvbnm";
      $uppercase = "ASDFGHJKLZXCVBNMQWERTYUOP";
      $numbers = "123456789";
      $specialcharacters = "{}[];:,./<>?_+~!@#";
      $randomCode = "";
      mt_srand(crc32(microtime()));
      $max = strlen($lowercase) - 1;
      for ($x = 0; $x < abs($length/3); $x++) {
	  $randomCode .= $lowercase{mt_rand(0, $max)};
      }
      $max = strlen($uppercase) - 1;
      for ($x = 0; $x < abs($length/3); $x++) {
	  $randomCode .= $uppercase{mt_rand(0, $max)};
      }
      $max = strlen($specialcharacters) - 1;
      for ($x = 0; $x < abs($length/3); $x++) {
	  $randomCode .= $specialcharacters{mt_rand(0, $max)};
      }
      $max = strlen($numbers) - 1;
      for ($x = 0; $x < abs($length/3); $x++) {
	  $randomCode .= $numbers{mt_rand(0, $max)};
      }
      return str_shuffle($randomCode);
  }

function get_user_name_by_id($id) {
    global $database;
    $result_set = $database->query("select `username` from `tracking_login` WHERE `id` = ".$id);
    $arr = $database->fetch_array($result_set);
    return $arr['username'];
}    
  
?>