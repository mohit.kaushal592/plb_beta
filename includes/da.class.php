<?php
/**
 * Da for specific domain
 * @author Akhtar Khan
 * @project PLB Tool
 * @module Da
 */
class Da{

    // you can obtain you access id and secret key here: http://www.seomoz.org/api/keys
    private $accessID;	
    //Add your secretKey here
    private $secretKey; 
    
    // Set your expires for several minutes into the future.
    // Values excessively far in the future will not be honored by the Mozscape API.
    private $expires;
    
    // A new linefeed is necessary between your AccessID and Expires.
    private $stringToSign;
    
    // Get the "raw" or binary output of the hmac hash.
    private $binarySignature;
    
    // We need to base64-encode it and then url-encode that.
    private $urlSafeSignature;
    
    // Add up all the bit flags you want returned.
    // Learn more here: http://apiwiki.seomoz.org/categories/api-reference
    private $cols = '68719476736';
    
    function __construct($options=array()){
        if(!is_array($options)){
            $options = (array)$options;
        }
/*
        $_defaultOptions = array(
            'accessID'=>'mozscape-5a8a20f424',
            'secretKey'=>'7430bccf8cc4e9fc66555f801458e6e1',
            'expires'=> time() + 300
            );*/
               
     $_defaultOptions = array(
            'accessID'=>'mozscape-5a8a20f424',
            'secretKey'=>'88711627b02d09112031fc673afb0365',
            'expires'=> time() + 3000
            );
        $options = array_merge($_defaultOptions, $options);
        $this->setAccessId($options['accessID']);
        $this->setSecretKey($options['secretKey']);
        $this->setExpires($options['expires']);
        $this->startApi();
      /*  ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
    }
    
    public function setAccessId($id){
        $this->accessID = $id;
    }
    
    public function setSecretKey($secretKey){
        $this->secretKey = $secretKey;
    }
    
    public function setExpires($expires){
        $this->expires = $expires;
    }
    
    private function setStringToSign(){
        $this->stringToSign = $this->accessID."\n".$this->expires;
    }
    
    private function setBinarySignature(){
        $this->binarySignature = hash_hmac('sha1', $this->stringToSign, $this->secretKey, true);
    }
    
    private function setUrlSafeSignature(){
        $this->urlSafeSignature = urlencode(base64_encode($this->binarySignature));
    }
    
    public function startApi(){
        $this->setStringToSign();
        $this->setBinarySignature();
        $this->setUrlSafeSignature();
    }
    
    public function getDa($domain){ 
        $api_key = 'u4tiaFBopeiIKOxiBgxEvabg9MiMsZWvWCLA0wyXVO73iYKMZqt2pEAROTPNWPel';
        $metrics_url = 'https://api.moz.com/v2/url_metrics';
        $headers = [
            'Authorization: Bearer ' . $api_key,
            'Content-Type: application/json'
        ];
        $payload = json_encode(['targets' => [$domain]]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $metrics_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        $response = curl_exec($ch);
        if(curl_errno($ch)){
            echo 'cURL Error: ' . curl_error($ch);
        } 
        if ($response === false) {
            $error = curl_error($ch);
            curl_close($ch);
            return 'Error fetching DA and Spam Score: ' . $error;
        } 
        curl_close($ch); 
        $data = json_decode($response, true); 
        if (isset($data['results']) && count($data['results']) > 0) {
            $result = $data['results'][0];
            $da = isset($result['domain_authority']) ? $result['domain_authority'] : 'N/A';
            $spam_score = isset($result['spam_score']) ? $result['spam_score'] : 'N/A';
            return $da;
        }else{
            return 'Failed to fetch data.'; 
        } 
    }
    
    public function getDaOld($domain){
        if(!empty($domain)){
            // Now put your entire request together.
            // This uses the Mozscape URL Metrics API.
            //$requestUrl = "http://lsapi.seomoz.com/linkscape/url-metrics/";
            $requestUrl = "https://api.moz.com/v2/url_metrics";
            $requestUrl .= urlencode($domain);
            $requestUrl .= "?Cols=".$this->cols;
            $requestUrl .= "&AccessID=".$this->accessID;
            $requestUrl .= "&Expires=".$this->expires;
            $requestUrl .= "&Signature=".$this->urlSafeSignature;
            // We can easily use Curl to send off our request.
            $options = array(
                    CURLOPT_RETURNTRANSFER => true
                    );
            
            $ch = curl_init($requestUrl);
            curl_setopt_array($ch, $options);
            $content = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($content);
            $result = object_to_array($data);
			
			// //echo '<pre>'; 
            return $content;
            // //print_r($result);
            // //return 33433;
            // exit;
            
            return round((float)$result['pda'], 2);
        }
        //return 0;
    }
	
function getSS($domain){

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://lsapi.seomoz.com/v2/links',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "target":"'.$domain.'",
    "target_scope":"page",
    "limit":1,
    "filter":"external+nofollow"
}',
  CURLOPT_HTTPHEADER => array(
    'AccessID: mozscape-5a8a20f424',
    'secretKey: 88711627b02d09112031fc673afb0365',
    'Authorization: Basic bW96c2NhcGUtNWE4YTIwZjQyNDpiNGZlMDgxZWViMjY1MDFmODJhZDMyNTk2OWE5MzM5NQ==',
    'Content-Type: text/plain',
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$data = json_decode($response);
$result = object_to_array($data);
$ssresult = 0;
if(!empty($result['results'])){
    $ssresult = $result['results'][0]->target->spam_score;
}
return $ssresult;





}

	public function getSpamscore($domain){

if(!empty($domain)){
            // This uses the Mozscape URL Metrics API.
            $requestUrl = "https://lsapi.seomoz.com/v2/links";
            $postdata = array('target' => $domain,'target_scope'=>'page','limit'=>1);
            $postdata = json_encode($postdata);
            $options = array(CURLOPT_RETURNTRANSFER => true,CURLOPT_POSTFIELDS=>$postdata,CURLOPT_HTTPHEADER=>array("authorization: Basic bW96c2NhcGUtNWE4YTIwZjQyNDpiNGZlMDgxZWViMjY1MDFmODJhZDMyNTk2OWE5MzM5NQ=="));
            $ch = curl_init($requestUrl);
            curl_setopt_array($ch, $options);
            $content = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($content,true);
                if(!empty($data['results'])){
                 $datarecord =  (isset($data['results']['0']['target']['spam_score'])?$data['results']['0']['target']['spam_score']:0);
                }
return $datarecord;
        }
        return 0;
    }

    public function getDomainlocation($domain){
       $whourl =  'https://www.whoisxmlapi.com/whoisserver/WhoisService?apiKey=at_Ji8G8pSz329DVGZOGaWGL0yZka15n&domainName='.$domain.'&outputFormat=json';

         $ch = curl_init($whourl);
         $options = array(
                CURLOPT_RETURNTRANSFER => true
            );
                     curl_setopt_array($ch, $options);
            $content = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($content,true);
            $country= "";
if(isset($data['WhoisRecord']['administrativeContact'])){
               $admindetils = $data['WhoisRecord']['administrativeContact'];
               $country = isset($admindetils['countryCode'])?$admindetils['countryCode']:"";
            }
            if(empty($country)){
              if(isset($data['WhoisRecord']['registrant'])){
              	$admindetils = $data['WhoisRecord']['registrant'];
               $country = isset($admindetils['countryCode'])?$admindetils['countryCode']:"";
              }
          }

          if(empty($country)){
          	$country = "USA";
          }

/*
            if(isset($data['WhoisRecord']['administrativeContact'])){
               $admindetils = $data['WhoisRecord']['administrativeContact'];
               $state = $admindetils['state'];
               $country = $admindetils['countryCode'];
               $contstr = $country;
            }
            */
            return $country;
    }



    
    
}
?>