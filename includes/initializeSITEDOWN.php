<?php
/**
 * @package	PLB tool
 * @module	Initialize
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */

 /**
  * Session expire time
  * 12 hours = 12*60*60 = 43200 sec
  */ echo "Site is down for 1 Hour from 01:00PM to 02:00PM<br>Sorry for the inconvenience!";exit;
ini_set('session.gc_maxlifetime', 43200);
ini_set('session.gc_probability',1);
ini_set('session.gc_divisor',1);
ini_set('session.cookie_lifetime', 0);
// set timeout unlimited
ini_set('max_execution_time', 0);
// Define the core paths
// Define them as absolute paths to make sure that require_once works as expected
defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);
defined('SITE_ROOT') ? null : define('SITE_ROOT', dirname(dirname(__FILE__)));
defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'includes');
defined('WWW_ROOT') ? null : define('WWW_ROOT', SITE_ROOT.DS.'public');

// load config file first
require_once(LIB_PATH.DS.'config.php');

// load basic functions next so that everything after can use them
require_once(LIB_PATH.DS.'functions.php');

// load core objects
require_once(LIB_PATH.DS.'view_data.php');
require_once(LIB_PATH.DS.'session.php');
require_once(LIB_PATH.DS.'database.php');
require_once(LIB_PATH.DS.'database_object.php');
require_once(LIB_PATH.DS.'pagination'.DS.'pagination.php');
require_once(LIB_PATH.DS."phpMail.php");
require_once(LIB_PATH.DS."pr.class.php");
require_once(LIB_PATH.DS."da.class.php");
require_once(LIB_PATH.DS.'phpexcel'.DS."PHPExcel.php");


// load database-related classes
if(!empty($_config['db_files'])){
    $_dbfl = $_config['db_files'];
    foreach($_dbfl as $_fl){
        if(!empty($_fl)){
            require_once(LIB_PATH.DS.'model'.DS.$_fl.'.php');    
        }
    }
}


?>