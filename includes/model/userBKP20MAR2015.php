<?php
/**
 * @package	PLB tool
 * @module	User
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class User extends DatabaseObject {
	
	public $table_name="tracking_login";
	public $db_fields = array('id', 'username', 'password', 'first_name', 'last_name', 'email_id', 'status', 'user_type', 'created_on','designation','mobile_no','employ_id','address','alternate_email','user_pic');
	
	public $relationTables = array(
		'belongsTo' =>array(
		),
		'hasMany' => array(
		)
	);
	
	public function authenticate($username="", $password="") {
		global $database;
		$username = $database->escape_value($username);
		$password = $database->escape_value($password);
		
		$sql  = "SELECT ". $this->_selectFields() ." FROM ". $this->_getTableAliasForSql();
		$sql .= " WHERE User.username = '{$username}' ";
		$sql .= "AND User.password = md5('{$password}') ";
		$sql .= "AND User.status = 1 ";
		$sql .= "LIMIT 1";
		$result_array = $this->find_by_sql($sql);;
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	public function find_list($options=array()){
		$options = is_array($options)?$options : (array)$options;
		$_defaultOptions = array('fields'=>array('id','first_name','last_name'));
		$options = array_merge($_defaultOptions, $options);
		$users = $this->find_all($options);
		$list = array();
		if(!empty($users)){
			foreach($users as $user){
				$list[$user['User']['id']] = ucwords($user['User']['first_name'].' '.$user['User']['last_name']);
			}
		}
		return $list;
	}
	
	public function getList(){
		$usersList = $this->find_list(array('order'=>'User.first_name ASC, User.last_name ASC'));
		return $usersList;
	}

}

?>