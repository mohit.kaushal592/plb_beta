<?php
/**
 * @package	PLB tool
 * @module	Domain
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Domain extends DatabaseObject {
	
	public $table_name="tbl_plb_domains";
	public $db_fields = array('id','name','is_specific_camp','b_camp_string','user_id', 'status','added_on');
	//Ajit: add Field  user_id on 15 DESC 2015
	public $relationTables = array(
		'belongsTo' =>array(
                    'User'=>array(
				'foreignKey'=> 'user_id'
			),
		),
		'hasMany' => array(
			'BlacklistDomain'=>array(
				'foreignKey'=> 'domain_id'
			)
		)
	);
	
}

?>