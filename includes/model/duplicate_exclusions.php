<?php
/**
 * @package	PLB tool
 * @module	Influencers
 * @author	Nitin Pant
 * @email	nitin.pant@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class DuplicateExclusions extends DatabaseObject {
	
	public $table_name="duplicate_exclusions";
	public $db_fields = array('id','campaign_id','type');
        
	//Nitin
        
        public function fetchAll($options=array(), $paginate=false){
		$options = is_array($options)?$options:(array)$options;
		if($paginate==true){
			$options['limit'] = $this->__setPaginationLimitOffset($options, $paginate);
		}
		$_options = $options;
		$_options['fields'] = array('DISTINCT DuplicateExclusions.id');
		unset($_options['contain']);
		if(!empty($options['contain'])){
			foreach($options['contain'] as $_K=>$_V){
				$k = is_array($_V) ? $_K : $_V;
				$_options['contain'][$k]['fields'] = false; 
				if(is_array($_V)){
					$_options['contain'][$k] = $options['contain'][$k];
				}
				$_options['contain'][$k]['fields'] = false; 
			}
		}
		$approvalIds = $this->find_all($_options);
		if(!empty($approvalIds)){
			$_approvalIds = array();
			foreach($approvalIds as $_appId){
				$_approvalIds[] = $_appId['DuplicateExclusions']['id'];
			}
			$approvalIds = $_approvalIds;
		}
		$results = array();
		if(!empty($approvalIds)){
			$options['where'] = !empty($options['where']) ? $options['where'].' AND DuplicateExclusions.id IN('. implode(',', $approvalIds).') ' : ' DuplicateExclusions.id IN('. implode(',', $approvalIds).') '; 
			
			if(!empty($options['contain'])){
				foreach($options['contain'] as $_K=>$_V){
					$k = is_array($_V) ? $_K : $_V;
					if(is_array($_V)){
						if(!empty($options['contain'][$k]['where'])){
							unset($options['contain'][$k]['where']);
						}
					}
				}
			}
			unset($options['limit']);
			$results = $this->find_all($options);
		}
		
		return $results;
	}
	
}

?>