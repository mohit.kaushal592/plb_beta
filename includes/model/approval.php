<?php
/**
 * @package	PLB tool
 * @module	Approval
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Approval extends DatabaseObject {
	
	public $table_name="tbl_approval_links_main";
	public $db_fields = array('id', 'domain','domain_final', 'domain_id', 'ip', 'pr', 'da', 'added_on', 'child_update_date', 'approached_by', 'user_id', 'campaigns_count', 'camps_id', 'narration_text', 'narration_admin', 'r_data', 'ip_conflict_status', 'superadmin_status', 'status', 'content_writer','payment_type','sc','ac','keyword','sv','rankval','title','title_id','target_url','annual_traffic','dr','category','backlinks','adult','gambling','index_status','index_count','index_time');
	// Ajit: add some field  'payment_type' on 23 June 2017
	
	public $relationTables = array(
		'belongsTo' =>array(
			'User'=>array(
				'foreignKey'=> 'user_id'
			),
			'ApprovalLink'=>array(
				'foreignKey'=> false,
				'conditions' => 'ApprovalLink.parent_id=Approval.id',
				'relation' => 'hasMany'
			)
		),
		'hasMany' => array(
		)
	);
	
	public $daFilterOptions = array(
		'list'=> array(
			'lte-30'=>'Below or Equal 30',
			'gt-30' => 'Above 30',
			'gt-40'=> 'Above 40',
			'gt-50'=> 'Above 50',
			'gt-60'=> 'Above 60',
			'gt-70'=> 'Above 70',
			'gt-80'=> 'Above 80',
			'gt-90'=> 'Above 90'
		),
		'operators'=>array(
			'lte'=>'<=',
			'lt'=>'<',
			'gt' => '>'
		)
	);

	public $scFilterOptions = array(
		'list'=> array('lt-5'=>'Less then 5%',
			'lt-10'=>'Less then 10%',
			'lt-20'=>'Less then 20%',
	       'lt-30'=>'Less then 30%'
	   ),
	);

	public $acFilterOptions = array(
		'list'=> array('US'=>'US',
			'IN'=>'IN'
	   ),
	);

	public function downloadAll($options=array(), $paginate=false){
		$_options = $options;
		$_options['group'] = 'Approval.id';
		if(!empty($options['contain'])){
			foreach($options['contain'] as $_K=>$_V){
				$k = is_array($_V) ? $_K : $_V;
					if(is_array($_V)){
						if(!empty($options['contain'][$k]['where'])){
							unset($_options['contain'][$k]['where']);
						}
				}
		}
	}

		$approvalList = $this->find_all($_options);
		return $approvalList;

	}
	
	public function fetchAll($options=array(), $paginate=false){
		$options = is_array($options)?$options:(array)$options;
		if($paginate==true){
			$options['limit'] = $this->__setPaginationLimitOffset($options, $paginate);
		}
		$_options = $options;
		$_options['fields'] = array('DISTINCT Approval.id');
		unset($_options['contain']);
		if(!empty($options['contain'])){
			foreach($options['contain'] as $_K=>$_V){
				$k = is_array($_V) ? $_K : $_V;
				$_options['contain'][$k]['fields'] = false; 
				if(is_array($_V)){
					$_options['contain'][$k] = $options['contain'][$k];
				}
				$_options['contain'][$k]['fields'] = false; 
			}
		}
		
		if(!empty($_options['contain']['ApprovalLink'])){
			$_options['contain']['ApprovalLink']['fields'] = array('ApprovalLink.added_on');
		}
		$approvalIds = $this->find_all($_options);
		if(!empty($approvalIds)){
			$_approvalIds = array();
			foreach($approvalIds as $_appId){
				$_approvalIds[] = $_appId['Approval']['id'];
			}
			$approvalIds = $_approvalIds;
		}
		$results = array();
		if(!empty($approvalIds)){
			$options['where'] = !empty($options['where']) ? $options['where'].' AND Approval.id IN('. implode(',', $approvalIds).') ' : ' Approval.id IN('. implode(',', $approvalIds).') '; 
			
			if(!empty($options['contain'])){
				foreach($options['contain'] as $_K=>$_V){
					$k = is_array($_V) ? $_K : $_V;
					if(is_array($_V)){
						if(!empty($options['contain'][$k]['where'])){
							unset($options['contain'][$k]['where']);
						}
					}
				}
			}
			unset($options['limit']);
			$results = $this->find_all($options);
		}
		
		return $results;
	}
	
}

?>