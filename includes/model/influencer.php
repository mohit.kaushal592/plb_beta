<?php
/**
 * @package	PLB tool
 * @module	Influencers
 * @author	Nitin Pant
 * @email	nitin.pant@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Influencer extends DatabaseObject {
	
	public $table_name="tbl_influencers";
	public $db_fields = array('id','name','blog_url','domain_ids','category','twitter_handle', 'facebook_id','gplus_id', 'instagram_id' , 'price', 'user_id', 'location', 'comment', 'added_on', 'fb_likes', 'twitter_followers', 'instagram_followers', 'gplus_followers', 'klout_score', 'domain_authority','twitter_handle_price','facebook_id_price','instagram_id_price');
	//Nitin
	public $relationTables = array(
		'belongsTo' =>array(
                    'User'=>array(
				'foreignKey'=> 'user_id'
			),
		)
	);
	
}

?>