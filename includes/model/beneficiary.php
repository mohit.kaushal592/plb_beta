<?php
/**
 * @package	PLB tool
 * @module	Payment
 * @author	Ajit Singh
 * @email	ajit.singh@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Beneficiary extends DatabaseObject {
	
	public $table_name="tbl_beneficiary";
	public $db_fields = array('id', 'bencode', 'benname', 'benaccount', 'ifsccode', 'address', 'city', 'state', 'zip_code', 'grossamount', 'tdsamount', 'postdate');
	
	
	

}

?>