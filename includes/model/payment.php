<?php
/**
 * @package	PLB tool
 * @module	Payment
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class Payment extends DatabaseObject {
	
	public $table_name="tracking_data";
	public $db_fields = array('id', 'child_table_id', 'domain', 'campaign_name', 'campaign_id', 'url', 'target_url', 'client_mail', 'paypal_email', 'ip', 'anchor_text', 'geo', 'approached_by', 'user_id', 'narration_text', 'status', 'amount', 'currency', 'pr_value', 'da_value', 'start_date', 'end_date', 'added_on', 'payment_date', 'month', 'year', 'ip_conflict', 'cron_status','bencode','benname','benaccount','ifsccode','address','city','state','zip_code','tdsamount','grossamount','payment_type','invoice_name','invoice_file','invoice_amount','response_status','response_time','index_status','index_time','index_count','invoice_month','category');
	//public $db_fields = array('id', 'child_table_id', 'domain', 'campaign_name', 'campaign_id', 'url', 'target_url', 'client_mail', 'paypal_email', 'ip', 'anchor_text', 'geo', 'approached_by', 'user_id', 'narration_text', 'status', 'amount', 'currency', 'payment_month', 'payment_year', 'pr_value', 'da_value', 'start_date', 'end_date', 'added_on', 'payment_date', 'month', 'year', 'ip_conflict', 'cron_status','ben_name','bank_address','bank_city','bank_state','zip_code','account_no','phone','bank_IFSC','ben_code');
	// Ajit: add some field  'bank_name','bank_address','bank_city','bank_state','zip_code','account_no','phone','bank_IFSC' on 15 DESC 2015
	public $relationTables = array(
		'belongsTo' =>array(
                    'User' => array(
                        'foreignKey' =>  'user_id'
                    ),
                    'Campaign' => array(
                        'foreignKey' =>  'campaign_id'
                    ),
                    'ApprovalLink' => array(
                        'foreignKey' =>  'child_table_id'
                    ),
		),
		'hasMany' => array(
		)
	);
	 

}

?>