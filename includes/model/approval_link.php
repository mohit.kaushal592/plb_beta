<?php
/**
 * @package	PLB tool
 * @module	Approval
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class ApprovalLink extends DatabaseObject {
	
	public $table_name="tbl_approval_links_child";
	//public $db_fields = array('id','approached_by', 'user_id','campaign_name', 'campaign_id', 'added_on', 'parent_id', 'approve_status', 'r_data_i', 'secondary_approval', 'is_secondary_approval', 'secondary_app_user', 'status_update_date', 'secondary_status_update_date', 'final_link_submit_status', 'link_submit_date', 'updated_on', 'cron_status');
        /**** add 2 columns  *******/
	public $db_fields = array('id','approached_by', 'user_id','campaign_name', 'campaign_id', 'added_on', 'parent_id', 'approve_status', 'r_data_i', 'secondary_approval', 'is_secondary_approval', 'secondary_app_user', 'status_update_date', 'secondary_status_update_date', 'final_link_submit_status', 'Amount', 'Currency', 'link_submit_date', 'updated_on', 'cron_status','payment_type');
	public $relationTables = array(
		'belongsTo' =>array(
			'Approval'=>array(
				'foreignKey'=> 'parent_id'
			),
			'User'=>array(
				'foreignKey'=> 'user_id'
			),
			'Campaign'=>array(
				'foreignKey'=> 'campaign_id'
			)
		),
		'hasMany' => array(
		)
	);
	

}

?>