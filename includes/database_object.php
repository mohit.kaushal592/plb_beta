<?php
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');

class DatabaseObject {
        
        public $totalRecords = null;

    // Common Database Methods
    public function find_all($options=array(), $paginate=false) {
        // calling class name
        $modelClass = $this->__get_called_class();
        
        $where = $order = $group = '';
        $defaultOpts = array('where'=>'', 'order'=>'', 'group'=>'');
        if(!is_array($options)){
                $options = (array)$options;
        }
        $options = array_merge($defaultOpts, $options);
        if(!empty($options)){
                $where  = $this->__getWhereClause($options);
                $order = is_array($options['order'])? implode(', ', $options['order']) : (!empty($options['order']) ? $options['order'] : '');
                $group = is_array($options['group'])? implode(', ', $options['group']) : (!empty($options['group']) ? $options['group'] : '');
        }
        $where = !empty($where) ? ' WHERE '. $where : '';
        $order = !empty($order) ? ' ORDER BY '. $order : '';
        $group = !empty($group) ? ' GROUP BY '. $group : '';
        
        // pagination limit and offset
        $limit = ' '. $this->__setPaginationLimitOffset($options, $paginate);
        
        //$limit = !empty($limit) ? ' LIMIT '. $limit : ' LIMIT 0, 20 ';
        $joins = $this->_getRelatedTables($options);
        $joins = isset($joins) ? $joins : ''; 
        $results = $this->find_by_sql("SELECT ". $this->_selectFields($options) ." FROM ". $this->_getTableAliasForSql(). $joins .  $where . $group. $order. $limit);
        if($this->_hasMany($options)){
                $this->_getHasManyRelatedData($results, $options);
        }
        if($habtm = $this->__isHasAndBelongsToMany($options)){
            $habtm = array($modelClass=>$habtm);
            $results = $this->__formatDataForHABTM($results, $habtm);
        }
        return $results;
    }

    public function find_by_id($id=0) {
        // calling class name
        $modelClass = $this->__get_called_class();
        
            $result_array = $this->find_by_sql("SELECT ". $this->_selectFields() ." FROM ".$this->_getTableAliasForSql()." WHERE ".$modelClass.".id={$id} LIMIT 1");
            return !empty($result_array) ? array_shift($result_array) : false;
    }

    public function find_by_sql($sql="", $options=array()) {
            global $database;        
            $result_set = $database->query($sql);
            $collection_array = array();
            while ($row = $database->fetch_array($result_set)) {
                    $this->buildCollectionArray($row, $collection_array);
            }
            return $collection_array;
    }
	
	 public function find_by_sqlval($sql="", $options=array()) {
            global $database; 
           // echo $sql."<br/>";
            $result_set = $database->query($sql);
            $result = array();
            while ($row = $database->fetch_array($result_set)) {
                  $result[]=$row;
            }
            return $result;
    }

    public function count_all() {
        global $database;
        // calling class name
        $modelClass = $this->__get_called_class();
        
        $sql = "SELECT COUNT(*) FROM ".$this->_getTableAliasForSql();
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    
    public function count($options=array()) {
            global $database;
        // calling class name
        $modelClass = $this->__get_called_class();
        
        $where = $order = $group = '';
        $distinctField = "$modelClass.id";
        if(!empty($options)){
                $where  = $this->__getWhereClause($options);
                $order = is_array($options['order'])? implode(', ', $options['order']) : (!empty($options['order']) ? $options['order'] : '');
                $group = is_array($options['group'])? implode(', ', $options['group']) : (!empty($options['group']) ? $options['group'] : '');
                $distinctField = !empty($options['distinct']) ? $options['distinct'] : $distinctField;
        }
        $where = !empty($where) ? ' WHERE '. $where : '';
        $order = !empty($order) ? ' ORDER BY '. $order : '';
        $group = !empty($group) ? ' GROUP BY '. $group : '';
        
        $joins = $this->_getRelatedTables($options);
        $joins = isset($joins) ? $joins : '';
        $sql = "SELECT COUNT(DISTINCT $distinctField) FROM ". $this->_getTableAliasForSql(). $joins . $where . $group . $order;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    
    protected function sanitized_attributes($data) {
        global $database;
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach($data as $key => $value){
                if(in_array($key, $modelClassOb->db_fields)){
                        $clean_attributes[$key] = $database->escape_value($value);
                }
        }
        return $clean_attributes;
    }
    
    public function save($data) {
        // calling class name
        $modelClass = $this->__get_called_class();
        
        $_data = $data;
        // A new record won't have an id yet.
        if(isset($data[$modelClass])){
                $_data = $data[$modelClass];
        } 
        return isset($_data['id']) ? $this->update($data) : $this->create($data);
    }
    
    public function create($data) {
        global $database;
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes(isset($data[$modelClass]) ? $data[$modelClass] : $data);
        $sql = "INSERT INTO ".$modelClassOb->table_name." (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        if($database->query($sql)) {
                return true;
        } else {
                return false;
        }
    }

    public function update($data) {
        global $database;
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes(isset($data[$modelClass]) ? $data[$modelClass] : $data);
        $attribute_pairs = array();
        foreach($attributes as $key => $value) {
                $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE ".$modelClassOb->table_name." SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE id=". $database->escape_value($data[$modelClass]['id']);
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete($conditions=null) {
        global $database;
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        if(isset($conditions)  and !empty($conditions)){
            // - escape all values to prevent SQL injection
            // - use LIMIT 1
            $sql = "DELETE FROM ".$modelClassOb->table_name;
            $sql .= " WHERE ". $conditions;
            $sql .= " LIMIT 1";
            $database->query($sql);
            return ($database->affected_rows() == 1) ? true : false;
        }
        return false;
    }
    
    protected function _selectFields($options=array()){
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        if(!is_array($options)){
                $options = (array)$options;
        }
        $fields = array();
        if(key_exists('fields', $options) and !empty($options['fields'])){
            if($options['fields'] !==false){
                foreach($options['fields'] as $_field){
                    $_fldExp = explode('.', $_field);
                    if(count($_fldExp)>1 and preg_match('/'.$modelClass.'$/', $_fldExp[0])){
                        $actFld = $_field;
                        array_shift($_fldExp);
                        $_field = implode('.', $_fldExp);
                        $fields[] = " ".$actFld." AS ".$modelClass."_{$_field}";
                    }else{
                        $fields[] = " ".$modelClass.".{$_field} AS ".$modelClass."_{$_field}";
                    }
                }
            }
        } else if($modelClassOb->db_fields){
                foreach($modelClassOb->db_fields as $_field){
                        $fields[] = " ".$modelClass.".{$_field} AS ".$modelClass."_{$_field}";
                }
        }
        if(key_exists('contain', $options)){
                foreach($options['contain'] as $_containKey => $_containVal){
                    $_containModel = is_array($_containVal) ? $_containKey : $_containVal;
                    $_containModelOb = new $_containModel;
                    if(key_exists($_containModel, $modelClassOb->relationTables['belongsTo'])){
                            
                        if(!empty($options['contain'][$_containModel]) and key_exists('fields', $options['contain'][$_containModel])){
                            if($options['contain'][$_containModel]['fields']!==false){
                                foreach($options['contain'][$_containModel]['fields'] as $_field){
                                    $_fldExp = explode('.', $_field);
                                    if(count($_fldExp)>1 and  preg_match('/'.$_containModel.'$/', $_fldExp[0])){
                                        $actFld = $_field;
                                        array_shift($_fldExp);
                                        $_field = implode('.', $_fldExp);
                                        $fields[] = " ".$actFld." AS ".$_containModel."_{$_field}";
                                    }else{
                                        $fields[] = " ".$_containModel.".{$_field} AS ".$_containModel."_{$_field}";
                                    }
                                }
                            }
                        }else if($_containModelOb->db_fields){
                            foreach($_containModelOb->db_fields as $_fld){
                                    $fields[] = " ".$_containModel.".{$_fld} AS ".$_containModel."_{$_fld}";					
                            }
                        }
                    }
                }
        }
        return !empty($fields) ? implode(',', $fields) : ' * ';
    }
    
    protected function _getTableAliasForSql(){
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        return $modelClassOb->table_name.' '. $modelClass;
    }
    
    protected function _getRelatedTables($options=array()){
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        if(!is_array($options)){
                $options = (array)$options;
        } 
        $tableJoins = array();
        if(key_exists('contain', $options)){
                foreach($options['contain'] as $_containKey => $_containVal){
                        $_containModel = is_array($_containVal) ? $_containKey : $_containVal;
                        $_containModelOb = new $_containModel;
                        if(key_exists($_containModel, $modelClassOb->relationTables['belongsTo'])){
                            if($modelClassOb->relationTables['belongsTo'][$_containModel]['foreignKey']!=false){
                                $tableJoins[] = ' INNER JOIN '. $_containModelOb->table_name.' '. $_containModel.' ON ('. $_containModel.'.id = '. $modelClass.'.'.$modelClassOb->relationTables['belongsTo'][$_containModel]['foreignKey'] .') ';
                            }else{
                                $tableJoins[] = ' INNER JOIN '.$_containModelOb->table_name.' '. $_containModel.' ON ('.$modelClassOb->relationTables['belongsTo'][$_containModel]['conditions'].') ';
                            }
                        }
                }
        }
        
        return implode(' ', $tableJoins);
    }
    
    protected function buildCollectionArray($record, &$collctArray){
        // calling class name
        $modelClass = $this->__get_called_class();
        
        if(!empty($record)){
                $_record=array();
                foreach($record as $attribute=>$value){
                        list($tblAlias, $attributes) = explode('_', $attribute, 2);
                        if(class_exists($tblAlias)){
                                $_record[$tblAlias][$attributes] = $value;
                        }else{
                                $_record[$attribute]=$value;
                        }
                }
                if(!empty($_record)){
                        $collctArray[] = $_record;
                }
        }
    }
    
    protected function _hasMany($options=array()){
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        if(!is_array($options)){
                $options = (array)$options;
        }
        $exists = false;
        if(key_exists('contain', $options)){
                foreach($options['contain'] as $_containKey => $_containVal){
                    $_containModel = is_array($_containVal) ? $_containKey : $_containVal;
                    if(key_exists($_containModel, $modelClassOb->relationTables['hasMany'])){
                            $exists=true;
                            break;
                    }
                }
                
        }
        return $exists;
    }
    protected function _getHasManyRelatedData(&$data, $options=array()){
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        if(!is_array($options)){
                $options = (array)$options;
        }
        if(key_exists('contain', $options)){
                $hasManyModel=array();
                foreach($options['contain'] as $_containKey => $_containVal){
                    $_containModel = is_array($_containVal) ? $_containKey : $_containVal;
                    if(key_exists($_containModel, $modelClassOb->relationTables['hasMany'])){
                            $hasManyModel[] = $_containModel;
                    }
                }
                if(!empty($hasManyModel)){ 
                        $idArr = array();
                        if(!empty($data)){
                                foreach($data as $_d){
                                        $idArr[] = $_d[$modelClass]['id'];
                                }
                        }
                        if(!empty($idArr)){
                                foreach($hasManyModel as $_m){
                                        $_mOb= new $_m;
                                    $fields = (is_array($options['contain'][$_m]) and key_exists('fields', $options['contain'][$_m])) ? $options['contain'][$_m]['fields'] : null;
                                    $sql = array('where'=> $_m.'.'.$modelClassOb->relationTables['hasMany'][$_m]['foreignKey'].' IN ('.implode(', ', $idArr).')');
                                    if(is_array($options['contain'][$_m]) and key_exists('where', $options['contain'][$_m])){
                                        $sql['where'] .= ' AND '.$options['contain'][$_m]['where'];
                                    }
                                    if(isset($fields)){
                                        $sql['fields'] = $fields;
                                    }
                                    $mData = $_mOb->find_all($sql);
                                        
                                        foreach($data as &$_dt){
                                                foreach($mData as $_mData){
                                                        $_mKey = $_mData[$_m][$modelClassOb->relationTables['hasMany'][$_m]['foreignKey']];
                                                        if(!key_exists($_m, $_dt)){
                                                                $_dt[$_m] = array();
                                                        }
                                                        if($_mKey == $_dt[$modelClass]['id']){
                                                                $_dt[$_m][] = $_mData[$_m];
                                                        }
                                                }
                                        }
                                }
                        }
                        
                }
        }
    }
    
    protected function __setPaginationLimitOffset($options=array(), $paginate=false){
            global $viewData;
            if(!is_array($options)){
                    $options = (array)$options;
            }
            if($paginate==true){ 
                $_rp = (isset($_REQUEST['_rp']) and (int)$_REQUEST['_rp']>10) ? (int)$_REQUEST['_rp'] : 10;
                    $pages = new Pagination($_rp,'p');
                    $this->totalRecords = $this->count($options);
                    $pages->set_total($this->totalRecords);
                    $viewData->set('pageLinks', $pages->page_links());
                    $viewData->set('recordStartFrom', $pages->get_start_from());
                    $viewData->set('totalRecords', $this->totalRecords);
                    return $pages->get_limit();
            } else { 
                    return isset($options['limit']) ? (preg_match('/limit/i', $options['limit']) ? $options['limit'] : 'LIMIT '. $options['limit']) : '';
            }
    }
    public function find_list($options=array()){
        // calling class name
        $modelClass = $this->__get_called_class();
        
        $where = $order = $group = '';
        $defaultOpts = array('where'=>'', 'order'=>'');
        if(!is_array($options)){
                $options = (array)$options;
        }
        $options = array_merge($defaultOpts, $options);
        if(!empty($options)){
                $where  = $options['where'];
                $order = is_array($options['order'])? implode(', ', $options['order']) : (!empty($options['order']) ? $options['order'] : '');
                $group = is_array($options['group'])? implode(', ', $options['group']) : (!empty($options['group']) ? $options['group'] : '');
        }
        $where = !empty($where) ? ' WHERE '. $where : '';
        $order = !empty($order) ? ' ORDER BY '. $order : '';
        $group = !empty($group) ? ' GROUP BY '. $group : '';
        
        
        $results = $this->find_by_sql("SELECT ". $this->_selectFields($options) ." FROM ". $this->_getTableAliasForSql(). $where . $group . $order);
        if(!empty($results)){
            $list=array();
            foreach($results as $_result){
                $keys = array_keys($_result[$modelClass]);
                $list[$_result[$modelClass][$keys[0]]] = $_result[$modelClass][$keys[1]];
            }
            $results = !empty($list) ? $list : $results;
        }
        return $results;
    }
    
    protected function __getWhereClause($options){
        // calling class name
        $modelClass = $this->__get_called_class();
       $modelClassOb = new $modelClass;
        if(!empty($options)){
            $options = is_array($options) ? $options : (array)$options;
            $_where = array();
            if(key_exists('where', $options)){
                $_where[]  = is_array($options['where']) ? implode(' AND ', $options['where']) : $options['where'];
            }
            if(key_exists('contain', $options)){
                foreach($options['contain'] as $_containKey => $_containVal){
                    $_containModel = is_array($_containVal) ? $_containKey : $_containVal;
                    if(key_exists($_containModel, $modelClassOb->relationTables['belongsTo'])){
                        if(is_array($_containVal) && !empty($_containVal['where'])){
                            $_where[] = $_containVal['where'];
                        }
                    }
                }
            }
            $_where = array_filter($_where);
            return !empty($_where) ? implode(' AND ', $_where) : '';
        }
        return;
    }
    
    protected function __isHasAndBelongsToMany($options=array()){
        // calling class name
        $modelClass = $this->__get_called_class();
        $modelClassOb = new $modelClass;
        if(!empty($options)){
            $options = is_array($options)?$options:(array)$options;
        }
        if(key_exists('contain', $options)){
            $_habtm = array();
            foreach($options['contain'] as $_containKey => $_containVal){
                $_containModel = is_array($_containVal) ? $_containKey : $_containVal;
                if(key_exists($_containModel, $modelClassOb->relationTables['belongsTo'])){
                    if(isset($modelClassOb->relationTables['belongsTo'][$_containModel]['relation']) and $modelClassOb->relationTables['belongsTo'][$_containModel]['relation'] == 'hasMany'){
                       $_habtm[] = $_containModel;
                    }
                }
            }
        }
        return !empty($_habtm) ? $_habtm : false;
    }
    
    private function __formatDataForHABTM($result, $habtm=array()){
        if(!empty($result)){
            $_result = array();
            $habtm = is_array($habtm)?$habtm:(array)$habtm;
            $_m = array_keys($habtm);
            $_m = (is_string($_m[0]) and strlen($_m[0])>1) ? $_m[0] : '';
            $_data=array(); 
            foreach($result as $_res){
                foreach($habtm[$_m] as $_habtm){
                    if(key_exists($_habtm, $_res)){
                        if(key_exists($_m, $_res) and isset($_res[$_m]['id'])){
                            if(!key_exists($_res[$_m]['id'], $_data)){
                                $_data[$_res[$_m]['id']] = $_res;
                            }
                        }
                        if(key_exists($_habtm, $_res)){
                            if(!isset($_data[$_res[$_m]['id']][$_habtm][0])){
                                unset($_data[$_res[$_m]['id']][$_habtm]);
                            }
                            if(key_exists($_m, $_res) and isset($_res[$_m]['id'])){
                                $_data[$_res[$_m]['id']][$_habtm][] = $_res[$_habtm];
                            }
                        }
                    }
                }
            } 
            return !empty($_data) ? array_values($_data) : $result;
        }
        return $result;
    }
    
    protected function __get_called_class(){
        return version_compare(phpversion(), "5.3.0", "<") ? get_called_class() : get_called_class();
    }
}