<?php
class ViewData{
    private $data = array();
    
    public function set($name, $value){
        $this->data[$name] = $value;
    }
    
    public function get($name){
        return key_exists($name, $this->data) ? $this->data[$name] : null;
    }
    
    public function scripts($js=array(), $options=array()){
        $default_options = array('inline'=>true);
        if(!is_array($options)){
            $options = (array) $options;
        }
        $options = array_merge($default_options, $options);
        if(!is_array($js)){
            $js = (array)$js;
        }
        if($options['inline']==true){
            $this->_script_render($js);
        }else{
            $this->data['script_for_layout'][] = $js;
        }
    }
    
    public function script_for_layout(){
        if(key_exists('script_for_layout', $this->data)){
            $this->_script_render($this->data['script_for_layout']);
        }
        if(key_exists('script_blk_layout', $this->data)){
            $this->_script_blk_render($this->data['script_blk_layout']);
        }
    }
    
    private function _script_render($js=array()){
        if(!empty($js)){
            foreach($js as $_jsName){
                if(is_array($_jsName)){
                    $this->_script_render($_jsName);
                } else {
                    echo "<script type='text/javascript' src='{$_jsName}'></script>";
                }
            }
        }
    }
    
    private function _script_blk_render($js=array()){
        if(!empty($js)){
            foreach($js as $_script){
                if(is_array($_script)){
                    $this->_script_blk_render($_script);
                } else {
                    echo "\n<script type='text/javascript'>\n{$_script}\n</script>";
                }
            }
        }
    }
    
    public function scriptStart(){
        ob_start();
    }
    
    public function scriptEnd(){
        $script = ob_get_clean();
        $this->data['script_blk_layout'][] = $script;
    }
    
    public function setTitle($title){
        $this->data['_title_for_layout'] = $title;
    }
    
    public function getTitle(){
        return $this->data['_title_for_layout'];
    }
}

$viewData = new ViewData();

?>