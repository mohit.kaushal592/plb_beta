<?php
/**
 * @package	PLB tool
 * @module	MySQL Database
 * @author	Akhtar Khan
 * @email	akhtar.khan@adlift.com
 * */
require_once(LIB_PATH.DS."config.php");

class MySQLDatabase {
	
	private $connection;
	public  $last_query;
	private $magic_quotes_active;
	private $real_escape_string_exists;
	private $dblogs=false;
	private $queryLogs = array();
	
	function __construct() {
		$this->open_connection();
		$this->magic_quotes_active = false; //get_magic_quotes_gpc();
		$this->real_escape_string_exists = function_exists( "mysqli_real_escape_string" );
		$this->dblogs = DB_LOGS == 1 ? true : false;
	}

	public function open_connection() {
		$this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS);
		if (!$this->connection) {
			die("Database connection failed: " . mysqli_error());
		} else {
			$db_select = mysqli_select_db($this->connection,DB_NAME);
			if (!$db_select) {
				die("Database selection failed: ".DB_NAME . mysqli_error());
			}
		}
	}

	public function close_connection() {
		if(isset($this->connection)) {
			mysqli_close($this->connection);
			unset($this->connection);
		}
	}

	public function query($sql) {
		 $this->last_query = $sql;
		//echo $this->last_query;
		//echo "<br/>";
		$startTime = microtime(true);
		$result = mysqli_query($this->connection,$sql);
		$execTime = microtime(true) - $startTime;
		$this->confirm_query($result);
		$this->queryLogs[] = array('sql'=>$sql, 'time'=> round($execTime, 3), 'error'=>mysqli_error());
		return $result;
	}
	
	public function escape_value( $value ) {
		if( $this->real_escape_string_exists ) {
			// PHP v4.3.0 or higher
			// undo any magic quote effects so mysql_real_escape_string can do the work
			//if( $this->magic_quotes_active ) { $value = stripslashes( $value ); }
			$value = mysqli_real_escape_string($this->connection,$value);
		} else { // before PHP v4.3.0
			// if magic quotes aren't already on then add slashes manually
			if( !$this->magic_quotes_active ) { $value = addslashes( $value ); }
			// if magic quotes are active, then the slashes already exist
		}
		return $value;
	}
	
	public function fetch_array($result_set) {
		return mysqli_fetch_assoc($result_set);
	}
	
	public function fetch_data_array($result_set) {
		$result=array();
		while($row = mysqli_fetch_assoc($result_set)){
			$result[] = $row;
		}
		return $result;
	}
  
	public function num_rows($result_set) {
		return mysqli_num_rows($result_set);
	}
  
	public function insert_id() {
		// get the last id inserted over the current db connection
		return mysqli_insert_id($this->connection);
	}
  
	public function affected_rows() {
		return mysqli_affected_rows($this->connection);
	}

	private function confirm_query($result) {
		if (!$result) {
			echo  $this->last_query;
			$output = "Database query failed: " . mysqli_error() . "<br /><br />";
			if($this->dblogs){
				$output .= "Last SQL query: " . $this->last_query;
			}
			die( $output );
		}
	}
	
	public function last_query(){
		return $this->last_query;
	}
	
	public function get_sql_logs(){
		return $this->queryLogs;
	}
        
        public function fetch_data_array_values($result_set) {
		$result=array();
		while($row = mysqli_fetch_array($result_set)){
			$result[] = $row[0];
		}
		return $result;
	}
	
}

$database = new MySQLDatabase();
$db =& $database;

?>
