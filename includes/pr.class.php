<?php
class PR {


function getPR($domain){
	$url = 'https://openpagerank.com/api/v1.0/getPageRank';
$query = http_build_query(array(
		'domains' => array(
			$domain
		)
	));
$url = $url .'?'. $query;
$ch = curl_init();
$headers = ['API-OPR:00g8cs00wwksgwk4ckwo8w4c0c804gwo4cs8w8sc'];
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$output = curl_exec ($ch);
curl_close ($ch);
$output = json_decode($output,true);
$prcode = 0;
if($output['status_code']==200){
$prcode =(!empty($output['response'][0]['page_rank_integer']))?$output['response'][0]['page_rank_integer']:0;
}
return $prcode;
}




	public function getGooglePagerank($url) {
		$url = strstr($url,"http://")? $url:"http://".$url;
		$fp = fsockopen("toolbarqueries.google.com", 80, $errno, $errstr, 30);
		if (!$fp) {
				echo "$errstr ($errno)<br />\n";
		} 
		else {
			$out = "GET /tbr?client=navclient-auto&ch=".$this->CheckHash($this->HashURL($url))."&features=Rank&q=info:".$url."&num=100&filter=0 HTTP/1.1\r\n";
			$out .= "Host: toolbarqueries.google.com\r\n";
			$out .= "User-Agent: Mozilla/4.0 (compatible; GoogleToolbar 2.0.114-big; Windows XP 5.1)\r\n";
			$out .= "Connection: Close\r\n\r\n";
			fwrite($fp, $out);
echo $out;
			while (!feof($fp)) {
				$data = fgets($fp, 128);
				$pos = strpos($data, "Rank_");
				if($pos === false){} else{
					$pagerank = substr($data, $pos + 9);
				}
			}
			fclose($fp);
			return (int)$pagerank;
			
			
		}
		//return 0;
	}
	 
	public function StrToNum($Str, $Check, $Magic)
	{
	    $Int32Unit = 4294967296;  // 2^32
	 
	    $length = strlen($Str);
	    for ($i = 0; $i < $length; $i++) {
		$Check *= $Magic;
	 
		if ($Check >= $Int32Unit) {
		    $Check = ($Check - $Int32Unit * (int) ($Check / $Int32Unit));
		    $Check = ($Check < -2147483648) ? ($Check + $Int32Unit) : $Check;
		}
		$Check += ord($Str{$i});
	    }
	    return $Check;
	}
	 
	public function HashURL($String)
	{
	    $Check1 = $this->StrToNum($String, 0x1505, 0x21);
	    $Check2 = $this->StrToNum($String, 0, 0x1003F);
	 
	    $Check1 >>= 2;
	    $Check1 = (($Check1 >> 4) & 0x3FFFFC0 ) | ($Check1 & 0x3F);
	    $Check1 = (($Check1 >> 4) & 0x3FFC00 ) | ($Check1 & 0x3FF);
	    $Check1 = (($Check1 >> 4) & 0x3C000 ) | ($Check1 & 0x3FFF);
	 
	    $T1 = (((($Check1 & 0x3C0) << 4) | ($Check1 & 0x3C)) <<2 ) | ($Check2 & 0xF0F );
	    $T2 = (((($Check1 & 0xFFFFC000) << 4) | ($Check1 & 0x3C00)) << 0xA) | ($Check2 & 0xF0F0000 );
	 
	    return ($T1 | $T2);
	}
	 
	public function CheckHash($Hashnum)
	{
	    $CheckByte = 0;
	    $Flag = 0;
	 
	    $HashStr = sprintf('%u', $Hashnum) ;
	    $length = strlen($HashStr);
	 
	    for ($i = $length - 1;  $i >= 0;  $i --) {
		$Re = $HashStr{$i};
		if (1 === ($Flag % 2)) {
		    $Re += $Re;
		    $Re = (int)($Re / 10) + ($Re % 10);
		}
		$CheckByte += $Re;
		$Flag ++;
	    }
	 
	    $CheckByte %= 10;
	    if (0 !== $CheckByte) {
		$CheckByte = 10 - $CheckByte;
		if (1 === ($Flag % 2) ) {
		    if (1 === ($CheckByte % 2)) {
		        $CheckByte += 9;
		    }
		    $CheckByte >>= 1;
		}
	    }
	 
	    return '7'.$CheckByte.$HashStr;
	}
}

?>
