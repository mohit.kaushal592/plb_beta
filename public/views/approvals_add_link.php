<style>
    .modal-overlay-1 {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 999;
    }

    .modal-1 {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background: white;
        padding: 20px;
        width: 700px;
        height: 200px;
        border-radius: 8px;
        box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
    }

    .modal-header-1 {
        font-size: 18px;
        margin-bottom: 10px; 
    }

    .modal-content-1 {
        margin-top: 40px;
        margin-bottom: 40px;
        text-align:center;
    }

    .modal-actions-1 {
        display: flex;
        justify-content: space-between;
    }

    .modal-actions-1 button {
        padding: 8px 16px;
        border: none;
        cursor: pointer;
        border-radius: 5px;
    }

    .modal-actions-1 .btn-submit {
        background-color: #28a745;
        color: white;
    }

    .modal-actions-1 .btn-cancel {
        background-color: #dc3545;
        color: white;
    }

    .btn-open-modal {
        margin: 20px;
        padding: 10px 20px;
        background-color: #007bff;
        color: white;
        border: none;
        border-radius: 5px;
        cursor: pointer;
    }

    .button-position{
      margin-left:10px;
      position: absolute; 
    }
</style>
<?php
  if(isset($_POST['file_upload'])){
    // echo "<pre>";
    // print_r($_FILES); 
    // die('ddd');
 
    $filePath = $_FILES['linkFile']['tmp_name'];
    $apiUrl = 'https://api.adlift.com/marketing/bulk-upload/'; 
    $ch = curl_init(); 
    $file = new CURLFile($filePath); 
    $postData = [
        'file' => $file
    ];
 
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    $response = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'cURL Error: ' . curl_error($ch);
    } else {
        echo 'API Response: ' . $response;
    }
    curl_close($ch);
  }
?>
<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  <p>Please Check <span style="color:#28B779">Domain History</span> before submission to prevent domain duplicacy for same campaign
    <button type="button" class="btn btn-success button-position" onclick="openModal()">Upload Link</button>
  </p>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add Link</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" class="form-horizontal" method="post" id="AddLinkForm">
            <div class="control-group">
			        <a href="#" class="history DomainIpHistory" id="DomainAutofill" style="margin-right:1px;position: absolute;margin-top:-38px">
                <img src="img/auto-fill.png" style="height: 30px;width: 128px;"/>
              </a> 
			        <a href="#" class="history DomainIpHistory" id="DomainHistory"><img src="img/history.png"/></a>
              <label class="control-label">Domain Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Domain Name" name="data[Approval][domain]" id="ApprovalDomain" minlength="4" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Select Campaign :</label>
              <div class="controls">
                <select name="data[Approval][campaign][]" multiple="multiple" id="ApprovalCampaign" title="Please select campaign." required>
                 <?php $campsList = $viewData->get('campaignsList');
                 echo getFormOptions($campsList);
                 ?>
                </select>
              </div>
            </div>
			<div class="control-group">
              <label class="control-label">Narration text :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Approval][narration_text]" id="ApprovalNarrationText" />
              </div>
            </div>
              <div class="control-group">
              <label class="control-label">Payment Type :</label>
              <div class="controls">
                <select name="data[Approval][payment_type]" id="ApprovalPayment_type" title="Please select Payment Type." required>
                <option value="">Select Payment Type</option>
				<option value="Paypal">Paypal</option>
				<option value="NEFT">NEFT</option>
                </select>
              </div>
            </div>
              
              
              <div class="control-group">
              <label class="control-label">Amount :</label>
              <div class="controls">
                <input  type="number" class="span11" pattern="^\d+(\.)\d{2}$" placeholder="" name="data[Approval][Amount]" id="PaymentAmount" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Currency :</label>
              <div class="controls">
                  <select onchange="return checkCurrency(this.value);" name="data[Approval][Currency]" id="PaymentCurrency" title="Please select Currency." required>
		  <option value=''>Select Currency</option>
		  <?php echo getFormOptions(array('INR'=>'INR', 'Euro'=>'Euro', 'Dollars'=>'Dollars', 'Pounds'=>'Pounds'), $payment['Approval']['Currency']) ?>
                </select>
              </div>
            </div>

            <!-- <div class="control-group">
              <label class="control-label">Category :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Category Name" name="data[Approval][category]" required/>
              </div>
            </div> -->  
            <div class="control-group">
                <label class="control-label">Category :</label>
                <div class="controls">
                <select name="data[Approval][category][]" id="ApprovalCategory" multiple="multiple"  title="Please select category." required>
                    <!-- <option value="">Select</option>
                    <option value="Business">Business</option>
                    <option value="Finance">Finance</option>
                    <option value="Lifestyle/Fashion">Lifestyle/Fashion</option>
                    <option value="Technology">Technology</option>
                    <option value="Health">Health</option>
                    <option value="Education">Education</option>
                    <option value="Marketing/digital marketing">Marketing/digital marketing</option>
                    <option value="Travel">Travel</option>
                    <option value="News">News</option>
                    <option value="Sports">Sports</option>
                    <option value="Automobile">Automobile</option>
                    <option value="Real estate">Real estate</option>
                    <option value="Food">Food</option>
                    <option value="Parenting">Parenting</option>
                    <option value="Home decor/ improvement">Home decor/ improvement</option>
                    <option value="Multi Category">Multi Category</option>
                    <option value="Generic">Generic</option> -->
                    <?php echo getFormOptions(array("Business"=>"Business","Finance"=>"Finance","Lifestyle/Fashion"=>"Lifestyle/Fashion","Technology"=>"Technology","Health"=>"Health","Education"=>"Education","Marketing/digital marketing"=>"Marketing/digital marketing","Travel"=>"Travel","News"=>"News","Sports"=>"Sports","Automobile"=>"Automobile","Real estate"=>"Real estate","Food"=>"Food","Parenting"=>"Parenting","Home decor/ improvement"=>"Home decor/ improvement","Generic"=>"Generic"), $payment['Approval']['category']) ?>
                </select>
                </div>
            </div>

            <div class="control-group">
              <label class="control-label">DR Value :</label>
              <div class="controls">
                <input type="number" class="span11" placeholder="DR Value" name="data[Approval][dr]" value='0' min="0" id="ApprovalDr">
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Backlinks :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Backlinks Value" name="data[Approval][backlinks]" id="ApprovalBacklink"/>
              </div>
            </div>

            <div class="control-group">
                <label class="control-label">Adult Content :</label>
                <div class="controls">
                <select name="data[Approval][adult]" id="ApprovalAdult" title="Please select adult content." required>
                    <option value="">Select</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Gambling/Casino/Betting :</label>
                <div class="controls">
                <select name="data[Approval][gambling]" id="ApprovalGambling" title="Please select gambling content." required>
                    <option value="">Select</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
                </div>
            </div>


<!--            Ajit:   Add No Payment option in currency on 15 DESC 2015-->
              
			<div class="form-actions">
              <button type="button" class="btn btn-danger" id="ApprovalCalcDaPr">Calculate  PR,DA and IP</button>
            </div>
			
            <div class="control-group">
			<a href="#" class="history DomainIpHistory" id="IpHistory"><img src="img/iphistory.png"/></a>
              <label class="control-label">IP Address :</label>
              <div class="controls">
                <input type="text"  class="span11" placeholder="IP Address" name="data[Approval][ip]" id="ApprovalIp"  required/>
              </div>
            </div>
            <!-- <div class="control-group">
              <label class="control-label">PR value :</label>
              <div class="controls">
                <input type="text" class="span11"  placeholder="PR value" name="data[Approval][pr]" id="ApprovalPr" required/>
              </div>
            </div> -->
            <input type="hidden" class="span11"  placeholder="PR value" name="data[Approval][pr]" id="ApprovalPr" value="0"/>
            <div class="control-group">
              <label class="control-label">DA value :</label>
              <div class="controls">
                <input type="text" class="span11"  placeholder="DA value" name="data[Approval][da]" id="ApprovalDa" required/>
				</div>
            </div>
			<div class="control-group">
              <label class="control-label">Spam Score (%) :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Spam Score" name="data[Approval][sc]" id="ApprovalSc" required/>
				</div>
            </div>
            <div class="control-group">
              <label class="control-label">Index Count :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Index Count" name="data[Approval][index_count]" id="ApprovalIndexCount" readonly/>
          </div>
            </div>
            <div class="control-group">
              <label class="control-label">Index Status :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Index Status" name="data[Approval][index_status]" id="ApprovalIndexStatus" readonly/>
				</div>
            </div>
            <div class="control-group">
              <label class="control-label">Annual Traffic :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Traffic value" name="data[Approval][annual_traffic]" id="ApprovalTraffic" value="0"/>
                </select>
              </div>
            </div>
            <div class="control-group">
                <label class="control-label">Administrative Country :</label>
                <div class="controls">
                    <input type="text" class="span11"  placeholder="Administrative Country" name="data[Approval][ac]" id="ApprovalAc" required/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Content Writer :</label>
                <div class="controls">
                    <label>
                    <input type="radio"  name="data[Approval][content_writer]"  id="ApprovalContentWriter0" value="0" /> Adlift </label>
                    <label>
                    <input type="radio" name="data[Approval][content_writer]" id="ApprovalContentWriter1" value="1" /> External</label>
                    <span class="error content-writer" style="display: none;">Please choose content writer.</span>
                </div>
            </div>
           
<?php 
$keyword = isset($_REQUEST['keyword'])?$_REQUEST['keyword']:"";
$sv = isset($_REQUEST['sv'])?$_REQUEST['sv']:"";
$rank = isset($_REQUEST['rank'])?$_REQUEST['rank']:"";
$title = isset($_REQUEST['title'])?$_REQUEST['title']:"";
$title_id = isset($_REQUEST['title_id'])?$_REQUEST['title_id']:"";
$target_url = isset($_REQUEST['target_url'])?$_REQUEST['target_url']:"";
date_default_timezone_set('Asia/Calcutta');  
?> 
<input type="hidden" name="data[Approval][keyword]" value="<?php echo $keyword; ?>">
<input type="hidden" name="data[Approval][sv]" value="<?php echo $sv; ?>">
<input type="hidden" name="data[Approval][rankval]" value="<?php echo $rank; ?>">
<input type="hidden" name="data[Approval][title]" value="<?php echo $title; ?>">
<input type="hidden" name="data[Approval][title_id]" value="<?php echo $title_id; ?>">
<input type="hidden" name="data[Approval][target_url]" value="<?php echo $target_url; ?>">
<input type="hidden" name="data[Approval][index_time]" value="<?php echo date('Y-m-d h:i:s'); ?>">




            <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script>
    let form_data = sessionStorage.getItem('linkFormData');
    let json_form_data = JSON.parse(form_data)
 
    if(json_form_data != null){
      document.getElementById("ApprovalDomain").value = json_form_data?.Approval?.domain 
      document.getElementById("ApprovalNarrationText").value = json_form_data?.Approval?.narration_text
      document.getElementById("PaymentAmount").value = json_form_data?.ApprovalLink[0]?.Amount
      document.getElementById("ApprovalDr").value = json_form_data?.Approval?.dr
      document.getElementById("ApprovalBacklink").value = json_form_data?.Approval?.backlinks
      document.getElementById("ApprovalPayment_type").value = json_form_data?.Approval?.payment_type
      document.getElementById("PaymentCurrency").value = json_form_data?.ApprovalLink[0]?.Currency
      document.getElementById("ApprovalAdult").value = json_form_data?.Approval?.adult 
      document.getElementById("ApprovalGambling").value = json_form_data?.Approval?.gambling

      let campaign_id = []
      json_form_data?.ApprovalLink?.map(item=>{
        campaign_id.push(item.campaign_id)
      }) 
      const selectElement = document.getElementById("ApprovalCampaign");
      const valuesToSelect = campaign_id;
      for (let option of selectElement.options) {
          if (valuesToSelect.includes(option.value)) { 
              option.selected = true;
          }
      }

      const selectElement1 = document.getElementById("ApprovalCategory");
      const valuesToSelect1 = [json_form_data?.Approval?.category];
      for (let option of selectElement1.options) {
          if (valuesToSelect1.includes(option.value)) { 
              option.selected = true;
          }
      }

    }
  </script>  
    <?php $viewData->scripts(array('js/approvals_add_link.js?1=2'), array('inline'=>false)) ?>
  
    <div class="modal-overlay-1" id="modalOverlay">
        <div class="modal-1">
            <div class="modal-header-1">
              Upload Link Files
              <span style="position:absolute;margin-left:434px;"><a href="img/plb.xlsx" style="color:blue;" download>Demo sheet</a></span>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="modal-content-1">
                    <input type="file" name="linkFile" accept=".csv, .xlsx" required> <br />
                    .csv or .xlsx
                </div>
                <div class="modal-actions-1">
                    <button type="button" class="btn-cancel" onclick="closeModal()">Clear</button>
                    <button type="submit" name="file_upload" class="btn-submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        function openModal() {
            document.getElementById('modalOverlay').style.display = 'block';
        }
        function closeModal() {
            document.getElementById('modalOverlay').style.display = 'none';
        }
    </script>