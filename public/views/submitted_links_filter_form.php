<?php $filterVars = $viewData->get('filterVars') ?>
<form action="submitted_links.php" method="post" class="form-horizontal filter-form" id="FilterForm">
    <input type="hidden" name="exportList" id="exportList" value="" />
    <input type="hidden" name="exportListBank" id="exportListBank" value="" />
    <div class="control-group">
        <div class="span3">
            <label>User</label>
            <select name="users[]" id="FilterUser" multiple="multiple">
            <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Campaign</label>
            <select name="campaigns[]" id="FilterCampaign"  multiple="multiple">
            <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Domain</label>
            <input name="domain" autocomplete="off" id="FilterDomain" type="text" class="span10" value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
        </div>
        <div class="span3">
            <label>Status</label>
            <select name="payment_status[]" id="FilterPaymentStatus" multiple="multiple">
            <?php echo getFormOptions(array('1'=>'Paid', '0'=>'Rejected', '-1'=>'Underprocess'), $filterVars['status']); ?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <div class="span3">
            <label>Month</label>
            <select name="month[]" id="FilterMonth" multiple="multiple">
            <?php echo getFormOptions(monthsList(), $filterVars['month']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Year</label>
            <select name="year[]" id="FilterYear" multiple="multiple">
            <?php echo getFormOptions(yearsList(array('min'=>'2012','max'=>date('Y'))), $filterVars['year']); ?>
            </select>
        </div>
        <div class="span3" id="WebmasterBlock">
            <label>Webmaster</label>
            <input type="hidden" id="FilterClientMail" value="<?php echo !empty($filterVars['client_mail']) ? join(",", $filterVars['client_mail']) : '' ?>" />
        </div>
        <div class="span3" id="PaypalBlock">
            <label>Paypal</label>
            <input type="hidden" id="FilterPaypalEmail" value="<?php echo !empty($filterVars['paypal_email']) ? join(",", $filterVars['paypal_email']) : '' ?>" />
        </div>
    </div>
    <div class="control-group">
        <div class="span3">
            <label>PR (operator)</label>
            <select name="pr_op" id="FilterPrOperator">
            <?php echo getFormOptions(array('eq'=>'= (Equal to)', 'lt'=>'< (Less than)', 'lte'=>'<= (Less than equal to)', 'gt'=>'> (Greater than)', 'gte'=>'>= (Greater than equal to)'), $filterVars['pr_op']); ?>
            </select>
        </div>
        <div class="span3">
            <label>PR (value)</label>
            <select name="pr_value" id="FilterPrValue">
            <?php $prValues[0] = 'Select Value'; for($i=1;$i<=10;$i++){$prValues[$i] = $i;} ?>
            <?php echo getFormOptions($prValues, $filterVars['pr_value']); ?>
            </select>
        </div>
        <div class="span3">
            <label>DA (operator)</label>
            <select name="da_op" id="FilterDaOperator">
            <?php echo getFormOptions(array('eq'=>'= (Equal to)', 'lt'=>'< (Less than)', 'lte'=>'<= (Less than equal to)', 'gt'=>'> (Greater than)', 'gte'=>'>= (Greater than equal to)'), $filterVars['da_op']); ?>
            </select>
        </div>
        <div class="span3">
            <label>DA (value)</label>
            <select name="da_value" id="FilterDaValue">
            <?php $daValues[0] = 'Select Value'; for($i=1;$i<=100;$i = ($i==1?$i+4 : $i+5)){$daValues[$i] = $i;} ?>
            <?php echo getFormOptions($daValues, $filterVars['da_value']); ?>
            </select>
        </div>
    </div>
    <div class="control-group">
        <div class="span3">
            <label>Payment Date</label>
            <div  data-date="" class="input-append date datepicker">
                <input type="text" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="span10" >
                <span class="add-on"><i class="icon-th"></i></span> 
            </div>
            <div  data-date="" class="input-append date datepicker">
                <input type="text" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="span10" >
                <span class="add-on"><i class="icon-th"></i></span> 
            </div>
        </div>
        <div class="span3">
            <label>Submitted Date</label>
            <div  data-date="" class="input-append date datepicker">
                <input type="text" name="from" value="<?php echo !empty($filterVars['dateFromSubmted']) ? $filterVars['dateFromSubmted'] : '' ?>" id="FilterDateFromSubmted"  data-date-format="mm-dd-yyyy" class="span10" >
                <span class="add-on"><i class="icon-th"></i></span> 
            </div>
            <div  data-date="" class="input-append date datepicker">
                <input type="text" value="<?php echo !empty($filterVars['dateToSubmted']) ? $filterVars['dateToSubmted'] : '' ?>" name="to" id="FilterDateToSubmted"  data-date-format="mm-dd-yyyy" class="span10" >
                <span class="add-on"><i class="icon-th"></i></span> 
            </div>
        </div>
        <!-- Start: Added by Jitendra: 28-Nov-2014 --->  
        <div class="span3">
            <label>Pay for month of</label>
            <select name="payment_month[]" id="FilterPaymentMonth" multiple="multiple">
            <?php echo getFormOptions(monthsList(), $filterVars['payment_month']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Pay for year of</label>
            <select name="payment_year[]" id="FilterPaymentYear" multiple="multiple">
            <?php echo getFormOptions(yearsList(array('min'=>'2012','max'=>date('Y'))), $filterVars['payment_year']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Payment By</label>
            <select name="payment_type" id="Filterpayment_type">
            <?php echo getFormOptions(array(''=>'Select','Paypal'=>'Paypal', 'NEFT'=>'NEFT'), $filterVars['payment_type']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Bencode</label>
            <input name="bencode" autocomplete="off" id="Filterbencode" type="text" class="span10" value="<?php echo !empty($filterVars['bencode']) ? $filterVars['bencode'] : '' ?>" class="typeahead" />
        </div>
        </div>
		<div class="control-group">
        <div class="span3">
            <label>Response Status</label>
            <select name="response_status" id="Filterresponse_status"> 
            <?php echo getFormOptions(array(''=>'Select','200'=>'200', '404'=>'404', '500'=>'500', '502'=>'502', '409'=>'409'), $filterVars['response_status']); ?>
            </select>
        </div>
        <div class="span3">
            <label>Index Status</label>
            <select name="index_status" id="Filterindex_status">
            <?php echo getFormOptions(array(''=>'Select','Index'=>'Index', 'Not Index'=>'Not Index'), $filterVars['index_status']); ?> 
            </select>
        </div>
        <!-- <div class="span3">
            <label>Response Update</label>
            <select name="response_update[]" id="FilterResponseUpdate" multiple="multiple">
             //echo getFormOptions(array('r_status'=>'Response Status','i_status'=>'Index Status'), $filterVars['response_update']);
            </select>
        </div> -->
		<!-- <div class="span3">
            <label>Invoice Payment</label>
            <div class="input-append1 date1 datepicker1">
				<input type="month" autocomplete="off" name="invoiceDateFrom" id="invoiceDateFrom" class="span10" value=" //echo !empty($filterVars['invoiceDateFrom']) ? $filterVars['invoiceDateFrom'] : '' ?>" class="typeahead" />
            </div>
            <div  class="input-append1 date2 datepicker2 mt-2" style="margin-top:10px;">
				<input type="month" autocomplete="off" name="invoiceDateTo" id="invoiceDateTo" class="span10" value="// echo !empty($filterVars['invoiceDateTo']) ? $filterVars['invoiceDateTo'] : '' ?>" class="typeahead" />
            </div>
        </div> -->
		
        <div class="span3">
            <label>Invoice Payment</label>
            <div  data-date="" class="input-append date datepicker">
                <input type="text" name="from" value="<?php echo !empty($filterVars['invoiceDateFrom']) ? $filterVars['invoiceDateFrom'] : '' ?>" id="invoiceDateFrom"  data-date-format="mm-dd-yyyy" class="span10" >
                <span class="add-on"><i class="icon-th"></i></span> 
            </div>
            <div  data-date="" class="input-append date datepicker">
                <input type="text" value="<?php echo !empty($filterVars['invoiceDateTo']) ? $filterVars['invoiceDateTo'] : '' ?>" name="to" id="invoiceDateTo"  data-date-format="mm-dd-yyyy" class="span10" >
                <span class="add-on"><i class="icon-th"></i></span> 
            </div>
        </div>
        <div class="span3">
            <label>Select Category</label>
            <select name="data[Approval][category][]" id="FilterCategory" multiple="multiple"  title="Please select category.">
                <?php echo getFormOptions(array("Business"=>"Business","Finance"=>"Finance","Lifestyle/Fashion"=>"Lifestyle/Fashion","Technology"=>"Technology","Health"=>"Health","Education"=>"Education","Marketing/digital marketing"=>"Marketing/digital marketing","Travel"=>"Travel","News"=>"News","Sports"=>"Sports","Automobile"=>"Automobile","Real estate"=>"Real estate","Food"=>"Food","Parenting"=>"Parenting","Home decor/ improvement"=>"Home decor/ improvement","Generic"=>"Generic"), $filterVars['filter_category']) ?>
            </select>
        </div>

        <!-- End: Added by Jitendra: 28-Nov-2014 --->  
    </div>
    <div class="form-actions">
        <button type="reset" class="btn btn-primary">Reset</button>
        <button type="button" class="btn btn-success" onclick="filterSubmit();">Filter</button>
    </div>
</form>