<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>

  <hr> 
  <p>Please Check <span style="color:#28B779">Domain History</span> before submission to prevent domain duplicacy for same campaign</p>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Submit Link</h5>
        </div>
        <div class="widget-content nopadding">
	  <?php $approvalLink = $viewData->get('approvalLink') ?>
	 
	 
          <form action="approvals.php?act=submit_link_save" class="form-horizontal"  method="post" id="SubmitLinkForm">
	    <input type="hidden" name="data[Payment][child_table_id]" value="<?php echo $approvalLink['ApprovalLink']['id'] ?>" />
	    <input type="hidden" name="data[Payment][month]" value="<?php echo date('m') ?>" />
	    <input type="hidden" name="data[Payment][year]" value="<?php echo date('Y') ?>" />
            <div class="control-group">
			<a href="#" class="history DomainIpHistory" id="DomainHistory"><img src="img/history.png"/></a>
              <label class="control-label">Domain Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Domain Name" name="data[Payment][domain]" value="<?php echo $approvalLink['Approval']['domain'] ?>" id="PaymentDomain" minlength="4" readonly="readonly"/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Select Campaign :</label>
              <div class="controls">
		<input type="hidden" name="data[Payment][campaign_id]" value="<?php echo $approvalLink['ApprovalLink']['campaign_id'] ?>" />
                <select name="data[Payment][campaign_name]"  id="PaymentCampaignName" title="Please select campaign.">
                 <option><?php echo $approvalLink['ApprovalLink']['campaign_name'] ?></option>
                </select>
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Narration text :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][narration_text]" id="PaymentNarrationText" />
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Page Url :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][url]" id="PaymentUrl" required/>
              </div>
            </div>
		<div class="control-group">
              <label class="control-label">Webmaster Email :</label>
              <div class="controls">
                <input type="email" class="span11" placeholder="" name="data[Payment][client_mail]" id="PaymentClientMail" required/>
              </div>
            </div>
			 <input type="hidden"   name="data[Payment][payment_type]" value="<?php echo $approvalLink['ApprovalLink']['payment_type'];?>" >
          <?php
		  
          if($approvalLink['ApprovalLink']['payment_type']!='NEFT')
		  {?>			  
	      <div class="control-group">
              <label class="control-label">Paypal Email :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][paypal_email]" id="PaymentPaypalEmail" required/>
              </div>
            </div>
	     <?php
          }
          elseif($approvalLink['ApprovalLink']['Amount']>0){
			  
?>
<div class="control-group">
              <label class="control-label">Invoice Number:</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][invoice_name]" value="<?php echo $payment['Payment']['invoice_name'] ?>" id="Paymentinvoice_name"/>
              </div>
            </div>
			<div class="control-group">
              <label class="control-label">Invoice Amount:</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][invoice_amount]" value="<?php echo $payment['Payment']['invoice_amount'] ?>" id="Paymentinvoice_amount"/>
              </div>
            </div>
          <div class="control-group">
              <label class="control-label">Invoice File :</label>
              <div class="controls">
                <input type="hidden" class="span11"  name="data[Payment][invoice_file]" id="Paymentinvoice_file"/>
				
				
				
				<input id="sortpicture" type="file" name="invoice_file_val" />
<button type="button" id="uploadfile">Upload</button>
<span class="help-block">File extention type are doc,docx,pdf,jpg,gif,jpeg,png</span>
<span class="help-block" id="filelink"><?php if($payment['Payment']['invoice_file']<>""){?><a href="http://beta.adlift.com/plb/uploads/invoice/<?php echo $payment['Payment']['invoice_file']?>" target="_blank">View File</a><?php }?></span>
              </div>
            </div>		
          <div class="control-group">
              <label class="control-label">Beneficiary Code :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][bencode]" id="Paymentbencode" required/>
              </div>
            </div>
			<div class="form-actions">
              <button type="button" class="btn btn-danger" id="PaymentGetbendetail">Get  beneficiary Information</button>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][benname]" id="Paymentbenname" required/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Account :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][benaccount]" id="Paymentbenaccount" required/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary IFSC Code :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][ifsccode]" id="Paymentifsccode" required/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Address :</label>
              <div class="controls">
                <textarea class="span11" name="data[Payment][address]" id="Paymentaddress" required/></textarea>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary City :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][city]" id="Paymentcity" required/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary State :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][state]" id="Paymentstate" required/>
              </div>
            </div>
			 <div class="control-group">
              <label class="control-label">Beneficiary Zip Code :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][zip_code]" id="Paymentzip_code" required/>
              </div>
            </div>
<?php
		  }?>			  
	      
	    <div class="control-group">
              <label class="control-label">Site Geolocation :</label>
              <div class="controls">
                <select name="data[Payment][geo]"  id="PaymentGeo" title="Please select geolocation.">
		  <option value=''>Select Country</option>
		  <option value='US'>US</option>
		  <option value='UK'>UK</option>
		  <option value='INDIA'>INDIA</option>
                </select>
              </div>
            </div>
	    <div class="control-group date-box">
              <label class="control-label">Start Date :</label>
              <div class="controls">
		<input type="text" class="span11 datepicker" placeholder="" name="data[Payment][start_date]"  value="<?php echo date('m/d/Y') ?>"  id="PaymentStartDate" required/>
		<span class="help-block">Date Format - MM/DD/YYYY</span>
              </div>
            </div>
	    <div class="control-group date-box">
              <label class="control-label">End Date :</label>
              <div class="controls">
                <input type="text" class="span11 datepicker" placeholder="" name="data[Payment][end_date]" value="<?php echo date('m/d/Y', strtotime('+1 Year')) ?>" id="PaymentEndDate" required/>
		<span class="help-block">Date Format - MM/DD/YYYY</span>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Anchor :</label>
              <div class="controls">
                <textarea class="span11" style="resize: none;" name="data[Payment][anchor_text]" value="" id="PaymentAnchorText" required></textarea>
		<span class="help-block">Each anchor must be in new line</span>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Amount :</label>
              <div class="controls">
                <input type="number" class="span11" pattern="^\d+(\.)\d{2}$" placeholder="" name="data[Payment][amount]" value="<?php echo $approvalLink['ApprovalLink']['Amount'] ?>" id="PaymentAmount" required <?php if(strtotime($approvalLink['ApprovalLink']['added_on']) > strtotime('2014-12-02 00:00:00')){?>readonly="readonly"<?php }?> />
              </div>
            </div>
			  <?php
          if($approvalLink['ApprovalLink']['payment_type']=='NEFT' && $approvalLink['ApprovalLink']['Amount']>0)
		  {?>
  <div class="form-actions">
              <button type="button" class="btn btn-danger" id="PaymentGettdsdetail">Get  TDS Detail</button>
  </div>
 <div class="control-group">
              <label class="control-label">TDS Amount :</label>
              <div class="controls">
                <input type="number" class="span11" readonly placeholder="" name="data[Payment][tdsamount]" value="" id="PaymentTdsamount" required  />
              </div>
            </div>
 <div class="control-group">
              <label class="control-label">Gross Amount :</label>
              <div class="controls">
                 <input type="number" class="span11" readonly placeholder="" name="data[Payment][grossamount]" value="" id="PaymentGrossamount" required  />
              </div>
            </div>			
	<?php
		  }?>
	    <div class="control-group">
              <label class="control-label">Currency :</label>
              <div class="controls">
                <?php 
                if(strtotime($approvalLink['ApprovalLink']['added_on']) > strtotime('2014-12-02 00:00:00'))
                {?>
                <input type="hidden" name="data[Payment][currency]" id="PaymentCurrency" value="<?php echo $approvalLink['ApprovalLink']['Currency']; ?>" />  
                <select name="data[Payment][currency_disabled]"  id="PaymentCurrencyDisabled" title="Please select Currency." disabled="disabled">
		  <option value=''>Select Currency</option>
                  <?php echo getFormOptions(array('INR'=>'INR', 'Euro'=>'Euro', 'Dollars'=>'Dollars', 'Pounds'=>'Pounds'), $approvalLink['ApprovalLink']['Currency']) ?>
		</select> 
                <?php }else{?> 
                <select name="data[Payment][currency]" id="PaymentCurrency" title="Please select Currency.">
		  <option value=''>Select Currency</option>
                  <?php echo getFormOptions(array('INR'=>'INR', 'Euro'=>'Euro', 'Dollars'=>'Dollars', 'Pounds'=>'Pounds'), $approvalLink['ApprovalLink']['Currency']) ?>
		</select>
                <?php }?>
              </div>
            </div>
            
            <div class="control-group">
              <label class="control-label">Payment For Month Of:</label>
              <div class="controls">
		<select name="data[Payment][payment_month]" id="PaymentPaymentMonth" title="Please select payment for the month of." required>
                 <option value="">Select One</option>
                 <option value="1">January</option>
                 <option value="2">February</option>
                 <option value="3">March</option>
                 <option value="4">April</option>
                 <option value="5">May</option>
                 <option value="6">June</option>
                 <option value="7">July</option>
                 <option value="8">August</option>
                 <option value="9">September</option>
                 <option value="10">October</option>
                 <option value="11">November</option>
                 <option value="12">December</option>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Payment For Year Of:</label>
              <div class="controls">
		<select name="data[Payment][payment_year]"  id="PaymentCampaignName" title="Please select payment for the year of." required>
                 <option value="">Select One</option>
                 <?php
                 for($i=-1; $i<3; $i++)
                 {
                     echo '<option value="'.(date('Y')+$i).'">'.(date('Y')+$i).'</option>';
                 }
                 ?>
                </select>
              </div>
              <div class="control-group">
              <label class="control-label">Submitted month and year:</label>
              <div class="controls">
                <input type="month" name="data[Payment][invoice_year]" value="" id="PaymentInvoiceMonth"/>
              </div>
            </div>
            
            
            
            
	    <div class="form-actions">
              <button type="button" class="btn btn-danger" id="PaymentCalcDaPr">Calculate  PR,DA and IP</button>
            </div>
			
            <div class="control-group">
			<!--<a href="#" class="history DomainIpHistory" id="IpHistory"><img src="img/iphistory.png"/></a>-->
              <label class="control-label">IP Address :</label>
              <div class="controls">
                <input type="text"  class="span11"  placeholder="IP Address" name="data[Payment][ip]" id="PaymentIp" value="<?php echo $approvalLink['Approval']['ip'] ?>"  required/>
              </div>
            </div>
            <!-- <div class="control-group">
              <label class="control-label">PR value :</label>
              <div class="controls">
                <input type="text" class="span11" readonly="readonly" placeholder="PR value" name="data[Payment][pr_value]" value="<?php echo $approvalLink['Approval']['pr'] ?>" id="PaymentPr" required/>
              </div>
            </div> -->
            <input type="hidden" class="span11" readonly="readonly" placeholder="PR value" name="data[Payment][pr_value]" value="<?php echo $approvalLink['Approval']['pr'] ?>" id="PaymentPr" value="0"/>

            <div class="control-group">
              <label class="control-label">DA value :</label>
              <div class="controls">
                <input type="text" class="span11" readonly="readonly" placeholder="DA value" name="data[Payment][da_value]" value="<?php echo $approvalLink['Approval']['da'] ?>" id="PaymentDa" required/>
				</div>
            </div>
    
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/approvals_submit_edit_link.js'), array('inline'=>false)) ?>
	  <?php
          if($approvalLink['ApprovalLink']['payment_type']=='NEFT')
		  {?>	
	<?php $viewData->scriptStart() ?>
	jQuery(document).ready(function($){
		 // calculate da, pr, ip
		 
    $('#PaymentGetbendetail').bind('click', function(){
      
          getBenDetail($('#Paymentbencode').val());
       
    });
	 $('#PaymentGettdsdetail').bind('click', function(){
      
          getTdsDetail($('#Paymentbencode').val(),$('#PaymentAmount').val(),'');
       
    });
		
	});
	
	<?php $viewData->scriptEnd() ?>
	<?php
		  }?>
	
	