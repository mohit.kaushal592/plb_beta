<?php $csvGetFilter = $viewData->get('filterVars'); ?>
<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
      <!-- Filter Box -->
      <?php include(WWW_ROOT.DS."layouts".DS."filter_submitted.php") ?>
      <!-- Filter Box End -->
      <!-- Actions -->
	  <?php if(canUserDoThis(array('payment','link_delete'))): ?>
		
	  <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	   <?php if(canUserDoThis('payment')): ?>
	     <li><a href="javascript:void(0)" rel="link_payment" class="doAct"><i class="icon-money"></i> Payment</a></li>
	    <?php endif ?>
	    <?php if(canUserDoThis('link_delete')): ?>
	     <li><a href="javascript:void(0)" rel="link_delete" class="doAct"><i class="icon-trash icon-large"></i> Delete Link</a></li>
	    <?php endif ?>
	    <!-- <li><a href="javascript:void(0)" class="exportList"><i class="icon-download-alt icon-large"></i> Export</a></li> -->
		<?php
			if(count($csvGetFilter)){
		?>	
	    	<li><a href="javascript:void(0)" class="exportList"><i class="icon-download-alt icon-large"></i> Export</a></li>
		<?php }else{ ?>
			<li><a href="#" title="Please first update the filter." onClick="alert('Please select the filter first.')"><i class="icon-download-alt icon-large"></i> Export</a></li>
		<?php } ?>
		<li><a href="javascript:void(0)" class="exportListBank"><i class="icon-download-alt icon-large"></i> Export Bankformat</a></li>
          </ul>
        </div>
	  <?php endif ?>
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100, 150=>150, 200=>200, 250=>250, 200=>300, 350=>350, 400=>400, 450=>450, 500=>500, 1000=>1000,2000=>2000), $_REQUEST['_rp']); ?>
	   </select>
	   </label>
	  </div>
	   
	  	<div class="btn-group1 action-right1" style="display:<?= ($_SESSION['User']['user_type'] == 'superadmin')?'inline':'none'?>">
	  		<label>
				Response Update :
			<input type="checkbox" id="FilterResponseUpdate" value="r_status"> Response Status
			<input type="checkbox" id="FilterIndexUpdate" value="i_status"> Index Status
			<button class="btn btn-primary" id="updateResponse">Apply</button>
            <!-- <select name="response_update[]" id="FilterResponseUpdate" multiple="multiple">
            	<?php //echo getFormOptions(array('r_status'=>'Response Status','i_status'=>'Index Status'), $filterVars['response_update']); ?>
            </select> -->
			  
			</label>
        </div>
	  <!-- End Actions -->
        <div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Submitted Links Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <span></span></h5></div>
          </div>
          <div class="widget-content nopadding" id="SubmittedLinksData">
	   
          </div>
        </div>
	 </div>
       <div class="span11" id="SubmittedLinksPaging">
	</div>
    </div>
    <?php $viewData->scriptStart() ?>
    $(function(){
		 
	    
	     toggleAcceptRejectCheckbox();
       var SubmittedLinksData = function(data, $where, $qStr){
       var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
	  $where.html(loader);
	
	  $.post('submitted_links.php'+ ($qStr.length>0?$qStr : '?act=data'), data, function(responseData){
	     $where.html(responseData.html);
	     $('#SubmittedLinksPaging').html(responseData.pagination);
	     $('.ttl-record h5 span').html(responseData.totalRecords);
	     tblResizeInIt();
	     uniformInIt($where);
	     checkAllInIt();
	     toggleAcceptRejectCheckbox();
	     $('table.sortable_tbl').tablesorter();
	     $('#SubmittedLinksPaging').find('.pagination a').bind('click', function(e){
	      e.preventDefault();
	      SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), $(this).attr('href'));
	     });
	  }, 'json')
	  .fail(function(){
	   $where.html('<div>Error in request processing.</div>');
	  });
       }
       SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), '');
       
       $('#RowPerPage').on('multiselectcreate', function(event, ui){
	 $('#RowPerPage').multiselect('destroy');
      })
      $('#RowPerPage').on('change', function(){
		  var _rp = $('#RowPerPage').val();
		  SubmittedLinksData(<?php echo json_encode($_POST) ?>, $('#SubmittedLinksData'), '?act=data&_rp='+_rp);
	  });
    });
	jQuery(document).ready(function($){
		 checkAllInIt();
		  });
    <?php $viewData->scriptEnd() ?>
    <?php $viewData->scripts(array('js/submitted_links_index.js'), array('inline'=>false)) ?>
