<?php
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akhtar Khan")
							 ->setLastModifiedBy("Akhtar Khan")
							 ->setTitle("Domains DA")
							 ->setSubject("Domains DA")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file")*/;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Domain Name')
            ->setCellValue('B1', 'Campaign Name')
            ->setCellValue('C1', 'Url')
            ->setCellValue('D1', 'PR')
            ->setCellValue('E1', 'DA')
            ->setCellValue('F1', 'Webmaster Email')
            ->setCellValue('G1', 'Paypal Email')
            ->setCellValue('H1', 'User')
            ->setCellValue('I1', 'Anchor Text')
            ->setCellValue('J1', 'IP')
            ->setCellValue('K1', 'Amount')
            ->setCellValue('L1', 'Currency')
            ->setCellValue('M1', 'Site Geo')
            ->setCellValue('N1', 'Added On')
            ->setCellValue('O1', 'Paid On')
            ->setCellValue('P1', 'Response Status')
            ->setCellValue('Q1', 'Index Status')
            ->setCellValue('R1', 'Index Count')
            ->setCellValue('S1', 'Invoice Payment')
            ->setCellValue('T1', 'Category');
$objPHPExcel->getActiveSheet()->getStyle('A1:R1')->getFont()->setBold(true);
$submittedlinks = $viewData->get('submittedLinks');
if(!empty($submittedlinks)){
    $i=2;
    //echo "<pre>"; print_r($submittedlinks); die;
    foreach($submittedlinks as $_submittedLink){
        $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'. $i, $_submittedLink['Payment']['domain'])
                    ->setCellValue('B'. $i, $_submittedLink['Payment']['campaign_name'])
                    ->setCellValue('C'. $i, $_submittedLink['Payment']['url'])
                    ->setCellValue('D'. $i, $_submittedLink['Payment']['pr_value'])
                    ->setCellValue('E'. $i, round($_submittedLink['Payment']['da_value'], 2))
                    ->setCellValue('F'. $i, $_submittedLink['Payment']['client_mail'])
                    ->setCellValue('G'. $i, $_submittedLink['Payment']['paypal_email'])
                    ->setCellValue('H'. $i, ucwords($_submittedLink['User']['first_name'].' '.$_submittedLink['User']['last_name']))
                    ->setCellValue('I'. $i, $_submittedLink['Payment']['anchor_text'])
                    ->setCellValue('J'. $i, $_submittedLink['Payment']['ip'])
                    ->setCellValue('K'. $i, $_submittedLink['Payment']['amount'])
                    ->setCellValue('L'. $i, $_submittedLink['Payment']['currency'])
                    ->setCellValue('M'. $i, $_submittedLink['Payment']['geo'])
                    ->setCellValue('N'. $i, date('Y-m-d', strtotime($_submittedLink['Payment']['added_on'])))
                    ->setCellValue('O'. $i, $_submittedLink['Payment']['payment_date'])
                    ->setCellValue('P'. $i, $_submittedLink['Payment']['response_status'])
                    ->setCellValue('Q'. $i, $_submittedLink['Payment']['index_status'])
                    ->setCellValue('R'. $i, $_submittedLink['Payment']['index_count'])
                    ->setCellValue('S'. $i, $_submittedLink['Payment']['invoice_month'])
                    ->setCellValue('T'. $i, $_submittedLink['Payment']['category']);
        $i++;
    }
}
            
$objPHPExcel->getActiveSheet()->setTitle('Submited Links');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="submitted_links_report_'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>