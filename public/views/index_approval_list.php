<table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Domain</th>
        <th>PR</th>
        <th>DA</th>
        <th>User</th>
        <th>Date</th>
        <!--<th>Count</th>-->
        <th>Campaign</th>
                        
      </tr>
    </thead>
    <tbody>
      <?php $approvals = $viewData->get('approvals') ?>
      <?php if(!empty($approvals)): ?>
      <?php foreach($approvals as $_approval): ?>
      <tr>
        <td style="color:#DA542E;">
          <?php echo $_approval['Approval']['domain'] ?>
          <span style="color:#000; display: block;float: none;"><?php echo $_approval['Approval']['ip'] ?></span>
        </td>
        <td><?php echo round($_approval['Approval']['pr'],2) ?></td>
        <td><?php echo round($_approval['Approval']['da'],2) ?></td>
        <td class="center"><?php echo ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name']) ?></td>
        <td class="center"><?php echo date_to_text($_approval['Approval']['added_on']) ?></td>
        <!--<td class="center"><?php echo $_approval['Approval']['campaigns_count'] ?></td>-->
        <td class="center">
        <?php
          $_camps = array();
          if(!empty($_approval['ApprovalLink'])):
            foreach($_approval['ApprovalLink'] as $_appLink):
              $_camps[] = $_appLink['campaign_name'];
            endforeach;
          endif;
          echo ucwords(implode(', ', $_camps));
          ?>
        </td>
      </tr>
      <?php endforeach ?>
      <?php else: ?>
      <tr><td colspan="7">Record not found.</td></tr>
      <?php endif ?>
    </tbody>
  </table>