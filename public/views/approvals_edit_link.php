<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php $approval = $viewData->get('approval') ?>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Information of Link - <?php echo $approval['Approval']['domain'] ?></h5>
        </div>
        <div class="widget-content nopadding">
          <input type="hidden" id="_ApprovalLinkId" value="<?php echo $approval['Approval']['id'] ?>"/>
            <table class="table table-striped table-bordered">
              <tbody>
                <tr>
                  <td>Domain</td>
                  <td><?php echo $approval['Approval']['domain'] ?></td>
                </tr>
                <tr>
                  <td>IP Address</td>
                  <td><?php echo $approval['Approval']['ip'] ?></td>
                </tr>
                <tr>
                  <td>PR</td>
                  <td><?php echo $approval['Approval']['pr'] ?></td>
                </tr>
                <tr>
                  <td>DA</td>
                  <td><?php echo round($approval['Approval']['da'],2) ?></td>
                </tr>
                <tr>
                  <td>Content Writer</td>
                  <td><?php echo $approval['Approval']['content_writer']==1?'External' : ($approval['Approval']['content_writer']==0?'Adlift' : '') ?></td>
                </tr>
                <tr>
                  <td>Narration Text</td>
                  <td><textarea id="narrationText" class="span12" style="resize:none;height: 125px;"><?php echo $approval['Approval']['narration_text'] ?></textarea></td>
                </tr>
              </tbody>
            </table>
            <div class="form-actions" style="margin-top: 0;text-align: right;">
              <button class="btn btn-success" id="updateNarration" type="button">Submit</button>
            </div>
          </div>
      </div>
    </div>
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Campaigns under Link - <?php echo $approval['Approval']['domain'] ?></h5>
          <div class="buttons"> <a id="add-event" data-toggle="modal"
href="#modal-add-event" class="btn btn-inverse btn-mini"><i
class="icon-plus icon-white"></i> Add Campaign</a>
               <div class="modal hide" id="modal-add-event">
                 <div class="modal-header">
                   <button type="button" class="close"
data-dismiss="modal">x</button>
                   <h3>Add Campaigns to link - <?php echo $approval['Approval']['domain'] ?></h3>
                 </div>
                 <div class="modal-body">
                   <form action="" method="post" id="ApprovalEditLinkSave">
                    <input type="hidden" name="data[ApprovalLink][parent_id]" value="<?php echo $approval['Approval']['id'] ?>"/>
                    <p>Select Campaigns:</p>
                    <p>
                     <?php $campsAdded = array() ?>
                     <?php foreach($approval['ApprovalLink'] as $_approvalLink): ?>
                     <?php $campsAdded[] = $_approvalLink['campaign_id'] ?>
                     <?php endforeach ?>
                      <select name="data[ApprovalLink][campaign][]" id="ApprovalLinkCampaign" multiple="multiple" required>
                         <?php foreach($viewData->get('campaignsList') as $campId=>$campName): ?>
                         <?php if(!in_array($campId, $campsAdded)): ?>
                         <option value="<?php echo $campId ?>"><?php echo $campName ?></option>
                         <?php endif ?>
                         <?php endforeach ?>
                      </select>
                      <span class="error" style="display:none;">Please select campaign</span>
                    </p>
                   </form>
                 </div>
                 <div class="modal-footer"> <a href="#" class="btn"
data-dismiss="modal">Cancel</a> <a href="javascript:void(0)" id="add-campaign-submit"
class="btn btn-primary">Add Campaigns</a> </div>
               </div>
             </div>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
                <thead>
                    <th style="text-align: left;">Campaign Name</th>
                    <th>Action</th>
                </thead>
              <tbody>
                <?php if(!empty($approval['ApprovalLink'])):
                foreach($approval['ApprovalLink'] as $_approvalLink):
                ?>
                <tr>
                  <td><?php echo ucwords($_approvalLink['campaign_name']) ?></td>
                  <td style="text-align: center;">
                    <?php
                    if($session->read('User.id')==$_approvalLink['user_id'] || $session->read('User.user_type')=='superadmin'):
                    ?>
                    <?php if($_approvalLink['final_link_submit_status']!=1): ?>
                    <a href="#" class="remove-campaign tip-top" title="Remove Campaign" rel="<?php echo $_approvalLink['id'] ?>"><i class="icon icon-remove icon-large"></i></a>
                    <?php endif; ?>
                    <?php endif; ?>
                  </td>
                </tr>
                <?php
                endforeach;
                endif;
                ?>
                
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/approvals_edit_link.js'), array('inline'=>false)) ?>