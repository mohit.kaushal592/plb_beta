<table class="table table-bordered table-striped">
    <tr>
        <td colspan="6" class="tbl-caption">Approval Campaigns History</td>
    </tr>
    <tr>
        <th>Domain</th>
        <th>DA</th>
        <th>PR</th>
        <th>Campaigns</th>
        <th>Date</th>
        <th>User</th>
    </tr>
    <?php $approvals = $viewData->get('approvals'); ?>
    <?php if(!empty($approvals)): ?>
        <?php foreach($approvals as $_approval): ?>
            <tr>
                <td><?php echo $_approval['Approval']['domain'] ?></td>
                <td><?php echo round($_approval['Approval']['da'], 2) ?></td>
                <td><?php echo round($_approval['Approval']['pr'],2) ?></td>
                <td>
                <?php
		    $_camps = array();
		    if(!empty($_approval['ApprovalLink'])):
		      foreach($_approval['ApprovalLink'] as $_appLink):
			$cmpHtm = "<li>";
			$cmpHtm .= "<span>". ucwords($_appLink['campaign_name'])."</span>" ;
			$cmpHtm .= getCampaignIcons($_appLink, $viewData);
			$cmpHtm .= "</li>";
			$_camps[] = $cmpHtm; 
		      endforeach;
		    endif;
		?>
		<?php   if($_approval['User']['id'] == $session->read('User.id') || $session->read('User.user_type') == 'superadmin'): 
		   $_camps[] = '<li><a href="approvals.php?act=edit_link&pid='. $_approval['Approval']['id'].'" class="tip-right" style="color:#428BCA;" title="Click to add campaign." > <i class="icon-plus icon-large"></i> Add More</a></li>';
		 endif ?>
		<ul><?php echo implode(' ', $_camps) ?></ul>
                </td>
                <td><?php echo date_to_text($_approval['Approval']['added_on']) ?></td>
                <td><?php echo ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name']) ?></td>
            </tr>
        <?php endforeach ?>
    <?php else: ?>
    <tr><td colspan="6">Domain history not found.</td></tr>
    <?php endif ?>
</table>
<br />

<table class="table table-bordered table-striped">
    <tr>
        <td colspan="11" class="tbl-caption">Link Submission History</td>
    </tr>
    <tr>
        <th>Campaign</th>
        <th>Domain</th>
        <th>Url</th>
        <th>DA</th>
        <th>PR</th>
        <th>Amount</th>
        <th>User</th>
        <th>Webmaster</th>
        <th>Paypal</th>
        <th>Status</th>
        <th>Date</th>
    </tr>
    <?php $payments = $viewData->get('payments'); ?>
    <?php if(!empty($payments)): ?>
        <?php foreach($payments as $_payment): ?>
            <tr>
                <td><?php echo $_payment['Payment']['campaign_name'] ?></td>
                <td><?php echo $_payment['Payment']['domain'] ?></td>
                <td><?php echo $_payment['Payment']['url'] ?></td>
                <td><?php echo round($_payment['Payment']['da_value'], 2) ?></td>
                <td><?php echo round($_payment['Payment']['pr_value'],2) ?></td>
                <td><?php echo currency_format($_payment['Payment']['amount'], $_payment['Payment']['currency']) ?></td>
                <td><?php echo ucwords($_payment['User']['first_name'].' '.$_payment['User']['last_name']) ?></td>
                <td><?php echo $_payment['Payment']['client_mail'] ?></td>
                <td><?php echo $_payment['Payment']['paypal_email'] ?></td>
                <td><?php echo $_payment['Payment']['status'] ?></td>
                <td><?php echo date_to_text($_approval['Approval']['added_on']) ?></td>
                
            </tr>
        <?php endforeach ?>
    <?php else: ?>
    <tr><td colspan="11">Domain submission history not found.</td></tr>
    <?php endif ?>
</table>
