<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
      <!-- Filter Box -->
      <?php include(WWW_ROOT.DS."layouts".DS."filter_approvals.php") ?>
      <!-- Filter Box End -->
      <!-- Actions -->
	  <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	   <?php if(canUserDoThis(array('forward','approve_phase_1','approve_phase_2','approval_delete'))): ?>
	    <?php if(canUserDoThis('forward')): ?>
	      <li><a href="javascript:void(0)" rel="approval_forward" class="doAct"><i class="icon-step-forward  icon-large"></i> Forward</a></li>
	     <?php endif ?>
	     <?php if(canUserDoThis('approve_phase_1')): ?>
	     <li><a href="javascript:void(0)" rel="approval_phase_1" class="doAct"><i class="icon-ok icon-large"></i><sup class="icontxt">1</sup> Approval Phase I</a></li>
	     <?php endif ?>
	     <?php if(canUserDoThis('approve_phase_2')): ?>
	     <li><a href="javascript:void(0)" rel="approval_phase_2" class="doAct"><i class="icon-ok icon-large"></i><sup class="icontxt">2</sup> Approval Phase II</a></li>
	     <?php endif ?>
	     <?php if(canUserDoThis('approval_delete')): ?>
	      <li><a href="javascript:void(0)" rel="approval_delete" class="doAct"><i class="icon-trash icon-large"></i> Delete Link</a></li>
	     <?php endif ?>
	    <?php endif ?>
	    <li><a id="_ExportToExcel" href="javascript:void(0)"><i class="icon-download-alt icon-large"></i> Export</a></li>
          </ul>
        </div>
	  
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $filterVars['row_per_page']); ?>
	   </select>
	   </label>
	  </div>
	  
	  <!-- End Actions -->
        <div class="widget-box">
	  <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Approval Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="approvals.php?act=do_action" method="post" id="ApprovalListForm">
	     <input type="hidden" name="whatDo" id="ApprovalWhatDo" />
	     <table class="table table-bordered table-striped tbl-resize approvals_list_tbl sortable_tbl">
	       <thead>
		 <tr>
		   <th style="width: 206px;">
		    <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
		     <span style="float:left"><input type="checkbox" id="checkAllApproval" /></span>
		    <?php endif ?>
		    Domain
		   </th>
		   <th style="width: 30px;">PR</th>
		   <th style="width: 40px;">DA</th>
		   <th>User</th>
		   <th>Date</th>
		   <th style="width: 25px;">Count</th>
		   <th style="width: 250px;">Campaign</th>
		   <th>Text</th>
		   <th>Admin Text</th>
		   <th>Content Writer</th>
		   <th>Action</th>
		 </tr>
	       </thead>
	       <tbody> 
		 <?php $approvals = $viewData->get('approvals') ?>
		<?php if(!empty($approvals)): ?>
		 <?php foreach($approvals as $approval): ?>
		 <tr class="odd gradeX" domain="<?php echo $approval['Approval']['domain'] ?>">
		   <td style="color:#DA542E;">
		    <?php if(canUserDoThis(array('forward','approval_delete'))): ?>
		    <input type="checkbox" id="ApprovalId<?php echo $approval['Approval']['id'] ?>" class="_approval_id" name="data[Approval][id][]" value="<?php echo $approval['Approval']['id'] ?>" />
		    <?php endif ?>
		    <a href="<?php echo $approval['Approval']['domain'] ?>" target="_blank"><?php echo $approval['Approval']['domain'] ?></a> 
		    <span class="domain-ip tip tip-top" title="click to view ip history"><?php echo $approval['Approval']['ip'] ?></span>
		   </td>
		   <td><?php echo round($approval['Approval']['pr'],2) ?></td>
		   <td><?php echo round($approval['Approval']['da'], 2) ?></td>
		   <td><?php echo ucwords($approval['User']['first_name'].' '.$approval['User']['last_name']) ?></td>
		   <td ><?php echo date_to_text($approval['Approval']['added_on']) ?></td>
		     <td ><?php echo $approval['Approval']['campaigns_count'] ?></td>
		   <td>
		     <?php
		     $_camps = array();
		     if(!empty($approval['ApprovalLink'])):
		       foreach($approval['ApprovalLink'] as $_appLink):
			 $_cmpHtml = "<li>";
			 if(canUserDoThis(array('approve_phase_1','approve_phase_2'), $_appLink, true)):
			  $_cmpHtml .= "<span>";
			  $_cmpHtml .= '<input type="checkbox" id="ApprovalLinkAccept'. $approval['Approval']['id'].'_'.$_appLink['id'].'" class="_approval_accept" name="data[ApprovalLink][accept][]" value="'.$_appLink['id'].'" /> ';
			  $_cmpHtml .= '<input type="checkbox" id="ApprovalLinkReject'. $approval['Approval']['id'].'_'.$_appLink['id'].'" class="_approval_reject" name="data[ApprovalLink][reject][]" value="'.$_appLink['id'].'" /> ';
			  $_cmpHtml .= '</span> ';
			 endif;
			 $_cmpHtml .= '<span>'.ucwords($_appLink['campaign_name']);
                         if($_appLink['final_link_submit_status'] == 0 && $_appLink['Amount'] != 0 && $_appLink['Currency'] != "")
                         {
                             $_cmpHtml .= ' ('.currency_format($_appLink['Amount'], $_appLink['Currency']).')';
                         }
                         elseif ($_appLink['final_link_submit_status'] == 1)
                         {
                             //$_cmpHtml .= ' ($ 231)';
                             //echo "select Amount, Currency from tracking_data where child_table_id=".$_appLink['id'];exit;
                             //$resultJitu = $dbObj->find_by_sql("select Amount, Currency from tracking_data where child_table_id=".$_appLink['id']);
                             //echo "<pre>";print_r($resultJitu);exit;
                         }
                     
			 $_cmpHtml .= '</span>';
			 $_cmpHtml .= getCampaignIcons($_appLink, $viewData);
			 
			 $_cmpHtml .= '</li>';
			 $_camps[] = $_cmpHtml;
		       endforeach;
		     endif;
		     ?>
		   <ul><?php echo implode(' ', $_camps) ?></ul>
		   </td>
		   <td><?php echo $approval['Approval']['narration_text'] ?></td>
		   <td id="_row_<?php echo $approval['Approval']['id'] ?>"><span style="display: block;width: 100%;"><?php echo nl2br($approval['Approval']['narration_admin']) ?></span>
		   <?php if(canUserDoThis(array('admin_comment'))): ?>
		   <a data-toggle="modal" data-backdrop='static' data-target="#AdminCommentAdd" onmouseover="approvalId='<?php echo $approval['Approval']['id'] ?>'; domainname='<?php echo $approval['Approval']['domain'] ?>'"  href="#" title="Click to add comment." class="tip-right"><i class="icon-plus icon-large"></i> &nbsp;</a>
		   <?php endif ?>
		   </td>
		   <td><?php echo $approval['Approval']['content_writer']==1?'External' : ($approval['Approval']['content_writer']==0?'Adlift' : '') ?></td>
		   <td>
		    <?php   //if($approval['User']['id'] == $session->read('User.id') || $session->read('User.user_type') == 'superadmin'):  ?>
<!--		    <a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>" class="tip-top" title="Click to edit link." target="_blank"> <i class="icon icon-edit icon-large"></i></a>-->
		    <?php //endif ?>
                    
                    <?php   if($session->read('User.user_type') == 'superadmin'):  ?>
		    <a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>" class="tip-top" title="Click to edit link." target="_blank"> <i class="icon icon-edit icon-large"></i></a>
		    <?php endif ?>
		   </td>
		 </tr>
		 <?php endforeach ?>
		 <?php else: ?>
		 <tr><td colspan="10" class="no-record-found">No records found.</td></tr>
		 <?php endif ?>
	       </tbody>
	     </table>
	    </form>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>
	 </div>
     
     <div id="AdminCommentAdd" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
     <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Admin Comment <span></span></h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
       <a href="#" class="btn" data-dismiss="modal">Cancel</a>
       <a href="javascript:void(0)" id="admin-add-comment" class="btn btn-primary">Submit</a>
      </div>
    </div>
  </div>
     
   </div>
    
<?php $viewData->scripts(array('js/approvals_list.js'), array('inline'=>false)) ?>