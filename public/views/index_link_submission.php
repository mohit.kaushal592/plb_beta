<table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>Month</th>
        <th>Average DA</th>
        <th>Average Price (USD)</th>
        <th>Submitted Links</th>
      </tr>
    </thead>
    <tbody>
      <?php $submittedLinks = $viewData->get('submittedLinks') ?>
      <?php if(!empty($submittedLinks)): ?>
      <?php foreach($submittedLinks as $_date=>$_submittedLink): ?>
      <tr>
        <td style="color:#DA542E;">
          <?php echo date('M Y', strtotime($_date)) ?>
        </td>
        <td style="text-align: center;"><?php echo array_avg($_submittedLink['da']);  ?></td>
        <td style="text-align: center;"><?php echo array_avg($_submittedLink['amount']) ?></td>
        <td style="text-align: center;"><?php echo $_submittedLink['links'] ?></td>
      </tr>
      <?php endforeach ?>
      <?php else: ?>
      <tr><td colspan="4">Record not found.</td></tr>
      <?php endif ?>
    </tbody>
  </table>