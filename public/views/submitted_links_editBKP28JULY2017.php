<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  <p>Please Check <span style="color:#28B779">Domain History</span> before submission to prevent domain duplicacy for same campaign</p>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Submit Link</h5>
        </div>
        <div class="widget-content nopadding">
	  <?php $payment = $viewData->get('payment') ?>
          <form action="submitted_links.php?act=edit_save" class="form-horizontal"  method="post" id="SubmitLinkForm">
	    <input type="hidden" name="data[Payment][id]" id="PaymentId" value="<?php echo $payment['Payment']['id'] ?>" />
	    <input type="hidden" name="data[Payment][child_table_id]" value="<?php echo $payment['Payment']['child_table_id'] ?>" />
            <div class="control-group">
			<a href="#" class="history DomainIpHistory" id="DomainHistory"><img src="img/history.png"/></a>
              <label class="control-label">Domain Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Domain Name" value="<?php echo $payment['Payment']['domain'] ?>" id="PaymentDomain" minlength="4" readonly="readonly"/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Select Campaign :</label>
              <div class="controls">
		<input type="hidden" name="data[Payment][campaign_id]" value="<?php echo $payment['Payment']['campaign_id'] ?>" />
                <select name="data[Payment][campaign_name]"  id="PaymentCampaignName" title="Please select campaign.">
                 <option><?php echo $payment['Payment']['campaign_name'] ?></option>
                </select>
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Narration text :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][narration_text]" id="PaymentNarrationText" value="<?php echo $payment['Payment']['narration_text'] ?>" />
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Page Url :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="" name="data[Payment][url]" id="PaymentUrl" value="<?php echo $payment['Payment']['url'] ?>" required/>
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Paypal Email :</label>
              <div class="controls">
                <input type="email" class="span11" placeholder="" name="data[Payment][paypal_email]" id="PaymentPaypalEmail"  value="<?php echo $payment['Payment']['paypal_email'] ?>" required/>
              </div>
            </div>
	      <div class="control-group">
              <label class="control-label">Webmaster Email :</label>
              <div class="controls">
                <input type="email" class="span11" placeholder="" name="data[Payment][client_mail]" id="PaymentClientMail" value="<?php echo $payment['Payment']['client_mail'] ?>" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Site Geolocation :</label>
              <div class="controls">
                <select name="data[Payment][geo]"  id="PaymentGeo" title="Please select geolocation.">
		  <option value=''>Select Country</option>
		  <?php echo getFormOptions(array('US'=>'US', 'UK'=>'UK', 'INDIA'=>'INDIA'), $payment['Payment']['geo']) ?>
                </select>
              </div>
            </div>
	    <div class="control-group date-box">
              <label class="control-label">Start Date :</label>
              <div class="controls">
		<input type="text" class="span11 datepicker" placeholder="" name="data[Payment][start_date]"  value="<?php echo date('m/d/Y', strtotime($payment['Payment']['start_date'])) ?>"  id="PaymentStartDate" required/>
		<span class="help-block">Date Format - MM/DD/YYYY</span>
              </div>
            </div>
	    <div class="control-group date-box">
              <label class="control-label">End Date :</label>
              <div class="controls">
                <input type="text" class="span11 datepicker" placeholder="" name="data[Payment][end_date]" value="<?php echo date('m/d/Y', strtotime($payment['Payment']['end_date'])) ?>" id="PaymentEndDate" required/>
		<span class="help-block">Date Format - MM/DD/YYYY</span>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Anchor :</label>
              <div class="controls">
                <textarea class="span11" style="resize: none;" name="data[Payment][anchor_text]" id="PaymentAnchorText" required><?php echo $payment['Payment']['anchor_text'] ?></textarea>
		<span class="help-block">Each anchor must be in new line</span>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Amount :</label>
              <div class="controls">
                  <input type="number" class="span11" pattern="^\d+(\.)\d{2}$" placeholder="" name="data[Payment][amount]" value="<?php echo $payment['Payment']['amount'] ?>" id="PaymentAmount" required readonly="readonly" />
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label">Currency :</label>
              <div class="controls">
                <input type="hidden" name="data[Payment][currency]" id="PaymentCurrency" value="<?php echo $payment['Payment']['currency']; ?>" />
                <select name="data[Payment][currency_disabled]"  id="PaymentCurrencyDisabled" title="Please select Currency." disabled="disabled">
		  <option value=''>Select Currency</option>
		  <?php echo getFormOptions(array('INR'=>'INR', 'Euro'=>'Euro', 'Dollars'=>'Dollars', 'Pounds'=>'Pounds'), $payment['Payment']['currency']) ?>
                </select>
              </div>
            </div>
            
            <?php
            $monthArr = array(
                '1'=>'January',
                '2'=>'February',
                '3'=>'March',
                '4'=>'April',
                '5'=>'May',
                '6'=>'June',
                '7'=>'July',
                '8'=>'August',
                '9'=>'September',
                '10'=>'October',
                '11'=>'November',
                '12'=>'December'
                );
            ?>
            <div class="control-group">
              <label class="control-label">Payment For Month Of:</label>
              <div class="controls">
                <input type="hidden" name="data[Payment][payment_month]" id="PaymentPaymentMonth" value="<?php echo $payment['Payment']['payment_month']; ?>" />  
                <select name="data[Payment][payment_month_duplicate]" id="PaymentPaymentMonthDuplicate" title="Please select payment for the month of." required disabled="disabled">
                 <?php
                 foreach($monthArr as $key => $val)
                 {
                 ?> 
                    <option value="<?php echo $key;?>" <?php if($key == $payment['Payment']['payment_month']){echo " selected='selected'";}?>><?php echo $val;?></option>
                 <?php }?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Payment For Year Of:</label>
              <div class="controls">
                <input type="hidden" name="data[Payment][payment_year]" id="PaymentPaymentYear" value="<?php echo $payment['Payment']['payment_year']; ?>" />
                <select name="data[Payment][payment_year_duplicate]"  id="PaymentPaymentYearDuplicate" title="Please select payment for the year of." required disabled="disabled">
                 <?php
                 for($i=-1; $i<3; $i++)
                 {
                 ?>
                     <option value="<?php echo (date('Y')+$i);?>" <?php if((date('Y')+$i) == $payment['Payment']['payment_year']){echo " selected='selected'";}?>><?php echo (date('Y')+$i);?></option>
                 <?php
                 }
                 ?>
                </select>
              </div>
            </div>
            
            
            
	    <div class="form-actions">
              <button type="button" class="btn btn-danger" id="PaymentCalcDaPr">Calculate  PR,DA and IP</button>
            </div>
			
            <div class="control-group">
			<!--<a href="#" class="history DomainIpHistory" id="IpHistory"><img src="img/iphistory.png"/></a>-->
              <label class="control-label">IP Address :</label>
              <div class="controls">
                <input type="text"  class="span11" placeholder="IP Address" name="data[Payment][ip]" id="PaymentIp" value="<?php echo $payment['Payment']['ip'] ?>"  required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">PR value :</label>
              <div class="controls">
                <input type="text" class="span11" readonly="readonly" placeholder="PR value" name="data[Payment][pr_value]" value="<?php echo $payment['Payment']['pr_value'] ?>" id="PaymentPr" required/>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">DA value :</label>
              <div class="controls">
                <input type="text" class="span11" readonly="readonly" placeholder="DA value" name="data[Payment][da_value]" value="<?php echo round($payment['Payment']['da_value'], 2) ?>" id="PaymentDa" required/>
				</div>
            </div>
    
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/approvals_submit_link.js'), array('inline'=>false)) ?>