<?php
date_default_timezone_set("Asia/Calcutta");   //India time (GMT+5:30)
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akhtar Khan")
							 ->setLastModifiedBy("Akhtar Khan")
							 ->setTitle("Approval Links Data")
							 ->setSubject("Approval Links Data")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")*/
							 ->setCategory("Adlift PLB Tool");
$objPHPExcel->setActiveSheetIndex(0)
	    ->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
	    ->setCellValue('A1', 'Approval Links Report');
	    
$objPHPExcel->getActiveSheet()->mergeCells('A1:J1');
$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setSize(20);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A3', 'Domain')
	    ->setCellValue('B3', 'IP')
            ->setCellValue('C3', 'PR')
            ->setCellValue('D3', 'DA')
            ->setCellValue('E3', 'SS')
            ->setCellValue('F3', 'Country')
            ->setCellValue('G3', 'Campaign')
            ->setCellValue('H3', 'User')
            ->setCellValue('I3', 'Date')
            ->setCellValue('J3', 'Content Writer')
            ->setCellValue('K3', 'Approval Status')
            ->setCellValue('L3', 'Link Submission')
            ->setCellValue('M3', 'Category');
$objPHPExcel->getActiveSheet()->getStyle('A3:L3')->getFont()->setBold(true);
$approvals = $viewData->get('approvals');
if(!empty($approvals)){
    $approvalStatusArray = array('Approved'=>1, 'Rejected'=>0, 'Pending'=>-1);
    $i=4;
    foreach($approvals as $c=>$approval){
    	/*if($c==50000){ 
    		$time_end = microtime(true);
    		 $execution_time = ($time_end - $time_start); 
  
echo " It ".$execution_time." seconds to execute the script"; die("_______");}
*/
	foreach($approval['ApprovalLink'] as $_approval){
	    $objPHPExcel->getActiveSheet()
			->setCellValue('A'. $i, $approval['Approval']['domain'])
			->setCellValue('B'. $i, $approval['Approval']['ip'])
			->setCellValue('C'. $i, $approval['Approval']['pr'])
			->setCellValue('D'. $i, round($approval['Approval']['da'], 2))
			->setCellValue('E'. $i, $approval['Approval']['sc'])
			->setCellValue('F'. $i, $approval['Approval']['ac'])
			->setCellValue('G'. $i, $_approval['campaign_name'])
			->setCellValue('H'. $i, ucwords($approval['User']['first_name'].' '.$approval['User']['last_name']))
			->setCellValue('I'. $i, date('Y-m-d', strtotime($_approval['added_on'])))
			->setCellValue('J'. $i, $approval['Approval']['content_writer']==1 ? 'External' : 'AdLift')
			->setCellValue('K'. $i, array_search($_approval['approve_status'], $approvalStatusArray))
			->setCellValue('L'. $i, $_approval['final_link_submit_status']==1? 'Submitted' : 'Pending')
			->setCellValue('M'. $i, $approval['Approval']['category']);
			
	    $i++;
	}
    }
}
            
$objPHPExcel->getActiveSheet()->setTitle('Approval Links');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="approval_links_report_'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>