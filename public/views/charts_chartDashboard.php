<!DOCTYPE html>
<html lang="en">
<style>
#campaignFilter {
    width: 200px;
    /* Adjust width as needed */
}
a{
    text-decoration:none
}
</style>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- css-files -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <link rel="stylesheet" href="css/bootstrap.min.css" /> -->
    <!-- <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="css/uniform.css" />
    <link rel="stylesheet" href="css/datepicker.css" />
    <link rel="stylesheet" href="css/fullcalendar.css" /> -->
    <!-- <link rel="stylesheet" href="css/style.css" /> -->
    <!-- <link rel="stylesheet" href="css/media.css" />
    <link rel="stylesheet" href="css/select2.css" />
    <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css" /> -->
    <!-- <link href="css/font-awesome.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.css" rel="stylesheet" />
    <link href="css/jquery.multiselect.filter.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/jquery.gritter.css" /> -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="Assets/Css/bootstrap.min.css">
    <link rel="stylesheet" href="Assets/Css/Style.css">
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css"> -->
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <!-- Bootstrap CSS -->
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Bootstrap Datepicker CSS (Cosmo theme) -->
    <!-- <link
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css"
        rel="stylesheet"> -->

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <!-- Bootstrap Datepicker JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>

    <!-- Bootstrap Datepicker Localization (optional) -->
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.en-GB.min.js">
    </script>


</head>

<script>
// Set Highcharts license
Highcharts.setOptions({
    credits: {
        enabled: false // Disable credits to remove watermark
    }
});

// get state data start
fetch('https://api.adlift.com/marketing/stats/').then(res => {
    return res.json()
}).then(response => {
    $('.total_da').text(`${response.total_da}`)
    $('.total_traffic').text(`${response.total_traffic}`)
    $('.total_unique_sites').text(`${response.total_unique_sites}`)
    $('.total_websites').text(`${response.total_websites}`)
    $('.total_premium_sites').text(`${response.total_premium_sites}`)
    $('.total_campaigns').text(`${response.total_campaigns}`)
}).catch(() => {
    alert('some error to fetch stat data')
})
// get state data end

// category bar chart start
function categoryChart(filter = '') {
    // fetch(`https://api.adlift.com/marketing/category_month${filter}`).then(res => {
    // Get the current date and format it as "YYYY-MM"
    const now = new Date();
    const currentMonth = `${now.getFullYear()}-${String(now.getMonth() + 1).padStart(2, '0')}`;

    // Construct the fetch URL dynamically
    let fetchUrl;
    if (filter) {
        // If 'filter' has a value, use it in the URL
        fetchUrl = `https://api.adlift.com/marketing/category_month${filter}`;
    } else {
        // If 'filter' is empty, use the current month by default
        fetchUrl = `https://api.adlift.com/marketing/category_month?month=${currentMonth}`;
    }

    // fetch(`https://api.adlift.com//marketing/da_cost${filter}`).then(res => {
        fetch(fetchUrl).then(res => {
        return res.json()
    }).then(response => {
        const categoryNameArr = []
        const categoryValueArr = []
        for (let categoryName in response) {
            categoryNameArr.push(categoryName)
            categoryValueArr.push(response[categoryName])
        }
        Highcharts.chart('categoryChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: categoryNameArr,
                crosshair: true,
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'No of Sites' // Label for the y-axis
                }
            },
            series: [{
                name: 'Categories',
                data: categoryValueArr
            }]
        })
    }).catch(() => {
        alert('some error to fetch category data')
    })
}
// category bar chart end

// number of site by cost chart start
function noOfSiteCost(filter = '') {
    fetch(`https://api.adlift.com/marketing/sites${filter}`).then(res => {
        return res.json()
    }).then(response => {
        const modifyResponse = response.data[0]
        const categoryNameArr = []
        const categoryValueArr = []
        for (let categoryName in modifyResponse) {
            categoryNameArr.push(categoryName)
            categoryValueArr.push(modifyResponse[categoryName])
        }
        Highcharts.chart('numberOfSiteByCostChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: categoryNameArr,
                crosshair: true,
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'No. of Sites' // Label for the y-axis
                }
            },
            series: [{
                name: 'Price Range',
                data: categoryValueArr
            }],
            exporting: {
                enabled: false // Disable exporting module
            }
        })
    }).catch(() => {
        alert('some error to fetch category data')
    })
}
// number of site by cost chart end

// top category chart start
fetch('https://api.adlift.com/marketing/category').then(res => {
    return res.json()
}).then(response => {
    var topCategoryObject = []
    for (let categoryName in response) {
        var obj = {
            name: categoryName,
            value: response[categoryName]
        }
        topCategoryObject.push(obj)
    }
    Highcharts.chart('topCategoryChartContainer', {
        chart: {
            type: 'packedbubble',
            height: '100%'
        },
        title: {
            text: '',
            align: 'center'
        },
        tooltip: {
            useHTML: true,
            pointFormat: '<b>{point.name}:</b> {point.value}'
        },
        plotOptions: {
            packedbubble: {
                minSize: '30%',
                maxSize: '120%',
                zMin: 0,
                zMax: 1000,
                layoutAlgorithm: {
                    splitSeries: false,
                    gravitationalConstant: 0.02
                },
                dataLabels: {
                    enabled: true,
                    format: '{point.name}',
                    filter: {
                        property: 'y',
                        operator: '>',
                        value: 250
                    },
                    style: {
                        color: 'black',
                        textOutline: 'none',
                        fontWeight: 'normal'
                    }
                }
            }
        },
        series: [{
            name: 'Category',
            data: topCategoryObject
        }],
        exporting: {
            enabled: false // Disable exporting module
        }
    })
}).catch(() => {
    alert('some error to fetch top category data')
})
// top category chart end

// brandlift bar chart start
function brandLiftChart(filter = '') {
    // Get the current date and format it as "YYYY-MM"
    const now = new Date();
    const currentMonth = `${now.getFullYear()}-${String(now.getMonth() + 1).padStart(2, '0')}`;

    // Construct the fetch URL dynamically
    let fetchUrl;
    if (filter) {
        // If 'filter' has a value, use it in the URL
        fetchUrl = `https://api.adlift.com/marketing/brandlift${filter}`;
    } else {
        // If 'filter' is empty, use the current month by default
        fetchUrl = `https://api.adlift.com/marketing/brandlift?month=${currentMonth}`;
    }

    fetch(fetchUrl).then(res => {
    // fetch(`https://api.adlift.com/marketing/brandlift${filter}`).then(res => {
        return res.json()
    }).then(response => {
        const nameArr = []
        const idValueArr = []
        for (let id in response) {
            nameArr.push(response[id].name)
            idValueArr.push(parseInt(response[id].total_ids))
        }
        Highcharts.chart('brandLiftChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: nameArr,
                crosshair: true,
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'No of Sites' // Label for the y-axis
                }
            },
            series: [{
                name: 'Team Members',
                data: idValueArr
            }],
            exporting: {
                enabled: false // Disable exporting module
            }
        })
    }).catch(() => {
        alert('some error to fetch brand lift data')
    })
}
// brandlift bar chart end 

// Aps chart start
function apsChart(filter = '') {
    fetch(`https://api.adlift.com//marketing/aps${filter}`).then(res => {
        return res.json()
    }).then(response => {
        const nameArr = []
        const idValueArr = []
        for (let id in response) {
            nameArr.push(response[id].da_range)
            idValueArr.push(parseInt(response[id].id_count))
        }
        Highcharts.chart('apsChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: nameArr,
                crosshair: true,
                title: {
                    text: 'DA Range' // Label for the x-axis
                },
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'Domain Score' // Label for the y-axis
                }
            },
            series: [{
                name: 'Adlift Score',
                data: idValueArr
            }],
            exporting: {
                enabled: false // Disable exporting module
            }
        })
    }).catch(() => {
        alert('some error to fetch brand lift data')
    })
}
// Aps chart end 

// Unique blog chart start
function uniqBlogChart(filter = '') {
    console.log(filter)
    fetch(`https://api.adlift.com/marketing/unique_charts/${filter}`).then(res => {
        return res.json()
    }).then(response => {
        let updateResponse = response.data
        const nameArr = []
        const totalDomainArr = []
        const uniqueDomainArr = []

        for (let id in updateResponse) {
            nameArr.push(updateResponse[id].full_name)
            totalDomainArr.push(parseInt(updateResponse[id].total_domain_count))
            uniqueDomainArr.push(parseInt(updateResponse[id].unique_domain_count))
        }
        Highcharts.chart('uniqueBlogChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: nameArr,
                crosshair: true,
                title: {
                    text: 'Team Members' // Label for the x-axis
                },
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'No. of links' // Label for the y-axis
                }
            },
            series: [{
                    name: 'Total Links',
                    data: totalDomainArr
                },
                {
                    name: 'Unique Links',
                    data: uniqueDomainArr
                }
            ],
            exporting: {
                enabled: false // Disable exporting module
            }

        })
    }).catch(() => {
        alert('some error to fetch unique blog data')
    })
}
// Unique blog chart end 

// Payment chart start
// function paymentChart(filter = '') {
//     fetch(`https://api.adlift.com/marketing/payment${filter}`).then(res => {
//         return res.json()
//     }).then(response => {
//         let updateResponse = response.data
//         const nameArr = []
//         const paidArr = []
//         const pendingArr = []
//         for (let id in updateResponse) {
//             nameArr.push(updateResponse[id].campaign_names)
//             paidArr.push(parseInt(updateResponse[id].Paid))
//             pendingArr.push(parseInt(updateResponse[id].Pending))
//         }
//         Highcharts.chart('paymentChartContainer', {
//             chart: {
//                 type: 'column'
//             },
//             title: {
//                 text: '',
//                 align: 'center'
//             },
//             xAxis: {
//                 categories: nameArr,
//                 crosshair: true,
//                 accessibility: {
//                     description: 'Payment'
//                 }
//             },
//             plotOptions: {
//                 column: {
//                     stacking: 'normal',
//                     dataLabels: {
//                         enabled: false // Disable data labels inside bars
//                     }
//                 }
//             },
//             series: [{
//                     name: 'Paid',
//                     data: paidArr,
//                     color: '#2CAFFE' // Light blue
//                 },
//                 {
//                     name: 'Pending',
//                     data: pendingArr,
//                     color: '#f08080' // Light red
//                 }
//             ],
//             exporting: {
//                 enabled: false // Disable exporting module
//             }

//         });

//     }).catch(() => {
//         alert('some error to fetch payment data')
//     })
// }
// Payment chart end 

// No of sites chart start
function noOfSite(filter = '') {
    // fetch(`https://api.adlift.com/marketing/links${filter}`).then(res => {
    // Get the current date and format it as "YYYY-MM"
    const now = new Date();
    const currentMonth = `${now.getFullYear()}-${String(now.getMonth() + 1).padStart(2, '0')}`;

    // Construct the fetch URL dynamically
    let fetchUrl;
    if (filter) {
        // If 'filter' has a value, use it in the URL
        fetchUrl = `https://api.adlift.com/marketing/links${filter}`;
    } else {
        // If 'filter' is empty, use the current month by default
        fetchUrl = `https://api.adlift.com/marketing/links?month=${currentMonth}`;
    }

    fetch(fetchUrl).then(res => {
        return res.json()
    }).then(response => {
        let updateResponse = response.data
        const nameArr = []
        const durationArr = []
        const uniqueDomainsArr = []
        for (let id in updateResponse) {
            nameArr.push(updateResponse[id].campaign_name)
            durationArr.push(parseInt(updateResponse[id].duration))
            uniqueDomainsArr.push(parseInt(updateResponse[id].unique_domains))
        }
        Highcharts.chart('noOfSiteChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: nameArr,
                crosshair: true,
                title: {
                    text: 'Campaigns' // Label for the x-axis
                },
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'No. of links' // Label for the y-axis
                }
            },
            series: [{
                    name: 'Total Links',
                    data: durationArr,
                    color: '#2CAFFE' // Light blue
                },
                {
                    name: 'No of Added Links',
                    data: uniqueDomainsArr,
                    color: '#544fc5' // Light red
                }
            ],
            exporting: {
                enabled: false // Disable exporting module
            }

        })
    }).catch(() => {
        alert('some error to fetch no of sites data')
    })
}
// No of sites chart end 

// Add a dropdown to the HTML for month selection
const dropdown = document.createElement('select');
dropdown.id = 'monthFilter';
dropdown.innerHTML = `
        <option value="2023-12">December 2023</option>
        <option value="2024-01">January 2024</option>
        <option value="2024-02">February 2024</option>
        <!-- Add more options as needed -->
    `;
// Amount chart start
// function amountChart(filter=''){ 
//     fetch(`https://api.adlift.com/marketing/amount_chart${filter}`).then(res=>{
//         return res.json()
//     }).then(response=>{  
//         let updateResponse = response.data   
//         const nameArr = [] 
//         const domainArr = []  
//         for(let id in updateResponse){ 
//             nameArr.push(updateResponse[id].name)
//             domainArr.push(parseInt(updateResponse[id].TotalAmount)) 
//         }  
//         Highcharts.chart('amountChartContainer', {
//             chart: {
//                 type: 'column'
//             },
//             title: {
//                 text: '',
//                 align: 'center'
//             }, 
//             xAxis: {
//                 categories: nameArr,
//                 crosshair: true,
//                 accessibility: {
//                     description: 'Countries'
//                 }
//             },  
//             series: [
//                 {
//                     name: 'Total Amount',
//                     data: domainArr
//                 }
//             ],
//             exporting: {
//                 enabled: false // Disable exporting module
//             }

//         })
//     }).catch(()=>{
//         alert('some error to fetch amount data')
//     })
// }
// Amount chart end 

async function amountChart(filter = '') {
    try {
        // Use async/await for cleaner async code
        const response = await fetch(`https://api.adlift.com/marketing/amount_chart${filter}`);
        const data = await response.json();

        // Destructure 'data' from the response for clarity
        const {
            data: chartData
        } = data;

        // Use map to transform data, more functional approach
        const categories = chartData.map(item => item.name);
        const amounts = chartData.map(item => parseInt(item.AverageAmount, 10));
        const total_links = chartData.map(item => parseInt(item.TotalLinks, 10));

        // Combine names, amounts, and total links into a single array of objects
        const seriesData = chartData.map(item => ({
            name: item.name, // Assuming 'name' is the category name
            y: parseInt(item.AverageAmount, 10), // 'y' is the value Highcharts uses for plotting
            totalLinks: parseInt(item.TotalLinks, 10) // Custom property to store total links
        }));
        // Initialize Highcharts
        Highcharts.chart('amountChartContainer', {
            chart: {
                type: 'column',
                zoomType: 'xy' // Enable zooming
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories,
                crosshair: true,
                accessibility: {
                    description: 'Names'
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Average Amount'
                },
                plotLines: [{ // Static line at 3000
                    value: 3000,
                    color: '#544fc5', // Color of the line
                    dashStyle: 'longdashdot', // Style of the line
                    width: 2, // Width of the line
                    label: {
                        text: '' // Label for the line
                    }
                }]
            },
            // tooltip: {
            //     shared: true,
            //     // Custom formatter for tooltip
            //     formatter: function() {
            //         // 'this' context contains all the information about the hovered point(s)
            //         let tooltipText = `<span style="font-size:10px">${this.x}</span><br/>`;
            //         this.points.forEach((point) => {
            //             const formattedAmount = point.y.toLocaleString('en-US');
            //             tooltipText += `<span style="color:${point.series.color}">\u25CF</span> Average Amount: <b>${formattedAmount}</b><br/>`;
            //             tooltipText += `<span style="color:${point.series.color}">\u25CF</span> No of Sites: <b>${point.point.total_links}</b><br/>`;
            //         });
            //         return tooltipText;
            //     }
            // },
            // plotOptions: {
            //     column: {
            //         stacking: 'normal',
            //         dataLabels: {
            //             enabled: false // Disable data labels inside bars
            //         }
            //     }
            // },
            tooltip: {
                shared: true,
                formatter: function() {
                    let tooltipText = `<span style="font-size:10px">${this.x}</span><br/>`;
                    this.points.forEach((point) => {
                        const formattedAmount = point.y.toLocaleString('en-US');
                        tooltipText += `<span style="color:${point.series.color}">\u25CF</span> Average Amount <b>${formattedAmount}</b><br/>`;
                        //tooltipText += `<span style="color:${point.series.color}">Total Links: <b>${point.point.totalLinks}</b><br/>`;
                    });
                    return tooltipText;
                }
            },
            series: [{
                name: 'Team Members',
                data: amounts,
                color: '#2CAFFE' // Light red
            }, ],
            exporting: {
                enabled: false
            }
        });

    } catch (error) {
        // Improved error handling
        console.error('Error fetching amount data:', error);
        alert('Some error occurred while fetching amount data.');
    }
}

// Approval chart start
fetch('https://api.adlift.com/marketing/pie_chart/').then(res => {
    return res.json()
}).then(response => {
    let updateResponse = response
    let data = {}
    const nameArr = []
    for (let id in updateResponse) {
        data = {
            name: updateResponse[id].status_label,
            y: updateResponse[id].count
        }
        nameArr.push(data)
    }

    Highcharts.chart('approvalChartContainer', {
    chart: {
        type: 'pie'
    },
    title: {
        text: '',
        align: 'center'
    },
    tooltip: {
        // Use a formatter function to customize the tooltip content
        formatter: function() {
            // Format this point's y-value (count) in US format with commas
            const formattedCount = this.point.y.toLocaleString('en-US');
            return `${this.point.name}: <b>${formattedCount}</b>`;
        }
    },
    series: [{
        name: 'Domain count',
        data: nameArr
    }],
    exporting: {
        enabled: false // Disable exporting module
    }
});
}).catch(() => {
    console.log('some error to fetch approval data')
})
// Approval chart end 

// Da chart start
function daChart(filter = '') {

    // Get the current date and format it as "YYYY-MM"
    const now = new Date();
    const currentMonth = `${now.getFullYear()}-${String(now.getMonth() + 1).padStart(2, '0')}`;

    // Construct the fetch URL dynamically
    let fetchUrl;
    if (filter) {
        // If 'filter' has a value, use it in the URL
        fetchUrl = `https://api.adlift.com/marketing/da_cost${filter}`;
    } else {
        // If 'filter' is empty, use the current month by default
        fetchUrl = `https://api.adlift.com/marketing/da_cost?month=${currentMonth}`;
    }

    // fetch(`https://api.adlift.com//marketing/da_cost${filter}`).then(res => {
        fetch(fetchUrl).then(res => {
        return res.json()
    }).then(response => {
        const updateResponse = response.data
        const amountArr = []
        const daArr = []
        for (let id in updateResponse) {
            amountArr.push(updateResponse[id].Count)
            daArr.push(updateResponse[id].Da)
        }
        Highcharts.chart('daChartContainer', {
            chart: {
                type: 'column'
            },
            title: {
                text: '',
                align: 'center'
            },
            xAxis: {
                categories: daArr,
                crosshair: true,
                accessibility: {
                    description: 'Countries'
                }
            },
            yAxis: {
                title: {
                    text: 'No of Sites' // Label for the y-axis
                }
            },
            tooltip: {
                // Use a formatter function to customize the tooltip content
                formatter: function() {
                    // Format this.y (the count for the hovered column) in US format with commas
                    const formattedCount = this.y.toLocaleString('en-US');
                    return `No of Sites: <b>${formattedCount}</b>`;
                }
            },
            series: [{
                name: 'DA Range',
                data: amountArr
            }],
            exporting: {
                enabled: false // Disable exporting module
            }
        })
    }).catch(() => {
        alert('some error to fetch DA data')
    })
}
// Da chart end 


$(document).ready(function() {
    // Initialize Datepicker
    $('#datepicker').datepicker({
        format: 'yyyy-mm', // Specify the date format
        startView: 'months', // Show the month view by default
        minViewMode: 'months', // Allow selection of only months
        autoclose: true, // Close the datepicker when a date is selected
        todayHighlight: true, // Highlight today's date
        clearBtn: true, // Show a button to clear the selected date
        orientation: "bottom auto", // Position the datepicker
        templates: {
            leftArrow: '<i class="fa fa-chevron-left"></i>',
            rightArrow: '<i class="fa fa-chevron-right"></i>'
        }
    });
});
</script>

<body>
    <div class="header_container">
        <div class="cards_Container">
            <div class="row">
                <div class="col-lg-4 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Premium Sites</h5>
                            <h5 class="card_value total_premium_sites">00</h5>

                        </div>
                        <div>
                            <img src="Assets/Icons/premium.svg" class="dashboardWigetImg" alt="">
                        </div>

                    </a>
                </div>

                <div class="col-lg-4 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Avearge DA</h5>
                            <h5 class="card_value total_da">00</h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/Avreg.svg" class="dashboardWigetImg" alt="">
                        </div>

                    </a>
                </div>
                <div class="col-lg-4 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Total Traffic (All Sites)</h5>
                            <h5 class="card_value total_traffic">00</h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/TotalTrafic.svg" class="dashboardWigetImg" alt="">
                        </div>

                    </a>
                </div>
                <div class="col-lg-4 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Publications till date</h5>
                            <h5 class="card_value total_websites">00</h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/publications.svg" class="dashboardWigetImg" alt="">
                        </div>

                    </a>
                </div>

                <div class="col-lg-4 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Unique Sites</h5>
                            <h5 class="card_value total_unique_sites">00</h5>
                        </div>
                        <div>
                            <img src="Assets/Icons/siteUnique.svg" class="dashboardWigetImg" alt="">
                        </div>

                    </a>
                </div>

                <div class="col-lg-4 my-2">
                    <a href="#" class="dash_card">
                        <div>
                            <h5 class="card_heading">Total Campaigns</h5>
                            <h5 class="card_value total_campaigns">00</h5>

                        </div>
                        <div>
                            <img src="Assets/Icons/averag.svg" class="dashboardWigetImg" alt="">
                        </div>

                    </a>
                </div>
            </div>
        </div>
    </div>



    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="chart_title">
                        Top Categories
                    </h4>
                    <!-- <h4 class="card_view">View Report ></h4> -->
                <!-- Currency Dropdown and Datepicker on the Right Side -->
                <div class="d-flex align-items-center">
                    <!-- Datepicker Input Field -->
                    <div class="form-group mb-0">
                        <label for="datepicker" class="sr-only">Select Date</label>
                        <input type="month" class="form-control total-category-filter" value="<?= date('Y-m')?>" id="datepicker12">
                    </div>
                </div>
                </div>
                <div class="pieChart_Card">
                    <div id="categoryChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
    

        <!-- Payment Chart  -->
        <!-- <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        <span class="icon"><img src="Assets/Icons/Wallet.svg">  </span> Payments
                    </h4>
                    <div class="d-flex align-items-center">
                    <div class="form-group mb-0 mr-2">
                        <label for="datepicker" class="sr-only">Start Date</label>
                            <input type="date" class="form-control" id="payment-start-date" placeholder="Start Date" onChange="document.getElementById('payment-end-date').value='' ">
                        </div>
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">End Date</label>
                            <input type="date" class="form-control" id="payment-end-date" placeholder="End Date">
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card"> 
                    <div id="paymentChartContainer"></div>
                </div>
            </div>
        </div> -->

        <!-- End Payment Chart  -->






        <!-- <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        No. of Sites
                    </h4>

                </div>
                <div class="pieChart_Card">
                <h4>Chart Img</h4>
                    <div id="noOfSiteChartContainer"></div> 
                </div>
            </div>
        </div> -->


    <div class="header_container">
        <div class="chartContainerTwo">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Cost Bifurcation
                    </h4>
                    <div class="d-flex align-items-center">
                        <div class="form-group1 mb-0 mr-2">
                            <label for="currencyPicker" class="sr-only">Select Currency</label>
                            <select class="form-control" id="site-cost-select-currency">
                                <option value="dollars">USD ($)</option>
                                <option value="rupees">INR (₹)</option>
                            </select>
                        </div>
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">Select Date</label>
                            <input type="month" class="form-control" id="site-cost-select-month" placeholder="Select Month" value="<?= date('Y-m')?>">
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card">
                    <div id="numberOfSiteByCostChartContainer"></div>
                </div>
            </div>
        </div>
        <div class="chartContainerThree">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Top Categories
                    </h4>
                    <h4 class="card_view">View Report ></h4>

                </div>
                <div class="pieChart_Card">
                    <div id="topCategoryChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        DA Bifurcation
                    </h4>
                    <!-- Datepicker on the Right Side -->
                    <div class="d-flex align-items-center">
                        <!-- Datepicker Input Field -->
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">Select Date</label>
                            <input type="month" class="form-control" id="da-select-month" placeholder="Select Month" value="<?= date('Y-m')?>">
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card">
                    <div id="daChartContainer"></div>
                </div>
            </div>
        </div>
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        ADS
                    </h4>
                    <!-- <h4 class="card_view">View Report ></h4> -->
                    <div class="d-flex align-items-center">
                        <!-- Currency Dropdown -->
                        <div class="form-group mb-0 mr-2">
                            <label for="categoryPicker" class="sr-only">Select Category</label>
                            <select class="form-control" id="aps-select-category">
                                <option value="">Select Category</option>
                                <option value="Finance">Finance</option>
                                <option value="Technology">Technology</option>
                                <option value="Lifestyle">Lifestyle</option>
                                <option value="Education">Education</option>
                                <option value="Automobile">Automobile</option>
                                <option value="Business">Business</option>
                                <option value="Real estate">Real estate</option>
                                <option value="Digital marketing">Digital marketing</option>
                                <option value="General">General</option>
                                <option value="Brandlift">Brandlift</option>
                                <option value="Tech">Tech</option>
                                <option value="Finance ">Finance </option>
                                <option value="Health">Health</option>
                                <option value="Logistic">Logistic</option>
                                <option value="Tech and games">Tech and games</option>
                                <option value="Fashion">Fashion</option>
                                <option value="Logistics">Logistics</option>
                                <option value="Realstate">Realstate</option>
                                <option value="Game">Game</option>
                                <option value="Gaming">Gaming</option>
                                <option value="Technology,">Technology,</option>
                                <option value="Lifestyle,tech">Lifestyle,tech</option>
                                <option value="News">News</option>
                                <option value="Medical">Medical</option>
                                <option value="Medicial">Medicial</option>
                                <option value="Architect">Architect</option>
                                <option value="Multi">Multi</option>
                                <option value="Tecnology">Tecnology</option>
                                <option value="Games">Games</option>
                                <option value="Hotel">Hotel</option>
                                <option value="Real-estate">Real-estate</option>
                                <option value="Generic">Generic</option>
                            </select>
                        </div>

                        <!-- Datepicker Input Field -->
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">Select Date</label>
                            <input type="month" class="form-control" id="aps-select-month" placeholder="Select Month" value="<?= date('Y-m')?>">
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card">
                    <div id="apsChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_container">
        <div class="chartContainerThree">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Approval Listing
                    </h4>
                </div>
                <div class="pieChart_Card">
                    <div id="approvalChartContainer"></div>
                </div>
            </div>
        </div>
        <div class="chartContainerTwo">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        BrandLift Sites
                    </h4>
                    <!-- <h4 class="card_view">View Report ></h4> -->
                    <!-- Currency Dropdown and Datepicker on the Right Side -->
                    <div class="d-flex align-items-center">
                        <!-- Datepicker Input Field -->
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">Select Date</label>
                            <input type="month" class="form-control" id="brandLift-select-month"
                                placeholder="Select Month" value="<?= date('Y-m')?>">
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card">
                    <!-- <h4>Chart Img</h4> -->
                    <div id="brandLiftChartContainer"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- 
    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        Unique Blog Chart
                    </h4>

                </div>
                <div class="pieChart_Card">
                    <div id="uniqueBlogChartContainer"></div>
                </div>
            </div>
        </div>
    </div>
     -->

    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">Unique Blogs Per User</h4>
                    <div class="d-flex align-items-center">
                        <div class="form-group mb-0 mr-2">
                            <label for="blog-campaign" class="sr-only">Select Campaign</label>
                            <select class="form-control" id="blog-campaign" placeholder="Select Campaign">
                                <!-- Campaign options will be populated dynamically -->
                            </select>
                        </div>

                        <!-- Currency Dropdown -->
                        <div class="form-group mb-0 mr-2">
                            <label for="datepicker" class="sr-only">Start Date</label>
                            <input type="date" class="form-control" id="blog-start-date" onChange="document.getElementById('blog-end-date').value='' " placeholder="From Date"  >
                        </div>
                        <!-- Datepicker Input Field -->
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">End Date</label>
                            <input type="date" class="form-control" id="blog-end-date" placeholder="To Date"  >
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card">
                    <div id="uniqueBlogChartContainer"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading">
                    <h4 class="tableMainTitle">
                        No. of Sites
                    </h4>
                <div class="form-group mb-0">
                    <label for="currencyPicker" class="sr-only">Select Currency</label>
                    <select class="form-control" id="currencyPicker">
                        <option value="$">USD ($)</option>
                        <option value="£">GBP (£)</option>
                        <option value="€">EUR (€)</option>
                        <option value="₹">INR (₹)</option>
                    </select>
                </div>
                    <div class="form-group mb-0">
                        <label for="datepicker" class="sr-only">Select Date</label>
                        <input type="text" class="form-control" id="datepicker1" placeholder="Select Month">
                    </div>
                </div>
                <div class="pieChart_Card"> 
                    <div id="noOfSiteChartContainer"></div> 
                </div>
            </div>
        </div>
    </div> -->
    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading d-flex justify-content-between align-items-center">
                    <!-- Title on the Left Side -->
                    <h4 class="tableMainTitle">
                        No. of Sites by Campaigns
                    </h4>
                    <!-- Currency Dropdown and Datepicker on the Right Side -->
                    <div class="d-flex align-items-center">

                        <!-- Datepicker Input Field -->
                        <div class="form-group mb-0">
                            <label for="datepicker" class="sr-only">Select Date</label>
                            <input type="month" class="form-control" id="site-select-month" placeholder="Select Month" value="<?= date('Y-m')?>">
                        </div>
                    </div>
                </div>
                <div class="pieChart_Card">
                    <div id="noOfSiteChartContainer"></div>
                </div>
            </div>
        </div>
    </div>



    <div class="header_container">
        <div class="chartContainer">
            <div class="chartCard my-2">
                <div class="chartHeading d-flex justify-content-between align-items-center">
                    <h4 class="tableMainTitle">
                        Average Amount By User
                    </h4>
                    <!-- Datepicker Input Field -->
                    <div class="form-group mb-0">
                        <label for="datepicker" class="sr-only">Select Date</label>
                        <input type="month" class="form-control" id="amount-select-month" placeholder="Select Month" value="<?= date('Y-m')?>">
                    </div>
                </div>

                <div class="pieChart_Card">
                    <div id="amountChartContainer"></div>
                </div>
            </div>
        </div>
    </div>


</body>
<script>
$(document).ready(function() {
    // AJAX call to fetch campaign data
    $.ajax({
        url: 'https://api.adlift.com/marketing/campaigns_list/',
        method: 'GET',
        success: function(response) {
            // Populate the dropdown with campaign options
            var campaigns = response.data;
            var campaignFilter = $('#blog-campaign');
            $.each(campaigns, function(index, campaign) {
                campaignFilter.append($('<option>', {
                    value: campaign.Campaign_id,
                    text: campaign
                        .Campaign_name // Display campaign name in the dropdown
                }));
            });
        },
        error: function(xhr, status, error) {
            console.error('Failed to fetch campaigns:', error);
        }
    });
});

// Filter start
categoryChart()
// paymentChart()
noOfSite()
noOfSiteCost()
amountChart()
brandLiftChart()
daChart()
apsChart()
uniqBlogChart()



document.getElementById('datepicker12').addEventListener('change', (e) => {
    categoryChart(`?month=${e.target.value}`)
})
// document.getElementById('payment-end-date').addEventListener('change', (e) => {
//     const paymentStartDate = $('#payment-start-date').val()
//     if (paymentStartDate != undefined) {
//         paymentChart(`?end_date=${e.target.value}&start_date=${paymentStartDate}`)
//     }
// })
document.getElementById('site-select-month').addEventListener('change', (e) => {
    const selectCurrency = $('#site-select-currency').val()
    if (selectCurrency != undefined && selectCurrency != '') {
        noOfSite(`?month=${e.target.value}&currency=${selectCurrency}`)
    } else {
        noOfSite(`?month=${e.target.value}`)
    }
})
// document.getElementById('site-select-currency').addEventListener('change',(e)=>{
//     const selectMonth = $('#site-select-month').val()
//     if(selectMonth != undefined && selectMonth != ''){
//         noOfSite(`?currency=${e.target.value}&month=${selectMonth}`)
//     }else{
//         noOfSite(`?currency=${e.target.value}`)
//     }
// })
document.getElementById('site-cost-select-month').addEventListener('change', (e) => {
    const selectCurrency = $('#site-cost-select-currency').val()
    if (selectCurrency != undefined && selectCurrency != '') {
        noOfSiteCost(`?month=${e.target.value}&currency=${selectCurrency}`)
    } else {
        noOfSiteCost(`?month=${e.target.value}`)
    }
})
document.getElementById('site-cost-select-currency').addEventListener('change', (e) => {
    const selectMonth = $('#site-cost-select-month').val()
    if (selectMonth != undefined && selectMonth != '') {
        noOfSiteCost(`?currency=${e.target.value}&month=${selectMonth}`)
    } else {
        noOfSiteCost(`?currency=${e.target.value}`)
    }
})
document.getElementById('da-select-month').addEventListener('change', (e) => {
    const selectCurrency = $('#da-select-currency').val()
    if (selectCurrency != undefined) {
        daChart(`?month=${e.target.value}&currency=${selectCurrency}`)
    } else {
        daChart(`?month=${e.target.value}`)
    }
})
// document.getElementById('da-select-currency').addEventListener('change', (e) => {
//     const selectMonth = $('#da-select-month').val()
//     if (selectMonth != undefined && selectMonth != '') {
//         daChart(`?currency=${e.target.value}&month=${selectMonth}`)
//     } else {
//         daChart(`?currency=${e.target.value}`)
//     }
// })
document.getElementById('aps-select-month').addEventListener('change', (e) => {
    const selectCategory = $('#aps-select-category').val()
    if (selectCategory != undefined && selectCategory != '') {
        apsChart(`?month=${e.target.value}&category=${selectCategory.toLowerCase()}`)
    } else {
        apsChart(`?month=${e.target.value}`)
    }
})
document.getElementById('aps-select-category').addEventListener('change', (e) => {
    const selectMonth = $('#aps-select-month').val()
    if (selectMonth != undefined && selectMonth != '') {
        apsChart(`?category=${e.target.value.toLowerCase()}&month=${selectMonth}`)
    } else {
        apsChart(`?category=${e.target.value.toLowerCase()}`)
    }
})
document.getElementById('amount-select-month').addEventListener('change', (e) => {
    amountChart(`?month=${e.target.value}`)
})
document.getElementById('brandLift-select-month').addEventListener('change', (e) => {
    brandLiftChart(`?month=${e.target.value}`)
})

document.getElementById('blog-end-date').addEventListener('change', (e) => {
    const blogStartDate = $('#blog-start-date').val()
    const campaignID = $('#blog-campaign').val()
    if (campaignID != undefined && campaignID != '') {
        uniqBlogChart(`?end_date=${e.target.value}&start_date=${blogStartDate}&campaign_id=${campaignID}`)
    } else {
        uniqBlogChart(`?end_date=${e.target.value}&start_date=${blogStartDate}`)
    }
})
document.getElementById('blog-campaign').addEventListener('change', (e) => {
    const blogStartDate = $('#blog-start-date').val()
    const blogEndDate = $('#blog-end-date').val()
    if (blogEndDate != undefined && blogEndDate != '') {
        uniqBlogChart(`?end_date=${blogEndDate}&start_date=${blogStartDate}&campaign_id=${e.target.value}`)
    } else {
        uniqBlogChart(`?campaign_id=${e.target.value}`)
    }
})



// Filter end
</script>

</html>