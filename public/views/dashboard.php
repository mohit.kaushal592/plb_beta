<?php $approvalCount = $viewData->get('approvalCount') ?>
       <div class="quick-actions_homepage"> 
      <ul class="quick-actions">
        <li class="bg_lb"> <a href="index.php"> <i class="icon-dashboard"></i> Dashboard </a> </li>
        <li class="bg_lg span3"> <a href="charts.php?act=approval"> <i class="icon-signal"></i> Charts &amp; graphs</a> </li>
        <li class="bg_ly"> <a href="approvals.php<?php echo $session->read('User.user_type')=='user' ? '?_uid='. base64_encode($session->read('User.id')) : '' ?>"> <i class="icon-inbox"></i>Approval Listing </a><span class="label label-success" id="_goToApprovalList" style="cursor: pointer;" data-href="approvals.php?_st=<?php echo base64_encode(-1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>"><?php echo $approvalCount['pending'] ?></span> </li>
        <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
	<li class="bg_lo"> <a href="users.php"> <i class="icon-th"></i> User Listing</a> </li>
	<li class="bg_ls"> <a href="javascript:void(0)" class="updateApprovalLinkPr"> <i class="icon-tint"></i> Update Domain PR</a> </li>
	<?php endif ?>
      </ul>
    </div>

    <div class="row-fluid">
      <div class="span<?php echo $session->read('User.user_type')=='user' ? 4 : 6 ?>">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-check"></i> </span>
            <h5>Approval  Listing</h5>
          </div>
          <div class="widget-content">
            <div class="pie" id="ApprovalPieGraph"></div>
          </div>
        </div>
      </div>
      <div class="span<?php echo $session->read('User.user_type')=='user' ? 5 : 6 ?>">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-money"></i> </span>
            <h5>Payment</h5>
          </div>
          <div class="widget-content">
            <div class="bars graph" id="ApprovalBarGraph"></div>
          </div>
        </div>
      </div>
      <?php if($session->read('User.user_type')=='user'): ?>
      <div class="span3">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-check"></i> </span>
            <h5>Approval  Links</h5>
          </div>
          <div class="widget-content">
            <ul class="site-stats">
	      <li class="bg_ly" onclick="window.location='approvals.php?<?php echo $session->read('User.user_type')=='user' ? '_uid='. base64_encode($session->read('User.id')) : '' ?>';">
	      <i class="icon-list"></i> <strong><?php echo $approvalCount['total'] ?></strong> <small>Total Links</small>
	      </li>
	      <li class="bg_lg" onclick="window.location='approvals.php?_st=<?php echo base64_encode(1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
		<i class="icon-ok"></i> <strong><?php echo $approvalCount['approved'] ?></strong> <small>Approved</small>
	      </li>
	      <li class="bg_lo" onclick="window.location='approvals.php?_st=<?php echo base64_encode(0) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
		<i class="icon-ban-circle"></i> <strong><?php echo $approvalCount['rejected'] ?></strong> <small>Rejected</small>
	      </li>
	      <li class="bg_lh" onclick="window.location='approvals.php?_st=<?php echo base64_encode(-1) ?><?php echo $session->read('User.user_type')=='user' ? '&_uid='. base64_encode($session->read('User.id')) : '' ?>';">
		<i class="icon-ok"></i> <strong><?php echo $approvalCount['pending'] ?></strong> <small>Pending</small>
	      </li>
	    </ul>
          </div>
        </div>
      </div>
      <?php endif ?>
    </div>
	
	    <hr>
    <div class="row-fluid">
      <div class="span<?php echo $session->read('User.user_type')=='user' ? 7 : 12 ?>">
        <div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Approval Listing</h5>
          </div>
          <div class="widget-content nopadding" id="LatestApprovalList">
            <p> &nbsp; No record found</p>
          </div>
        </div>
        
      </div>
      <?php if($session->read('User.user_type')=='user'): ?>
      <div class="span5">
        <div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Links Submission</h5>
          </div>
          <div class="widget-content nopadding" id="LinkSubmissionList">
            <p> &nbsp; No record found</p>
          </div>
        </div>
        
      </div>
      <?php endif ?>
    </div>
<?php $viewData->scripts(array('js/jquery.flot.min.js','js/jquery.flot.pie.min.js','js/charts.js'), array('inline'=>false)) ?>
<?php $viewData->scriptStart() ?>
$(document).ready(function(){
  var LatestApprovalList = function(){
    var $where = $('#LatestApprovalList');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=approval_list', function(data){
      $where.html(data);
    }, 'html').fail(function(){
      $where.html("<div>Error occurred in request processing</div>");
    });
  }
  LatestApprovalList();
  <?php if($session->read('User.user_type')=='user'): ?>
  var LinkSubmissionList = function(){
    var $where = $('#LinkSubmissionList');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=link_submission', function(data){
      $where.html(data);
    }, 'html').fail(function(){
      $where.html("<div>Error occurred in request processing</div>");
    });
  }
  LinkSubmissionList()
  <?php endif ?>
  
  
  var ApprovalPieGraph = function(){
    var $where = $('#ApprovalPieGraph');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=pie_graph_approval', function(jsonData){
      var data = []
      $.each(jsonData, function(k, v){
	data.push({ label: k, data: v });
      });
      if(data.length>0){
	renderPieGraph(data, $where);
      }else{
	$where.html("<div>Record not found oto render graph.</div>");
      }
    }, 'json').fail(function(){
      $where.html("<div>Error occurred in request processing</div>");
    });
  }
  ApprovalPieGraph();
  
  var ApprovalBarGraph = function(){
    var $where = $('#ApprovalBarGraph');
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $where.html(loader);
    $.get('index.php?act=bar_graph_payment', function(jsonData){
      var dataDone = [];
      var dataPending = [];
      var xLabel = [];
      var data = [];
      var cnt = 0;
      $.each(jsonData, function(k, v){
	  if(v.done){
	    dataDone.push([cnt, v.done]);
	    xLabel.push([cnt, k]);
	    cnt = cnt+1;
	  }
	  if(v.pending){
	    dataPending.push([cnt, v.pending]);
	    xLabel.push([cnt, k]);
	    cnt = cnt+1;
	  }
      });
      data.push({ label: 'Done', data: dataDone});
      data.push({ label: 'Pending', data: dataPending});
      if(data.length>0){ 
	renderBarGraph({'data': data, 'where': $where, 'axisLabel' : {'x': 'Campaigns', 'y': 'Links'}, 'x_ticks': {'label':xLabel, hide:true}, 'tooltip': true});
      }else{
	$where.html("<div>Record not found oto render graph.</div>");
      }
    }, 'json').fail(function(){
      $where.html("<div>Error occurred in request processing</div>");
    });
  }
  ApprovalBarGraph();
  
  $('#_goToApprovalList').on('click', function(){
    var url = $(this).attr('data-href');
    if(url.length>0){
      window.location = url;
    }
  });
});
<?php $viewData->scriptEnd() ?>