<form action="submitted_links.php?act=do_action" method="post" id="PaymentListForm">
<input type="hidden" name="whatDo" id="PaymentWhatDo" />
 
<table class="table table-bordered table-striped tbl-resize sortable_tbl">
 <thead>
   <tr>
       <?php if(canUserDoThis(array('payment','link_delete'))): ?>
        <th style="width: 15px;"><span style="float:left"><input type="checkbox" id="checkAllPayment"/></span></th>
       <?php endif ?>
       <th>Campaign</th>
       <th>Domain</th>
       <th>Url</th>
       <th>Webmaster</th>
       <th>Payment Type</th>
       <th>Anchor</th>
       <th>Amount</th>
       <th>User</th>
       <th>Comment</th>
       <th style="width:70px;">Submitted On / Paid On</th>
       <th style="width:70px;">Response Status</th>
       <th style="width:70px;">Index Status</th>
       <th style="width:70px;">Traffic</th>
       <th style="width:70px;">Category</th>
       <th style="width:90px;">Pay for Mon / Yr</th>
       <th style="width:90px;">Invoice Payment</th>
       <th style="width:100px;"> <span style="float:left"><input type="checkbox" id="checkAllPaymentAccepted"  /></span>
          <span style="float:left"><input type="checkbox" id="checkAllPaymentReceted" style="opacity: 1;"/></span>Status</th>
       <th>Action</th>
   </tr>
 </thead>
 <tbody> 
   <?php $submittedLinks = $viewData->get('submittedLinks'); ?>
   <?php //echo "<pre>"; print_r($submittedLinks); ?>
  <?php if(!empty($submittedLinks)): ?>
   <?php foreach($submittedLinks as $submittedLink): ?>
   <tr class="odd gradeX">
       <?php if(canUserDoThis(array('payment','link_delete'))): ?>
       <td>
       <?php if(canDeleteLink($submittedLink['Payment']['status'], $submittedLink['Payment']['user_id'])): ?> 
        <input type="checkbox" id="PaymentId<?php echo $submittedLink['Payment']['id'] ?>" class="_payment_id" name="data[Payment][id][]" value="<?php echo $submittedLink['Payment']['id'] ?>" />
       <?php endif ?>
       </td>
       <?php endif ?>
       <td><?php echo $submittedLink['Payment']['campaign_name'] ?></td>
       <td style="color:#DA542E;">
        <a href="<?php echo $submittedLink['Payment']['domain'] ?>" target="_blank"><?php echo $submittedLink['Payment']['domain'] ?></a> <br />
        <?php echo $submittedLink['Payment']['ip'] ?> <br />
        DA:  <?php echo round($submittedLink['Payment']['da_value'], 2) ?> <br />
        DR: <?php echo round($submittedLink['Payment']['pr_value'],2) ?>
       </td>
       <td><a href="<?php echo $submittedLink['Payment']['url'] ?>" target="_blank"><?php echo $submittedLink['Payment']['url'] ?></a></td>
       <td><?php echo $submittedLink['Payment']['client_mail'] ?></td>
       <td><?php if($submittedLink['Payment']['payment_type']!='NEFT')echo $submittedLink['Payment']['paypal_email'];else echo 'NEFT'; ?></td>
       <td><?php $anchors = explode('[', $submittedLink['Payment']['anchor_text']);
        if(!empty($anchors)):
         $anchAr = array();
         foreach($anchors as $anchr):
          $anchAr[] = "<li>$anchr</li>";
         endforeach;
         
         echo !empty($anchAr) ? "<ol>". join('', $anchAr)."</ol>" : null;
        endif;
       ?></td>
       <td><?php if($submittedLink['Payment']['payment_type']!='NEFT')
	   {		   echo currency_format($submittedLink['Payment']['amount'], $submittedLink['Payment']['currency']) ;
	   }
	   else
	   {
		    echo 'Gross Amount: '.currency_format($submittedLink['Payment']['amount'], $submittedLink['Payment']['currency']) ;
			echo "<br/>";
			 echo 'TDS: '.currency_format($submittedLink['Payment']['tdsamount'], $submittedLink['Payment']['currency']) ;
			 echo "<br/>";
			  echo 'Net Amount: '.currency_format($submittedLink['Payment']['grossamount'], $submittedLink['Payment']['currency']) ;
		   
	   }?></td>
       <td><?php echo ucwords($submittedLink['User']['first_name'].' '.$submittedLink['User']['last_name']) ?></td>
       <td><?php echo $submittedLink['Payment']['narration_text'] ?></td>
       <td><?php echo date_to_text($submittedLink['Payment']['added_on']) ?>/<?php echo ($submittedLink['Payment']['payment_date']!= '0000-00-00') ? date_to_text($submittedLink['Payment']['payment_date']) : '' ?></td>

       <td><?php 
            $responseStatus = $submittedLink['Payment']['response_status'];
            $responseTime = $submittedLink['Payment']['response_time'];

            if ($responseStatus === '200') {
                echo '<span style="color: green;">'.$responseStatus.'</span>';
            } else {
                echo '<span style="color: red;">'.$responseStatus.'</span>';
            }
        ?> <br /> <?php echo $responseTime ?></td>

       <td> 
            <?php 
                $status = $submittedLink['Payment']['index_status'];
                $indexCount = $submittedLink['Payment']['index_count'];
                
          //       if ($status === 'Index') {
          //         echo '<button style="background-color: lightgreen; border: none; padding: 3px 8px; border-radius: 20px; display: block;">'.$status.'</button>';
          //     } else {
          //         echo '<button style="background-color: lightcoral; border: none; padding: 3px 8px; border-radius: 20px; display: block;">'.$status.'</button>';
          //     }
          if ($status === 'Index') {
            echo '<span style="color: green;">'.$status.'</span>';
              } else {
                  echo '<span style="color: red;">'.$status.'</span>';
              }
          ?><br>
         <?php echo $indexCount; ?>
        </td>
        <td>
            <?php echo $submittedLink['Payment']['traffic']; ?>
        </td>
        <td>
            <?php echo $submittedLink['Payment']['category']; ?>
        </td>
       <td><?php echo $submittedLink['Payment']['payment_month'].' / '.$submittedLink['Payment']['payment_year']; ?></td>
       <td>
          <?php
              if($submittedLink['Payment']['invoice_month'] != ''){ 
                $invoice_month = explode('-',$submittedLink['Payment']['invoice_month']); 
                echo $invoice_month[0].' / '.$invoice_month[1]; 
              }
          ?>
      </td>
       <td><?php
       $iconStatus = $submittedLink['Payment']['status'] == 'Paid' ? 1 : ($submittedLink['Payment']['status'] == 'Underprocess'? -1 : 0) ;
       
       $_cmpHtml = "";
       if($iconStatus=='-1'){
            if(canUserDoThis('payment')):
             $_cmpHtml .= "<span>";
             $_cmpHtml .= '<input type="checkbox" id="PaymentAccept'. $submittedLink['Payment']['id'].'_0" class="_payment_accept" name="data[Payment][accept][]" value="'.$submittedLink['Payment']['id'].'" title="Paid"/> ';
             $_cmpHtml .= '<input type="checkbox" id="PaymentReject'. $submittedLink['Payment']['id'].'_1" class="_payment_reject" name="data[Payment][reject][]" value="'.$submittedLink['Payment']['id'].'"  title="Rejected"/> ';
             $_cmpHtml .= '</span> ';
            endif;			 
       }
       if(!empty($_cmpHtml)){
        echo "<ul><li>".$_cmpHtml."</li></ul>";
       }else{
        echo getIconHtml($iconStatus, array('1'=>'Paid', '-1'=>'Underprocess'));
       }
       
       ?></td>
       <td>
        <?php if($iconStatus == -1 and canUserDoThis('payment_edit', array('user_id'=>$submittedLink['User']['id']))): ?>
        <a href="submitted_links.php?act=edit&lid=<?php echo $submittedLink['Payment']['id'] ?>" title="Click to edit link." class="tip-left"><i class="icon-edit icon-large"></i> &nbsp;</a>
        <?php endif ?>
		<a target="_blank" href="submitted_links.php?act=view&lid=<?php echo $submittedLink['Payment']['id'] ?>"  class="tip-left"><i class="icon-circle icon-large"></i> &nbsp;</a>
		<?php if($submittedLink['Payment']['invoice_file']<>""){?>
		<a href="http://beta.adlift.com/plb/uploads/invoice/<?php echo $submittedLink['Payment']['invoice_file']?>" target="_blank" title="Click to View  Invoice"> <i class="icon-cog icon-large"></i> </a>
		<?php
		}?>
       </td>
   </tr>
   <?php endforeach ?>
   <?php else: ?>
   <tr><td colspan="9" class="no-record-found">No records found.</td></tr>
   <?php endif ?>
 </tbody>
</table>
</form>
<?php //echo "<pre>"; print_r($_SESSION['response_status_value']); die('1__'); ?>
<script>
$(document).ready(function(){ 
    let arr_id_selected = []
    $(document).on('click', '#checkAllPayment' ,function(){  
        $("._payment_id").each(function(k,data) { 
             arr_id_selected.push(data.value);
        });
    })
    $('._payment_id').on('change',(e)=>{   
        arr_id_selected.push(e.target.value);
    }) 
    $('#updateResponse').on('click',(e)=>{
        let is_response_selected = ($('#FilterResponseUpdate').is(":checked"))?'r_status':''
        let is_index_selected = ($('#FilterIndexUpdate').is(":checked"))?'i_status':''
        var formData = {
            domain_wise_id: JSON.stringify(arr_id_selected),
            status_type:[is_response_selected,is_index_selected]
        }; 
        $.post('submitted_links.php?act=updateDomain', formData, function(response){
            $('#customUploadBlock').hide(); 
            alert('Message: ' + response.message.data);
        },'json').fail(function(jqXHR, textStatus, errorThrown){ 
            $('#customUploadBlock').hide();
            alert('Error: ' + JSON.parse(jqXHR.responseText).message);
        });
        $('#FilterResponseUpdate').parent().removeClass('checked'); 
        $('#FilterIndexUpdate').parent().removeClass('checked'); 
    })
});
</script>
<?php if(isset($_SESSION['response_status_value'])){ ?>
  <!-- <script> 
    var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
    $('#customUploadBlock').html(loader);
    var formData = {
        domain_wise_id: <?= json_encode($getDomainWiseIDArr); ?>
    }; 
    $.post('submitted_links.php?act=upateDomain', formData, function(response){
        $('#customUploadBlock').hide(); 
        alert('Success: ' + response.message);
    },'json').fail(function(jqXHR, textStatus, errorThrown){ 
        $('#customUploadBlock').hide();
        alert('Error: ' + JSON.parse(jqXHR.responseText).message);
    });
  </script> -->
<?php } ?>