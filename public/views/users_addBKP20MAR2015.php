<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
 <?php echo output_message($session->message()) ?>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add User</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="users.php?act=add" class="form-horizontal"  method="post" id="AddUserForm" enctype="multipart/form-data" >
            <div class="control-group">
              <label class="control-label" for="UserFirstName">First Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="First Name" name="data[User][first_name]" id="UserFirstName" minlength="2" value="<?php echo htmlentities($_POST[data][User][first_name]) ?>" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserLastName">Last Name :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Last Name" name="data[User][last_name]"  value="<?php echo htmlentities($_POST[data][User][last_name]) ?>" id="UserLastName" />
              </div>
            </div>
	    <!--<div class="control-group">
              <label class="control-label" for="UserUsername">Username :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Username" name="data[User][username]" id="UserUsername"  value="<?php echo htmlentities($_POST[data][User][username]) ?>" required/>
              </div>
            </div>-->
	    <div class="control-group">
              <label class="control-label" for="UserEmailId">Email :</label>
              <div class="controls">
                <input type="email" class="span11" placeholder="Email Address" name="data[User][email_id]" id="UserEmailId" value="<?php echo htmlentities($_POST[data][User][email_id]) ?>" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserPassword">Password :</label>
              <div class="controls">
                <input type="password" class="span11" placeholder="Password" name="data[User][password]" id="UserPassword" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserRetypePassword">Retype Password :</label>
              <div class="controls">
                <input type="password" class="span11" placeholder="Retype Password" name="data[User][re_password]" id="UserRetypePassword" equalTo="#UserPassword" required/>
              </div>
            </div>

	    <div class="control-group">
              <label class="control-label" for="UserUserType">User Type :</label>
              <div class="controls">
                <select name="data[User][user_type]" id="UserUserType">
                  <option value="superadmin">Super Admin</option>
                  <option value="admin">Admin</option>
                  <option value="user" selected="selected">User</option>
                </select>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserEmployId">Employ Id :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Employ Id" name="data[User][employ_id]" id="UserEmployId" value="<?php echo htmlentities($_POST[data][User][employ_id]) ?>" />
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserDesignation">Designation :</label>
              <div class="controls">
                <input type="text" class="span11" placeholder="Designation" name="data[User][designation]" id="UserDesignation" value="<?php echo htmlentities($_POST[data][User][designation]) ?>" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserMobileNo">Mobile No :</label>
              <div class="controls">
                <input type="phone" class="span11" placeholder="Mobile No" name="data[User][mobile_no]" id="UserMobileNo" value="<?php echo htmlentities($_POST[data][User][mobile_no]) ?>" required/>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserAlternateEmail">Alternate Email :</label>
              <div class="controls">
                <input type="email" class="span11" placeholder="Alternate Email Address" name="data[User][alternate_email]" id="UserAlternateEmail" value="<?php echo htmlentities($_POST[data][User][alternate_email]) ?>" />
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserUserPic">Profile Picture :</label>
              <div class="controls">
                <input type="file" class="span11" name="data[User][user_pic]" id="UserUserPic" />
		<span class="help-block">Please select picture size 128 x 128</span>
              </div>
            </div>
	    <div class="control-group">
              <label class="control-label" for="UserAddress">Address :</label>
              <div class="controls">
                <textarea class="span11" name="data[User][address]" id="UserAddress"></textarea>
              </div>
            </div>
	    <div class="form-actions">
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
 <?php $viewData->scriptStart() ?>
    jQuery(document).ready(function(){
      jQuery('#AddUserForm').validate({errorElement : 'span'});
    })
  <?php $viewData->scriptEnd() ?>