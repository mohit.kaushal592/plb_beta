<?php

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Ajit Singh")
							 ->setLastModifiedBy("Ajit Singh")
							 ->setTitle("Unique Blogs Data")
							 ->setSubject("Unique Blogs Data")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")*/
							 ->setCategory("Adlift PLB Tool");
$objPHPExcel->setActiveSheetIndex(0)
	    ->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
	    ->setCellValue('A1', 'Unique Blogs Report');
	    
$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
$objPHPExcel->getActiveSheet()->getStyle("A1:C1")->getFont()->setSize(20);

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A3', 'Domain')
	    ->setCellValue('B3', 'User Name')
                
            ->setCellValue('C3', 'Date');
$objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFont()->setBold(true);
$dateFrom = $viewData->get('dateFrom');
$uniqueblogs = $viewData->get('uniqueblogs');
/*echo "<pre>";
print_r($uniqueblogs);
exit;*/


if(!empty($uniqueblogs)){
   
    $i=4;
    foreach($uniqueblogs as $uniqueblog){
	 if(checkpriviues($uniqueblog['id'],$dateFrom))
            {
	    $objPHPExcel->getActiveSheet()
			->setCellValue('A'. $i, $uniqueblog['domain'])
			->setCellValue('B'. $i, $uniqueblog['first_name'].' '.$uniqueblog['last_name'])
					
			->setCellValue('C'. $i, date('Y-m-d', strtotime($uniqueblog['payment_date'])))
			;
	    $i++;
            }
	
    }
}
           
$objPHPExcel->getActiveSheet()->setTitle('Unique Blogs');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="unique_blogs_report_'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
   
exit;
?>