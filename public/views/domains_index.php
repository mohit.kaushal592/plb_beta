<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
        <!-- Filter Box -->
      <?php include(WWW_ROOT.DS."layouts".DS."filter_domains.php") ?>
      <!-- Filter Box End -->
	<!-- Actions -->
	  
	  <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	  <?php if(canUserDoThis(array('paypal','domain_delete'))): ?>
	   <?php if(canUserDoThis('paypal')): ?>
	     <li><a href="javascript:void(0)" rel="update_domain" class="doAct"><i class="icon icon-tint icon-large"></i> Domain Action</a></li>
	    <?php endif ?>
	    <?php if(canUserDoThis('domain_delete')): ?>
	     <li><a href="javascript:void(0)" rel="paypal_delete" class="doAct"><i class="icon-trash icon-large"></i> Delete Paypal</a></li>
	    <?php endif ?>
	    <?php endif ?>
	    <li><a id="_ExportToExcel" href="javascript:void(0)"><i class="icon-download-alt icon-large"></i> Export</a></li>
          </ul>
        </div>
	  
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $_GET['_rp']); ?>
	   </select>
	   </label>
	  </div>
	  <!-- End Actions -->
	
	<div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Domains Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="domains.php?act=do_action" method="post" id="DominsListForm">
	     <input type="hidden" name="whatDo" id="DomainsWhatDo" />
	    <table class="table table-bordered table-striped tbl-resize sortable_tbl">
              <thead>
                <tr>
		 <?php if(canUserDoThis('paypal')): ?>
		 <th style="width: 50px;"></th>
		 <?php  endif ?>
                  <th>Domains</th>
                  <th>Campaigns (Black Listed)</th>
		  <th>Status</th>
                </tr>
              </thead>
              <tbody> 
                <?php $domains = $viewData->get('domains') ?>
               <?php if(!empty($domains)): ?>
	        <?php foreach($domains as $domain): ?>
                <tr class="odd gradeX">
		 <?php if(canUserDoThis('paypal')): ?>
		 <td>
		  <input type="checkbox" name="data[Domain][accept][]" class="_accept_domain" value="<?php echo $domain['Domain']['id'] ?>"/>
		  <input type="checkbox" name="data[Domain][reject][]" class="_reject_domain" value="<?php echo $domain['Domain']['id'] ?>"/>
		 </td>
		 <?php  endif ?>
                  <td>   <a href="<?php echo $domain['Domain']['name'] ?>" target="_blank"><?php echo $domain['Domain']['name'] ?> </a></td>
                  <td style="text-align: center;">
		   <?php if($domain['Domain']['status']==1): ?>
		     <?php if(isset($domain['BlacklistDomain']) and count($domain['BlacklistDomain'])>0): ?>
		     <a href="blacklisted.php?act=campaign&d=<?php echo base64_encode($domain['Domain']['name']) ?>" class="tip-right show-campaigns" title="This domain is balck listed for some campaigns. Click to see!"><i class="icon-user icon-large"></i> &nbsp;</a>
		     <?php endif ?>
		     <a data-toggle="modal" data-backdrop='static' data-target="#BlackListCampaign" onmouseover="domainname='<?php echo base64_encode($domain['Domain']['name']) ?>'"  href="#" title="Click to blacklist domain for one or more campaigns." class="tip-right"><i class="icon-plus icon-large"></i> &nbsp;</a>
		   <?php else: ?>
		   <i class="icon-minus tip-right" title="Domain black listed for all campaigns."> &nbsp;</i>
		   <?php endif ?>
		  </td>
                  <td style="text-align: center;">
                    <?php echo getIconHtml($domain['Domain']['status'], array(0=>'Black Listed', 1=>'Active')) ?>
                  </td>
                </tr>
                <?php endforeach ?>
		<?php else: ?>
		<tr><td colspan="9" class="no-record-found">No records found.</td></tr>
		<?php endif ?>
              </tbody>
            </table>
	    </form>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>
	 </div>
   </div>
    
    <!-- Modal -->
<div class="modal fade" id="BlackListCampaign" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Blacklist Domain for one or more campaigns</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="modalSubmitBtn" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="CampaignEdit" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
     <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Edit DA/PR for Campaign - <span></span></h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
       <a href="#" class="btn" data-dismiss="modal">Cancel</a>
       <a href="javascript:void(0)" id="edit-campaign-submit" class="btn btn-primary">Submit</a>
      </div>
    </div>
  </div>
    <?php $viewData->scripts(array('js/domains_index.js'), array('inline'=>false)) ?>