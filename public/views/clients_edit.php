<?php $campaign = $viewData->get('campaign') ?>
<form action="clients.php?act=edit" method="post" id="CampaignEditSave" data-target="#CampaignEdit">
    <input type="hidden" name="cid" value="<?php echo $campaign['Campaign']['id'] ?>"/>
    <input type="hidden" name="data[Campaign][id]" value="<?php echo $campaign['Campaign']['id'] ?>"/>
    <p>Minimum Required DA</p>
    <p><input type="text" name="data[Campaign][plb_da]" value="<?php echo $campaign['Campaign']['plb_da'] ?>" required/>
    <span class="error"  style="display: none;">Please enter minimum DA.</span>
    </p>
    <p>Minimum Required PR</p>
    <p><input type="text" name="data[Campaign][plb_pr]" value="<?php echo $campaign['Campaign']['plb_pr'] ?>" required/>
    <span class="error" style="display: none;">Please enter minimum PR.</span>
    </p>
</form>