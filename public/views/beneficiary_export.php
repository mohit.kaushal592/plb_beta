<?php
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Akhtar Khan")
							 ->setLastModifiedBy("Akhtar Khan")
							 ->setTitle("Domains DA")
							 ->setSubject("Domains DA")
							/* ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file")*/;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'BenCode')
            ->setCellValue('B1', 'BenName')
            ->setCellValue('C1', 'Address1')
            ->setCellValue('D1', 'Address2')
            ->setCellValue('E1', 'City')
            ->setCellValue('F1', 'State')
            ->setCellValue('G1', 'Zip_Code')
            ->setCellValue('H1', 'Phone')
            ->setCellValue('I1', 'Email')
            ->setCellValue('J1', 'Beneficiary Account No.')
            ->setCellValue('K1', 'Input Only Internal Fund Transfer Account no.')
            ->setCellValue('L1', 'Delivery_Address1')
            ->setCellValue('M1', 'Delivery_Address2')
            ->setCellValue('N1', 'Delivery_City')
			->setCellValue('O1', 'Delivery_State')
			->setCellValue('P1', 'Delivery_Zip_Code')
			->setCellValue('Q1', 'PrintLocation')
			->setCellValue('R1', 'CustomerID')
			->setCellValue('S1', 'IFSC')
			->setCellValue('T1', 'MailTo')
			->setCellValue('U1', 'NEFT')
			->setCellValue('V1', 'RTGS')
			->setCellValue('W1', 'CHQ')
			->setCellValue('X1', 'DD')
			->setCellValue('Y1', 'IFTO')
            ->setCellValue('Z1', 'FirstLinePrint');
$objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);
$submittedlinks = $viewData->get('submittedLinks');
if(!empty($submittedlinks)){
    $i=2;
    foreach($submittedlinks as $_submittedLink){
        $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'. $i, $_submittedLink['Beneficiary']['bencode'])
                    ->setCellValue('B'. $i, $_submittedLink['Beneficiary']['benname'])
					->setCellValue('C'. $i, $_submittedLink['Beneficiary']['address'])
					->setCellValue('D'. $i, '')
					->setCellValue('E'. $i, $_submittedLink['Beneficiary']['city'])
					->setCellValue('F'. $i, $_submittedLink['Beneficiary']['state'])
					->setCellValue('G'. $i, $_submittedLink['Beneficiary']['zip_code'])
					->setCellValue('H'. $i, '')
					->setCellValue('I'. $i, '')
					->setCellValue('J'. $i, 'AC-'.$_submittedLink['Beneficiary']['benaccount'])
					->setCellValue('K'. $i, '')
					->setCellValue('L'. $i, '')
					->setCellValue('M'. $i, '')
					->setCellValue('N'. $i, '')
					->setCellValue('O'. $i, '')
					->setCellValue('P'. $i, '')
					->setCellValue('Q'. $i, '')
					->setCellValue('R'. $i, '')
					->setCellValue('S'. $i, $_submittedLink['Beneficiary']['ifsccode'])
					->setCellValue('T'. $i, '')
					->setCellValue('U'. $i, '')
					->setCellValue('V'. $i, '')
					->setCellValue('W'. $i, '')
					->setCellValue('X'. $i, '')
					->setCellValue('Y'. $i, '')
					->setCellValue('Z'. $i, '');
					
        $i++;
    }
}
            
$objPHPExcel->getActiveSheet()->setTitle('Submited Links');
$objPHPExcel->setActiveSheetIndex(0);
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save('domains_da_'.time().'.xlsx');

// Redirect output to a client�s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="beneficiary_report_'.time().'.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>