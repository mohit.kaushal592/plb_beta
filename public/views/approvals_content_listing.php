<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
<?php
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "http://164.52.218.39/marketing/articles/",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json"
	),
	));
	
	$approvals = json_decode(curl_exec($curl),true);
	$err = curl_error($curl);
	// echo "<pre>";
	// print_r($err); die;
	curl_close($curl);
?>

  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
      <!-- Filter Box -->
      <?php //include(WWW_ROOT.DS."layouts".DS."filter_approvals.php") ?>
      <!-- Filter Box End -->
      <!-- Actions -->
	  &nbsp;&nbsp;
	  <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	   <?php if(canUserDoThis(array('forward','approve_phase_1','approve_phase_2','approval_delete'))): ?>
	    <?php if(canUserDoThis('forward')): ?>
	      <li><a href="javascript:void(0)" rel="approval_forward" class="doAct"><i class="icon-step-forward  icon-large"></i> Forward</a></li>
	     <?php endif ?>
	     <?php if(canUserDoThis('approve_phase_1')): ?>
	     <li><a href="javascript:void(0)" rel="approval_phase_1" class="doAct"><i class="icon-ok icon-large"></i><sup class="icontxt">1</sup> Approval Phase I</a></li>
	     <?php endif ?>
	     <?php if(canUserDoThis('approve_phase_2')): ?>
	     <li><a href="javascript:void(0)" rel="approval_phase_2" class="doAct"><i class="icon-ok icon-large"></i><sup class="icontxt">2</sup> Approval Phase II</a></li>
	     <?php endif ?>
	     <?php if(canUserDoThis('approval_delete')): ?>
	      <li><a href="javascript:void(0)" rel="approval_delete" class="doAct"><i class="icon-trash icon-large"></i> Delete Link</a></li>
	     <?php endif ?>
	    <?php endif ?>
	    <li><a id="_ExportToExcel" href="javascript:void(0)"><i class="icon-download-alt icon-large"></i> Export</a></li>
          </ul>
        </div>
		
		<div class="btn-group action-right p-4">
		&nbsp;&nbsp;
          <a href="javascript:void(0)" class="btn btn-primary">Select Project</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	      	<li>
				<a href="javascript:void(0)" rel="approval_forward" class="doAct"><i class="icon-step-forward  icon-large"></i> A...</a>
			</li>
			<li>
				<a href="javascript:void(0)" rel="approval_forward" class="doAct"><i class="icon-step-forward  icon-large"></i> B...</a>
			</li>
			<li>
				<a href="javascript:void(0)" rel="approval_forward" class="doAct"><i class="icon-step-forward  icon-large"></i> C...</a>
			</li>
          </ul>
		  &nbsp;&nbsp;
        </div> 
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $filterVars['row_per_page']); ?>
	   </select>
	   </label>
	  </div>
	  
	  <!-- End Actions -->
        <div class="widget-box">
	  <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Content Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="approvals.php?act=do_action" method="post" id="ApprovalListForm">
	     <input type="hidden" name="whatDo" id="ApprovalWhatDo" />
	     <table class="table table-bordered table-striped tbl-resize approvals_list_tbl sortable_tbl">
	       <thead>
		 <tr>
		   <th style="width: 10%;">Keyword</th>
		   <th style="width: 10%;">SV</th> 
		   <th style="width: 5%;">Rank</th>
		   <th style="width: 15%;">Title</th>
		   <th style="width: 10%;">Status</th>
		   <th style="width: 25%;">Article Link</th>
		   <th style="width: 10%;">Date</th>
		   <th style="width: 10%;">Created By</th> 
		   <th style="width: 5%;">Action</th>
		 </tr>
	       </thead>
	       <tbody> 
			
		 <?php //$approvals = $viewData->get('approvals') ?>
		<?php if(!empty($approvals) && empty($err)): 
			foreach($approvals as $approval):  	
		?>
		 <tr class="odd gradeX" domain="<?php echo $approval['Approval']['domain'] ?>">
		   <td style="color:#DA542E;">  
				<?php echo ucwords($approval['content_writing']['manager']['seo']['keyword_sets'][0]['keywords']); ?> 
		   </td>
		   <td><?php echo $approval['content_writing']['manager']['seo']['keyword_sets'][0]['sv']; ?></td>
		   <td><?php echo $approval['content_writing']['manager']['seo']['keyword_sets'][0]['rank']; ?></td>
		   <td><?php echo ucwords($approval['content_writing']['title']) ?></td>
		   <td><?php echo ucwords($approval['content_writing']['manager']['seo']['status']) ?></td>
		   <td><?php echo $approval['content_writing']['articelink'] ?></td>  
		   <td><?php echo date('Y-m-d',strtotime($approval['content_writing']['updated_at'])) ?></td>  
		   <td><?php echo $approval['content_writing']['created_by']['first_name'] ?></td>  
		    
		   <td>
		    	<a href="approvals.php?act=edit_link&pid=<?php echo $approval['Approval']['id'] ?>" class="tip-top" title="Click to edit link." target="_blank"> <i class="icon icon-edit icon-large"></i></a>
		   </td>
		 </tr>
		 <?php endforeach ?>
		 <?php else: ?>
		 <tr><td colspan="10" class="no-record-found">No records found.</td></tr>
		 <?php endif ?>
	       </tbody>
	     </table>
	    </form>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>
	 </div>
     
     <div id="AdminCommentAdd" class="modal fade bs-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
     <div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h3>Admin Comment <span></span></h3>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
       <a href="#" class="btn" data-dismiss="modal">Cancel</a>
       <a href="javascript:void(0)" id="admin-add-comment" class="btn btn-primary">Submit</a>
      </div>
    </div>
  </div>
     
   </div>
    
<?php $viewData->scripts(array('js/approvals_list.js'), array('inline'=>false)) ?>