<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr> 
  <p>Please select user for clients (campaigns) to secondary approval.</p>
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Clients - Assign User</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="" class="form-horizontal"  method="post" id="AddDomainForm">
	    <?php $clients = $viewData->get('clients'); ?>
	    <?php $users = $viewData->get('users'); ?>
	    <?php $campaignUsers = $viewData->get('campaignUsers') ? $viewData->get('campaignUsers') : array(); ?>
            <table class="table table-striped">
	      <thead>
		<tr>
		  <th style="text-align: left;">Client</th>
		  <th style="text-align: left;">Current User</th>
		  <th style="text-align: left;">Assign To</th>
		  <th style="width: 10%;">Action</th>
		</tr>
	      </thead>
	      <tbody>
		<?php if(!empty($clients)): foreach($clients as $client): ?>
		<tr id="rw_<?php echo $client['Campaign']['id'] ?>">
		  <td><?php echo $client['Campaign']['name'] ?>
		  <input type="hidden" id="rw_<?php echo $client['Campaign']['id'] ?>_id" name="data[Campaign][id]" value="<?php echo $client['Campaign']['id'] ?>"></td>
		  <td><?php echo $campaignUsers[$client['Campaign']['secondary_approval_user']] ?></td>
		  <td>
		    <select id="rw_<?php echo $client['Campaign']['id'] ?>_user" name="data[Campaign][secondary_approval_user]">
		    <?php echo getFormOptions($users, $client['Campaign']['secondary_approval_user']) ?>
		    </select>
		  </td>
		  <td style="text-align: center;"><button type="button" class="btn btn-success _updateRow" data-tbl-row="rw_<?php echo $client['Campaign']['id'] ?>">Update</button></td>
		</tr>
		<?php endforeach; endif; ?>
	      </tbody>
	    </table>
          </form>
        </div>
      </div>
    </div>
  </div>
  
    <?php $viewData->scripts(array('js/clients_assign_user.js'), array('inline'=>false)) ?>