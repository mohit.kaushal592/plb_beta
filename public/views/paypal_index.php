<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
    <?php echo output_message($session->message()) ?>
    <div class="row-fluid">
     <div class="span12">
      <!-- Filter Box -->
      <?php include(WWW_ROOT.DS."layouts".DS."filter_paypal.php") ?>
      <!-- Filter Box End -->
      <!-- Actions -->
	  <?php if(canUserDoThis(array('paypal','paypal_delete'))): ?>
	  <div class="btn-group action-right">
          <a href="javascript:void(0)" class="btn btn-primary">Actions</a>
          <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="caret"></span></a>
          <ul class="dropdown-menu">
	   <?php if(canUserDoThis('paypal')): ?>
	     <li><a href="javascript:void(0)" rel="update_paypal" class="doAct"><i class="icon-money"></i> Paypal Action</a></li>
	     <li><a href="javascript:void(0)" rel="update_domain" class="doAct"><i class="icon icon-tint icon-large"></i> Domain Action</a></li>
	    <?php endif ?>
	    <?php if(canUserDoThis('paypal_delete')): ?>
	     <li><a href="javascript:void(0)" rel="paypal_delete" class="doAct"><i class="icon-trash icon-large"></i> Delete Paypal</a></li>
	    <?php endif ?>
          </ul>
        </div>
	  <?php endif ?>
	  <div class="btn-group action-right">
	   <label>Records: 
	   <select id="RowPerPage" class="row-per-page">
	    <?php echo getFormOptions(array(10=>10, 20=>20, 30=>30, 40=>40, 50=>50, 60=>60, 70=>70, 80=>80, 90=>90, 100=>100), $_GET['_rp']); ?>
	   </select>
	   </label>
	  </div>
	  <!-- End Actions -->
      
        <div class="widget-box">
          <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-th" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Paypal Listing</h5>
	    <div class="span2 ttl-record"><h5>Total Records - <?php echo $viewData->get('totalRecords') ?></h5></div>
          </div>
          <div class="widget-content nopadding">
            <form action="paypal.php?act=do_action" method="post" id="PaypalListForm">
	     <input type="hidden" name="whatDo" id="PaypalWhatDo" />
	    <table class="table table-bordered table-striped tbl-resize sortable_tbl">
              <thead>
                <tr>
		 <?php if(canUserDoThis('paypal')): ?>
		 <!--<th style="width:50px;"></th>-->
		 <?php endif ?>
                  <th>Paypal ID </th>
                  <th>Domains</th>
                </tr>
              </thead>
              <tbody> 
                <?php $paypalLists = $viewData->get('paypalLists') ?>
               <?php if(!empty($paypalLists)): ?>
	        <?php foreach($paypalLists as $paypalList): ?>
                <tr class="odd gradeX">
		 <?php if(canUserDoThis('paypal')): ?>
		 <!--<td></td>-->
		 <?php endif ?>
                  <td>
		   <ul>
		   <li>
		    <?php if(canUserDoThis('paypal')): ?>
		    <span><input type="checkbox" name="data[Paypal][accept][]" class="_accept_paypal" value="<?php echo $paypalList['Paypal']['id'] ?>"/>
		  <input type="checkbox" name="data[Paypal][reject][]" class="_reject_paypal" value="<?php echo $paypalList['Paypal']['id'] ?>"/></span>
		    <?php endif ?>
		   <span><?php echo $paypalList['Paypal']['name'] ?> </span>
		   <?php echo getIconHtml($paypalList['Paypal']['status'], array(0=>'Black Listed', 1=>'Active')) ?></li>
		  </ul> </td>
                  <td>
                    <?php
		    $_domains = array();
		    if(!empty($paypalList['Domain'])):
		      foreach($paypalList['Domain'] as $_domain):
		       $_htm = "<span>";
		       $_htm .= '<input type="checkbox" name="data[Domain][accept][]" class="_accept_domain" value="'. $_domain['id']. '"/>';
		       $_htm .= '<input type="checkbox" name="data[Domain][reject][]" class="_reject_domain" value="'.$_domain['id'].'"/>';
		       $_htm .= "</span>";
		       $_htm .= '<span>'. $_domain['name'] . '</span>';
		       $_htm .= getIconHtml($_domain['status'], array(0=>'Black Listed', 1=>'Active'));
			$_domains[] = "<li>". $_htm. "</li>";
		      endforeach;
		    endif;
		    ?>
		  <ul><?php echo implode(' ', $_domains) ?></ul>
                  </td>
                </tr>
                <?php endforeach ?>
		<?php else: ?>
		<tr><td colspan="9" class="no-record-found">No records found.</td></tr>
		<?php endif ?>
              </tbody>
            </table>
          </div>
        </div>
        <?php echo $viewData->get('pageLinks') ?>
	 </div>
   </div>
    <?php $viewData->scripts(array('js/paypal_index.js'), array('inline'=>false)) ?>