<?php $filterVars = $viewData->get('filterVars') ;



?>
<form action="beneficiary.php" method="post" class="form-horizontal filter-form" id="FilterForm">
	<input type="hidden" name="exportList" id="exportList" value="" />
	<input type="hidden" name="exportListBank" id="exportListBank" value="" />
	<div class="control-group">
	  
	  <div class="span3">
	   <label>Ben Code</label>
	    <input name="domain" autocomplete="off" id="Filterbencode" type="text" class="span10" value="<?php echo !empty($filterVars['bencode']) ? $filterVars['bencode'] : '' ?>" class="typeahead" />
	  </div>
	   <div class="span3">
	   <label>Ben Name</label>
	    <input name="domain" autocomplete="off" id="Filterbenname" type="text" class="span10" value="<?php echo !empty($filterVars['benname']) ? $filterVars['benname'] : '' ?>" class="typeahead" />
	  </div>
	   <div class="span3">
	   <label>Ben Account</label>
	    <input name="domain" autocomplete="off" id="Filterbenaccount" type="text" class="span10" value="<?php echo !empty($filterVars['benaccount']) ? $filterVars['benaccount'] : '' ?>" class="typeahead" />
	  </div>
	   <div class="span3">
	   <label>IFSC Code</label>
	    <input name="domain" autocomplete="off" id="Filterifsccode" type="text" class="span10" value="<?php echo !empty($filterVars['ifsccode']) ? $filterVars['ifsccode'] : '' ?>" class="typeahead" />
	  </div>
	  
	 
	</div>
	
      
       
       
       <div class="control-group">
	<div class="span3">
	   <label>Post Date</label>
	     <div  data-date="" class="input-append date datepicker">
	       <input type="text" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="span10" >
	       <span class="add-on"><i class="icon-th"></i></span> </div>
	     <div  data-date="" class="input-append date datepicker">
	       <input type="text" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="span10" >
	       <span class="add-on"><i class="icon-th"></i></span> </div>
	 </div>
	  
         
          <!-- End: Added by Jitendra: 28-Nov-2014 --->  
       </div>
       
       <div class="form-actions">
	 <button type="reset" class="btn btn-primary">Reset</button>
	 <button type="button" class="btn btn-success" onclick="filterSubmit();">Filter</button>
       </div>
    </form>