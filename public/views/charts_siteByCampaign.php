<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&display=swap"> <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&display=swap">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="Assets/Css/bootstrap.min.css">
    <link rel="stylesheet" href="Assets/Css/Style.css">
    
    <!-- <script src="https://code.jquery.com/jquery-3.7.0.js"></script> -->
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
   
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
 
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>  
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.en-GB.min.js">
    </script>

 
    <link rel = "stylesheet" href = "https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" />
    <script src = "https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" defer></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

</head>
<style>
h1 {
    text-align: start;
    margin-bottom: 20px;
}

#container {
    width: 100%;
    height: 400px;
    margin-bottom: 20px;
}

table {
    width: 100%;
    border-collapse: collapse;
    margin-top: 20px;
}

table, th, td {
    border: 1px solid #ddd;
}

th, td {
    padding: 8px;
    text-align: left;
  font-size: 12px;
}

th {
    background:white;
    color:black;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropbtn {
/*     background-color: #4CAF50; */
    color: black;
    padding: 10px 20px;
    border: none;
    cursor: pointer;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content label {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  font-size: 12px;
}

.dropdown-content label:hover {
    background-color: #f1f1f1;
}

.dropdown-content input {
    margin-right: 10px;
}

.apply-btn {
    background-color: #4CAF50;
    color: white;
    border: none;
    padding: 10px;
    width: 100%;
    cursor: pointer;
}

.apply-btn:hover {
    background-color: #45a049;
}

.show {
    display: block;
}

.custom-select {
  width: 200px;
  height: 40px;
  background-color: #f0f0f0;
  border: 1px solid #ccc;
  border-radius: 5px;
  padding: 5px 10px;
  font-size: 16px;
  color: #333;
  outline: none;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  cursor: pointer;
  transition: all 0.3s ease;
}

.custom-select:hover {
  border-color: #888;
}

.custom-select:focus {
  border-color: #555;
}

.custom-select option {
  color: #333;
}

.dataTables_filter {
    display: none;
}

#dataTable_length{
    width: 100%; 
    text-align: end;
    margin-right:3px;
}

</style>
<?php
ob_start();
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

$start_year = 2000;
$end_year = date('Y'); 
$get_end_year = date('Y'); 
$full_selected_date = "";

try{
    $is_campaign_selected = '';
    if(isset($_GET['campaign_id'])){
        $is_campaign_selected = $_GET['campaign_id'];
    }
    if(isset($_GET['year'])){
        $get_end_year = $_GET['year'];
    } 
    if(isset($_GET['fromMonth'])){
        $from_month = $_GET['fromMonth'];
        $to_month   = $_GET['toMonth'];
        $full_selected_date = "$from_month - $to_month";
    } 
    $url = "https://api.adlift.ai/marketing/Bars-unique-links/?year=$get_end_year";
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $response = curl_exec($ch);
    if(curl_errno($ch)){
        echo 'Curl error: ' . curl_error($ch);
    }else{ 
        $response_data = json_decode($response,true); 
        
        //echo "<pre>"; print_r($response); die;

        foreach($response_data as $campaign_data){
            $campaign_name      =   $campaign_data['Campaign_name'];
            $campaign_id        =   $campaign_data['Campaign_id'];
            $a_monthly_data     =   $campaign_data['Monthly_data'];
            
            $total_month    =   0;
            $total_added    =   0;
            $total_pending  =   0;
            $a_month = [];
            $count = 0;
            foreach($a_monthly_data as $month=>$monthly_data){  
                $count++;
                if(isset($from_month)){
                    if ($count >= $from_month && $count <= $to_month) { }else{
                        continue;
                    }
                }
                $a_month[] = $month;
                $total_month += (int)$monthly_data['Total'];
                $total_added += (int)$monthly_data['Added'];
                $total_pending += (int)$monthly_data['Pending'];

                if(isset($is_campaign_selected) && !empty($is_campaign_selected)){
                    if($campaign_id == $is_campaign_selected){
                        $campaign_month_wise_data[$campaign_name][$month] = [
                            'total'=>$monthly_data['Total'],
                            'added'=>$monthly_data['Added'],
                            'pending'=>$monthly_data['Pending']
                        ]; 
                    }
                }else{
                    $campaign_month_wise_data[$campaign_name][$month] = [
                        'total'=>$monthly_data['Total'],
                        'added'=>$monthly_data['Added'],
                        'pending'=>$monthly_data['Pending']
                    ];
                }

            }
            $campaign_year_wise_total_data[$campaign_name.'__'.$campaign_id] = [
                'total'=>$total_month,
                'added'=>$total_added,
                'pending'=>$total_pending,
            ]; 
            if(isset($is_campaign_selected) && !empty($is_campaign_selected)){
                if($campaign_id == $is_campaign_selected){
                    //echo "<pre>"; print_r($campaign_month_wise_data['Adlift']); die;
                    if(!empty($campaign_month_wise_data)){
                        foreach($campaign_month_wise_data as $a_company_data){
                            foreach($a_company_data as $month=>$company_data){
                                $a_campaign_name[] = "$month";
                                $a_campaign_total[] = $company_data['total'];
                                $a_campaign_added[] = $company_data['added'];
                                $a_campaign_pending[] = $company_data['pending'];
                            }   
                        }
                    } 

                    // $a_campaign_name[] = $campaign_name;
                    // $a_campaign_total[] = $total_month;
                    // $a_campaign_added[] = $total_added;
                    // $a_campaign_pending[] = $total_pending;
                }
            }else{
                $a_campaign_name[] = $campaign_name;
                $a_campaign_total[] = $total_month;
                $a_campaign_added[] = $total_added;
                $a_campaign_pending[] = $total_pending;
            }
        }
        // echo "<pre>"; print_r($a_campaign_name); 
        // echo "<pre>"; print_r($a_campaign_total); 
        // echo "<pre>"; print_r($a_campaign_added); 
        // echo "<pre>"; print_r($a_campaign_pending); 
        // die;
        $a_campaign_name = implode('","',$a_campaign_name);
        $a_campaign_total = implode(',',$a_campaign_total);
        $a_campaign_added = implode(',',$a_campaign_added);
        $a_campaign_pending = implode(',',$a_campaign_pending);
    }
    //echo "<pre>"; print_r($a_campaign_name);
    curl_close($ch);
}catch(Exception $e) {
    echo 'Error: ' . $e->getMessage();
    die('error');
} 

if(isset($_POST['date_filter'])){
    $filter_date = $_POST['filter_date'];
    $campaign_id = $_POST['campaign_id'];
    $a_date_range = explode('-',$filter_date);
    $a_from_date_info = explode('/',trim($a_date_range[0]));
    $a_to_date_info = explode('/',trim($a_date_range[1]));

    $from_month   =   $a_from_date_info[0];
    $from_date    =   $a_from_date_info[1];
    $from_year    =   $a_from_date_info[2];

    $to_month     =   $a_to_date_info[0];
    $to_date      =   $a_to_date_info[1];
    $to_year      =   $a_to_date_info[2];

    echo "<meta http-equiv='refresh' content='0;url=charts.php?act=siteByCampaign&campaign_id=$campaign_id&year=$from_year&fromMonth=$from_month&toMonth=$to_month'>";
    exit();
}

$current_date = date('Y-m-d'); 
$last_month_date = date('Y-m-d', strtotime('-1 month'));
?>

<body><br /><br />
    <h1>No. of Sites by Campaign</h1>
    <div>
    
    <form method="POST" action="" style="margin-bottom:20px;text-align:right">
        <input type="hidden" name="act" value="siteByCampaign">   
        <div class="dropdown"> 
            <input type="text" name="filter_date" value="<?= $full_selected_date; ?>" style="height: 36px;border-radius: 7px;margin-top: 10px;"/>
        </div> &nbsp;&nbsp;&nbsp;
        <div class="dropdown">
            <select class="custom-select" name="campaign_id">
                <option value="0">Select Campaign</option>
                <?php foreach($campaign_year_wise_total_data as $campaign_name=>$data){ ?>
                   <option value="<?= explode('__',$campaign_name)[1]; ?>" <?= ($is_campaign_selected == explode('__',$campaign_name)[1])?'selected':''?> >
                        <?= explode('__',$campaign_name)[0]; ?>
                    </option>
                <?php } ?>
            </select>
        </div> &nbsp;&nbsp;&nbsp;
    
        <div class="dropdown">
            <button type="submit" name="date_filter" class="apply-btn">Apply</button>
        </div>
    </form>
    <div id="container" style="width: 100%; overflow-x: auto;">
        <div id="noOfSiteChartContainer"></div>
    </div>
    <table id="dataTable" class="display">
        <thead style="background-color: #DA542E !important;color: #fff;">
            <tr>
                <th>Campaign</th>
                <th>Month</th>
                <th>No of Links</th>
                <th>Added Links</th>
                <th>Pending Links</th>
            </tr>
        </thead>
        <tbody>
            <?php  
                foreach($campaign_month_wise_data as $campaign_name=>$a_month_data){ 
                    foreach($a_month_data as $month_name=>$data){ 
                        $total_month = $data['total'];
                        $total_added = $data['added'];
                        $total_pending = $data['pending'];
                        $a_data[] = [
                            'campaign_name' => $campaign_name,
                            'month_name' => $month_name,
                            'month_name' => $month_name,
                            'total_added' => $total_added,
                            'total_pending' => $total_pending,
                        ] 
            ?>
                <tr data-campaign="<?= $campaign_name; ?>">
                    <td><?= $campaign_name; ?></td>
                    <td><?= $month_name; ?></td>
                    <td><?= $total_month; ?></td>
                    <td><?= $total_added; ?></td>
                    <td><?= $total_pending; ?></td>
                </tr>
            <?php } } ?>
        </tbody>
    </table>
    <div id="paginationContainer"></div>
    <!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script src="script.js"></script>
  
</body>
 
<script>

$(document).ready(function () {
    var table = $('#dataTable').DataTable({
    "paging": true,
    "pageLength": 12
    });
});
document.addEventListener('DOMContentLoaded', function () {
    Highcharts.chart('noOfSiteChartContainer', {
        chart: {
            type: 'column', 
            scrollablePlotArea: {
                minWidth: 3500,
                scrollPositionX: 1
            }
        },
        title: {
            text: ''
        },
        xAxis: { 
            categories: ["<?= $a_campaign_name; ?>"],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of Links',
                align: ''
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            column: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'No of Links',
            data: [<?= $a_campaign_total; ?>]
        }, {
            name: 'Added Links',
            data: [<?= $a_campaign_added; ?>]
        }, {
            name: 'Pending Links',
            data: [<?= $a_campaign_pending; ?>]
        }]
    });
});

// script.js
document.addEventListener('DOMContentLoaded', function() {
    var dropdown = document.querySelector('.dropdown');
    var dropbtn = document.querySelector('.dropbtn');
    var dropdownContent = document.querySelector('.dropdown-content');
    var checkboxes = document.querySelectorAll('.dropdown-content input[type="checkbox"]');
    var applyBtn = document.querySelector('.apply-btn');
    
    dropbtn.addEventListener('click', function(event) {
        event.stopPropagation(); // Prevent the click from propagating to the document
        dropdownContent.classList.toggle('show');
    });

    window.addEventListener('click', function(event) {
        if (!dropdown.contains(event.target)) {
            if (dropdownContent.classList.contains('show')) {
                dropdownContent.classList.remove('show');
            }
        }
    });

    applyBtn.addEventListener('click', function() {
        var selectedOptions = [];
        checkboxes.forEach(function(box) {
            if (box.checked) {
                selectedOptions.push(box.value);
            }
        });
        console.log("Selected options: ", selectedOptions);
        // Optionally, you can close the dropdown after applying
        dropdownContent.classList.remove('show');
    });

    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener('change', function() {
            // Optional: You can add real-time feedback here if needed
        });
    });
});
$('input[name="filter_date"]').daterangepicker();
</script> 
</html>