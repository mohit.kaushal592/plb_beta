<h4 style="color:#DA542E;"><?php echo $viewData->getTitle() ?></h4>
  <hr>
<div class="row-fluid">
    <div class="span12">
        <!-- Filter form -->
        <div class="widget-box"> <?php $filterVars = $viewData->get('filterVars') ?>
            <div class="widget-title"> <span class="icon"> <i class="icon-search"></i> </span>
              <h5>Advance Filter</h5>
            </div>
            <div class="widget-content nopadding">
              <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
                   <div class="control-group">
                       <div class="span5">
                       <label>Date</label>
                         <div  data-date="" class="input-append date datepicker">
                           <input type="text" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="span10" >
                           <span class="add-on"><i class="icon-th"></i></span> </div>
                         <div  data-date="" class="input-append date datepicker">
                           <input type="text" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="span10" >
                           <span class="add-on"><i class="icon-th"></i></span> </div>
                       </div>
                   </div>
                   <div class="form-actions">
                    <button type="reset" class="btn btn-primary">Reset</button>
                    <button type="submit" class="btn btn-success">Filter</button>
                   </div>
               </form>
            </div>
        </div>
        <div class="widget-box">
	  <div class="widget-title" style="background: #DA542E;"> <span class="icon"> <i class="icon-check" style="color: #fff;"></i> </span>
            <h5 style="color: #fff;">Approval Links</h5>
          </div>
          <div class="widget-content nopadding">
            <div id="approvals-chart"><p style="padding-left:10px;">Record not found to render chart.</p></div>
          </div>
        </div>
    </div>
</div>

<?php $viewData->scripts(array('js/highcharts/highcharts.js', 'js/highcharts/themes/grid.js'), array('inline'=>false)) ?>
<?php $viewData->scriptStart() ?>
$(document).ready(function(){
	$('#FilterForm').bind('submit', function(e){
		e.preventDefault();
		var dateFrom = $('#FilterDateFrom').val();
		var dateTo = $('#FilterDateTo').val();
		var dateRange = (dateFrom+'_'+dateTo);
		dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		queryString._dt = $.base64.encode(dateRange);
		var urlParams = [];
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});
<?php echo $viewData->get('chart') ? $viewData->get('chart')->render("chart1") : ''; ?>
<?php $viewData->scriptEnd() ?>