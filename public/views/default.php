<?php $viewData->set('bodyClass', preg_replace("/\.php$/i", '', $useLayout))?>
<?php include('layouts/header.php') ?>
<?php include('layouts/left_sidebar.php') ?>
<?php include('layouts/content_start.php') ?>

<?php 
if(file_exists(dirname(__FILE__).DS.$useLayout)){ 
    include(dirname(__FILE__).DS.$useLayout);
} else {
    header("HTTP/1.0 404 Not Found");
    include(dirname(__FILE__).DS.'error404.php');
}

?>

<?php include('layouts/footer.php') ?>
