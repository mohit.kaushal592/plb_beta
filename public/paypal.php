<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

 function paypal_index(){
    global $viewData;
    $viewData->setTitle('Paypal Listing');
    $Paypal = new Paypal;
    $filters = __paypalFilterVars(); 
      $paypalFilter = array('order'=>array('Paypal.id DESC'));
      if(!empty($filters['Paypal'])){
       $paypalFilter['where'] = !empty($paypalFilter['where']) ? $paypalFilter['where'].' AND '. $filters['Paypal'] : $filters['Paypal'];
      }
    $paypalLists = $Paypal->fetchAll($paypalFilter, true);
    $viewData->set('paypalLists', $paypalLists);
 }
 
 function paypal_email_list_json(){
      global $viewData;
      $Paypal = new Paypal;
      $options=array();
      if(!empty($_GET['q'])){
         $options['where'] = "Paypal.name LIKE '%".$_GET['q']."%'";
         $options['limit'] = !empty($_GET['page_limit']) and (int)$_GET['page_limit']>0 ? '0, '.$_GET['page_limit'] : '0, 10';
      
         echo json_encode(select2DataFormat($Paypal->getList($options)));
      }
      exit;
}
 
 function paypal_do_action(){
  switch($_REQUEST['whatDo']){
   case 'update_paypal':
    __action_update_paypal($_REQUEST['data']);
    break;
  }
  exit;
 }
 
 function __action_update_paypal($data=array()){
   global $database;
   $msg = array('msg'=>'Sorry, paypal status could not changed. Please try again later.', 'status'=>'error');
   if(!empty($data)){
      $Paypal = new Paypal;
    // domain approval accept for campaigns
    if(!empty($data['Paypal']['accept'])){
     $_ids = $data['Paypal']['accept'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Paypal->table_name.' SET status=1 WHERE id IN('. join(',', $_ids). ') AND status!=1');
      $msg = array('msg'=>'Paypal status has been changed.', 'status'=>'success');
     }
    }
    // domain approval reject for campaigns
    if(!empty($data['Paypal']['reject'])){
     $_ids = $data['Paypal']['reject'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Paypal->table_name.' SET status=0 WHERE id IN('. join(',', $_ids). ') AND status!=0');
      $msg = array('msg'=>'Paypal status has been changed.', 'status'=>'success');
     }
    }
   }
   echo json_encode($msg);
 }
 
 function __paypalFilterVars(){
  global $viewData;
  $filter = array();
  $filterVars = array();
  if(!empty($_GET['_pe'])){
      $pe = base64_decode($_GET['_pe']);
      $filterVars['paypal_email'] = explode(',', $pe);
      $filter['Paypal'][] = " Paypal.name IN ('".join("','", $filterVars['paypal_email'])."') ";
  }
  if(!empty($_GET['_st'])){
      $st = base64_decode($_GET['_st']);
      $filterVars['status'] = explode(',', $st);
      $filter['Paypal'][] = " Paypal.status IN ('".join("','", $filterVars['status'])."') ";
  }
   $filters['Paypal'] = $filter['Paypal'] ? implode(' AND ', $filter['Paypal']) : '';
   $viewData->set('filterVars', $filterVars);
   return $filters;
 }
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('paypal_'.$action)){ 
 call_user_func('paypal_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'paypal_'.$_GET['act'].'.php' : 'paypal_index.php';
include "views/default.php";
 ?>