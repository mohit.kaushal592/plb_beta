<?php
require_once("../includes/initialize.php");
require_once("../includes/Highcharts/Highchart.php");
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}
function charts_chartDashboard(){
    global $viewData, $session;
    $viewData->setTitle('Anylatic Dashboard');
}
function charts_siteByCampaign(){
    global $viewData, $session;
    $viewData->setTitle('No. of Sites by Campaign');
}
function charts_approval() {
    global $viewData, $session;
    $viewData->setTitle('Approval Links Chart');
    $Approval = new Approval;

    $filters = __chartsFilterVars();

    $approvalLinkFilter = array(
        'fields' => array('id'),
        'contain' => array(
            'User' => array(
                'fields' => array('id', 'first_name', 'last_name')
            ),
            'ApprovalLink' => array(
                'fields' => array('id', 'approve_status')
            )
        )
    );
    if ($session->read('User.user_type') == 'user') {
        $approvalLinkFilter['where'] = "Approval.user_id='" . $session->read('User.id') . "'";
    }

    if (!empty($filters['ApprovalLink'])) {
        $approvalLinkFilter['where'] = !empty($approvalLinkFilter['where']) ? $approvalLinkFilter['where'] . ' AND ' . $filters['ApprovalLink'] : $filters['ApprovalLink'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));
        $filterC = array();
        if (!empty($approvalLinkFilter['where'])) {
            $filterC[] = $approvalLinkFilter['where'];
        }
        $approvalLinkFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(ApprovalLink.added_on, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(ApprovalLink.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }

    $_approvalLinks = $Approval->find_all($approvalLinkFilter);
    if (!empty($_approvalLinks)) {
        $chartData = array();
        $userCount = 0;
        foreach ($_approvalLinks as $_approvls) {
            $user = trim(ucwords($_approvls['User']['first_name'] . ' ' . $_approvls['User']['last_name']));
            if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
                $userCount++;
                $chartData['users'][$userCount] = $user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
            foreach ($_approvls['ApprovalLink'] as $_approvalLink) {
                switch ($_approvalLink['approve_status']) {
                    case '-1':
                        $chartData['series']['Pending']['data'][$userNo] += 1;
                        break;
                    case '0':
                        $chartData['series']['Rejected']['data'][$userNo] += 1;
                        break;
                    case '1':
                        $chartData['series']['Approved']['data'][$userNo] += 1;
                        break;
                    default:
                        break;
                }
            }
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Approved']['data']) || !key_exists($k, $chartData['series']['Approved']['data'])) {
                $chartData['series']['Approved']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Rejected']['data']) || !key_exists($k, $chartData['series']['Rejected']['data'])) {
                $chartData['series']['Rejected']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Pending']['data']) || !key_exists($k, $chartData['series']['Pending']['data'])) {
                $chartData['series']['Pending']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Approved']['data']);
        ksort($chartData['series']['Rejected']['data']);
        ksort($chartData['series']['Pending']['data']);
    }
    $chartData['title'] = 'Approval Links Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Links';
    __setChartData($chartData, "approvals-chart");
}

/* * ** Start: Added by Ajit : 15-DESC-2014  ***************** */

///////////Unique Blogs////////////
function charts_unique_blogs() {
    global $viewData, $session;
    $viewData->setTitle('Unique Blogs Chart');
    $Domain = new Domain;

    $Campaign = new Campaign;
$campaignsList = $Campaign->getList();
$viewData->set('campaignsList', $campaignsList);

    $filters = __chartsFilterVars("Domain");


    $User = new User;

    $userList = $User->getList();

    $viewData->set('usersList', $userList);

    $domainFilter = array(
        'fields' => array('id', 'name', 'added_on'),
        'contain' => array(
            'User' => array(
                'fields' => array('id', 'first_name', 'last_name')
            )
        )
    );

    $Approval = new Approval;

    $approvalFilter = array(
        'fields' => array('ip', 'pr', 'da'),
        'limit' => '1',
        'order' => array(
            'Approval.added_on DESC'
        )
    );

    if ($session->read('User.user_type') == 'user') {
        //$domainFilter['where'] = "Domain.user_id='".$session->read('User.id')."'";
    }
    // print_r($filters['Domain']);
    if (!empty($filters['Domain'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Domain'] : $filters['Domain'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Domain.added_on, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Domain.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }
/*
    $sqlquery = "select Domain.*,User.first_name,User.last_name from tbl_plb_domains Domain left join tracking_login User on User.id=Domain.user_id "
            . "left join tbl_approval_links_main Main on Main.domain_id=Domain.id "
            . "left join tbl_approval_links_child Child on Child.parent_id=Main.id where Child.final_link_submit_status='1'  ";

    $sqlquerywhere = " and ";

    $sqlquerywhereall = '';
    if (!empty($_GET['_uid'])) {
        $users = base64_decode($_GET['_uid']);

        $sqlquerywhereall = ' and Domain.user_id IN (' . $users . ') ';
    }

    if (!empty($filters['Domain'])) {
        $sqlquerywhere.=$filters['Domain'];
    }

    if (empty($_GET['_dt'])) {
        if (!empty($filters['Domain'])) {
            $sqlquerywhere.="  and ";
        }
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));

        $sqlquerywhere.="DATE_FORMAT(Domain.added_on, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Domain.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }
   */
    $dbob = New DatabaseObject();
    

    $sqlquery = "select tracking_data.domain,tracking_data.payment_date,tracking_data.user_id,User.first_name,User.last_name,tbl_plb_domains.id  from tracking_data inner join tbl_plb_domains on tracking_data.domain = tbl_plb_domains.name inner join tracking_login User on tracking_data.user_id = User.id where ";
           
           
    if (empty($_GET['_dt'])) {       
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));
        $sqlquerywhere=" 	tracking_data.payment_date >= '" . $dateStart . "'  and tracking_data.payment_date<= '" . $dateEnd . "'";
    } else {
        
		list($dateFrom, $dateTo) = explode('_', base64_decode($_GET['_dt']));
        $dateStart=  $dateFrom;         
        if (!empty($dateFrom) and ! empty($dateTo)) {           
            $sqlquerywhere= " DATE_FORMAT(tracking_data.payment_date, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($dateFrom)) . "'";
            $sqlquerywhere.=" and DATE_FORMAT(tracking_data.payment_date, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($dateTo)) . "'";
        } else if (!empty($dateFrom) and empty($dateTo)) {         
            $sqlquerywhere= " DATE_FORMAT(tracking_data.payment_date, '%Y-%m-%d') = '" . date('Y-m-d', strtotime($dateFrom)) . "'";
        } 
    }
	$sqlquerywhereall=" 1=1";
	if (!empty($_GET['_uid'])) {
		$userid = base64_decode($_GET['_uid']);
		$sqlquerywhere.=" and tracking_data.user_id IN (" . $userid . ")";		//tbl_plb_domains
		$sqlquerywhereall="  tracking_data.user_id IN (" . $userid . ")";    	//tbl_plb_domains
	}
            
   //  $sqlquerywhere.=" and tbl_plb_domains.id NOT IN (select distinct domian.id from tracking_data tracking left join tbl_plb_domains domian on domian.name=tracking.domain where DATE_FORMAT(tracking.payment_date, '%Y-%m-%d') <'" . date('Y-m-d', strtotime($dateFrom)) . "')";

    $sqlquerygroup = "   group by  tracking_data.domain  order by User.first_name , User.last_name";
    //echo $sqlquery . $sqlquerywhere . $sqlquerygroup;
    if (!empty($_GET['_uid'])) {
   
		$_approvalLinks = $dbob->find_by_sqlval($sqlquery . $sqlquerywhere . $sqlquerygroup);

		
		// exit;
		//echo "<pre>";  print_r($_approvalLinks);  exit;
		// $_approvalLinks = $Domain->find_all($domainFilter);

		$isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

		if ($isExport) {
			/* if(count($_approvalLinks)>0)
			  {
			  foreach($_approvalLinks as $key=>$val)
			  {

			  $domain_id=$val['Domain']['id'];
			  $approvalFilter['where'] = "Approval.domain_id='".$domain_id."'";
			  $approvals = $Approval->fetchAll($approvalFilter);

			  $_approvalLinks[$key]['Domain']['da']=$approvals[0]['Approval']['da'];
			  $_approvalLinks[$key]['Domain']['ip']=$approvals[0]['Approval']['ip'];
			  $_approvalLinks[$key]['Domain']['pr']=$approvals[0]['Approval']['pr'];



			  }


			  } */

			//echo "<pre>";print_r($_approvalLinks);exit;
			$viewData->set('dateFrom', $dateFrom);            
			$viewData->set('uniqueblogs', $_approvalLinks);

			include_once "views/unique_blogs_export.php";
			exit;
		}
		if (!empty($_approvalLinks)) {

			$_approvalAllLinks = $dbob->find_by_sqlval($sqlquery . $sqlquerywhereall . $sqlquerygroup);
			$allData = array();
			foreach ($_approvalAllLinks as $_approvls) {
				if (isset($allData[$_approvls['user_id']])) {
					$allData[$_approvls['user_id']] += 1;
				} else {
					$allData[$_approvls['user_id']] = 1;
				}
			}

			//    print_r($allData);exit;
			$chartData = array();
			$userCount = 0;
			foreach ($_approvalLinks as $_approvls) {
				
				if(checkpriviues($_approvls['id'],$dateFrom))
				{
				//  echo "<pre>";
				//  print_r($_approvls);

				$user = trim(ucwords($_approvls['first_name'] . ' ' . $_approvls['last_name']));
				$userid = $_approvls['user_id'];
				if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
					$userCount++;
					$chartData['users'][$userCount] = $user;
				}
				$userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
				//  foreach($_approvls['Domain'] as $_approvalLink){
				$chartData['series']['Blog']['data'][$userNo] += 1;

				$chartData['series']['Total Blog']['data'][$userNo] = intval($allData[$userid]);
				//}
				}
			}

			foreach ($chartData['users'] as $k => $u) {
				if (!isset($chartData['series']['Blog']['data']) || !key_exists($k, $chartData['series']['Blog']['data'])) {
					$chartData['series']['Blog']['data'][$k] = 0;
				}
			}

			ksort($chartData['series']['Blog']['data']);
		}

	}
//exit;
    $chartData['title'] = 'Unique Blogs Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Blogs';
    __setChartData($chartData, "unique-blogs-chart");
}

function checkpriviues($id,$dateFrom) {
     $dbob = New DatabaseObject();
       $sql="select distinct domian.id from tracking_data tracking left join tbl_plb_domains domian on domian.name=tracking.domain where DATE_FORMAT(tracking.payment_date, '%Y-%m-%d') <'" . date('Y-m-d', strtotime($dateFrom)) . "' and tracking.payment_date != '0000-00-00' and domian.id='".$id."'";             
     $_approvalAllLinks = $dbob->find_by_sqlval($sql);
  
     $count=count($_approvalAllLinks);
    if($count>0)
    {
    return false;
    }
 else {
         return true; 
    }
}

/* * ** End: Added by Ajit: 15-DESC-2015  ***************** */

/* * ** Start: Added by Ajit : :29-JAN-2016  ***************** */

///////////Spend////////////
function charts_spend() {
    global $viewData, $session;
    $viewData->setTitle('Spend Chart');
    $Campaign = new Campaign;

    $filters = __chartsFilterVars("Tracking");
    $campaignsList = $Campaign->getList();

    $viewData->set('campaignsList', $campaignsList);

    if (!empty($filters['Tracking'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Tracking'] : $filters['Tracking'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }
    $sqlquery = "select Tracking.Campaign_name,Tracking.Campaign_id,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";
    $sqlquerywhere = "  ";

    if (!empty($_GET['_camp'])) {
        $camp = base64_decode($_GET['_camp']);

        $sqlquerywhereall = '  Tracking.campaign_id  IN (' . $camp . ') ';
    } else {
        $sqlquerywhereall = ' 1=1';
    }

    if (!empty($filters['Tracking'])) {
        $sqlquerywhere.=$filters['Tracking'];
    }

    if (empty($_GET['_dt'])) {

        if (!empty($filters['Tracking'])) {
            $sqlquerywhere.= "  and ";
        }

        $dateStart = date('Y-m-01');

        $dateEnd = date('Y-m-' . date('t'));

        $sqlquerywhere.="DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }
    $sqlquerygroup = "   and Tracking.Status='Paid'  group by Tracking.id order by Tracking.Campaign_name asc";
    $dbob = New DatabaseObject();
    $sqlquery . $sqlquerywhere . $sqlquerygroup;
    $spenddatas = $dbob->find_by_sqlval($sqlquery . $sqlquerywhere . $sqlquerygroup);
    //  echo "<pre>";
//   print_r($spenddatas);
//   exit;
    // $_approvalLinks = $Domain->find_all($domainFilter);

    $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

    if ($isExport) {

        $viewData->set('spenddatas', $spenddatas);

        include_once "views/spend_export.php";
        exit;
        exit;
    }

    if (!empty($spenddatas)) {




        $chartData = array();
        $userCount = 0;
        $convertedamouuntinr = get_currency('USD', 'INR', 1);
        $convertedamouuntponds = get_currency('USD', 'GBP', 1);
        $convertedamouunteuro = get_currency('USD', 'EUR', 1);
        //echo $sqlquery.$sqlquerywhereall.$sqlquerygroup;
        $_approvalAllLinks = $dbob->find_by_sqlval($sqlquery . $sqlquerywhereall . $sqlquerygroup);
        $allData = array();
        foreach ($_approvalAllLinks as $_approvls) {
            $amount = trim($_approvls['Amount']);
            $currency = trim($_approvls['Currency']);
            if ($currency == 'INR') {
                $amount = round(($amount / $convertedamouuntinr), 2);
            } elseif ($currency == 'Euro') {
                $amount = round(($amount / $convertedamouunteuro), 2);
            } elseif ($currency == 'Pounds') {
                $amount = round(($amount / $convertedamouuntponds), 2);
            } else {
                $amount = trim($_approvls['Amount']);
            }

            if (isset($allData[$_approvls['Campaign_id']])) {
                $allData[$_approvls['Campaign_id']] += $amount;
            } else {
                $allData[$_approvls['Campaign_id']] = $amount;
            }
        }
        //    print_r($allData);exit;

        foreach ($spenddatas as $_spend) {


            //  echo "<pre>";
            //  print_r($_spend);

            $campaign = trim($_spend['Campaign_name']);
            $currency = trim($_spend['Currency']);
            $amount = trim($_spend['Amount']);
            if ($currency == 'INR') {
                $amount = round(($amount / $convertedamouuntinr), 2);
            } elseif ($currency == 'Euro') {
                $amount = round(($amount / $convertedamouunteuro), 2);
            } elseif ($currency == 'Pounds') {
                $amount = round(($amount / $convertedamouuntponds), 2);
            } else {
                $amount = trim($_spend['Amount']);
            }


            $campaign_id = trim($_spend['Campaign_id']);
            if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                $userCount = $userCount + $amount;
                $chartData['users'][$userCount] = $campaign;
            }
            $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;
            //  foreach($_approvls['Domain'] as $_approvalLink){
            $chartData['series']['Amount']['data'][$userNo] += $amount;
            $chartData['series']['Total Amount']['data'][$userNo] = $allData[$campaign_id];
            //}
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Amount']['data']) || !key_exists($k, $chartData['series']['Amount']['data'])) {
                $chartData['series']['Amount']['data'][$k] = 0;
            }
        }

//print_r($chartData);
        //exit;
// echo "//////////dddddddddddasfasfasfasfasf/////////";
        //asort($chartData['users']);
    }

    $chartData['title'] = 'Spend Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Amount';

    __setChartData($chartData, "spend-chart");
}

///////////Spend by Campaign////////////
function charts_spend_campaign() {
    global $viewData, $session;
    $viewData->setTitle('Spend By Campaign  Chart ');
    $User = new User;
    $Campaign = new Campaign;

    $filters = __chartsFilterVars("Tracking");

    $userList = $User->getList();

    $viewData->set('usersList', $userList);
    $campaignsList = $Campaign->getList();
    $viewData->set('campaignsList', $campaignsList);

    if (!empty($filters['Tracking'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Tracking'] : $filters['Tracking'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }



    $sqlquery = "select Tracking.Campaign_name,Tracking.Amount,Tracking.da_value,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";

    if (!empty($filters['Tracking'])) {
        $sqlquery.=$filters['Tracking'];
    }

    if (empty($_GET['_dt'])) {

        if (!empty($filters['Tracking'])) {
            $sqlquery.= "  and ";
        }

        $dateStart = date('Y-m-01');

        $dateEnd = date('Y-m-' . date('t'));

        $sqlquery.="DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }

    $sqlquery.="   and Tracking.Status='Paid' ";



    $sqlquery.="order by USER.first_name asc,USER.last_name asc";
    if (!empty($_GET['_camp'])) {


        $dbob = New DatabaseObject();
        $spenddatas = $dbob->find_by_sqlval($sqlquery);
        
         $convertedamouuntinr = get_currency('USD', 'INR', 1);
            $convertedamouuntponds = get_currency('USD', 'GBP', 1);
            $convertedamouunteuro = get_currency('USD', 'EUR', 1);
        $isExportChart = base64_decode($_REQUEST['_exportchart']) == 1 ? true : false;
         
          if ($isExportChart) {

            $viewData->set('spenddatas', $spenddatas);

            include_once "views/spend_campaign_exportchart.php";
            exit;
            exit;
        }
        $isExportDetail = base64_decode($_REQUEST['_exportDetail']) == 1 ? true : false;
         
          if ($isExportDetail) {

            $viewData->set('spenddatas', $spenddatas);

            include_once "views/spend_campaign_exportchartWithoutDetail.php";
            exit;
            exit;
        }



        //  echo "<pre>";
//   print_r($spenddatas);
//   exit;
        // $_approvalLinks = $Domain->find_all($domainFilter);

        if (!empty($_GET['_camp'])) {
            $campaignid = base64_decode($_GET['_camp']);
        }

        $campaigndata = $Campaign->find_all(array('where' => "Campaign.id in ({$campaignid})"));
        $compainnamearry = array();
        $compainname = '';
        foreach ($campaigndata as $comapin) {
            if ($k > 0) {
                $compainname.=' , ';
            }
            $compainname.=$comapin['Campaign']['name'];
            $compainnamearry[] = trim($comapin['Campaign']['name']);
            $k++;
        }

        if (!empty($_GET['_comdt'])) {

            $sqlquery = "select Tracking.Campaign_name,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";

            $sqlquery.=" Tracking.campaign_id IN (" . $campaignid . ")";

            if (!empty($_GET['_uid'])) {
                $userid = base64_decode($_GET['_uid']);
                $sqlquery.=" and Tracking.user_id IN (" . $userid . ")";
            }

            list($comdateFrom, $comdateTo) = explode('_', base64_decode($_GET['_comdt']));
            $viewData->set('comdateFrom', $comdateFrom);
            $viewData->set('comdateTo', $comdateTo);
            $sqlquery.=" and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($comdateFrom)) . "' AND DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($comdateTo)) . "' ";
            $sqlquery.="   and Tracking.Status='Paid' ";



            $sqlquery.="order by USER.first_name asc,USER.last_name asc";
            // echo "<br>";

            $spenddatascom = $dbob->find_by_sqlval($sqlquery);
        }


        $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

        if ($isExport) {

            $viewData->set('spenddatas', $spenddatas);
            $viewData->set('spenddatascom', $spenddatascom);

            include_once "views/spend_campaign_export.php";
            exit;
            exit;
        }
        if (!empty($spenddatas)) {
            $userCount = 0;
            $chartData = array();
            

            //   $chartData = array();
            // $userCount = 0;
            if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                $set1 = 'P1->';
            } else {
                $set1 = '';
            }
            $userblogdata = array();


            foreach ($spenddatas as $_spend) {
                //  echo "<pre>";
                // print_r($_spend);

                $campaign = trim($_spend['first_name'] . ' ' . $_spend['last_name']);
                $currency = trim($_spend['Currency']);
                $amount = trim($_spend['Amount']);
                $campaign_name = trim($_spend['Campaign_name']);
                if ($currency == 'INR') {
                    $amount = round(($amount / $convertedamouuntinr), 2);
                } elseif ($currency == 'Euro') {
                    $amount = round(($amount / $convertedamouunteuro), 2);
                } elseif ($currency == 'Pounds') {
                    $amount = round(($amount / $convertedamouuntponds), 2);
                } else {
                    $amount = trim($_spend['Amount']);
                }



                if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                    //  $userCount = $userCount + $amount;
                    $userCount ++;
                    $chartData['users'][$userCount] = $campaign;
                }
                $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;
                //  foreach($_approvls['Domain'] as $_approvalLink){

                if (isset($chartData['series'][$set1 . 'Total']['data'][$userNo])) {
                    $chartData['series'][$set1 . 'Total']['data'][$userNo] += (int) $amount;
                } else {
                    $chartData['series'][$set1 . 'Total']['data'][$userNo] = (int) $amount;
                }
                if (isset($chartData['series'][$set1 . $campaign_name]['data'][$userNo])) {
                    $chartData['series'][$set1 . $campaign_name]['data'][$userNo] += (int) $amount;
                } else {
                    $chartData['series'][$set1 . $campaign_name]['data'][$userNo] = (int) $amount;
                }
                if (isset($userblogdata[$campaign])) {
                    $userblogdata[$campaign] = $userblogdata[$campaign] + 1;
                } else {
                    $userblogdata[$campaign] = 1;
                }



                //}
            }


            if (!empty($spenddatascom)) {
                $set2 = 'P2->';
                $userblogcomdata = array();

                foreach ($spenddatascom as $_spend) {
                    //  echo "<pre>";
                    // print_r($_spend);

                    $campaign = trim($_spend['first_name'] . ' ' . $_spend['last_name']);
                    $currency = trim($_spend['Currency']);
                    $amount = trim($_spend['Amount']);
                    $campaign_name = trim($_spend['Campaign_name']);
                    if ($currency == 'INR') {
                        $amount = round(($amount / $convertedamouuntinr), 2);
                    } elseif ($currency == 'Euro') {
                        $amount = round(($amount / $convertedamouunteuro), 2);
                    } elseif ($currency == 'Pounds') {
                        $amount = round(($amount / $convertedamouuntponds), 2);
                    } else {
                        $amount = trim($_spend['Amount']);
                    }



                    if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                        $userCount ++;
                        $chartData['users'][$userCount] = $campaign;
                    }
                    $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;


                    if (isset($chartData['series'][$set2 . 'Total' . $setend]['data'][$userNo])) {
                        $chartData['series'][$set2 . 'Total' . $setend]['data'][$userNo] += (int) $amount;
                    } else {
                        $chartData['series'][$set2 . 'Total' . $setend]['data'][$userNo] = (int) $amount;
                    }
                    if (isset($chartData['series'][$set2 . $campaign_name . $setend]['data'][$userNo])) {
                        $chartData['series'][$set2 . $campaign_name . $setend]['data'][$userNo] += (int) $amount;
                    } else {
                        $chartData['series'][$set2 . $campaign_name . $setend]['data'][$userNo] = (int) $amount;
                    }

                    if (isset($userblogcomdata[$campaign])) {
                        $userblogcomdata[$campaign] = $userblogcomdata[$campaign] + 1;
                    } else {
                        $userblogcomdata[$campaign] = 1;
                    }
                }
            }



            //print_r($chartData['users']);exit;

            foreach ($chartData['users'] as $k => $u) {
                if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                    if ($userblogcomdata[$u] <> "") {
                        $concompuserdata = '-' . $userblogcomdata[$u];
                    } else {
                        $concompuserdata = '-0';
                    }
                }
                if ($userblogdata[$u] <> "") {
                    $userblogdataval = $userblogdata[$u];
                } else {
                    $userblogdataval = '0';
                }
                $chartData['users'][$k] = $u . '(' . $userblogdataval . $concompuserdata . ')';

                if (!isset($chartData['series'][$set1 . 'Total']['data']) || !key_exists($k, $chartData['series'][$set1 . 'Total']['data'])) {
                    $chartData['series'][$set1 . 'Total']['data'][$k] = 0;
                }
                if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                    if (!isset($chartData['series'][$set2 . 'Total' . $setend]['data']) || !key_exists($k, $chartData['series'][$set2 . 'Total' . $setend]['data'])) {
                        $chartData['series'][$set2 . 'Total' . $setend]['data'][$k] = 0;
                    }
                }
                foreach ($compainnamearry as $name) {
                    if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                        if ((!isset($chartData['series'][$set2 . $name . $setend]['data']) || !key_exists($k, $chartData['series'][$set2 . $name . $setend]['data'])) && ($name <> "")) {
                            $chartData['series'][$set2 . $name . $setend]['data'][$k] = 0;
                        }
                    }

                    if ((!isset($chartData['series'][$set1 . $name]['data']) || !key_exists($k, $chartData['series'][$set1 . $name]['data'])) && ($name <> "")) {
                        $chartData['series'][$set1 . $name]['data'][$k] = 0;
                    }
                }
            }
            foreach ($compainnamearry as $name) {

                if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                    ksort($chartData['series'][$set2 . $name . $setend]['data']);
                }
                ksort($chartData['series'][$set1 . $name]['data']);
            }

            if (!empty($spenddatascom)) {
                ksort($chartData['series'][$set2 . 'Total' . $setend]['data']);
            }
            ksort($chartData['series'][$set1 . 'Total']['data']);
        }
        //echo "<pre>";
        //  print_r($userblogdata);
        // print_r($chartData['users']);
     //   print_r($chartData);
        // exit;
        //    $k = 0;

        $chartData['title'] = 'Spend Chart by ' . $compainname;
        $subtitle=$set1. date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
        if($set2<>"") $subtitle.=' and '.$set2. date('F j, Y', strtotime($comdateFrom)) . (!empty($comdateTo) ? ' to ' . date('F j, Y', strtotime($comdateTo)) : '');
        $chartData['subtitle'] =$subtitle;
        $chartData['yAxis']['title'] = 'Amount';




        __setChartData($chartData, "spend-chart");
    }
}

///////////Spend by Campaign////////////
function charts_spend_user() {
    global $viewData, $session;
    $viewData->setTitle('Spend By User  Chart ');
    $User = new User;
    $Campaign = new Campaign;

    $filters = __chartsFilterVars("Tracking");

    $userList = $User->getList();

    $viewData->set('userList', $userList);
    $campaignsList = $Campaign->getList();
    $viewData->set('campaignsList', $campaignsList);


    if (!empty($filters['Tracking'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Tracking'] : $filters['Tracking'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }



    $sqlquery = "select Tracking.Campaign_name,Tracking.da_value,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";

    if (!empty($filters['Tracking'])) {
        $sqlquery.=$filters['Tracking'];
    }

    if (empty($_GET['_dt'])) {

        if (!empty($filters['Tracking'])) {
            $sqlquery.= "  and ";
        }

        $dateStart = date('Y-m-01');

        $dateEnd = date('Y-m-' . date('t'));

        $sqlquery.="DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }




    $sqlquery.="  and Tracking.Status='Paid'";



    $sqlquery.="order by Tracking.Campaign_name ";
    if (!empty($_GET['_uid'])) {


        $dbob = New DatabaseObject();
        $spenddatas = $dbob->find_by_sqlval($sqlquery);
        
          $convertedamouuntinr = get_currency('USD', 'INR', 1);
            $convertedamouuntponds = get_currency('USD', 'GBP', 1);
            $convertedamouunteuro = get_currency('USD', 'EUR', 1);
        $isExportChart = base64_decode($_REQUEST['_exportchart']) == 1 ? true : false;
         
          if ($isExportChart) {

            $viewData->set('spenddatas', $spenddatas);

            include_once "views/spend_user_exportchart.php";
            exit;
            exit;
        }
        $isExportDetail = base64_decode($_REQUEST['_exportDetail']) == 1 ? true : false;
         
          if ($isExportDetail) {

            $viewData->set('spenddatas', $spenddatas);

            include_once "views/spend_user_exportchartWithoutDetail.php";
            exit;
            exit;
        }
        //  echo "<pre>";
//   print_r($spenddatas);
//   exit;
        // $_approvalLinks = $Domain->find_all($domainFilter);
        if (!empty($_GET['_uid'])) {
            $userid = base64_decode($_GET['_uid']);
        }


        $userdata = $User->find_all(array('where' => "User.id in ({$userid})"));

        $usernamearry = array();
        $username = '';
        foreach ($userdata as $user) {
            if ($k > 0) {
                $username.=' , ';
            }
            $username.=$user['User']['first_name'] . ' ' . $user['User']['last_name'];
            $usernamearry[] = trim($user['User']['first_name'] . ' ' . $user['User']['last_name']);
            $k++;
        }


        $viewData->set('userdata', $userdata);
       
        
          if (!empty($_GET['_comdt'])) {

            $sqlquery = "select Tracking.Campaign_name,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";
            
             if (!empty($_GET['_camp'])) {
                  $campaignid = base64_decode($_GET['_camp']);
                     $sqlquery.=" Tracking.campaign_id IN (" . $campaignid . ")";
             }

         

          
                $sqlquery.=" and Tracking.user_id IN (" . $userid . ")";
            

            list($comdateFrom, $comdateTo) = explode('_', base64_decode($_GET['_comdt']));
            $viewData->set('comdateFrom', $comdateFrom);
            $viewData->set('comdateTo', $comdateTo);
            $sqlquery.=" and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($comdateFrom)) . "' AND DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($comdateTo)) . "' ";
            $sqlquery.="   and Tracking.Status='Paid' ";



            $sqlquery.="order by USER.first_name asc,USER.last_name asc";
            // echo "<br>";

            $spenddatascom = $dbob->find_by_sqlval($sqlquery);
        }

        $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

        if ($isExport) {

            $viewData->set('spenddatas', $spenddatas);

            include_once "views/spend_user_export.php";
            exit;
            exit;
        }
        
         
        
        
            
            
            
            
            
            

        if (!empty($spenddatas)) {
            $chartData = array();
            $userCount = 0;
                     if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                $set1 = 'P1->';
            } else {
                $set1 = '';
            }
              $userblogdata = array();
          

            foreach ($spenddatas as $_spend) {
                //  echo "<pre>";
                //   print_r($_spend);

                $campaign = trim($_spend['Campaign_name']);
                $currency = trim($_spend['Currency']);
                $amount = trim($_spend['Amount']);
                $user_name = trim($_spend['first_name'] . ' ' . $_spend['last_name']);

                if ($currency == 'INR') {
                    $amount = round(($amount / $convertedamouuntinr), 2);
                } elseif ($currency == 'Euro') {
                    $amount = round(($amount / $convertedamouunteuro), 2);
                } elseif ($currency == 'Pounds') {
                    $amount = round(($amount / $convertedamouuntponds), 2);
                } else {
                    $amount = trim($_spend['Amount']);
                }



                if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                    $userCount ++;
                    $chartData['users'][$userCount] = $campaign;
                }
                $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;
                //  foreach($_approvls['Domain'] as $_approvalLink){
                if (isset($chartData['series'][$set1 .'Total']['data'][$userNo])) {
                    $chartData['series'][$set1 .'Total']['data'][$userNo] += $amount;
                } else {
                    $chartData['series'][$set1 .'Total']['data'][$userNo] = $amount;
                }

                if (isset($chartData['series'][$set1 .$user_name]['data'][$userNo])) {
                    $chartData['series'][$set1 .$user_name]['data'][$userNo] += $amount;
                } else {
                    $chartData['series'][$set1 .$user_name]['data'][$userNo] = $amount;
                }
                     if (isset($userblogdata[$campaign])) {
                    $userblogdata[$campaign] = $userblogdata[$campaign] + 1;
                } else {
                    $userblogdata[$campaign] = 1;
                }
                //}
            }

            
            if (!empty($spenddatascom)) {
                $set2 = 'P2->';
                $userblogcomdata = array();

                foreach ($spenddatascom as $_spend) {
                    //  echo "<pre>";
                    // print_r($_spend);
                     $campaign = trim($_spend['Campaign_name']);
                  
                    $currency = trim($_spend['Currency']);
                    $amount = trim($_spend['Amount']);
                   
                     $user_name = trim($_spend['first_name'] . ' ' . $_spend['last_name']);
                    if ($currency == 'INR') {
                        $amount = round(($amount / $convertedamouuntinr), 2);
                    } elseif ($currency == 'Euro') {
                        $amount = round(($amount / $convertedamouunteuro), 2);
                    } elseif ($currency == 'Pounds') {
                        $amount = round(($amount / $convertedamouuntponds), 2);
                    } else {
                        $amount = trim($_spend['Amount']);
                    }



                    if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                        $userCount ++;
                        $chartData['users'][$userCount] = $campaign;
                    }
                    $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;


                    if (isset($chartData['series'][$set2 . 'Total' . $setend]['data'][$userNo])) {
                        $chartData['series'][$set2 . 'Total' . $setend]['data'][$userNo] += (int) $amount;
                    } else {
                        $chartData['series'][$set2 . 'Total' . $setend]['data'][$userNo] = (int) $amount;
                    }
                    if (isset($chartData['series'][$set2 . $user_name . $setend]['data'][$userNo])) {
                        $chartData['series'][$set2 . $user_name . $setend]['data'][$userNo] += (int) $amount;
                    } else {
                        $chartData['series'][$set2 . $user_name . $setend]['data'][$userNo] = (int) $amount;
                    }

                    if (isset($userblogcomdata[$campaign])) {
                        $userblogcomdata[$campaign] = $userblogcomdata[$campaign] + 1;
                    } else {
                        $userblogcomdata[$campaign] = 1;
                    }
                }
            }

            
            foreach ($chartData['users'] as $k => $u) {
                if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                    if ($userblogcomdata[$u] <> "") {
                        $concompuserdata = '-' . $userblogcomdata[$u];
                    } else {
                        $concompuserdata = '-0';
                    }
                }
                if ($userblogdata[$u] <> "") {
                    $userblogdataval = $userblogdata[$u];
                } else {
                    $userblogdataval = '0';
                }
                $chartData['users'][$k] = $u . '(' . $userblogdataval . $concompuserdata . ')';
                if (!isset($chartData['series'][$set1 . 'Total']['data']) || !key_exists($k, $chartData['series'][$set1 . 'Total']['data'])) {
                    $chartData['series'][$set1 . 'Total']['data'][$k] = 0;
                }
                foreach ($usernamearry as $name) {
                     if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                        if ((!isset($chartData['series'][$set2 . $name . $setend]['data']) || !key_exists($k, $chartData['series'][$set2 . $name . $setend]['data'])) && ($name <> "")) {
                            $chartData['series'][$set2 . $name . $setend]['data'][$k] = 0;
                        }
                    }
                    if (!isset($chartData['series'][$set1 .$name]['data']) || !key_exists($k, $chartData['series'][$set1 .$name]['data'])) {
                        $chartData['series'][$set1 .$name]['data'][$k] = 0;
                    }
                }
            }
            foreach ($usernamearry as $name) {
                if (!empty($spenddatascom) && (count($spenddatascom) > 0)) {
                    ksort($chartData['series'][$set2 . $name . $setend]['data']);
                }
                ksort($chartData['series'][$set1 .$name]['data']);
            }
             if (!empty($spenddatascom)) {
                ksort($chartData['series'][$set2 . 'Total' . $setend]['data']);
            }
            ksort($chartData['series'][$set1 .'Total']['data']);
        }
        $chartData['title'] = 'Spend Chart by ' . $username;
         $subtitle=$set1. date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
        if($set2<>"") $subtitle.=' and '.$set2. date('F j, Y', strtotime($comdateFrom)) . (!empty($comdateTo) ? ' to ' . date('F j, Y', strtotime($comdateTo)) : '');
        $chartData['subtitle'] =$subtitle;
        $chartData['yAxis']['title'] = 'Amount';




        __setChartData($chartData, "spend-chart");
    }
}

function get_currency($from_Currency, $to_Currency, $amount) {
    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);

    $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

    $ch = curl_init();
    $timeout = 0;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode('bld>', $rawdata);
    $data = explode($to_Currency, $data[1]);

    return round($data[0], 2);
}

/* * ** End: Added by Ajit:29-JAN-2016  ***************** */

function charts_payment() {
    global $viewData, $database;
    $viewData->setTitle('Payments Chart');
    $Payment = new Payment;
    $User = new User;

    $filters = __chartsFilterVars('Payment');

    $paymentFilters = "Payment.status IN ('Paid', 'Underprocess','Rejected')";
    if (!empty($filters['Payment'])) {
        $paymentFilters = !empty($paymentFilters) ? $paymentFilters . ' AND ' . $filters['Payment'] : $filters['Payment'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));
        $paymentFilters = "DATE_FORMAT(Payment.added_on, '%Y-%m-%d') >= '" . $dateStart . "'";
        $paymentFilters .= " AND ";
        $paymentFilters .= "DATE_FORMAT(Payment.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }

    $paymentQuery = $database->query("SELECT COUNT(Payment.id) AS links, Payment.status AS status,  CONCAT(User.first_name, ' ', User.last_name) AS name 
FROM " . $Payment->table_name . " Payment INNER JOIN " . $User->table_name . " User ON(User.id=Payment.user_id)
 WHERE  " . $paymentFilters . " GROUP BY Payment.status, Payment.user_id");
    $payments = $database->fetch_data_array($paymentQuery);
    $chartData = array();
    $userCount = 0;
    if (!empty($payments)) {
        foreach ($payments as $payment) {
            $user = trim(ucwords($payment['name']));
            if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
                $userCount++;
                $chartData['users'][$userCount] = $user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;

            switch ($payment['status']) {
                case 'Underprocess':
                    $chartData['series']['Pending']['data'][$userNo] = (int) $payment['links'];
                    break;
                case 'Rejected':
                    $chartData['series']['Rejected']['data'][$userNo] = (int) $payment['links'];
                    break;
                case 'Paid':
                    $chartData['series']['Done']['data'][$userNo] = (int) $payment['links'];
                    break;
                default:
                    break;
            }
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Done']['data']) || !key_exists($k, $chartData['series']['Done']['data'])) {
                $chartData['series']['Done']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Rejected']['data']) || !key_exists($k, $chartData['series']['Rejected']['data'])) {
                $chartData['series']['Rejected']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Pending']['data']) || !key_exists($k, $chartData['series']['Pending']['data'])) {
                $chartData['series']['Pending']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Done']['data']);
        ksort($chartData['series']['Rejected']['data']);
        ksort($chartData['series']['Pending']['data']);
    }
    $chartData['title'] = 'Payment Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Links';
    __setChartData($chartData, "payments-chart");
}


/*Nitin - added on 13-07-2016*/

function charts_influencer() {
    global $viewData, $database;
    $viewData->setTitle('Influencers Chart');

    $Influencer = new Influencer;
   
    $filters = __chartsFilterVars('Influencers');

    $inFilters = "";
    if (!empty($filters['Influencers'])) {
        //var_dump($filters['Influencers']);
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));
    }
    
        $inFilters = "DATE_FORMAT(added_on, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($dateStart)) . "'";
        $inFilters .= " AND ";
        $inFilters .= "DATE_FORMAT(added_on, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($dateEnd)) . "'";
    
    $add_sql = " WHERE $inFilters ";
    
    $query = "SELECT COUNT(". $Influencer->table_name .".id) AS total, category FROM " . $Influencer->table_name . " $add_sql GROUP BY " . $Influencer->table_name . ".category";
    $influencerQuery = $database->query($query);
    $influencers = $database->fetch_data_array($influencerQuery);
    //var_dump($influencers);die;
    
     if (!empty($influencers)) {
        $chartData = array();
        $iCount = 0;
        foreach ($influencers as $influencer) {
            
            $categories = array('Technology','Finance','Business','Fashion','Travel','Health','Mommy');
            
            $user = $categories[$influencer['category']];
            //var_dump($user);
            if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
                $iCount++;
                $chartData['users'][$iCount] = $user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
            
            switch ($influencer['category']) {
                case '0':
                    $chartData['series']['Technology']['data'][$userNo] = (int) $influencer['total'];
                    break;
                case '1':
                    $chartData['series']['Finance']['data'][$userNo] = (int) $influencer['total'];
                    break;
                case '2':
                    $chartData['series']['Business']['data'][$userNo] = (int) $influencer['total'];
                    break;
                case '3':
                    $chartData['series']['Fashion']['data'][$userNo] = (int) $influencer['total'];
                    break;
                case '4':
                    $chartData['series']['Travel']['data'][$userNo] = (int) $influencer['total'];
                    break;
                case '5':
                    $chartData['series']['Health']['data'][$userNo] = (int) $influencer['total'];
                    break;
                case '6':
                    $chartData['series']['Mommy']['data'][$userNo] = (int) $influencer['total'];
                    break;
                default:
                    break;
            }
            
        }

        ksort($chartData['series']['Technology']['data']);
        ksort($chartData['series']['Finance']['data']);
        ksort($chartData['series']['Business']['data']);
        ksort($chartData['series']['Fashion']['data']);
        ksort($chartData['series']['Travel']['data']);
        ksort($chartData['series']['Health']['data']);
        ksort($chartData['series']['Mommy']['data']);

   $chartData['title'] = 'Influencers Chart';
   $chartData['subtitle'] =  'Category-wise'; // $categories[$influencer['category']];
   
   $chartData['yAxis']['title'] = 'Influencers Count';
    __setChartData($chartData, "influencers-chart");
    }
}


function __setChartData($chartData, $where = '') {
    global $viewData;
    $chart = new Highchart();
    $chart->chart->renderTo = $where;
    $chart->credits->enabled = false;
    $chart->chart->type = "column";
    $chart->title->text = $chartData['title'];
    $chart->subtitle->text = $chartData['subtitle'];

    $chart->xAxis->categories = !empty($chartData['users']) ? array_values($chartData['users']) : array();

    $chart->yAxis->min = 0;
    $chart->yAxis->title->text = $chartData['yAxis']['title'];
    $chart->tooltip->headerFormat = '<span style="font-size:10px">{point.key}</span><table>';
    $chart->tooltip->pointFormat = '<tr><td style="color:{series.color};padding:0">{series.name}: </td> <td style="padding:0"><b>{point.y}</b></td></tr>';
    $chart->tooltip->footerFormat = '</table>';
    $chart->tooltip->shared = true;
    $chart->tooltip->useHTML = true;

    $chart->plotOptions->column->pointPadding = 0.2;
    $chart->plotOptions->column->borderWidth = 0;
    if (!empty($chartData['series'])) {
        foreach ($chartData['series'] as $k => $v) {
            $chart->series[] = array(
                'name' => $k,
                'data' => array_values($v['data'])
            );
        }
    }
    $viewData->set('chart', $chart);
}

function __chartsFilterVars($model = 'ApprovalLink') {
    global $viewData;

    $filter = array();
    $filterVars = array();
    $field = "$model.";
    switch ($model) {
        case 'ApprovalLink':
            $field .= 'added_on';
            break;
        case 'Payment':
            $field .= 'added_on';
            break;
        case 'Domain':
            $field .= 'added_on';
            break;
        case 'Tracking':
            $field .= 'payment_date';

            break;
    }
    if (!empty($_GET['_dt'])) {
        list($dateFrom, $dateTo) = explode('_', base64_decode($_GET['_dt']));

        if (!empty($dateFrom) and ! empty($dateTo)) {
            $filterVars['dateFrom'] = $dateFrom;
            $filterVars['dateTo'] = $dateTo;
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($dateFrom)) . "'";
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($dateTo)) . "'";
        } else if (!empty($dateFrom) and empty($dateTo)) {
            $filterVars['dateFrom'] = $dateFrom;
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') = '" . date('Y-m-d', strtotime($dateFrom)) . "'";
        }
    }
    if (!empty($_GET['_camp'])) {
        $campigns = base64_decode($_GET['_camp']);
        $filterVars['campaigns'] = explode(',', $campigns);
        $filter[$model][] = $model . '.campaign_id IN (' . $campigns . ') ';
    }
    if (!empty($_GET['_uid'])) {
        $users = base64_decode($_GET['_uid']);
        $filterVars['users'] = explode(',', $users);
        $filter[$model][] = $model . '.user_id IN (' . $users . ') ';
    }
    $filters[$model] = $filter[$model] ? implode(' AND ', $filter[$model]) : '';
    $viewData->set('filterVars', $filterVars);
    $viewData->set('dateFrom', $dateFrom);
    if (strtotime($dateFrom) != strtotime($dateTo)) {
        $viewData->set('dateTo', $dateTo);
    }
    return $filters;
}

// auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'approval';
if (function_exists('charts_' . $action)) {
    call_user_func('charts_' . $action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'charts_' . $_GET['act'] . '.php' : 'charts_approval.php';
include "views/default.php";
?>