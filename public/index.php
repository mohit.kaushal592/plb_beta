<?php
 require_once("../includes/initialize.php"); 
 //require_once(realpath(dirname(__FILE__) . '/../includes/initialize.php'));

 // include dashboard view template
$useLayout = 'dashboard.php';
 
 if (!$session->is_logged_in()) { redirect_to("login.php"); }
 function initPage(){
    global $useLayout, $viewData;
    $redirectedUrl = explode('/', $_SERVER['REDIRECT_URL']);
    $selfUrl = explode('/', $_SERVER['PHP_SELF']); 
    if(in_array('public', $redirectedUrl) and !in_array('index.php', $redirectedUrl) and in_array('index.php', $selfUrl)){
        $viewData->setTitle('Error 404 - Page not Found');
        $useLayout = 'error404.php';
        header("HTTP/1.1 404 Not Found");
        return false;
    } 
    // if($_SESSION['User']['user_type'] == 'superadmin'){ 
	// 	redirect_to("new_dashboard.php");
	// }
    return true;
 }
  
 function index_dashboard(){
    global $viewData, $session;
    $viewData->setTitle('Dashboard');
    $approvalCount['pending'] = __getApprovalCountByStatus('-1');
    if($session->read('User.user_type')=='user'){
     $approvalCount['approved'] = __getApprovalCountByStatus('1');
     $approvalCount['rejected'] = __getApprovalCountByStatus('0');
     $approvalCount['total'] = array_sum($approvalCount);
    }
    $viewData->set('approvalCount', $approvalCount);
 }
 
 function index_approval_list(){
    global $session, $viewData;
    $Approval =new Approval;
    $approvalsConditions = array(
        'contain'=>array('ApprovalLink','User'),
        'order'=>array('Approval.added_on DESC'),
        'limit'=>'0, 10'
    );
    if($session->read('User.user_type')=='user'){
        $approvalsConditions['where'] = 'Approval.user_id='. $session->read('User.id');
    }
    $approvals = $Approval->fetchAll($approvalsConditions);
    $viewData->set('approvals', $approvals);
    include_once "views/index_approval_list.php";
    exit;
 }
 
 function index_link_submission(){
    global $session, $viewData;
    $Payment = new Payment;
    $Currency = new Currency;
    $links = $Payment->find_all(array(
       'where'=>"Payment.user_id='".$session->read('User.id')."' AND (Payment.added_on BETWEEN DATE_SUB( NOW() , INTERVAL 12 MONTH ) AND NOW())",
       'fields'=>array('id','amount','currency','pr_value','da_value','added_on')
    ));
    
    if(!empty($links)){
        $submittedLinks = array();
        foreach($links as $link){
            $submitOn = date('Y-m', strtotime($link['Payment']['added_on']));
            $submittedLinks[$submitOn]['da'][] = $link['Payment']['da_value'];
            $submittedLinks[$submitOn]['amount'][] = $Currency->calcPrice($link['Payment']['amount'], $link['Payment']['currency']);
            $submittedLinks[$submitOn]['links'] += 1;
        }
    }
    $viewData->set('submittedLinks', $submittedLinks);
    include_once "views/index_link_submission.php";
    exit;
 }
 
 function index_pie_graph_approval(){
    global $session, $viewData;
    $approvalCount['approved'] = __getApprovalCountByStatus('1');
    $approvalCount['rejected'] = __getApprovalCountByStatus('0');
    $approvalCount['pending'] = __getApprovalCountByStatus('-1');
    $approvalsTotal = array_sum($approvalCount);
    foreach($approvalCount as $k=>$v){
        $approvalCount[$k] = $approvalsTotal>0? round(round($v/$approvalsTotal, 4)*100,2) : 0;
    }
    echo json_encode($approvalCount);
    exit;
 }
 
 function index_bar_graph_payment(){
    global $session, $viewData, $database;
    $approvalCondByUser ='';
    if($session->read('User.user_type')=='user'){
        $approvalCondByUser = " AND user_id='".$session->read('User.id')."'";
    }
    $Payment = new Payment;
    $approval= array();
    // Payment paid
    $approvalsQuery = $database->query("SELECT COUNT(id) as links, campaign_name FROM ".$Payment->table_name." WHERE status = 'Paid' AND (added_on BETWEEN DATE_SUB( NOW() , INTERVAL 12 MONTH ) AND NOW())".$approvalCondByUser." GROUP BY campaign_name ORDER BY added_on DESC limit 0, 10");
    $approvalsPaid = $database->fetch_data_array($approvalsQuery);
    if(!empty($approvalsPaid)){
        foreach($approvalsPaid as $_paid){
            $approval[$_paid['campaign_name']]['done'] = $_paid['links'];
        }
    }
    
    // Payment Pending
    $approvalsPendingQ = $database->query("SELECT COUNT(id) as links, campaign_name FROM ".$Payment->table_name." WHERE status = 'Underprocess' AND (added_on BETWEEN DATE_SUB( NOW() , INTERVAL 12 MONTH ) AND NOW())".$approvalCondByUser." GROUP BY campaign_name ORDER BY added_on DESC limit 0, 10");
    $approvalsPending = $database->fetch_data_array($approvalsPendingQ);
    if(!empty($approvalsPending)){
        foreach($approvalsPending as $_pending){
            $approval[$_pending['campaign_name']]['pending'] = $_pending['links'];
        }
    }
        
    echo json_encode($approval);
    exit;
 }
 
 function __getApprovalCountByStatus($status){
  global $session;
  if(in_array($status, array(-1,0,1))){
    $ApprovalLink =new ApprovalLink;
    $ApprovalCountCond['where'][] = "ApprovalLink.approve_status = '{$status}'";
    if($session->read('User.user_type')=='user'){
        $ApprovalCountCond['contain']= array('Approval'=>array('where'=>"Approval.user_id='".$session->read('User.id')."'"));
    }
    $ApprovalCountCond['where'] = join(' AND ', $ApprovalCountCond['where']);
    $ApprovalCount = $ApprovalLink->count($ApprovalCountCond);
    return $ApprovalCount;
  }
  return $status;
 }
 
if(initPage()){ 
    // auto call function related to page if exists
    $action = isset($_GET['act']) ? $_GET['act'] : 'dashboard';
    if(function_exists('index_'.$action)){ 
     call_user_func('index_'.$action);
    }
    // include default template
    $useLayout = isset($_GET['act']) ? 'index_'.$_GET['act'].'.php' : 'dashboard.php';
    
} 
include "views/default.php";
?>