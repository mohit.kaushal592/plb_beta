<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }
 
 function approvals_index(){
  global $viewData;
  $viewData->setTitle('Approval Listing');
  $Approval = new Approval;
  $Campaign = new Campaign;
  $User = new User;
  $filters = __aprrovalsFilterVars(); 
  $approvalFilter = array(
     'contain'=>array(
        'ApprovalLink'=>array(),
        'User'
        ),
     'order'=>array(
        'ApprovalLink.added_on DESC'
        )
     );
  if(!empty($filters['Approval'])){
   $approvalFilter['where'] = !empty($approvalFilter['where']) ? $approvalFilter['where'].' AND '. $filters['Approval'] : $filters['Approval'];
  }
  if(!empty($filters['ApprovalLink'])){
   $approvalFilter['contain']['ApprovalLink']['where'] = !empty($approvalFilter['contain']['ApprovalLink']['where']) ? $approvalFilter['contain']['ApprovalLink']['where'].' AND '. $filters['ApprovalLink'] : $filters['ApprovalLink'];
  }
  
  $isExport = base64_decode($_REQUEST['_export'])==1? true : false;
  
  $approvals = $Approval->fetchAll($approvalFilter, !$isExport);
  $viewData->set('approvals', $approvals);
  if($isExport){
   include_once "views/approvals_export.php";
   exit;
  }
  
  $viewData->set('daOptions', $Approval->daFilterOptions['list']);
  $viewData->set('campaignsList', $Campaign->getList());
  $viewData->set('usersList', $User->getList());
 }
 
 function approvals_add_link(){
  global $viewData;
  $Campaign = new Campaign;
  $viewData->setTitle('Add Link');
  $viewData->set('campaignsList', $Campaign->getList());
 }
 
 function approvals_calc_da_pr_ip($domain=''){
  $domain = isset($_GET['domain']) ? $_GET['domain'] : $domain;
  $domainDetails = array('ip'=>'','da'=>'','pr'=>'');
  if($domain and !empty($domain)){
   $domainDetails = array_merge($domainDetails, __calc_da_pr_ip($domain));
  }
  echo json_encode($domainDetails);
  exit;
 }
 
 function approvals_save_add_link(){
  $msg = array();
  if(__isDomainBlackListed($_POST['data']['Approval']['domain'])){
   $msg['msg'] = 'This Domain is Black Listed';
   $msg['status'] = 'error';
   echo json_encode($msg);
   exit;
  }
  if(__isDomainBlacklistForCampaigns($_POST['data']['Approval']['domain'], $_POST['data']['Approval']['campaign'], $msg)){
    echo json_encode($msg);
    exit;
   }
  if(__isIpDuplicate($_POST['data']['Approval']['ip'], $_POST['data']['Approval']['campaign'], $msg)){
   echo json_encode($msg);
   exit;
  }
  if(__isAdda52HasDomain($_POST['data']['Approval']['domain'])){
    $msg['msg'] = 'Domain used for Adda52';
    $msg['status'] = 'error';
    echo json_encode($msg);
    exit;
  }
  // check link submission if campaign contain adda52, 266 is id for adda52
  if(in_array(266, $_POST['data']['Approval']['campaign']) and __isOtherCampaignsHasDomain($_POST['data']['Approval']['domain'], $msg)){
   echo json_encode($msg);
   exit;
  }
  // check da and pr for campaigns
  if(__isCampaignHasLowDaPr($_POST['data'], $msg)){
   echo json_encode($msg);
   exit;
  }
  if(__isDomainDuplicate($_POST['data']['Approval']['domain'], $_POST['data']['Approval']['campaign'], $msg)){
   echo json_encode($msg);
   exit;
  }
  // save data in tables
  __saveAddLink($_POST['data'], $msg);
  echo json_encode($msg);
  exit;
 }
 
 function approvals_domain_history(){
  global $viewData, $session;
  $domain = rtrim(strtolower($_GET['domain']),"/");	
  $domain = str_replace(array("https://","http://","www."),"",$domain);	
  $domain = get_domain_name($domain);
  $msg = array('status'=>'success', 'msg'=>'Domain history not found.', 'title'=> 'History for domain - '. $domain);
  if(!empty($domain)){
   ob_start();
   if(count(explode('.', $domain))>=2){
    $viewData->set('isDomainBlackListed', __isDomainBlackListed($domain));
   }
   __getApprovalHistory($domain);
   __getSubmittedLinkHistory($domain);
   include_once "views/approvals_domain_history.php";
   $_html = ob_get_contents();
   ob_end_clean();
   if(trim($_html)){
    $msg['msg'] = $_html;
   }
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_ip_history(){
  global $viewData, $session;
  $ip = $_GET['ip'];
  $msg = array('status'=>'success', 'msg'=>'Domain history not found.', 'title'=> 'History for IP - '. $ip);
  if(!empty($ip)){
   ob_start();
   __getApprovalHistory($ip, 'ip');
   __getSubmittedLinkHistory($ip, 'ip');
   include_once "views/approvals_ip_history.php";
   $_html = ob_get_contents();
   ob_end_clean();
   if(trim($_html)){
    $msg['msg'] = $_html;
   }
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_do_action(){
  switch($_REQUEST['whatDo']){
   case 'approval_phase_1':
    __action_approval_phase_1($_REQUEST['data']);
    break;
   case 'approval_phase_2':
    __action_approval_phase_2($_REQUEST['data']);
    break;
   case 'approval_forward':
    __action_approval_forward($_REQUEST['data']);
    break;
   case 'approval_delete':
    __action_approval_delete($_REQUEST['data']);
    break;
  }
  exit;
 }
 
 function approvals_list_reasons(){
  $RejectReason = new RejectReason;
  $reasons = $RejectReason->find_list(array('where'=>'RejectReason.status=1', 'fields'=>array('id','data'), 'order'=>'RejectReason.data ASC'));
  echo json_encode($reasons);
  exit;
 }
 function approvals_submit_link_save(){
  global $database, $session;
  $msg = array('msg'=>'Link could not submitted. Please try again later.', 'status'=>'error');
  if(!empty($_POST['data'])){
   if(!(__isPaypalEmailExistAndActive($_POST['data']['Payment']['paypal_email'], $msg))){
    echo json_encode($msg);
    exit;
   }
   $ApprovalLink = new ApprovalLink;
   $Approval = new Approval;
   $Payment = new Payment;
   $Paypal = new Paypal;
   
   $data = $_POST['data'];
   $anchorTxt = explode("\n", $data['Payment']['anchor_text']);
   $data['Payment']['anchor_text'] = implode('[', $anchorTxt);
   $data['Payment']['start_date'] = date('Y-m-d', strtotime($data['Payment']['start_date']));
   $data['Payment']['end_date'] = strtotime($data['Payment']['end_date'])<strtotime($data['Payment']['start_date']) ? date('Y-m-d', strtotime($data['Payment']['start_date'])) : date('Y-m-d', strtotime($data['Payment']['end_date']));
   $data['Payment']['status'] = 'Underprocess';
   $data['Payment']['approached_by'] = $session->read('User.email_id');
   $data['Payment']['user_id'] = $session->read('User.id');
   $data['Payment']['added_on'] = date('Y-m-d H:i:s');
   /**** Start: Added by Jitendra: 28-Nov-2014  ******************/
   $data['Payment']['payment_month'] = $data['Payment']['payment_month'];
   $data['Payment']['payment_year'] = $data['Payment']['payment_year'];
   /**** End: Added by Jitendra: 28-Nov-2014  ******************/
   
   $link = $ApprovalLink->find_all(array('contain'=>array('Approval'), 'where'=>"ApprovalLink.id='". $data['Payment']['child_table_id']."'"));
   if(count($link)>0 && $Payment->save($data)){
    $link = array_shift($link);
    // update status to link submitted
    $ApprovalLink->save(array('ApprovalLink'=>array('id'=>$link['ApprovalLink']['id'], 'final_link_submit_status'=>1, 'link_submit_date'=>date('Y-m-d'))));
    
    // update details in approval table
    $Approval->save(array('Approval'=>array('id'=>$link['Approval']['id'], 'ip'=>$data['Payment']['ip'], 'da'=>$data['Payment']['da_value'], 'pr'=>$data['Payment']['pr_value'])));
    
    
    
    // add domain id in paypal
    $paypal = $Paypal->find_all(array('where'=>"Paypal.name ='".$data['Payment']['paypal_email']."'"));
    if(!empty($paypal)){
     $paypal = array_shift($paypal);
     $_domainStr = array_filter(explode(',', $paypal['Paypal']['domain_id_string']));
     if(!in_array($link['Approval']['domain_id'], $_domainStr)){
      $_domainStr[] = $link['Approval']['domain_id'];
      $_paypalData['Paypal']['domain_id_string'] = join(',', $_domainStr);
      $_paypalData['Paypal']['id'] = $paypal['Paypal']['id'];
      $_paypalData['Paypal']['updated_on'] = date('Y-m-d H:i:s');
      $Paypal->save($_paypalData);
     }
    }else{
     $_paypalData['Paypal'] = array('name'=>$data['Payment']['paypal_email'], 'domain_id_string'=>$link['Approval']['domain_id'], 'status'=>1, 'added_on'=>date('Y-m-d H:i:s'), 'updated_on'=>date('Y-m-d H:i:s'));
     $Paypal->save($_paypalData);
    }
    
    
    $msg = array('msg'=>'Link has been submitted.', 'status'=>'success');
   }
   
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_submit_link_ip_check(){
  $msg = array('msg'=>'Invalid ip', 'status'=>'error');
  if(!empty($_POST['ip'])){
   $Payment = new Payment;
   $ipCount = $Payment->count(array('where'=>"Payment.ip='". $_POST['ip']."'"));
   if($ipCount>0){
    $msg = array('msg'=>'Ip already exists. Do you want to continue.', 'status'=>'error', 'type'=>'confirm');
   }else{
    $msg = array('msg'=>'Ip does not exists.', 'status'=>'success');
   }
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_submit_link(){
  global $viewData;
  $ApprovalLink = new ApprovalLink;
  $viewData->setTitle('Submit Link for Payment');
  if(empty($_GET['lid'])){
   redirect_to("approvals.php");
  }
  $approvalLink = $ApprovalLink->find_all(array('where'=>"ApprovalLink.id='".$_GET['lid']."'", 'contain'=>array('Approval')));
  if(!empty($approvalLink)){
   $approvalLink = array_shift($approvalLink);
   $viewData->set('approvalLink', $approvalLink);
  }else {
   redirect_to("approvals.php");
  }
  
 }
 
 function approvals_edit_link(){
  global $viewData;
  $viewData->setTitle('Edit link');
  $Approval  = new Approval;
  $Campaign=new Campaign;
  if(!empty($_GET['pid'])){
   $pid = (int) $_GET['pid'];
   if($pid==0){
    redirect_to('approvals.php');
   }
   $approval = $Approval->find_all(array('contain'=>array('ApprovalLink'),'where'=>'Approval.id='.$pid));
   $approval = array_shift($approval);
   if(count($approval)==0){
    redirect_to('approvals.php');
   }
   $viewData->set('campaignsList', $Campaign->getList());
   $viewData->set('approval', $approval);
  }else{
   redirect_to('approvals.php');
  }
  //11644
 }
 
 function approvals_edit_link_save(){
  $msg = array('msg'=>'Sorry, unable to update link at this time. Please try again later.', 'status'=>'error');
  if(!empty($_POST['data'])){
   $Approval = new Approval;
   $data['ApprovalLink']['parent_id'] = $_POST['data']['ApprovalLink']['parent_id'];
   $campaigns = $_POST['data']['ApprovalLink']['campaign'];
   $approval = $Approval->find_all(array('where'=>'Approval.id='. $data['ApprovalLink']['parent_id']));
   $approval = array_shift($approval);
   //__updateIpDaPr($approval);
   if(!empty($approval)){ 
    if(__isDomainBlackListed($approval['Approval']['domain'])){
     $msg['msg'] = 'This Domain is Black Listed';
     $msg['status'] = 'error';
     echo json_encode($msg);
     exit;
    }
    if(__isDomainBlacklistForCampaigns($approval['Approval']['domain'], $campaigns, $msg)){
     echo json_encode($msg);
     exit;
    }
    if(__isIpDuplicate($approval['Approval']['ip'], $campaigns, $msg)){
     echo json_encode($msg);
     exit;
    } 
    if(__isAdda52HasDomain($approval['Approval']['domain'])){
      $msg['msg'] = 'Domain used for Adda52';
      $msg['status'] = 'error';
      echo json_encode($msg);
      exit;
    }
    // check link submission if campaign contain adda52, 266 is id for adda52
    if(in_array(266, $campaigns) and __isOtherCampaignsHasDomain($approval['Approval']['domain'], $msg)){
     echo json_encode($msg);
     exit;
    }
    // check da and pr for campaigns
    if(__isCampaignHasLowDaPr(array('Approval'=>array('da'=>$approval['Approval']['da'], 'pr'=>$approval['Approval']['pr'], 'campaign'=>$campaigns)), $msg)){
     echo json_encode($msg);
     exit;
    }
    if(__isDomainDuplicate($approval['Approval']['domain'], $campaigns, $msg)){
     echo json_encode($msg);
     exit;
    }
    // save data in tables
    __saveEditLink(array('data'=>$approval, 'campaign'=>$campaigns), $msg);
   }
   
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_comment_save(){
  $msg = array('msg'=>'Sorry, unable to update comment at this time. Please try again later.', 'status'=>'error');
  if(!empty($_POST['narration_text']) and !empty($_POST['id'])){
   $data['Approval']['narration_text'] = $_POST['narration_text'];
   $data['Approval']['id'] = $_POST['id'];
   $Approval = new Approval;
   $Approval->save($data);
   $msg = array('msg'=>'Comment has been updated.', 'status'=>'success');
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_remove_link(){
  $msg = array('msg'=>'Sorry, unable to remove campaign at this time.', 'status'=>'error');
  if(!empty($_POST['approvalLinkId'])){
   $linkId = (int)$_POST['approvalLinkId'];
   if($linkId>0){
    $ApprovalLink = new ApprovalLink;
    $Approval = new Approval;
    $approvalLink = $ApprovalLink->find_all(array('where'=>'ApprovalLink.id='.$linkId.' AND ApprovalLink.final_link_submit_status=0'));
    if(count($approvalLink)>0){
     $approvalLink = array_shift($approvalLink);
     if($ApprovalLink->delete('id='.$linkId.' AND final_link_submit_status=0')){
      $linkCount = $ApprovalLink->count(array('where'=>"ApprovalLink.parent_id='".$approvalLink['ApprovalLink']['parent_id']."'"));
      if($linkCount==0){
       $Approval->delete('id='.$approvalLink['ApprovalLink']['parent_id']);
       $msg['window']='close';
      }
      $msg = array('msg'=>'Campaign has been removed from link', 'status'=>'success');
     }
    }
   }
  }
  echo json_encode($msg);
  exit;
 }
 
 /**
  * Update Pr for all domains (submitted for approval)
  */
 function approvals_update_pr(){
  global $database;
  $msg = array('msg'=>'Sorry, unable to update PR.', 'status'=>'error');
  $Approval = new Approval;
  $Payment = new Payment;
  $approvals = $Approval->find_all(array('contain'=>array('ApprovalLink'=>array('where'=>"ApprovalLink.approve_status='-1'", 'fields'=>array('id'))),
                            'fields'=>array('id', 'domain'),
                            'group'=>'ApprovalLink.parent_id'));
  if(!empty($approvals)){
   foreach($approvals as $_approval){
    $domainDetais = __calc_da_pr_ip($_approval['Approval']['domain'], array('ip'=>false,'da'=>false));
    $Approval->save(array('Approval'=>array('id'=>$_approval['Approval']['id'], 'pr'=>$domainDetais['pr'])));
    $msg = array('msg'=>'PR has been updated.', 'status'=>'success');
   }
  }
  echo json_encode($msg);
  exit;
 }
 
 function approvals_update_pr_total(){
  $Approval = new Approval;
  $totalRecord = $Approval->count(array('contain'=>array('ApprovalLink'),
                             'where' => "ApprovalLink.approve_status IN ('-1', '1')",
                             'distinct' => 'Approval.domain'
                            )
                             );
  echo json_encode(array('totalRecord'=>$totalRecord));
  exit;
 }
 
 function approvals_update_all_pr(){
  global $viewData;
  $viewData->setTitle('Update PR for All Domains');
 }
 
 function approvals_update_pr_for_all(){
  global $database;
  $msg = array('msg'=>'Sorry, unable to update PR.', 'status'=>'error');
  $Approval = new Approval;
  $ApprovalLink = new ApprovalLink;
  $Payment = new Payment;
  $limit = isset($_GET['limit']) ? $_GET['limit'] : '0, 10';
  $linkResults = $Approval->find_by_sql("SELECT DISTINCT Approval.domain FROM ".$Approval->table_name." Approval INNER JOIN ".$ApprovalLink->table_name." ApprovalLink ON (ApprovalLink.parent_id=Approval.id) WHERE ApprovalLink.approve_status IN ('-1', '1') LIMIT ". $limit);
  if(!empty($linkResults)){
   $_updatedDomains = array();
   foreach($linkResults as $linkResult){
    $domain = str_replace(array('http://', 'https://', 'www.'), '', array_shift($linkResult['domain']));
    $domainDetais = __calc_da_pr_ip($domain, array('ip'=>false,'da'=>false));
    
    // update pr in Approval table
    $database->query('UPDATE '. $Approval->table_name .' SET pr="'.$domainDetais['pr'].'"  WHERE '. "(domain LIKE 'http://$domain%' OR domain LIKE 'http://www.$domain%') AND pr !='{$domainDetais['pr']}'");
    
    // update payment table
    $database->query('UPDATE '. $Payment->table_name .' SET  pr_value="'.$domainDetais['pr'].'" WHERE '. "(domain LIKE 'http://$domain%' OR domain LIKE 'http://www.$domain%') AND pr_value !='{$domainDetais['pr']}'");
    $_updatedDomains[] = array('domain'=>$domain, 'pr'=>$domainDetais['pr']);
   }
   echo json_encode($_updatedDomains);
  }else{
   $msg = array('msg'=>'Record does not found.', 'status'=>'error');
   echo json_encode($msg);
  }
  exit;
 }
 
 function approvals_admin_comment(){
  global $viewData;
  $approvalId = $_GET['id'];
  if(!empty($approvalId)){
   $Approval = new Approval;
   $approvalData = $Approval->find_all(array('fields'=>array('id', 'domain', 'narration_admin'), 'where'=>"Approval.id = '$approvalId'"));
   if(!empty($approvalData)){
    $approvalData = array_shift($approvalData);
   }
   $viewData->set('approvalData', $approvalData);
  }
  include_once "views/approvals_admin_comment.php";
  exit;
 }
 
 function approvals_save_admin_comment(){
  if(!empty($_POST['data'])){
   $data = $_POST['data'];
   if(!empty($data['Approval']['id'])){
    $Approval = new Approval;
    $Approval->save($data);
   }else{
    echo "Invalid link id for submit comment.";
   }
  }else{
    echo "Invalid comment data.";
   }
  exit;
 }
 
 // below all functions (function name start with __ ) are private
 // these function are only for use with in function
 function __saveEditLink($data, &$msg){
  global $session, $database;
  if(!empty($data)){
   $Campaign = new Campaign;
   $ApprovalLink = new ApprovalLink;
   // data set for ApprovalLink Model
   $_dataAppLink['ApprovalLink']['added_on'] = $_dataAppLink['ApprovalLink']['link_submit_date'] = date('Y-m-d H:i:s');
   $_dataAppLink['ApprovalLink']['approached_by'] = $data['data']['Approval']['approached_by'];
   $_dataAppLink['ApprovalLink']['user_id'] = $data['data']['Approval']['user_id'];
   $_dataAppLink['ApprovalLink']['parent_id'] = $data['data']['Approval']['id'];
   
   $campaigns = $Campaign->find_all(array(
    'where' => 'Campaign.id IN ('. join(',', $data['campaign']) .')',
    'fields' => array('id','name','secondary_approval','secondary_approval_user')
   ));
   foreach($campaigns as $_campaign){
    $_dataAppLink['ApprovalLink']['campaign_name'] = $_campaign['Campaign']['name'];
    $_dataAppLink['ApprovalLink']['campaign_id'] = $_campaign['Campaign']['id'];
    $_dataAppLink['ApprovalLink']['is_secondary_approval'] = $_campaign['Campaign']['secondary_approval']==1? 1 : 0;
    $_dataAppLink['ApprovalLink']['secondary_app_user'] = $_campaign['Campaign']['secondary_approval_user'];
    $ApprovalLink->save($_dataAppLink);
    
    $msg['msg'] = 'Link has been added for approval.';
    $msg['status'] = 'success';
    //return true;
   }
  }else{
    $msg['msg'] = 'Link could not be added for approval. Please try again later.';
    $msg['status'] = 'error';
   // return false;
  }
 }
 
 function __updateIpDaPr(&$approval){
  global $database;
  if(!empty($approval['Approval']['domain'])){
   $domainDetais = __calc_da_pr_ip($approval['Approval']['domain']);
   if(!empty($domainDetais)){
    $Approval = new Approval;
    $Payment = new Payment;
    $approval['Approval']['ip'] = $domainDetais['ip'];
    $approval['Approval']['da'] = $domainDetais['da'];
    $approval['Approval']['pr'] = $domainDetais['pr'];
    $Approval->save(array('Approval'=>array('id'=>$approval['Approval']['id'], 'ip'=>$domainDetais['ip'], 'da'=>$domainDetais['da'], 'pr'=>$domainDetais['pr'])));
    
    // update ip in track_data table
    $domain = str_replace(array('http://', 'https://', 'www.'), '', $approval['Approval']['domain']);
    $payment = $Payment->find_all(array('where'=>"(Payment.domain LIKE 'http://$domain%' OR Payment.domain LIKE 'http://www.$domain%') AND Payment.ip !='{$domainDetais['ip']}'", 'fields'=>array('id')));
    
    if(!empty($payment)){
     $_paymentIds = array();
     foreach($payment as $_payment){
      $_paymentIds[] = $_payment['Payment']['id'];
     }
     if(!empty($_paymentIds)){
      $database->query('UPDATE '. $Payment->table_name .' SET ip="'.$domainDetais['ip'].'", pr_value="'.$domainDetais['pr'].'", da_value="'.$domainDetais['da'].'" WHERE id IN ('. join(',', $_paymentIds).')');
     }
    }
   }
  }
 }
 
 function __calc_da_pr_ip($domain, $options=array()){
  if(!empty($domain)){
   $_options = array('ip'=>true,'da'=>true,'pr'=>true);
   $options = array_merge($_options, (is_array($options)?$options:(array)$options));
   $domain = get_domain_name($domain);
    // get pr from google
    if($options['pr']==true){
     $pr =  new Pr();
     $domainDetails['pr'] = $pr->getGooglePagerank($domain);
    }
    
    // get da from moz
    if($options['da']==true){
     $da = new Da();
     $domainDetails['da'] = $da->getDa($domain);
    }
    
    // get ip
    if($options['ip']==true){
     $domainDetails['ip'] = gethostbyname($domain);
    }
    
    return $domainDetails;
  }
  return array();
 }
 function __isPaypalEmailExistAndActive($email=null, &$msg){ 
  if(!empty($email)){
   $Paypal = new Paypal;
   $paypalCount = $Paypal->count(array('where'=>"Paypal.name='".$email."' AND status=0"));
   if($paypalCount>0){
    $msg = array('msg'=>'Paypal email black listed.', 'status'=>'error');
    return false;
   }else{
    return true;
   }
  }else {
   $msg = array('msg'=>'Invalid paypal email.', 'status'=>'error');
  }
  return false;
 }
  
 
 function __getApprovalHistory($val, $filterBy='domain'){
  global $viewData;
  if(!empty($val)){
   if($filterBy=='ip'){
    $where = "Approval.ip = '$val'";
   }else{
    //$where = "Approval.domain LIKE 'http://{$val}%' OR Approval.domain LIKE 'http://www.{$val}%'";
    $where = "Approval.domain LIKE '%{$val}%'";
   }
   $Approval =new Approval;
   $approvals = $Approval->find_all(array(
      'contain' => array('ApprovalLink', 'User'=>array('fields'=>array('id','first_name','last_name'))),
      'where' => $where,
      'order' => 'Approval.child_update_date DESC'
     ));
  }
  $viewData->set('approvals', $approvals);
 }
 
 function __getSubmittedLinkHistory($val, $filterBy='domain'){
  global $viewData, $database;
  if(!empty($val)){
   if($filterBy=='ip'){
    $where = "Payment.ip = '$val'";
   }else{
    //$where = "Payment.domain LIKE 'http://{$val}%' OR Payment.domain LIKE 'http://www.{$val}%'";
    $where = "Payment.domain LIKE '%{$val}%'";
   }
   $Payment = new Payment;
   $payments = $Payment->find_all(array(
      'contain' => array('User'=>array('fields'=>array('id','first_name','last_name'))),
      'where' => $where,
      'order' => 'Payment.id DESC'
     ));
  }
  $viewData->set('payments', $payments);
 }
 
 function __saveAddLink($data, &$msg){
  global $session, $database;
  if(!empty($data)){
   $Approval = new Approval;
   $ApprovalLink = new ApprovalLink;
   $Campaign = new Campaign;
   // data set for Approval Model
   $_data['Approval'] = $data['Approval'];
   $domain = rtrim(strtolower($_data['Approval']['domain']),"/");	
   $domain = str_replace(array("https://","http://","www."),"",$domain);	
   $domain = get_domain_name($domain);
   $_data['Approval']['domain'] = 'http://'.$domain;
   $_data['Approval']['domain_final'] = 'http://'.$domain;
   $_data['Approval']['campaigns_count'] = count($_data['Approval']['campaign']);
   $_data['Approval']['added_on'] = $_data['Approval']['child_update_date'] = date('Y-m-d H:i:s');
   $_data['Approval']['approached_by'] = $session->read('User.email_id');
   $_data['Approval']['user_id'] = $session->read('User.id');
   
   // check and insert the domain
   $_data['Approval']['domain_id'] = __checkAndInsertDomain($domain);
   if($Approval->save($_data)){
    // data set for ApprovalLink Model
    $_dataAppLink['ApprovalLink']['added_on'] = $_dataAppLink['ApprovalLink']['link_submit_date'] = $_data['Approval']['added_on'];
    $_dataAppLink['ApprovalLink']['approached_by'] = $session->read('User.email_id');
    $_dataAppLink['ApprovalLink']['user_id'] = $session->read('User.id');
    $_dataAppLink['ApprovalLink']['parent_id'] = $database->insert_id();
    /*** Start: Added by Jitendra: 28-Nov-2014 *****/
    $_dataAppLink['ApprovalLink']['Amount'] = $_data['Approval']['Amount'];
    $_dataAppLink['ApprovalLink']['Currency'] = $_data['Approval']['Currency'];
    /*** End: Added by Jitendra: 28-Nov-2014 *****/
    $campaigns = $Campaign->find_all(array(
     'where' => 'Campaign.id IN ('. join(',', $_data['Approval']['campaign']) .')',
     'fields' => array('id','name','secondary_approval','secondary_approval_user')
    ));
    foreach($campaigns as $_campaign){
     $_dataAppLink['ApprovalLink']['campaign_name'] = $_campaign['Campaign']['name'];
     $_dataAppLink['ApprovalLink']['campaign_id'] = $_campaign['Campaign']['id'];
     $_dataAppLink['ApprovalLink']['is_secondary_approval'] = $_campaign['Campaign']['secondary_approval']==1? 1 : 0;
     $_dataAppLink['ApprovalLink']['secondary_app_user'] = $_campaign['Campaign']['secondary_approval_user'];
     $ApprovalLink->save($_dataAppLink);
    }
    
    $msg['msg'] = 'Link has been added for approval.';
    $msg['status'] = 'success';
    //return true;
   }else{
    $msg['msg'] = 'Link could not be added for approval. Please try again later.';
    $msg['status'] = 'error';
   // return false;
   }
  }
 }
 
 function __checkAndInsertDomain($domain){
  global $database;
  if(!empty($domain)){
   $Domain = new Domain;
    $_domain = $Domain->find_all(array(
                 'where' => "Domain.name LIKE 'http://".$domain."%' OR Domain.name LIKE 'http://www".$domain."%'",
                 'fields' => array('id'),
                 'order' => 'Domain.id DESC',
                 'limit' => '1'
                ));
    if(count($_domain)==0){
     $Domain->save(array('Domain'=>array('name'=>"http://".$domain, 'added_on'=>date('Y-m-d H:i:s'))));
     return $database->insert_id();
    }else{
     $_domain = array_shift($_domain);
     return $_domain['Domain']['id'];
    }
  }
  return 0;
 }
 
 function __isAdda52HasDomain($domain){
  global $database;
  $domain = rtrim($domain,"/");	
  $domain = str_replace(array("https://","http://","www."),"",$domain);	
  $domain = get_domain_name($domain);
  $ApprovalLink =new ApprovalLink;				
  $approvalLinkCount = $ApprovalLink->count(array(
                    'contain' => array('Approval'),
                    'where' => "(Approval.domain_final LIKE 'http://".$domain."%' OR Approval.domain_final LIKE 'http://www".$domain."%')
                                AND ApprovalLink.campaign_id=266 AND ApprovalLink.approve_status != 0
                                AND ApprovalLink.link_submit_date >= DATE_SUB(CURDATE() , INTERVAL 15 DAY)",
                    'order' => 'Approval.child_update_date DESC',
                    'limit' => '1'
                   ));
  return $approvalLinkCount>0 ? true : false;
 }
 
 function __isOtherCampaignsHasDomain($domain, &$msg){
  global $database;
  $domain = rtrim($domain,"/");	
  $domain = str_replace(array("https://","http://","www."),"",$domain);	
  $domain = get_domain_name($domain);
  $ApprovalLink = new ApprovalLink;
  $approvalLink = $ApprovalLink->find_all(array(
                    'contain' => array('Approval'),
                    'where' => "( Approval.domain_final LIKE 'http://".$domain."%' OR Approval.domain_final LIKE 'http://www".$domain."%' )
                                AND ApprovalLink.approve_status != 0
                                AND ApprovalLink.link_submit_date >= DATE_SUB(CURDATE() , INTERVAL 15 DAY)",
                    'order' => 'Approval.child_update_date DESC',
                    'limit' => '1'
                   ));
  if(count($approvalLink)>0){
   $msg['msg'] = 'Uncheck Adda52, Domain already used for '. $approvalLink[0]['ApprovalLink']['campaign_name'];
   $msg['status'] = 'error';
   return  true;
  }
  return false;
 }
 
 function __isCampaignHasLowDaPr($data, &$msg){
  $campaigns = $data['Approval']['campaign'];
  $da = $data['Approval']['da'];
  $pr = $data['Approval']['pr'];
  if(!empty($campaigns)){
   $Campaign = new Campaign;
   $campaignResults = $Campaign->find_all(array(
                        'where'=>'Campaign.id IN ('. join(',', $campaigns). ')',
                        'fields'=>array('id','name','plb_da','plb_pr'),
                        'order'=>'Campaign.name ASC'
                        ));
   $_error = false;
   $_html = "<table style='width:100%' class='table table-bordered table-striped'>";
   $_html .= "<tr><th colspan='2' class='tbl-caption'>Campaigns required minimum PR/DA</th></tr>";
   $_html .= "<tr><th style='text-align:left;'>Campaign Name</th><th style='text-align:left;'>PR/DA</th></tr>";
   foreach($campaignResults as $_campaign){
    $_da = explode(',', $_campaign['Campaign']['plb_da']);
    $_pr = explode(',', $_campaign['Campaign']['plb_pr']);
    // commented on 03-02-14, as said by mehta
    if(count($_da)==1){
     if(( $pr < $_pr[0]) || (round($da,1) < $_da[0] )){
      $_error = $_error==false? true : $_error;
      $_html .= "<tr>";
      $_html .= "<td>". ucwords($_campaign['Campaign']['name'])."</td>
                 <td><b>PR -></b> ".$_pr[0]."  <b>, DA -></b>".$_da[0]. "</td>";
      $_html .= "</tr>";
     }
    } else if(count($_da) > 1){ 
     $errorFound=true;
     if(($pr>=$_pr[0] &&  round($da,1)>=$_da[0])){
             $errorFound=false;
     } else if($errorFound===true && ($pr>=$_pr[1] &&  round($da)>=$_da[1])){
             $errorFound=false;
     }
     
     if($errorFound===true){
      $_error = $_error==false? true : $_error;
         $_html .= "<tr>";
         $_html .= "<td>".ucwords($_campaign['Campaign']['name'])."</td><td><b> PR -></b> ".$_pr[0].", <b>DA -></b>".$_da[0];
         $_html .= " <b>OR</b> <b>PR -></b>".$_pr[1].", <b> DA -></b>".$_da[1]; 	
         $_html .= "</td>";
         $_html .= "</tr>";
     }
    }
    // if condition added after comented above part
    /*if(( $pr < 3) || ($da < 40 )){
      $_error = $_error==false? true : $_error;
      $_html .= "<tr>";
      $_html .= "<td>". ucwords($_campaign['Campaign']['name'])."</td>
                 <td><b>PR -></b> 3  <b>, DA -></b> 40 </td>";
      $_html .= "</tr>";
     }*/
   }
   $_html .= '</table>';
   
   if($_error === true){
    $msg['msg'] = $_html;
    $msg['status'] = 'error';
    return true;
   }
  }
  
  return false;
 }
 
 function __isDomainBlackListed($domain){
  if(!empty($domain)){
   $domain = rtrim($domain,"/");	
   $domain = str_replace(array("https://","http://","www."),"",$domain);	
   $domain = get_domain_name($domain);
   $Domain = new Domain;
   $blackListedDomain = $Domain->count(array(
                     'where' => "( Domain.name LIKE 'http://".$domain."%' OR Domain.name LIKE 'http://www".$domain."%' )
                                 AND Domain.status = 0",
                     'limit' => '1'
                    ));
  }
  return $blackListedDomain>0? true : false;
 }
 
 function __isIpDuplicate($ip, $campaigns, &$msg){
  global $database;
  if(!empty($ip) and !empty($campaigns)){
    $campaigns = !is_array($campaigns) ? (array)$campaigns : $campaigns;
    $_isDuplacate = false;
    // exclude campaigns AdLift -59, Shopify -219, HBD Infographics - 313
    $_excludeCampaigns = array(59, 219, 313);
    foreach($campaigns as $key => $val){
      if(in_array($val, $_excludeCampaigns)){
         // skip if campaign exists in $_excludeCampaigns 
         unset($campaigns[$key]);
      }
    }
    $ApprovalLink = new ApprovalLink;
    
    $_html = "<table style='width:100%' class='table table-bordered table-striped'>";
    $_html .= "<tr><th colspan='5' class='tbl-caption'>Ip already used by given clients</th></tr>";
    $_html .= "<tr><th>Client</th><th>Status</th><th>Period</th><th>User</th><th>Remark</th></tr>";
    
    if(!empty($campaigns)){
     $approvals = $ApprovalLink->find_all(array(
                    'contain' => array(
                       'Approval'=>array('fields'=>array('id','domain')),
                       'User'=>array('fields'=>array('id','first_name','last_name'))
                    ),
                    'where' => "ApprovalLink.link_submit_date  >= DATE_SUB(CURDATE( ) , INTERVAL 30 DAY)
                               AND Approval.ip='$ip' AND ApprovalLink.campaign_id IN (". join(',', $campaigns) .")
                               AND ApprovalLink.approve_status != 0",
                    'order' => 'Approval.child_update_date DESC',
                    'fields' => array('id', 'campaign_name', 'approve_status', 'final_link_submit_status')
                   ));
    }
      if(!empty($approvals)){
       foreach($approvals as $_approval){
        $_isDuplacate = true;
        
        $_html .= "<tr>";
        if($_approval['ApprovalLink']['approve_status'] == 1){
                $_html .= "<td><b>".ucwords($_approval['ApprovalLink']['campaign_name'])." (<a href='".$_approval['Approval']['domain']."'>".$_approval['Approval']['domain']."</a>)</b></td>";
                $_html .= "<td>Approved</td>";
                $_html .= "<td><b>30 Days</b></td>";
                $_html .= "<td>".ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name'])."</td>";
                
                if($_approval['ApprovalLink']['final_link_submit_status']==1){
                 $_html .= "<td><b>Link Submitted</b></td>";
                } else{
                 $_html .= "<td><b>Link not submitted</b></td>";
                }				
          }
  
        if($_approval['ApprovalLink']['approve_status']==-1){		   
           
          $_html .= "<td><b>".ucwords($_approval['ApprovalLink']['campaign_name'])." (<a href='".$_approval['Approval']['domain']."'>".$_approval['Approval']['domain']."</a>)</b></td>";
          $_html .= "<td>For Approval</td>";
          $_html .= "<td><b>30 Days</b></td>";
          $_html .= "<td>".ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name'])."</td>";
          $_html .= "<td></td>";
        }
        $_html .= "</tr>";
       }
      }
    $_html .= "</table>";	

    if($_isDuplacate === true){
     $msg['msg'] = $_html;
     $msg['status'] = 'error';
     return true;
    }
   }
   return false;
 }
 
 function __isDomainDuplicate($domain, $campaigns, &$msg){
  global $database;
  if(!empty($domain) and !empty($campaigns)){
    $campaigns = !is_array($campaigns) ? (array)$campaigns : $campaigns;
    $domain = rtrim($domain,"/");	
    $domain = str_replace(array("https://","http://","www."),"",$domain);	
    $domain = get_domain_name($domain);
    $_isDuplacate = false;
    // exclude campaigns AdLift -59, Shopify -219, Panasonic - 297, Unitech-272, VKNagrani - 286
    $_excludeCampaigns = array(59, 219, 297, 272, 286);
    $_defaultInterval = 60; // days
    foreach($campaigns as $key => $val){
      if(in_array($val, $_excludeCampaigns)){
         // skip if campaign exists in $_excludeCampaigns 
         unset($campaigns[$key]);
      }
    }
    $ApprovalLink = new ApprovalLink;
    if(!empty($campaigns)){
     $approvalLink = $ApprovalLink->find_all(array(
                     'contain' => array('Approval', 'User'=>array('fields'=>array('id','first_name','last_name'))),
                     'where' => "( Approval.domain_final LIKE 'http://".$domain."%' OR Approval.domain_final LIKE 'http://www".$domain."%' )
                                 AND ( ApprovalLink.link_submit_date >= DATE_SUB(CURDATE() , INTERVAL ".$_defaultInterval." DAY)
                                   OR ApprovalLink.approve_status = '-1' )
                                 AND ApprovalLink.campaign_id IN (".join(',', $campaigns).")",
                     'order' => 'Approval.child_update_date DESC',
                     'limit' => '1'
                    ));
    }
    $_html = "<table style='width:100%' class='table table-bordered table-striped'>";
    $_html .= "<tr><th colspan='5' class='tbl-caption'>Domain already used by given clients</th></tr>";
    $_html .= "<tr><th>Client</th><th>Status</th><th>Period</th><th>User</th><th>Remark</th></tr>";
    if(!empty($approvalLink)){
       foreach($approvalLink as $_approval){
        $_isDuplacate = true;
        
        $_html .= "<tr>";
        if($_approval['ApprovalLink']['approve_status'] == 1){
                $_html .= "<td><b>".ucwords($_approval['ApprovalLink']['campaign_name'])." (<a href='".$_approval['Approval']['domain']."'>".$_approval['Approval']['domain']."</a>)</b></td>";
                $_html .= "<td>Approved</td>";
                $_html .= "<td><b>$_defaultInterval Days</b></td>";
                $_html .= "<td>".ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name'])."</td>";
                
                if($_approval['ApprovalLink']['final_link_submit_status']==1){
                 $_html .= "<td><b>Link Submitted</b></td>";
                } else{
                 $_html .= "<td><b>Link not submitted</b></td>";
                }				
          }
          
          if($_approval['ApprovalLink']['approve_status']==0){		   
           
            $_html .= "<td><b>".ucwords($_approval['ApprovalLink']['campaign_name'])." (<a href='".$_approval['Approval']['domain']."'>".$_approval['Approval']['domain']."</a>)</b></td>";
            $_html .= "<td>Rejected</td>";
            $_html .= "<td><b>$_defaultInterval Days</b></td>";
            $_html .= "<td>".ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name'])."</td>";
            $_html .= "<td></td>";
          }
  
        if($_approval['ApprovalLink']['approve_status']==-1){		   
           
          $_html .= "<td><b>".ucwords($_approval['ApprovalLink']['campaign_name'])." (<a href='".$_approval['Approval']['domain']."'>".$_approval['Approval']['domain']."</a>)</b></td>";
          $_html .= "<td>For Approval</td>";
          //$_html .= "<td><b>$_defaultInterval Days</b></td>";
          $_html .= "<td> </td>";
          $_html .= "<td>".ucwords($_approval['User']['first_name'].' '.$_approval['User']['last_name'])."</td>";
          $_html .= "<td></td>";
        }
        $_html .= "</tr>";
       }
      }
    $_html .= "</table>";	

    if($_isDuplacate === true){
     $msg['msg'] = $_html;
     $msg['status'] = 'error';
     return true;
    }
    
  }
  return false;
 }
 
 function __aprrovalsFilterVars(){
  global $viewData;
  $Approval = new Approval;
  $filter = array();
  $filterVars = array();
  if(!empty($_GET['_uid'])){
   $users = base64_decode($_GET['_uid']);
   $filterVars['users'] = explode(',', $users);
   $filter['Approval'][] = ' Approval.user_id IN ('.$users.') ';
  }
  if(!empty($_GET['_camp'])){
   $campigns = base64_decode($_GET['_camp']);
   $filterVars['campaigns'] = explode(',', $campigns);
   $filter['ApprovalLink'][] = ' ApprovalLink.campaign_id IN ('.$campigns.') ';
  }
  
  if(!empty($_GET['_st2'])){
   $status2 = base64_decode($_GET['_st2']);
   $filterVars['status_phase2'] = explode(',', $status2);
   $filter['ApprovalLink'][] = ' ApprovalLink.secondary_approval IN ('.$status2.') ';
   $filter['ApprovalLink'][] = ' ApprovalLink.is_secondary_approval=1 ';
   $filter['ApprovalLink'][] = ' ApprovalLink.approve_status IN (1) ';
  }else if(!empty($_GET['_st'])){
   $status = base64_decode($_GET['_st']);
   $filterVars['status'] = explode(',', $status);
   $filter['ApprovalLink'][] = ' ApprovalLink.approve_status IN ('.$status.') ';
  }
  
  if(!empty($_GET['_dt'])){
   list($dateFrom, $dateTo) = explode('_', base64_decode($_GET['_dt']));
   if(!empty($dateFrom) and !empty($dateTo)){
    $filterVars['dateFrom'] = $dateFrom;
    $filterVars['dateTo'] = $dateTo;
    $filter['Approval'][] = " DATE_FORMAT(Approval.added_on, '%Y-%m-%d') BETWEEN '".date('Y-m-d', strtotime($dateFrom))."' AND '".date('Y-m-d', strtotime($dateTo))."' "; 
   } else if(!empty($dateFrom) and empty($dateTo)){
    $filterVars['dateFrom'] = $dateFrom;
    $filter['Approval'][] = " DATE_FORMAT(Approval.added_on, '%Y-%m-%d') = '".date('Y-m-d', strtotime($dateFrom))."' "; 
   }
  }
  
  if(!empty($_GET['_fw'])){
   $fw = base64_decode($_GET['_fw']);
   $filterVars['forward'] = $fw;
   $fwStatus = $fw==2? '0,1' : $fw;
   $filter['Approval'][] = ' Approval.superadmin_status IN ('.$fwStatus.') ';
  }
  
  if(!empty($_GET['_cw'])){
   $cw = base64_decode($_GET['_cw']);
   $filterVars['content_writer'] = $cw;
   $cwStatus = $cw>-1? $cw : '0,1';
   $filter['Approval'][] = ' Approval.content_writer IN ('.$cwStatus.') ';
  }
  
  if(!empty($_GET['_da'])){
   $da = base64_decode($_GET['_da']);
   $filterVars['da'] = $da;
   list($op, $daVl) = explode('-', $da);
   $daVl = (int)$daVl;
   $op = isset($Approval->daFilterOptions['operators'][$op]) ? $Approval->daFilterOptions['operators'][$op] : '=';
   $filter['Approval'][] = ' Approval.da '.$op. $daVl ;
  }
  
  if(!empty($_GET['_ls'])){
   $ls = base64_decode($_GET['_ls']);
   $filterVars['link_status'] = $ls;
   $lsStatus = $ls>-1? $ls : '0,1';
   $filter['ApprovalLink'][] = ' ApprovalLink.final_link_submit_status IN ('.$lsStatus.') ';
  }
  
  if(!empty($_GET['_dom'])){
   $domain = trim(base64_decode($_GET['_dom']));
   $filterVars['domain'] = $domain;
   $domain = rtrim($domain,"/");	
   $domain = str_replace(array("https://","http://","www."),"",$domain);	
   $domain = preg_replace('/^\./', '', get_domain_name($domain));
   $filter['Approval'][] = " (Approval.domain LIKE 'http://".$domain."%' OR Approval.domain LIKE 'http://www.".$domain."%') ";
  }
  
  if(!empty($_GET['_rp'])){
   $filterVars['row_per_page'] = $_GET['_rp'];
  }
  
  $filters['Approval'] = $filter['Approval'] ? implode(' AND ', $filter['Approval']) : '';
  $filters['ApprovalLink'] = $filter['ApprovalLink'] ? implode(' AND ', $filter['ApprovalLink']) : '';
  //$filters = implode(' AND ', $filter);
  $viewData->set('filterVars', $filterVars);
  return $filters;
 }
 
 function __action_approval_phase_1($data=array()){
  global $database;
  $msg = array('msg'=>'Sorry, approval status could not changed. Please try again later.', 'status'=>'error');
  $ApprovalLink = new ApprovalLink;
  if(!empty($data)){
   // domain approval accept for campaigns
   if(!empty($data['ApprovalLink']['accept'])){
    $_ids = array();
    foreach($data['ApprovalLink']['accept'] as $_id){
     $_ids[] = $_id;
    }
    if(!empty($_ids)){
     $database->query('UPDATE '. $ApprovalLink->table_name.' SET approve_status=1 WHERE id IN('. join(',', $_ids). ')');
     $msg = array('msg'=>'Approval status has been changed.', 'status'=>'success');
    }
   }
   // domain approval reject for campaigns
   if(!empty($data['ApprovalLink']['reject'])){
    foreach($data['ApprovalLink']['reject'] as $_id){
     $ApprovalLink->save(array('ApprovalLink'=>array('id'=>$_id, 'approve_status'=>0, 'r_data_i'=> join(',', $data['ApprovalLink']['reason'][$_id]))));
     $msg = array('msg'=>'Approval status has been changed.', 'status'=>'success');
    }
   }
  }
  echo json_encode($msg);
 }
 
 function __action_approval_phase_2($data=array()){
  global $database, $session;
  $msg = array('msg'=>'Sorry, approval status could not changed. Please try again later.', 'status'=>'error');
  if(!empty($data)){
   $ApprovalLink = new ApprovalLink;
   // domain approval accept for campaigns
   if(!empty($data['ApprovalLink']['accept'])){
    $_ids = array();
    foreach($data['ApprovalLink']['accept'] as $_id){
     $_ids[] = $_id;
    }
    if(!empty($_ids)){
     $approvalLinks = $ApprovalLink->find_all(array('where'=>'ApprovalLink.id IN('. join(',', $_ids).') AND ApprovalLink.approve_status=1 AND ApprovalLink.is_secondary_approval=1 AND ApprovalLink.secondary_approval != 1', 'fields'=>array('id','secondary_approval','secondary_app_user')));
     if(!empty($approvalLinks)){
      foreach($approvalLinks as $_approvalLink){
       if($session->read('User.user_type') == 'superadmin' || $session->read('User.id') == $_approvalLink['ApprovalLink']['secondary_app_user']){
        $ApprovalLink->save(array('ApprovalLink'=>array('id'=>$_approvalLink['ApprovalLink']['id'], 'secondary_approval'=>1, 'secondary_status_update_date'=>date('Y-m-d H:i:s'), 'updated_on'=>date('Y-m-d H:i:s'))));
        $msg = array('msg'=>'Approval status has been changed.', 'status'=>'success');
       }
      }
     }
    }
   }
   // domain approval reject for campaigns
   if(!empty($data['ApprovalLink']['reject'])){
    
    $_ids = array();
    foreach($data['ApprovalLink']['reject'] as $_id){
     $_ids[] = $_id;
    }
    if(!empty($_ids)){
     $approvalLinks = $ApprovalLink->find_all(array('where'=>'ApprovalLink.id IN('. join(',', $_ids).') AND ApprovalLink.approve_status=1 AND ApprovalLink.is_secondary_approval=1 AND ApprovalLink.secondary_approval != 0', 'fields'=>array('id','secondary_approval','secondary_app_user')));
     if(!empty($approvalLinks)){
      foreach($approvalLinks as $_approvalLink){
       if($session->read('User.user_type') == 'superadmin' || $session->read('User.id') == $_approvalLink['ApprovalLink']['secondary_app_user']){
        $ApprovalLink->save(array('ApprovalLink'=>array('id'=>$_approvalLink['ApprovalLink']['id'], 'secondary_approval'=>0, 'secondary_status_update_date'=>date('Y-m-d H:i:s'), 'updated_on'=>date('Y-m-d H:i:s'), 'r_data_i'=> join(',', $data['ApprovalLink']['reason'][$_approvalLink['ApprovalLink']['id']]))));
        $msg = array('msg'=>'Approval status has been changed.', 'status'=>'success');
       }
      }
     }
    }
   }
  }
  echo json_encode($msg);
 }
 
 function __action_approval_forward($data=array()){
  $msg = array('msg'=>'Sorry, approval links could not forwarded. Please try again later.', 'status'=>'error');
  if(!empty($data['Approval']['id'])){
   $Approval = new Approval;
   foreach($data['Approval']['id'] as $_id){
    $Approval->save(array('Approval'=>array('id'=>$_id, 'superadmin_status'=>1)));
    $msg = array('msg'=>'Approval links has been forwarded.', 'status'=>'success');
   }
  }
  echo json_encode($msg);
 }
 
 function __action_approval_delete($data=array()){
  $msg = array('msg'=>'Sorry, approval links could not deleted. Please try again later.', 'status'=>'error');
  if(!empty($data['Approval']['id'])){
   $Approval = new Approval;
   $ApprovalLink = new ApprovalLink;
   $_ids = array();
   foreach($data['Approval']['id'] as $_id){
     $_ids[] = $_id;
   }
   
   if(!empty($_ids)){
    $_mids = array();
    foreach($_ids as $_lid){
     if($ApprovalLink->delete(" parent_id = '$_lid' AND final_link_submit_status!=1 ")){
      $_mids[]= $_lid;
      $msg = array('msg'=>'Approval links has been deleted.', 'status'=>'success');
     }
    }
    if(!empty($_mids)){
     $approvalLinks = $ApprovalLink->find_list(array(
                       'where'=>'ApprovalLink.parent_id IN ('. join(',', $_mids).')',
                       'fields'=>array('id','parent_id'),
                       'group'=>'ApprovalLink.parent_id'
                       ));
     foreach($_mids as $_k=>$_mi){
      if(in_array($_mi, $approvalLinks)){
       unset($_mids[$_k]);
      }
     }
     if(!empty($_mids)){
      $Approval->delete(" id IN (". join(',', $_mids) . ") ");
     }
    }
   }
  }
  echo json_encode($msg);
 }
 
 function __isDomainBlacklistForCampaigns($domain='', $campaigns=array(), &$msg){
  if(!empty($domain) and !empty($campaigns)){
   $domain = rtrim($domain,"/");	
   $domain = str_replace(array("https://","http://","www."),"",$domain);	
   $domain = get_domain_name($domain);
   $campaigns  = !is_array($campaigns) ? (array)$campaigns : $campaigns;
   $BlacklistDomain = new BlacklistDomain;
   $_campaigns = $BlacklistDomain->find_all(array('contain'=>array('Campaign'=>array('fields'=>array('id','name')), 'Domain'=>array('fields'=>array('id','name'), 'where'=>"(Domain.name = 'http://$domain' OR Domain.name = 'http://www.$domain')")), 'where'=>"BlacklistDomain.campaign_id IN (". implode(',', $campaigns) .")"));
   $campaignsCount = count($_campaigns);
   if($campaignsCount>0){
    $_html = '<div class="row-fluid"><div class="span12">';
    $_html .= "<table style='width:100%' class='table table-bordered table-striped'>";
    $_html .= "<tr><th class='tbl-caption' colspan='2'>This domain is black listed for given campaigns.</th></tr>";
    $_html .= "<tr><th>Name</th><th>Black Listed On</th></tr>";
    foreach($_campaigns as $_campaign){
     $_html .= "<tr>";
     $_html .= "<td>{$_campaign['Campaign']['name']}</td>";
     $_html .= "<td>". date_to_text($_campaign['BlacklistDomain']['created'])."</td>";
     $_html .= "</tr>";
    }
    $_html .= "</table>";
    $_html .= '</div</div>';
    
    $msg['msg'] = $_html;
    $msg['status'] = 'error';
   }
  }
  return $campaignsCount>0? true : false;
 }
 
// auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('approvals_'.$action)){ 
 call_user_func('approvals_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'approvals_'.$_GET['act'].'.php' : 'approvals_index.php';
include "views/default.php";

?>