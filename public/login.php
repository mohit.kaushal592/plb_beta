<?php
require_once("../includes/initialize.php");
if ($session->is_logged_in()) { redirect_to("index.php"); }

if (isset($_POST['loginNow'])) { // Form has been submitted.
    $data = $_POST['data'];
    $username = trim($data['username']);
    $password = trim($data['password']);
    $User = new User;
    // Check database to see if username/password exist.
	

    $found_user = $User->authenticate($username, $password);
  if ($found_user) { 
    $session->login($found_user);
    log_action('Login', "{$found_user['User']['username']} logged in.");
    redirect_to("index.php");
  } else { 
    $session->message("Username/password combination incorrect.");
  }
  
}

if($_REQUEST['act']=='forgot'){
    recoverPassword();
}

function recoverPassword(){
    global $session;
    if(!empty($_POST['email'])){
	$User = new User;
	$email = mysql_real_escape_string($_POST['email']);
	$data = $User->find_all(array('where'=>"User.email_id='{$email}' AND User.status=1", 'limit'=>1));
	if(!empty($data)){
	    $data = array_shift($data);
	    $newPassword = generatePassword(8);
	    $User->save(array('User'=>array('id'=>$data['User']['id'], 'password'=>md5($newPassword))));
	    __sendRecoveryPasswordMail($data, $newPassword);
	}else{
	    $session->message("Email address does not exists or inactive by admin. Please contact PLB admin.");
	}
    }
}

function __sendRecoveryPasswordMail($data, $pwd){
    global $session;
    if(!empty($data)){
	$mail = new PHPMail;
	$mail->setFrom('no-reply@adlift.com');
	$mail->setTo($data['User']['email_id']);
	$username = $data['User']['first_name'];
	$mail->setSubject("PLB - new generated password");
	$message = "Hello {$username},  <br />
			  <p>You have successfully generated new password, Please find login details below- <br />
			  <b>Email: </b> {$data['User']['email_id']} <br />
			  <b>Password: </b> {$pwd} <br />
			  </p>
			  <p>
			  <b>Thanks</b> <br />
			  Adlift PLB
			  </p>
			  ";
	$mail->setBody($message);
	if($mail->sendMail()){
	    $session->message("New password has been sent on email {$data['User']['email_id']}.");
	}else{
	    $session->message("New password could not be sent at this time. Please try again later");
	}
    }
}
// include login view template
include ('views/login.php');
?>