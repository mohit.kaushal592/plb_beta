<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

 function domains_index(){
    global $viewData;
    $viewData->setTitle('Domains Listing');
    $Domain  = new Domain;
    $filters = __domainFilterVars(); 
   $domainFilter = array('contain'=>array('BlacklistDomain'=>array('id')), 'order'=>array('Domain.id DESC'));
   if(!empty($filters['Domain'])){
    $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'].' AND '. $filters['Domain'] : $filters['Domain'];
   }
   
   $isExport = base64_decode($_REQUEST['_export'])==1? true : false;
   $domains = $Domain->find_all($domainFilter, !$isExport);
   $viewData->set('domains', $domains);
   if($isExport){
      include_once "views/domains_export.php";
      exit;
   }
 }
 
 function domains_add(){
   global $session, $viewData;
   $viewData->setTitle('Add New Domains');
   if(!empty($_POST['data'])){
      $data = $_POST['data'];
      $domains = explode("\n", $data['Domain']['name']);
      $Domain = new Domain;
      foreach($domains as $k=>$domain){
         if(trim($domain)!= ''){
            $domain = str_replace(array('http://', 'https://', 'www.'), '', $domain);
            $domain = array_shift(explode('/', $domain));
            $_data = array('Domain'=>array('name'=>"http://".$domain, 'status'=> $data['Domain']['status'], 'added_on'=>date('Y-m-d H:i:s')));
            $isSubmit = true;
            $_domain = $Domain->find_all(array('where'=>" (Domain.name LIKE 'http://".$domain."%' OR Domain.name LIKE 'http://www.".$domain."%') "));
            if(!empty($_domain)){
               $_domain = array_shift($_domain);
               if($_domain['Domain']['status'] != $data['Domain']['status']){
                  $_data['Domain']['id'] = $_domain['Domain']['id'];
               }else{
                  $isSubmit = false;
               }
            }
            if($isSubmit===true){
               $Domain->save($_data);   
            }
         }
      }
      $session->message('Domains has been added.');
      redirect_to('domains.php');
   }
 }
 
 function domains_list_json(){
   $Domain = new Domain;
   $q = isset($_GET['query']) ? " Domain.name LIKE '%{$_GET['query']}%'" : '';
   $filterArgs = array('fields'=>array('id','name'));
   if(!empty($q)){
      $filterArgs['where'] = $q;
   }
   $list = $Domain->find_list($filterArgs);
   $domains = array();
   if(!empty($list)){
      foreach($list as $_k=>$_v){
         $domains[] = array('id'=>$_k, 'name'=>$_v);
      }
   }
   echo json_encode($domains);
   exit;
 }
 
 function domains_do_action(){
  switch($_REQUEST['whatDo']){
   case 'update_domain':
    __action_update_domain($_REQUEST['data']);
    break;
  }
  exit;
 }
 
 function __domainFilterVars(){
   global $viewData;
  $filter = array();
  $filterVars = array();
  if(!empty($_GET['_dom'])){
   $domain = trim(base64_decode($_GET['_dom']));
   $filterVars['domain'] = $domain;
   $domain = rtrim($domain,"/");	
   $domain = str_replace(array("https://","http://","www."),"",$domain);	
   $domain = preg_replace('/^\./', '', get_domain_name($domain));
   $filter['Domain'][] = " (Domain.name LIKE 'http://".$domain."%' OR Domain.name LIKE 'http://www.".$domain."%') ";
  }
  if(!empty($_GET['_st'])){
      $st = base64_decode($_GET['_st']);
      $filterVars['status'] = explode(',', $st);
      $filter['Domain'][] = " Domain.status IN ('".join("','", $filterVars['status'])."') ";
  }
  $filters['Domain'] = $filter['Domain'] ? implode(' AND ', $filter['Domain']) : '';
   $viewData->set('filterVars', $filterVars);
   return $filters;
 }
 
 function __action_update_domain($data=array()){
   global $database;
   $msg = array('msg'=>'Sorry, domain status could not changed. Please try again later.', 'status'=>'error');
   if(!empty($data)){
      $Domain = new Domain;
    // domain approval accept for campaigns
    if(!empty($data['Domain']['accept'])){
     $_ids = $data['Domain']['accept'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Domain->table_name.' SET status=1 WHERE id IN('. join(',', $_ids). ') AND status!=1');
      $msg = array('msg'=>'Domain status has been changed.', 'status'=>'success');
     }
    }
    // domain approval reject for campaigns
    if(!empty($data['Domain']['reject'])){
     $_ids = $data['Domain']['reject'];
     if(!empty($_ids)){
      $database->query('UPDATE '. $Domain->table_name.' SET status=0 WHERE id IN('. join(',', $_ids). ') AND status!=0');
      $msg = array('msg'=>'Domain status has been changed.', 'status'=>'success');
     }
    }
   }
   echo json_encode($msg);
 }
 
 
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('domains_'.$action)){ 
 call_user_func('domains_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'domains_'.$_GET['act'].'.php' : 'domains_index.php';
include "views/default.php";
 ?>