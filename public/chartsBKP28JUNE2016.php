<?php

require_once("../includes/initialize.php");
require_once("../includes/Highcharts/Highchart.php");
if (!$session->is_logged_in()) {
    redirect_to("login.php");
}

function charts_approval() {
    global $viewData, $session;
    $viewData->setTitle('Approval Links Chart');
    $Approval = new Approval;

    $filters = __chartsFilterVars();

    $approvalLinkFilter = array(
        'fields' => array('id'),
        'contain' => array(
            'User' => array(
                'fields' => array('id', 'first_name', 'last_name')
            ),
            'ApprovalLink' => array(
                'fields' => array('id', 'approve_status')
            )
        )
    );
    if ($session->read('User.user_type') == 'user') {
        $approvalLinkFilter['where'] = "Approval.user_id='" . $session->read('User.id') . "'";
    }

    if (!empty($filters['ApprovalLink'])) {
        $approvalLinkFilter['where'] = !empty($approvalLinkFilter['where']) ? $approvalLinkFilter['where'] . ' AND ' . $filters['ApprovalLink'] : $filters['ApprovalLink'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));
        $filterC = array();
        if (!empty($approvalLinkFilter['where'])) {
            $filterC[] = $approvalLinkFilter['where'];
        }
        $approvalLinkFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(ApprovalLink.added_on, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(ApprovalLink.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }

    $_approvalLinks = $Approval->find_all($approvalLinkFilter);
    if (!empty($_approvalLinks)) {
        $chartData = array();
        $userCount = 0;
        foreach ($_approvalLinks as $_approvls) {
            $user = trim(ucwords($_approvls['User']['first_name'] . ' ' . $_approvls['User']['last_name']));
            if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
                $userCount++;
                $chartData['users'][$userCount] = $user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
            foreach ($_approvls['ApprovalLink'] as $_approvalLink) {
                switch ($_approvalLink['approve_status']) {
                    case '-1':
                        $chartData['series']['Pending']['data'][$userNo] += 1;
                        break;
                    case '0':
                        $chartData['series']['Rejected']['data'][$userNo] += 1;
                        break;
                    case '1':
                        $chartData['series']['Approved']['data'][$userNo] += 1;
                        break;
                    default:
                        break;
                }
            }
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Approved']['data']) || !key_exists($k, $chartData['series']['Approved']['data'])) {
                $chartData['series']['Approved']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Rejected']['data']) || !key_exists($k, $chartData['series']['Rejected']['data'])) {
                $chartData['series']['Rejected']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Pending']['data']) || !key_exists($k, $chartData['series']['Pending']['data'])) {
                $chartData['series']['Pending']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Approved']['data']);
        ksort($chartData['series']['Rejected']['data']);
        ksort($chartData['series']['Pending']['data']);
    }
    $chartData['title'] = 'Approval Links Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Links';
    __setChartData($chartData, "approvals-chart");
}

/* * ** Start: Added by Ajit : 15-DESC-2014  ***************** */

///////////Unique Blogs////////////
function charts_unique_blogs() {
    global $viewData, $session;
    $viewData->setTitle('Unique Blogs Chart');
    $Domain = new Domain;

    $filters = __chartsFilterVars("Domain");
    
    
    $User = new User;
    
    $userList = $User->getList(); 

    $viewData->set('usersList', $userList);  

    $domainFilter = array(
        'fields' => array('id', 'name', 'added_on'),
        'contain' => array(
            'User' => array(
                'fields' => array('id', 'first_name', 'last_name')
            )
        )
    );

    $Approval = new Approval;

    $approvalFilter = array(
        'fields' => array('ip', 'pr', 'da'),
        'limit' => '1',
        'order' => array(
            'Approval.added_on DESC'
        )
    );

    if ($session->read('User.user_type') == 'user') {
        //$domainFilter['where'] = "Domain.user_id='".$session->read('User.id')."'";
    }
    // print_r($filters['Domain']);
    if (!empty($filters['Domain'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Domain'] : $filters['Domain'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Domain.added_on, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Domain.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }

    $sqlquery = "select Domain.*,User.first_name,User.last_name from tbl_plb_domains Domain left join tracking_login User on User.id=Domain.user_id "
            . "left join tbl_approval_links_main Main on Main.domain_id=Domain.id "
            . "left join tbl_approval_links_child Child on Child.parent_id=Main.id where Child.final_link_submit_status='1' and ";
   
    
    if (!empty($filters['Domain'])) {
        $sqlquery.=$filters['Domain'];
    }
    
    if(empty($_GET['_dt'])) {
         if (!empty($filters['Domain'])) {
        $sqlquery.="  and ";
    }
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));

        $sqlquery.="DATE_FORMAT(Domain.added_on, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Domain.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }
    $sqlquery.="group by  Domain.id  order by User.first_name , User.last_name";
    $dbob = New DatabaseObject();
    $_approvalLinks = $dbob->find_by_sqlval($sqlquery);
    // echo "<pre>";
    //  print_r($_approvalLinks);
    //  exit;
    // $_approvalLinks = $Domain->find_all($domainFilter);

    $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

    if ($isExport) {
        /* if(count($_approvalLinks)>0)
          {
          foreach($_approvalLinks as $key=>$val)
          {

          $domain_id=$val['Domain']['id'];
          $approvalFilter['where'] = "Approval.domain_id='".$domain_id."'";
          $approvals = $Approval->fetchAll($approvalFilter);

          $_approvalLinks[$key]['Domain']['da']=$approvals[0]['Approval']['da'];
          $_approvalLinks[$key]['Domain']['ip']=$approvals[0]['Approval']['ip'];
          $_approvalLinks[$key]['Domain']['pr']=$approvals[0]['Approval']['pr'];



          }


          } */

        // echo "<pre>";
        //print_r($_approvalLinks);

        $viewData->set('uniqueblogs', $_approvalLinks);

        include_once "views/unique_blogs_export.php";
        exit;
        exit;
    }

    if (!empty($_approvalLinks)) {
        $chartData = array();
        $userCount = 0;
        foreach ($_approvalLinks as $_approvls) {
            //  echo "<pre>";
            //  print_r($_approvls);

            $user = trim(ucwords($_approvls['first_name'] . ' ' . $_approvls['last_name']));
            if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
                $userCount++;
                $chartData['users'][$userCount] = $user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;
            //  foreach($_approvls['Domain'] as $_approvalLink){
            $chartData['series']['Blog']['data'][$userNo] += 1;
            //}
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Blog']['data']) || !key_exists($k, $chartData['series']['Blog']['data'])) {
                $chartData['series']['Blog']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Blog']['data']);
    }
    $chartData['title'] = 'Unique Blogs Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Blogs';
    __setChartData($chartData, "unique-blogs-chart");
}

/* * ** End: Added by Ajit: 15-DESC-2015  ***************** */

/* * ** Start: Added by Ajit : :29-JAN-2016  ***************** */

///////////Spend////////////
function charts_spend() {
    global $viewData, $session;
    $viewData->setTitle('Spend Chart');
     $Campaign = new Campaign;

    $filters = __chartsFilterVars("Tracking");
    $campaignsList = $Campaign->getList();

    $viewData->set('campaignsList', $campaignsList);

    if (!empty($filters['Tracking'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Tracking'] : $filters['Tracking'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }
    $sqlquery = "select Tracking.Campaign_name,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";

    if (!empty($filters['Tracking'])) {
        $sqlquery.=$filters['Tracking'];
    } 
    
    if(empty($_GET['_dt'])) {
        
        if (!empty($filters['Tracking'])) {
        $sqlquery.=  "  and ";
    }
        
        $dateStart = date('Y-m-01');

        $dateEnd = date('Y-m-' . date('t'));

        $sqlquery.="DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }
    $sqlquery.="   and Tracking.Status='Paid'  group by Tracking.id order by Tracking.Campaign_name asc";
    $dbob = New DatabaseObject();
    $spenddatas = $dbob->find_by_sqlval($sqlquery);
    //  echo "<pre>";
//   print_r($spenddatas);
//   exit;
    // $_approvalLinks = $Domain->find_all($domainFilter);

    $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

    if ($isExport) {

        $viewData->set('spenddatas', $spenddatas);

        include_once "views/spend_export.php";
        exit;
        exit;
    }

    if (!empty($spenddatas)) {
         
        $chartData = array();
        $userCount = 0;
        $convertedamouuntinr = get_currency('USD', 'INR', 1);
        $convertedamouuntponds = get_currency('USD', 'GBP', 1);
        $convertedamouunteuro = get_currency('USD', 'EUR', 1);
        foreach ($spenddatas as $_spend) {
            //  echo "<pre>";
            //  print_r($_approvls);

            $campaign = trim($_spend['Campaign_name']);
            $currency = trim($_spend['Currency']);
            $amount = trim($_spend['Amount']);
            if ($currency == 'INR') {
                $amount = round(($amount / $convertedamouuntinr), 2);
            } elseif ($currency == 'Euro') {
                $amount = round(($amount / $convertedamouunteuro), 2);
            } elseif ($currency == 'Pounds') {
                $amount = round(($amount / $convertedamouuntponds), 2);
            } else {
                $amount = trim($_spend['Amount']);
            }



            if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                $userCount = $userCount + $amount;
                $chartData['users'][$userCount] = $campaign;
            }
            $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;
            //  foreach($_approvls['Domain'] as $_approvalLink){
            $chartData['series']['Amount']['data'][$userNo] += $amount;
            //}
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Amount']['data']) || !key_exists($k, $chartData['series']['Amount']['data'])) {
                $chartData['series']['Amount']['data'][$k] = 0;
            }
        }
    
//print_r($chartData);
// echo "//////////dddddddddddasfasfasfasfasf/////////";
   //asort($chartData['users']);
        
    }
    
    $chartData['title'] = 'Spend Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Amount';

    __setChartData($chartData, "spend-chart");
}

///////////Spend by Campaign////////////
function charts_spend_campaign() {
    global $viewData, $session;
    $viewData->setTitle('Spend By Campaign  Chart ');
    $User = new User;
      $Campaign = new Campaign;

    $filters = __chartsFilterVars("Tracking");

    $userList = $User->getList(); 

    $viewData->set('usersList', $userList);  
    $campaignsList = $Campaign->getList();
    $viewData->set('campaignsList', $campaignsList);

    if (!empty($filters['Tracking'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Tracking'] : $filters['Tracking'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }



    $sqlquery = "select Tracking.Campaign_name,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";

    if (!empty($filters['Tracking'])) {
        $sqlquery.=$filters['Tracking'];
    } 
    
    if(empty($_GET['_dt'])) {
        
        if (!empty($filters['Tracking'])) {
        $sqlquery.=  "  and ";
    }
        
        $dateStart = date('Y-m-01');

        $dateEnd = date('Y-m-' . date('t'));

        $sqlquery.="DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }
    if (!empty($_GET['_camp'])) {
        $campaignid = base64_decode($_GET['_camp']);
    } else {

        $campaignid = '284';
         $sqlquery.="     and Tracking.Campaign_id='" . $campaignid . "'";
    }
    $sqlquery.="   and Tracking.Status='Paid' ";



    $sqlquery.="order by USER.first_name asc,USER.last_name asc";



    $dbob = New DatabaseObject();
    $spenddatas = $dbob->find_by_sqlval($sqlquery);
    //  echo "<pre>";
//   print_r($spenddatas);
//   exit;
    // $_approvalLinks = $Domain->find_all($domainFilter);

    $campaigndata = $Campaign->find_all(array('where' => "Campaign.id = '{$campaignid}'"));

    $viewData->set('campaigndata', $campaigndata);

    $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

    if ($isExport) {

        $viewData->set('spenddatas', $spenddatas);

        include_once "views/spend_campaign_export.php";
        exit;
        exit;
    }

    if (!empty($spenddatas)) {
        $chartData = array();
        $userCount = 0;
        $convertedamouuntinr = get_currency('USD', 'INR', 1);
        $convertedamouuntponds = get_currency('USD', 'GBP', 1);
        $convertedamouunteuro = get_currency('USD', 'EUR', 1);
        foreach ($spenddatas as $_spend) {
            //  echo "<pre>";
            // print_r($_spend);

            $campaign = trim($_spend['first_name'] . ' ' . $_spend['last_name']);
            $currency = trim($_spend['Currency']);
            $amount = trim($_spend['Amount']);
            if ($currency == 'INR') {
                $amount = round(($amount / $convertedamouuntinr), 2);
            } elseif ($currency == 'Euro') {
                $amount = round(($amount / $convertedamouunteuro), 2);
            } elseif ($currency == 'Pounds') {
                $amount = round(($amount / $convertedamouuntponds), 2);
            } else {
                $amount = trim($_spend['Amount']);
            }



            if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                $userCount = $userCount + $amount;
                $chartData['users'][$userCount] = $campaign;
            }
            $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;
            //  foreach($_approvls['Domain'] as $_approvalLink){
            $chartData['series']['Amount']['data'][$userNo] += $amount;
            //}
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Amount']['data']) || !key_exists($k, $chartData['series']['Amount']['data'])) {
                $chartData['series']['Amount']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Amount']['data']);
    }
    $chartData['title'] = 'Spend Chart by ' . $campaigndata[0]['Campaign']['name'];
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Amount';




    __setChartData($chartData, "spend-chart");
}

///////////Spend by Campaign////////////
function charts_spend_user() {
    global $viewData, $session;
    $viewData->setTitle('Spend By User  Chart ');
    $User = new User;
      $Campaign = new Campaign;

    $filters = __chartsFilterVars("Tracking");

    $userList = $User->getList(); 

    $viewData->set('userList', $userList);  
    $campaignsList = $Campaign->getList();
    $viewData->set('campaignsList', $campaignsList);
   

    if (!empty($filters['Tracking'])) {
        $domainFilter['where'] = !empty($domainFilter['where']) ? $domainFilter['where'] . ' AND ' . $filters['Tracking'] : $filters['Tracking'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {

        $filterC = array();
        if (!empty($domainFilter['where'])) {
            $filterC[] = $domainFilter['where'];
        }
        $domainFilter['where'] = array_merge($filterC, array(
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'",
            "DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'"
        ));
    }



    $sqlquery = "select Tracking.Campaign_name,Tracking.Amount,Tracking.Currency,Tracking.payment_date,USER.first_name,USER.last_name from tracking_data Tracking left join tracking_login USER on USER.id=Tracking.user_id  where ";

    if (!empty($filters['Tracking'])) {
        $sqlquery.=$filters['Tracking'];
    }
    
    if(empty($_GET['_dt'])) {
        
        if (!empty($filters['Tracking'])) {
        $sqlquery.=  "  and ";
    }
        
        $dateStart = date('Y-m-01');

        $dateEnd = date('Y-m-' . date('t'));

        $sqlquery.="DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') >= '" . $dateStart . "'  and DATE_FORMAT(Tracking.payment_date, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }
   
 if (empty($_GET['_uid'])) {
        $userid = '12';
         $sqlquery.="   and Tracking.user_id='" . $userid . "'";
 }
 else {
     
  $userid =base64_decode($_GET['_uid']);
     
 }
    
    
    $sqlquery.="  and Tracking.Status='Paid'";



    $sqlquery.="order by Tracking.Campaign_name ";



    $dbob = New DatabaseObject();
    $spenddatas = $dbob->find_by_sqlval($sqlquery);
    //  echo "<pre>";
//   print_r($spenddatas);
//   exit;
    // $_approvalLinks = $Domain->find_all($domainFilter);

    $userdata = $User->find_all(array('where' => "User.id = '{$userid}'"));

    $viewData->set('userdata', $userdata);

    $isExport = base64_decode($_REQUEST['_export']) == 1 ? true : false;

    if ($isExport) {

        $viewData->set('spenddatas', $spenddatas);

        include_once "views/spend_user_export.php";
        exit;
        exit;
    }

    if (!empty($spenddatas)) {
        $chartData = array();
        $userCount = 0;

        $convertedamouuntinr = get_currency('USD', 'INR', 1);
        $convertedamouuntponds = get_currency('USD', 'GBP', 1);
        $convertedamouunteuro = get_currency('USD', 'EUR', 1);

        foreach ($spenddatas as $_spend) {
            //  echo "<pre>";
            // print_r($_spend);

            $campaign = trim($_spend['Campaign_name']);
            $currency = trim($_spend['Currency']);
            $amount = trim($_spend['Amount']);
            if ($currency == 'INR') {
                $amount = round(($amount / $convertedamouuntinr), 2);
            } elseif ($currency == 'Euro') {
                $amount = round(($amount / $convertedamouunteuro), 2);
            } elseif ($currency == 'Pounds') {
                $amount = round(($amount / $convertedamouuntponds), 2);
            } else {
                $amount = trim($_spend['Amount']);
            }



            if (!isset($chartData['users']) || !in_array($campaign, $chartData['users'])) {

                $userCount = $userCount + $amount;
                $chartData['users'][$userCount] = $campaign;
            }
            $userNo = isset($chartData['users']) ? array_search($campaign, $chartData['users']) : 0;
            //  foreach($_approvls['Domain'] as $_approvalLink){
            $chartData['series']['Amount']['data'][$userNo] += $amount;
            //}
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Amount']['data']) || !key_exists($k, $chartData['series']['Amount']['data'])) {
                $chartData['series']['Amount']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Amount']['data']);
    }
    $chartData['title'] = 'Spend Chart by ' . $userdata[0]['User']['first_name'] . ' ' . $userdata[0]['User']['last_name'];
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Amount';




    __setChartData($chartData, "spend-chart");
}

function get_currency($from_Currency, $to_Currency, $amount) {
    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);

    $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

    $ch = curl_init();
    $timeout = 0;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode('bld>', $rawdata);
    $data = explode($to_Currency, $data[1]);

    return round($data[0], 2);
}

/* * ** End: Added by Ajit:29-JAN-2016  ***************** */

function charts_payment() {
    global $viewData, $database;
    $viewData->setTitle('Payments Chart');
    $Payment = new Payment;
    $User = new User;

    $filters = __chartsFilterVars('Payment');

    $paymentFilters = "Payment.status IN ('Paid', 'Underprocess','Rejected')";
    if (!empty($filters['Payment'])) {
        $paymentFilters = !empty($paymentFilters) ? $paymentFilters . ' AND ' . $filters['Payment'] : $filters['Payment'];
        $dateStart = $viewData->get('dateFrom');
        $dateEnd = $viewData->get('dateTo');
    } else {
        $dateStart = date('Y-m-01');
        $dateEnd = date('Y-m-' . date('t'));
        $paymentFilters = "DATE_FORMAT(Payment.added_on, '%Y-%m-%d') >= '" . $dateStart . "'";
        $paymentFilters .= " AND ";
        $paymentFilters .= "DATE_FORMAT(Payment.added_on, '%Y-%m-%d') <= '" . $dateEnd . "'";
    }

    $paymentQuery = $database->query("SELECT COUNT(Payment.id) AS links, Payment.status AS status,  CONCAT(User.first_name, ' ', User.last_name) AS name 
FROM " . $Payment->table_name . " Payment INNER JOIN " . $User->table_name . " User ON(User.id=Payment.user_id)
 WHERE  " . $paymentFilters . " GROUP BY Payment.status, Payment.user_id");
    $payments = $database->fetch_data_array($paymentQuery);
    $chartData = array();
    $userCount = 0;
    if (!empty($payments)) {
        foreach ($payments as $payment) {
            $user = trim(ucwords($payment['name']));
            if (!isset($chartData['users']) || !in_array($user, $chartData['users'])) {
                $userCount++;
                $chartData['users'][$userCount] = $user;
            }
            $userNo = isset($chartData['users']) ? array_search($user, $chartData['users']) : 0;

            switch ($payment['status']) {
                case 'Underprocess':
                    $chartData['series']['Pending']['data'][$userNo] = (int) $payment['links'];
                    break;
                case 'Rejected':
                    $chartData['series']['Rejected']['data'][$userNo] = (int) $payment['links'];
                    break;
                case 'Paid':
                    $chartData['series']['Done']['data'][$userNo] = (int) $payment['links'];
                    break;
                default:
                    break;
            }
        }

        foreach ($chartData['users'] as $k => $u) {
            if (!isset($chartData['series']['Done']['data']) || !key_exists($k, $chartData['series']['Done']['data'])) {
                $chartData['series']['Done']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Rejected']['data']) || !key_exists($k, $chartData['series']['Rejected']['data'])) {
                $chartData['series']['Rejected']['data'][$k] = 0;
            }
            if (!isset($chartData['series']['Pending']['data']) || !key_exists($k, $chartData['series']['Pending']['data'])) {
                $chartData['series']['Pending']['data'][$k] = 0;
            }
        }

        ksort($chartData['series']['Done']['data']);
        ksort($chartData['series']['Rejected']['data']);
        ksort($chartData['series']['Pending']['data']);
    }
    $chartData['title'] = 'Payment Chart';
    $chartData['subtitle'] = date('F j, Y', strtotime($dateStart)) . (!empty($dateEnd) ? ' to ' . date('F j, Y', strtotime($dateEnd)) : '');
    $chartData['yAxis']['title'] = 'Links';
    __setChartData($chartData, "payments-chart");
}

function __setChartData($chartData, $where = '') {
    global $viewData;
    $chart = new Highchart();
    $chart->chart->renderTo = $where;
    $chart->credits->enabled = false;
    $chart->chart->type = "column";
    $chart->title->text = $chartData['title'];
    $chart->subtitle->text = $chartData['subtitle'];

    $chart->xAxis->categories = !empty($chartData['users']) ? array_values($chartData['users']) : array();

    $chart->yAxis->min = 0;
    $chart->yAxis->title->text = $chartData['yAxis']['title'];
    $chart->tooltip->headerFormat = '<span style="font-size:10px">{point.key}</span><table>';
    $chart->tooltip->pointFormat = '<tr><td style="color:{series.color};padding:0">{series.name}: </td> <td style="padding:0"><b>{point.y}</b></td></tr>';
    $chart->tooltip->footerFormat = '</table>';
    $chart->tooltip->shared = true;
    $chart->tooltip->useHTML = true;

    $chart->plotOptions->column->pointPadding = 0.2;
    $chart->plotOptions->column->borderWidth = 0;
    if (!empty($chartData['series'])) {
        foreach ($chartData['series'] as $k => $v) {
            $chart->series[] = array(
                'name' => $k,
                'data' => array_values($v['data'])
            );
        }
    }
    $viewData->set('chart', $chart);
}

function __chartsFilterVars($model = 'ApprovalLink') {
    global $viewData;
    
    $filter = array();
    $filterVars = array();
     $field = "$model.";
        switch ($model) {
            case 'ApprovalLink':
                $field .= 'added_on';
                break;
            case 'Payment':
                $field .= 'added_on';
                break;
            case 'Domain':
                $field .= 'added_on';
                break;
            case 'Tracking':
                $field .= 'payment_date';

                break;
        }
    if (!empty($_GET['_dt'])) {
        list($dateFrom, $dateTo) = explode('_', base64_decode($_GET['_dt']));
       
        if (!empty($dateFrom) and ! empty($dateTo)) {
            $filterVars['dateFrom'] = $dateFrom;
            $filterVars['dateTo'] = $dateTo;
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') >= '" . date('Y-m-d', strtotime($dateFrom)) . "'";
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') <= '" . date('Y-m-d', strtotime($dateTo)) . "'";
        } else if (!empty($dateFrom) and empty($dateTo)) {
            $filterVars['dateFrom'] = $dateFrom;
            $filter[$model][] = "DATE_FORMAT($field, '%Y-%m-%d') = '" . date('Y-m-d', strtotime($dateFrom)) . "'";
        }
     
       
    }
    if (!empty($_GET['_camp'])) {
        $campigns = base64_decode($_GET['_camp']);
        $filterVars['campaigns'] = explode(',', $campigns);
        $filter[$model][] = $model.'.campaign_id IN (' . $campigns . ') ';
    }
    if (!empty($_GET['_uid'])) {
        $users = base64_decode($_GET['_uid']);
        $filterVars['users'] = explode(',', $users);
        $filter[$model][] = $model.'.user_id IN (' . $users . ') ';
    }
    $filters[$model] = $filter[$model] ? implode(' AND ', $filter[$model]) : '';
    $viewData->set('filterVars', $filterVars);
    $viewData->set('dateFrom', $dateFrom);
    if (strtotime($dateFrom) != strtotime($dateTo)) {
        $viewData->set('dateTo', $dateTo);
    }
    return $filters;
}

// auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'approval';
if (function_exists('charts_' . $action)) {
    call_user_func('charts_' . $action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'charts_' . $_GET['act'] . '.php' : 'charts_approval.php';
include "views/default.php";
?>