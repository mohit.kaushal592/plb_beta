  </div>
</div>
<!--end-main-container-part-->
<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> <?php echo date('Y') ?> &copy; Adlift. Brought to you by <a href="http://adlift.com/">Adlift</a> </div>
</div>

<!--end-Footer-part-->

<?php $viewData->scripts(array(
          'js/jquery.min.js',
          'js/jquery-ui_1_10_3.js',
          'js/jquery.validate.js',
          'js/jquery.uniform.js',
          'js/bootstrap.min.js',
          'js/bootstrap-datepicker.js',
          'js/bootstrap-typeahead.js',
          'js/jquery.tablesorter.min.js',
          'js/jquery.base64.js',
          'js/adlift.js',
          'js/colResizable-1.3.min.js',
          'js/jquery.parseurl.min.js',
          'js/jquery.multiselect.js',
          'js/jquery.multiselect.filter.js',
          'js/select2.min.js'
          ))
?>
<?php $viewData->scriptStart() ?>
    jQuery(document).ready(function($){
      tblResizeInIt();
       multiSelectRender();
       uniformInIt();
       $('.datepicker').datepicker();
       $('.datepicker').on('changeDate', function(ev){
    $(this).datepicker('hide');
});
    });
  <?php $viewData->scriptEnd() ?>
<?php $viewData->script_for_layout() ?>

<?php sqlDebugLogs() ?>
</body>
</html>