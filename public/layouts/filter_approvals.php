<div class="widget-box">
    <?php $filterVars = $viewData->get('filterVars') ?>
    <div class="widget-title">
        <span class="icon"> <i class="icon-search"></i> </span>
        <h5>Advance Filter</h5>
    </div>
    <div class="widget-content nopadding">
        <form action="#" method="post" class="form-horizontal filter-form" id="FilterForm">
            <div class="control-group">
                <div class="span3">
                    <label>User</label>
                    <select name="users[]" id="FilterUser" multiple="multiple">
                    <?php echo getFormOptions($viewData->get('usersList'), $filterVars['users']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Campaign</label>
                    <select name="campaigns[]" id="FilterCampaign"  multiple="multiple">
                    <?php echo getFormOptions($viewData->get('campaignsList'), $filterVars['campaigns']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Domain</label>
                    <input name="domain" autocomplete="off" id="FilterDomain" type="text" class="span10" value="<?php echo !empty($filterVars['domain']) ? $filterVars['domain'] : '' ?>" class="typeahead" />
                </div>
                <div class="span3">
                    <label>Status (Approval Phase I)</label>
                    <select name="approval_st_p1[]" id="FilterAppStatusPhase1" multiple="multiple">
                    <?php echo getFormOptions(array('1'=>'Approved', '0'=>'Disapproved', '-1'=>'For approval'), $filterVars['status']); ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="span3">
                    <label>Status (Approval Phase II)</label>
                    <select name="approval_st_p2[]" id="FilterAppStatusPhase2" multiple="multiple">
                    <?php echo getFormOptions(array('1'=>'Approved', '0'=>'Disapproved', '-1'=>'For approval'), $filterVars['status_phase2']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Forward Status</label>
                    <select name="forword" id="FilterForword">
                    <?php echo getFormOptions(array('2'=>'All', '1'=>'Forwarded', '0'=>'Non Forwarded'), $filterVars['forward']); ?>
                    </select>
                </div>
                <div class="span5">
                    <label>Date</label>
                    <div  data-date="" class="input-append date datepicker">
                        <input type="text" name="from" value="<?php echo !empty($filterVars['dateFrom']) ? $filterVars['dateFrom'] : '' ?>" id="FilterDateFrom"  data-date-format="mm-dd-yyyy" class="span10" >
                        <span class="add-on"><i class="icon-th"></i></span> 
                    </div>
                    <div  data-date="" class="input-append date datepicker">
                        <input type="text" value="<?php echo !empty($filterVars['dateTo']) ? $filterVars['dateTo'] : '' ?>" name="to" id="FilterDateTo"  data-date-format="mm-dd-yyyy" class="span10" >
                        <span class="add-on"><i class="icon-th"></i></span> 
                    </div>
                </div>
            </div>
            <div class="control-group">
                <div class="span3">
                    <label>Link Status</label>
                    <?php
                        $isLinkStatusEnabled = (
                        (in_array($filterVars['link_status'], array(0,1, -1)) and $filterVars['link_status']!='')
                        or
                        (
                        (count($filterVars['status_phase2'])==1 and $filterVars['status_phase2'][0]==1)
                        or
                        (count($filterVars['status'])==1 and $filterVars['status'][0]==1)
                        )
                        );
                        ?>
                    <select name="link_status" id="FilterLinkStatus" data-dflt="<?php echo $filterVars['link_status'] ?>" <?php echo $isLinkStatusEnabled==true ? '' : 'disabled="disabled"' ?>>
                    <?php echo getFormOptions(array('-1'=>'All', '1'=>'Submitted', '0'=>'Not Submitted'), $filterVars['link_status']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Content Writer</label>
                    <select name="content_writer" id="FilterContentWriter">
                    <?php echo getFormOptions(array('-1'=>'All', '0'=>'Adlift', '1'=>'External'), $filterVars['content_writer']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>DA</label>
                    <select name="da" id="FilterDA">
                        <option value="">Select DA</option>
                        <?php echo getFormOptions($viewData->get('daOptions'), $filterVars['da']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>SC</label>
                    <select name="sc" id="FilterSC">
                        <option value="">Select SC</option>
                        <?php echo getFormOptions($viewData->get('scOptions'), $filterVars['sc']); ?>
                    </select>
                </div>
                </div>
				<div class="control-group">
                <div class="span3">
                    <label>Country</label>
                    <select name="ac" id="FilterAC">
                        <option value="">Country</option>
                        <?php echo getFormOptions($viewData->get('acOptions'), $filterVars['ac']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Adult Content</label>
                    <select name="adult" id="FilterAdult">
                    <?php echo getFormOptions(array('-1'=>'select','1'=>'Yes', '0'=>'No'), $filterVars['adult']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Gambling/Casino/Betting</label>
                    <select name="gambling" id="FilterGambling">
                    <?php echo getFormOptions(array('-1'=>'select','1'=>'Yes', '0'=>'No'), $filterVars['gambling']); ?>
                    </select>
                </div>
                <div class="span3">
                    <label>Index Status</label>
                    <select name="indexStatus" id="FilterIndexStatus">
                    <?php echo getFormOptions(array('-1'=>'select','Index'=>'Index', 'Not Index'=>'Not Index'), $filterVars['indexStatus']); ?>
                    </select>
                </div>
                <div class="span3" style="margin-top:5px;margin-left:0px">
                    <label>Select Category</label>
                    <select name="data[Approval][category][]" id="FilterCategory" multiple="multiple"  title="Please select category.">
                        <?php echo getFormOptions(array("Business"=>"Business","Finance"=>"Finance","Lifestyle/Fashion"=>"Lifestyle/Fashion","Technology"=>"Technology","Health"=>"Health","Education"=>"Education","Marketing/digital marketing"=>"Marketing/digital marketing","Travel"=>"Travel","News"=>"News","Sports"=>"Sports","Automobile"=>"Automobile","Real estate"=>"Real estate","Food"=>"Food","Parenting"=>"Parenting","Home decor/ improvement"=>"Home decor/ improvement","Generic"=>"Generic"), $filterVars['filter_category']) ?>
                    </select>
                </div>
            </div>
            <div class="form-actions">
                <button type="reset" class="btn btn-primary">Reset</button>
                <button type="submit" class="btn btn-success">Filter</button>
            </div>
        </form>
    </div>
</div>

<?php $viewData->scriptStart() ?>
jQuery.fn.reLoadLinkStatus = function(){
		var anotherSelectId = $(this).attr('id')=='FilterAppStatusPhase1' ? 'FilterAppStatusPhase2' : 'FilterAppStatusPhase1';
		var selectedOpt = $(this).multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		
		var anotherSelect = $('select#'+ anotherSelectId).multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		     
		var valDefault= $('select#FilterLinkStatus').attr('data-dflt');
		valDefault = valDefault.length>0 ? valDefault : '-1';

		if((selectedOpt.length==1 && selectedOpt[0]=='1') || (anotherSelect.length==1 && anotherSelect[0]=='1')){
			$('select#FilterLinkStatus').multiselect('enable');
			
		}else{
			$('select#FilterLinkStatus').val(valDefault).multiselect('disable');
		}
	}
$(document).ready(function(){
	// autocomplete
	$('#FilterDomain').typeahead({
		ajax: 'domains.php?act=list_json',
		display: 'name',
		val: 'name'
	});
	
	$('select[id^=FilterAppStatusPhase]').on('multiselectclick', function(event, ui){
		$(this).reLoadLinkStatus();
	});
	
	$('#FilterForm').bind('submit', function(e){
		e.preventDefault();
		var users = $("#FilterUser").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var campaigns = $("#FilterCampaign").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var approvalStPhase1 = $("#FilterAppStatusPhase1").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var approvalStPhase2 = $("#FilterAppStatusPhase2").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();
		var forword = $("#FilterForword").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var contentWriter = $("#FilterContentWriter").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var _daVal = $("#FilterDA").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var _scVal = $("#FilterSC").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];
		var _acVal = $("#FilterAC").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0];

		var linkStatus = !($("#FilterLinkStatus").prop('disabled')) ? ($("#FilterLinkStatus").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get()[0]) : '';
		contentWriter = contentWriter > -1 ? contentWriter : '';
		linkStatus = linkStatus >= -1 ? linkStatus : '';
		var dateFrom = $('#FilterDateFrom').val();
		var dateTo = $('#FilterDateTo').val();
		var domain = $('#FilterDomain').val();
		var gambling = $('#FilterGambling').val();
		var adult = $('#FilterAdult').val();
		var indexStatus = $('#FilterIndexStatus').val();
		var dateRange = (dateFrom+'_'+dateTo);
		dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
		var canSubmit = true;
		var _rp = $('#RowPerPage').val();
		
		if(approvalStPhase1.length>0 && approvalStPhase2.length>0){
			alert('Please choose either Status (Approval Phase I) or Status (Approval Phase II).');
			canSubmit=false;
		}
        var filter_category = $("#FilterCategory").multiselect("getChecked").map(function(){
			return this.value;    
		     }).get();  
		if(canSubmit==true){
			var curUrl = $.parseUrl();
			var queryString = curUrl.query;
			queryString._uid = $.base64.encode(users.toString());
			queryString._camp = $.base64.encode(campaigns.toString());
			queryString._st = $.base64.encode(approvalStPhase1.toString());
			queryString._st2 = $.base64.encode(approvalStPhase2.toString());
			queryString._dt = $.base64.encode(dateRange);
			queryString._dom = $.base64.encode(domain);
			queryString._gml = $.base64.encode(gambling);
			queryString._adlt = $.base64.encode(adult);
			queryString._indxSts = $.base64.encode(indexStatus);
			queryString._fw = $.base64.encode(forword);
			queryString._cw = $.base64.encode(contentWriter);
			queryString._ls = $.base64.encode(linkStatus);
			queryString._rp = _rp;
			queryString._da = $.base64.encode(_daVal);
			queryString._sc = $.base64.encode(_scVal);
			queryString._ac = $.base64.encode(_acVal);
            queryString._fc = $.base64.encode(filter_category.toString()); 
			queryString.p = 1;
			var urlParams = [];
			$.each(queryString, function(k,v){
				if(v.length>0){
					urlParams.push(k+'='+v);
				}
			});
			if(urlParams.length>=1){
				window.location = '?'+urlParams.join('&');
			}
		}
	});
	
	$('#RowPerPage').on('change', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var _rp = $('#RowPerPage').val();
		var urlParams = [];
		queryString._rp=_rp;
		queryString.p=1;
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});
<?php $viewData->scriptEnd() ?>