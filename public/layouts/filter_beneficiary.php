<div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-search"></i> </span>
          <h5>Advance Filter</h5>
        </div>
      <div class="widget-content nopadding" id="FilterFormBlock">
       
   </div>
  </div>

<?php $viewData->scriptStart() ?>

function inItFilterForm(){
	multiSelectRender();
	uniformInIt();
	checkAllInIt();
	// autocomplete
	$('#FilterDomain').typeahead({
		ajax: 'domains.php?act=list_json',
		display: 'name',
		val: 'name'
	});
	$('.datepicker').datepicker();
	$('#FilterForm').on('submit', function(e){
		e.preventDefault();
		
		
	
		
		
		
		var exportDta = $('#exportList').val();
		
		var dateFrom = $('#FilterDateFrom').val();
		var dateTo = $('#FilterDateTo').val();
		var dateRange = (dateFrom+'_'+dateTo);
		dateRange = dateRange.replace(/^\s*\_\s*$/g, '');
       
		var bencode = $('#Filterbencode').val();
		var benname = $('#Filterbenname').val();
		var benaccount = $('#Filterbenaccount').val();
		var ifsccode = $('#Filterifsccode').val();

		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		
		queryString._bc = $.base64.encode(bencode);
		queryString._bn = $.base64.encode(benname);
		queryString._ba = $.base64.encode(benaccount);
		queryString._bi = $.base64.encode(ifsccode);		
		queryString._export = $.base64.encode(exportDta);
		queryString._dt = $.base64.encode(dateRange);
		var urlParams = [];
		
		var postFormHtml = [];
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
				postFormHtml.push("<input type='text' name='"+k+"' value='"+v+"' />");
			}
		});
		
		/*if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}*/
		
		if(postFormHtml.length>0){
			
				var urlToSent = parseInt(exportDta)==1? 'beneficiary.php?act=data' : $(this).attr('action'); 
			
			var formFilter = $('<form acttion="'+ urlToSent +'" method="post"></form>');
			formFilter.attr('action', urlToSent);
			$('body').append(formFilter);
			formFilter.html(postFormHtml.join(''));
			formFilter.submit();
		}else{
			alert('Choose any option for filter.');
		}
	});
}
$(document).ready(function(){
	var postData = <?php echo json_encode($viewData->get('filterVars')) ?>;
	
	var loader = '<div style="display:block;width:100%;text-align: center;"><img src="img/ajax-loader.gif" /></div>';
	$('#FilterFormBlock').html(loader);
	$.post('beneficiary.php?act=filter_form',
		{'filterVars':postData},
		function(data){
			$('#FilterFormBlock').html(data);
			
			inItFilterForm();
		}, 'html')
		.fail(function(){
			$('#FilterFormBlock').html('<div>Error occurred in filter form processing.</div>');
		});
});
<?php $viewData->scriptEnd() ?>