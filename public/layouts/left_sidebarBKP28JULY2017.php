<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="<?php echo is_active_menu('index.php') ? 'active' : '' ?>"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span> <span class="label label-important">+</span></a>
	<ul>
        <li class="<?php echo is_active_menu('charts.php?act=approval') ? 'active' : '' ?>"><a href="charts.php?act=approval">Approval</a></li>
        <li class="<?php echo is_active_menu('charts.php?act=payment') ? 'active' : '' ?>"><a href="charts.php?act=payment">Payment</a></li>
        <?php if(in_array($session->read('User.user_type'), array('superadmin'))): ?>
        <li class="<?php echo is_active_menu('charts.php?act=unique_blogs') ? 'active' : '' ?>"><a href="charts.php?act=unique_blogs">Unique Blogs</a></li>
   <li class="<?php echo is_active_menu('charts.php?act=spend') ? 'active' : '' ?>"><a href="charts.php?act=spend">Spend</a></li>
   <li class="<?php echo is_active_menu('charts.php?act=spend_campaign') ? 'active' : '' ?>"><a href="charts.php?act=spend_campaign">Spend By Campaign  </a></li>
   <li class="<?php echo is_active_menu('charts.php?act=spend_user') ? 'active' : '' ?>"><a href="charts.php?act=spend_user">Spend By User </a></li>
   <li class="<?php echo is_active_menu('charts.php?act=influencer') ? 'active' : '' ?>"><a href="charts.php?act=influencer">Influencers </a></li>
  
            <?php endif ?>        
<!--<li class="<?php echo is_active_menu('charts.php?act=link_submission_user') ? 'active' : '' ?>"><a href="charts.php?act=link_submission_user">Link Submission User Wise</a></li>
	<li class="<?php echo is_active_menu('charts.php?act=link_submission_clientent') ? 'active' : '' ?>"><a href="charts.php?act=link_submission_client">Link Submission Client Wise</a></li>-->
      </ul>
    </li> 
    <li class="submenu"> <a href="#"><i class="icon icon-check"></i> <span>Approval</span> <span class="label label-important">+</span></a>
      <ul>
        <li class="<?php echo is_active_menu('approvals.php?act=add_link') ? 'active' : '' ?>"><a href="approvals.php?act=add_link">Add Link</a></li>
        <li class="<?php echo is_active_menu('approvals.php') ? 'active' : '' ?>"><a href="approvals.php">Listing</a></li>
        <?php if(in_array($session->read('User.user_type'), array('superadmin'))): ?>
        <li class="<?php echo is_active_menu('approvals.php?act=add_exceptions') ? 'active' : '' ?>"><a href="approvals.php?act=add_exceptions">Add Duplicate Exceptions</a></li>
        
        <li class="<?php echo is_active_menu('approvals.php?act=list_exceptions') ? 'active' : '' ?>"><a href="approvals.php?act=list_exceptions">List Duplicate Exceptions</a></li>
        <?php endif ?>  
      </ul>
    </li> 
<?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
    <li class="submenu"> <a href="#"><i class="icon icon-user"></i> <span>Users</span> <span class="label label-important">+</span></a>
      <ul>
        <li class="<?php echo is_active_menu('users.php?act=add') ? 'active' : '' ?>"><a href="users.php?act=add">Add User</a></li>
        <li class="<?php echo is_active_menu('users.php') ? 'active' : '' ?>"><a href="users.php">User Listing</a></li>
      </ul>
    </li>    
  <?php endif ?>  
    <li class="<?php echo is_active_menu('submitted_links.php') ? 'active' : '' ?>"><a href="submitted_links.php"><i class="icon icon-link"></i> <span>Link Listing</span></a></li>
   
   <?php //if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
   <?php if(in_array($session->read('User.user_type'), array('superadmin'))): ?>
    <li class="<?php echo is_active_menu('paypal.php') ? 'active' : '' ?>"> <a href="paypal.php"><i class="icon icon-money"></i> <span>Paypal Listing</span></a> </li>
   <?php endif ?>
   <?php if(in_array($session->read('User.user_type'), array('superadmin','admin'))): ?>
    <li class="submenu">
      <a href="#"><i class="icon icon-tint"></i> <span>Domains</span><span class="label label-important">+</span></a>
      <ul>
	<li class="<?php echo is_active_menu('domains.php?act=add') ? 'active' : '' ?>"><a href="domains.php?act=add"><span>Add New</span></a></li>
	<li class="<?php echo is_active_menu('domains.php') ? 'active' : '' ?>"><a href="domains.php"><span>List</span></a></li>
      </ul>
      </li>
    <li class="submenu">
      <a href="#"><i class="icon icon-th"></i>  <span>Clients</span><span class="label label-important">+</span></a>
      <ul>
	<li class="<?php echo is_active_menu('clients.php') ? 'active' : '' ?>"><a href="clients.php"><span>List</span></a></li>
	<li class="<?php echo is_active_menu('clients.php?act=assign_user') ? 'active' : '' ?>"><a href="clients.php?act=assign_user"><span>Assign User</span></a></li>
      </ul>
      </li>
    
    <li class="submenu">
      <a href="#"><i class="icon icon-tint"></i> <span>Update PR</span><span class="label label-important">+</span></a>
      <ul>
	<li><a href="javascript:void(0)" class="updateApprovalLinkPr"><span>Domains (Approval Pending)</span></a></li>
	<li class="<?php echo is_active_menu('approvals.php?act=update_all_pr') ? 'active' : '' ?>"><a href="approvals.php?act=update_all_pr"><span>All Domains</span></a></li>
      </ul>
      </li>
    
      <!-- Influencers Section added by Nitin -->
      
          <li class="submenu">
      <a href="#"><i class="icon icon-tint"></i> <span>Influencers</span><span class="label label-important">+</span></a>
      <ul>
	<li class="<?php echo is_active_menu('influencers.php?act=add') ? 'active' : '' ?>"><a href="influencers.php?act=add"><span>Add New</span></a></li>
	<li class="<?php echo is_active_menu('influencers.php') ? 'active' : '' ?>"><a href="influencers.php"><span>List</span></a></li>
      </ul>
          </li>
      
      
    <?php endif ?>  
  </ul>
</div>
<!--sidebar-menu-->