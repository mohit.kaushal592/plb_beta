<!DOCTYPE html>
<html lang="en">
<head>
<title>PLB - <?php echo $viewData->getTitle() ?></title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/uniform.css" />
<link rel="stylesheet" href="css/datepicker.css" />
<link rel="stylesheet" href="css/fullcalendar.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/media.css" />
<link rel="stylesheet" href="css/select2.css" />
<link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.min.css" />
<link href="css/font-awesome.css" rel="stylesheet" />
<link href="css/jquery.multiselect.css" rel="stylesheet" />
<link href="css/jquery.multiselect.filter.css" rel="stylesheet" />
<link rel="stylesheet" href="css/jquery.gritter.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
  <script type="text/javascript">
    var __baseUrl = '<?php echo get_base_url(true) ?>';
  </script>
</head>
<body class="<?php echo $viewData->get('bodyClass') ?>">
  <!-- loader -->
  <div class="loader_box_overlay hide">
    <div class="loader_box">
    <p><img src="img/spinner.gif"><span class="loader_box_txt">Please wait a while.</span></p>
    
    </div>
</div>
  <!-- loader end -->
<!-- model boxes -->
<div id="pop-up-box" class="modal hide fade">
<div class="modal-header">
<button data-dismiss="modal" class="close" type="button">&times;</button>
<h3>Warning!</h3>
</div>
<div class="modal-body">
<p>Sorry, some error occured in processing 1.</p>
</div>
</div>
<!-- end model boxes-->
<!--Header-part-->
<div id="header">
  <h1><a href="dashboard.html">Adlift</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome <?php echo ucwords($session->read('User.first_name')) ?></span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="users.php?act=profile"><i class="icon-user"></i> My Profile</a></li>
        <li><a href="users.php?act=change_password"><i class="icon-lock"></i> Change Password</a></li>
        <li class="divider"></li>
        <li><a href="logout.php"><i class="icon-key"></i> Log Out</a></li>
        
      </ul>
    </li>

    <!--<li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>-->
    <li class=""><a title="" href="logout.php"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->