<?php
 require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }
 
 function users_index(){ 
  global $viewData;
  $viewData->setTitle('Users Listing');
  $User = new User;
  $filters = __usersFilterVars();
  $userFilter = array('order'=>array('User.first_name ASC', 'User.last_name ASC', 'User.username ASC'));
  if(!empty($filters)){
   $userFilter['where'] = !empty($userFilter['where']) ? $userFilter['where'].' AND '. $filters['User'] : $filters['User'];
  }
  $users = $User->find_all($userFilter, true);
  $viewData->set('users', $users);
 }
 
 function users_add(){
  global $database, $session, $viewData;
  $viewData->setTitle('Add User');
  if(!empty($_POST['data']['User'])){
   $User = new User;
   // check user by email if exists
   $users = $User->find_all(array('where'=>"email_id='".$database->escape_value($_POST['data']['User']['email_id'])."'"));
   if(count($users)>0){
    $session->message('Email already exists. Please try using new Email');
   } elseif($_POST['data']['User']['password'] != $_POST['data']['User']['re_password']) {
    $session->message('Password doesn\'t matched.');
   } else {
    // add new user in database
    $data = $_POST['data'];
    $data['User']['password'] = md5($data['User']['password']);
    $data['User']['created_on'] = date('Y-m-d H:i:s');
    $data['User']['username'] = $data['User']['email_id'];
    //Added By Arvind
    $typeArray = explode("-", $data['User']['user_type']);
    $data['User']['user_type'] = $typeArray[0];
    if(isset($typeArray[1]) && $typeArray[1]=='supper')
    {
      $data['User']['supper_user'] = 1;  
    }else {
          $data['User']['supper_user'] = 0;   
        }
        
    //End By Arvind
    //upload profile picture
    if($_FILES['data']['error']['User']['user_pic']==0){
     $file = $_FILES['data']['name']['User']['user_pic'];
     $tmpfile = $_FILES['data']['tmp_name']['User']['user_pic'];
     $ext = end(explode('.', $file));
     $newPicName = uniqid('user_pic_').'.'.$ext;
     move_uploaded_file($tmpfile, WWW_ROOT.DS.'uploads'.DS.'profile'.DS.$newPicName);
     $data['User']['user_pic'] = $newPicName;
    }
    
    if($User->save($data)){
     $session->message('User has been created. '.$database->last_query());
     redirect_to('users.php');
    }else{
     $session->message('User could not created. Please try again later.');
     $viewData->set('user', $data);
    }
   }
  }
 }
 
 function users_profile(){
  global $viewData, $session;
  $viewData->setTitle('My Profile');
  if(!empty($_GET['uid']) and $session->read('User.user_type')!='superadmin'){
   redirect_to('index.php');
  }
  $User = new User;
  $user_id = !empty($_GET['uid']) ? $_GET['uid'] : $session->read('User.id');
  $user = $User->find_by_id($user_id) ;
  $viewData->set('user', $user);
 }
 
 function users_edit(){
  global $database, $session, $viewData;
  $viewData->setTitle('Edit User');
  if(!empty($_GET['uid']) and $session->read('User.user_type')!='superadmin'){
   redirect_to('index.php');
  }
  $User = new User;
  if(!empty($_POST['data']['User'])){
   // check user by email if exists
   $emailError=false;
   if(!empty($_POST['data']['User']['email_id'])){
    $userCheck = $User->find_all(array('where'=>"email_id='".$database->escape_value($_POST['data']['User']['email_id'])."'", 'limit'=>'1'));
    if(!empty($userCheck)){
     $userCheck = array_shift($userCheck);
     if($userCheck['User']['id']!=$_POST['data']['User']['id']){
      $session->message('User email already hold by another user. ');
      $emailError=true;
     }else{
      $data['User']['username'] = $data['User']['email_id'];
     }
    }
   }
   if(!$emailError){
    // add new user in database
    $data['User'] = $_POST['data']['User'];
    //Added By Arvind
    $typeArray = explode("-", $data['User']['user_type']);
    $data['User']['user_type'] = $typeArray[0];
      if(isset($typeArray[1]) && $typeArray[1]=='supper')
      {
        $data['User']['supper_user'] = 1;  
      }  else {
        $data['User']['supper_user'] = 0;   
      }
     //End By Arvind
   unset($data['User']['user_pic']);
   //upload profile picture
    if($_FILES['data']['error']['User']['user_pic']==0){
     $file = $_FILES['data']['name']['User']['user_pic'];
     $tmpfile = $_FILES['data']['tmp_name']['User']['user_pic'];
     $ext = end(explode('.', $file));
     $newPicName = uniqid('user_pic_').'.'.$ext;
     move_uploaded_file($tmpfile, WWW_ROOT.DS.'uploads'.DS.'profile'.DS.$newPicName);
     $data['User']['user_pic'] = $newPicName;
    }
   
    if($User->save($data)){
     $session->message('User has been updated. ');
     if(empty($_GET['uid'])){
       redirect_to('users.php?act=profile');
     } else {
       redirect_to('users.php');
     }
    }else{
     $session->message('User could not updated. Please try again later.');
     $viewData->set('user', $data);
    }
   } else {
    $emailError ? $session->message('User does not exists.') : null;
   }
  }
  $user_id = !empty($_GET['uid']) ? $_GET['uid'] : $session->read('User.id');
  $userData = $User->find_by_id($user_id);
  $viewData->set('user', $userData);
 }
 
 function users_change_password(){
  global $database, $session, $viewData;
  $viewData->setTitle('Change Password');
  if(!empty($_GET['uid']) and $session->read('User.user_type')!='superadmin'){
   redirect_to('index.php');
  }
  $User = new User;
  if(!empty($_POST['data']['User'])){
   $data['User'] = $_POST['data']['User'];
   if($data['User']['password']==$data['User']['re_password']){
    $data['User']['password'] = md5($data['User']['password']);
    if(!empty($data['User']['old_password'])){
     $userCount = $User->count(array('where'=>"User.id='".$data['User']['id']."' AND User.password='".md5($data['User']['old_password'])."'"));
     if($userCount>0){
      $User->save($data);
      if(empty($_GET['uid'])){
        redirect_to('users.php?act=profile');
      } else {
        redirect_to('users.php');
      }
     }else{
      $session->message('Old password does not matched.');
     }
    }else{
     $User->save($data);
     $session->message('Password has been changed.');
     if(empty($_GET['uid'])){
       redirect_to('users.php?act=profile');
     } else {
       redirect_to('users.php');
     }
    }
   }else{
      $session->message('Password does not matched.');
     }
  }
  $user_id = !empty($_GET['uid']) ? $_GET['uid'] : $session->read('User.id');
  $userData = $User->find_by_id($user_id);
  $viewData->set('user', $userData);
 }
 
 function users_ustatus(){
  global $database, $session, $viewData;
  if(!empty($_REQUEST['id']) and !empty($_REQUEST['st'])){
   $User = new User;
    $data['User'] = array('id'=>$_REQUEST['id'], 'status'=> ($_REQUEST['st']=='yes') ? 1 : 0);
    $userCount = $User->count(array('where'=>"id='".$database->escape_value($data['User']['id'])."'"));
    if($userCount>0){
     if($User->save($data)){
      $session->message('User status has been changed. ');
     }else{
      $session->message('User status could not changed. Please try again later.');
     }
     redirect_to('users.php');
    }
  }
 }
 
 function __updateFirstAndLastName(){
  global $database, $session, $viewData;
  $User = new User;
  $users = $User->find_all(array('limit'=>' 0,1000 '));
  if(!empty($users)){
   foreach($users as $_user){
    $emailSplit = explode('@', $_user['User']['email_id']);
    list($firstName, $lastName) = explode('.', $emailSplit[0]);
    $data = array('User'=>array('id'=>$_user['User']['id'], 'first_name'=>$firstName, 'last_name'=>$lastName));
    $User->save($data);
   }
  }
 }
 
 function __usersFilterVars(){
  global $viewData;
  $filter = array();
  $filterVars = array();
  if(!empty($_GET['_kw'])){
   $keyword = base64_decode($_GET['_kw']);
   $filterVars['keyword'] = $keyword;
   $filter['User'][] = " (
                        User.first_name LIKE '%{$keyword}%'
                        OR User.last_name LIKE '%{$keyword}%'
                        OR User.username LIKE '%{$keyword}%'
                        OR User.email_id LIKE '%{$keyword}%'
                        )
                       ";
  }
  
  if(!empty($_GET['_st'])){
   $status = base64_decode($_GET['_st']);
   $filter['User'][] = " User.status = '{$status}' ";
   $filterVars['status'] = $status;
  }
  
  $filters['User'] = $filter['User'] ? implode(' AND ', $filter['User']) : '';
  //$filters = implode(' AND ', $filter);
  $viewData->set('filterVars', $filterVars);
  return $filters;
 }
 
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('users_'.$action)){ 
 call_user_func('users_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'users_'.$_GET['act'].'.php' : 'users_list.php';
include "views/default.php";
?>