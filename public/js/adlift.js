// base64 conversion type set
$.base64.utf8encode = true;

$.urlParam = function(name){
	var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results==null){
		return null;
	}else{
		return results[1] || 0;
	}
}
jQuery.fn.reset = function () {
  $(this).each (function() { this.reset(); });
}
jQuery.fn.showModalBox = function(options, isAlert){
	var settings = {
                'headerText': 'Warning!',
                'bodyText': 'Sorry, some error occured in processing.',
		'class': ''
            };
	settings = $.extend(settings, options);
	
	$(this).find('.modal-header h3').html(settings.headerText);
	$(this).find('.modal-body p').html(settings.bodyText);
	isAlert==true ? $(this).addClass('alert-pop-up') : $(this).removeClass('alert-pop-up');
	$(this).modal('show');
	tooltipInIt();
}

jQuery.fn._submitLink = function(){
	var linkId = $.trim($(this).attr('rel'));
        var anchLink = document.createElement("a");
        anchLink.setAttribute("href",'approvals.php?act=submit_link&lid='+linkId);
        anchLink.setAttribute("target","_blank");
        document.body.appendChild(anchLink);
        anchLink.click();
        document.body.removeChild(anchLink);
}

function tooltipInIt() {
	$('.tip').tooltip();	
	$('.tip-left').tooltip({ placement: 'left' });	
	$('.tip-right').tooltip({ placement: 'right' });	
	$('.tip-top').tooltip({ placement: 'top' });	
	$('.tip-bottom').tooltip({ placement: 'bottom' });
}
// override default js alert property
window.alert = function(msg, title){
	var options = {};
	if (msg) { options.bodyText = msg; }
	if (title) { options.headerText = title; }
	
	if ($('div#pop-up-box').hasClass('modal')) {
		$('#pop-up-box').showModalBox(options, true);
	}else{
		//alert(msg);
	}
	
}

function select2InIt(ele, url, $options) {
	var options = $options || {};
	maxSelect = (options.maxSelect && maxSelect>0) ? maxSelect : 4;
	$(ele).select2({
		placeholder: "Select Options",
		minimumInputLength: options.minimumInputLength ? options.minimumInputLength : 3,
		maximumSelectionSize: maxSelect,
		quietMillis: options.quietMillis ? options.quietMillis : 300, 
		multiple: options.multiple ? options.multiple : true,
		containerCssClass: options.cssClass ? options.cssClass : 'span11',
		//width: options.width ? options.width : '206px',
		ajax: {
			url: url,
			dataType: 'json',
			data: function(term, page) {
				return {
					q: term,
					page_limit: 10
				};
			},
			results:  function(data, page) { 
				// parse the results into the format expected by Select2
				return {results:data}; 
			} 
		},
		initSelection: function(element, callback) {
			var data = [];
			$(element.val().split(",")).each(function(i, v) {
			    data.push({
				id: v,
				text: v
			    });
			});
			callback(data);
		}
	});
}

function multiSelectRender() {
	$("select").multiselect({
        beforeopen : function(e, ui){
         if(!($(this).attr('multiple') == true || $(this).attr('multiple') == 'multiple')){
           $(this).multiselect({'multiple':false});
           $(this).multiselect('refresh');
         }
        },
	checkAll: function(e, ui){ 
		var $checked = $(this).multiselect("widget").find("input:checked");
		$checked.closest('span').addClass('checked');
	},
	uncheckAll: function(e, ui){ 
		var $unchecked = $(this).multiselect("widget").find("input");
		$unchecked.closest('span').removeClass('checked');
	},
	position : {
		my: 'left bottom',
		at: 'left top'
	}

       }).multiselectfilter();
	
       $("select:not([multiple])").multiselect({selectedList : 1, header: false});
}

function tblResizeInIt() {
	var onSampleResized = function(e){
	       var columns = $(e.currentTarget).find("th");
	       var msg = "columns widths: ";
	       columns.each(function(){ msg += $(this).width() + "px; "; })		
       };	
	$(".tbl-resize").colResizable({
	       liveDrag:true, 
	       gripInnerHtml:"<div></div>", 
	       draggingClass:"dragging", 
	       onResize:onSampleResized
	});
}

function uniformInIt($where){
	if ($where && $where!='undefined') {
		$where.find('input[type=checkbox],input[type=radio],input[type=file]').uniform();	
	}else{
		$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
	}
}

function getDomainHistory(domain,isAutofill=''){
    if(domain!=''){ 
        $('.loader_box_overlay').show();
        $.get('approvals.php?act=domain_history',
            {
				'domain':domain,
				isAutofill
			},
            function(msg){
                $('.loader_box_overlay').hide(); 
                if(msg != "" && msg.msg){
                    $('#pop-up-box').showModalBox({'headerText': msg.title, 'bodyText': msg.msg});
					$('._submitLink').on('click', function(){
						$(this)._submitLink();
					});
                }
          }, 'json').fail(function(){
            $('.loader_box_overlay').hide();
            alert('error in request processing.');
        });
    }
}

function getBlackListDomain(url){
	if(url!=''){ 
	    $('.loader_box_overlay').show();
	    $.get(url,
		{},
		function(msg){
		    $('.loader_box_overlay').hide();
		    msg = jQuery.parseJSON(msg);
		    if(msg != "" && msg.msg){
			$('#pop-up-box').showModalBox({'headerText': msg.title, 'bodyText': msg.msg});
		    }
	      }).fail(function(){
		$('.loader_box_overlay').hide();
		alert('error in request processing.');
	    }, 'json');
	}
}

function getIpHistory(ip){
    if(ip!=''){ 
        $('.loader_box_overlay').show();
        $.get('approvals.php?act=ip_history',
            {'ip':ip},
            function(msg){
                $('.loader_box_overlay').hide(); 
                if(msg != "" && msg.msg){
                    $('#pop-up-box').showModalBox({'headerText': msg.title, 'bodyText': msg.msg});
		    $('._submitLink').on('click', function(){
			$(this)._submitLink();
		     });
                }
          }, 'json').fail(function(){
            $('.loader_box_overlay').hide();
            alert('error in request processing.');
        });
    }
}

function updateLoaderOverlayHeight() {
	var $h = $(document).height();
	$('.loader_box_overlay').css({'height': $h});
}

$(document).ready(function(){
	// === Sidebar navigation === //
	$('.submenu > a').click(function(e)
	{
		e.preventDefault();
		var submenu = $(this).siblings('ul');
		var li = $(this).parents('li');
		var submenus = $('#sidebar li.submenu ul');
		var submenus_parents = $('#sidebar li.submenu');
		if(li.hasClass('open'))
		{
			if(($(window).width() > 768) || ($(window).width() < 479)) {
				submenu.slideUp();
			} else {
				submenu.fadeOut(250);
			}
			li.removeClass('open');
			li.find('span.label').html('+');
		} else 
		{
			if(($(window).width() > 768) || ($(window).width() < 479)) {
				submenus.slideUp();			
				submenu.slideDown();
			} else {
				submenus.fadeOut(250);			
				submenu.fadeIn(250);
			}
			submenus_parents.removeClass('open');
			submenus_parents.find('span.label').html('+');
			li.addClass('open');
			li.find('span.label').html('-');
		}
	});
	
	var ul = $('#sidebar > ul');
	
	$('#sidebar > a').click(function(e)
	{
		e.preventDefault();
		var sidebar = $('#sidebar');
		if(sidebar.hasClass('open'))
		{
			sidebar.removeClass('open');
			ul.slideUp(250);
		} else 
		{
			sidebar.addClass('open');
			ul.slideDown(250);
		}
	});
	
	// active parent li
	var activeParentLi = function(){
		$('#sidebar').find('li.submenu li.active').each(function(i, el){
			if ($(el).parents('li.submenu').length) {
				$(el).parents('li.submenu').addClass('active open').find('span.label').html('-');
			}			
		})
		
		//$(activeLi).parents('li.submenu').addClass('active open');
		
	}
	activeParentLi();
	
	// === Resize window related === //
	$(window).resize(function()
	{
		if($(window).width() > 479)
		{
			ul.css({'display':'block'});	
			$('#content-header .btn-group').css({width:'auto'});		
		}
		if($(window).width() < 479)
		{
			ul.css({'display':'none'});
			fix_position();
		}
		if($(window).width() > 768)
		{
			$('#user-nav > ul').css({width:'auto',margin:'0'});
            $('#content-header .btn-group').css({width:'auto'});
		}
	});
	
	if($(window).width() < 468)
	{
		ul.css({'display':'none'});
		fix_position();
	}
	
	if($(window).width() > 479)
	{
	   $('#content-header .btn-group').css({width:'auto'});
		ul.css({'display':'block'});
	}
	
	// === Tooltips === //
	tooltipInIt();	
	
	// === Search input typeahead === //
	$('#search input[type=text]').typeahead({
		source: ['Dashboard','Form elements','Common Elements','Validation','Wizard','Buttons','Icons','Interface elements','Support','Calendar','Gallery','Reports','Charts','Graphs','Widgets'],
		items: 4
	});
	
	// === Fixes the position of buttons group in content header and top user navigation === //
	function fix_position()
	{
		var uwidth = $('#user-nav > ul').width();
		$('#user-nav > ul').css({width:uwidth,'margin-left':'-' + uwidth / 2 + 'px'});
        
        var cwidth = $('#content-header .btn-group').width();
        $('#content-header .btn-group').css({width:cwidth,'margin-left':'-' + uwidth / 2 + 'px'});
	}
	
		
	$('.lightbox_trigger').click(function(e) {
		
		e.preventDefault();
		
		var image_href = $(this).attr("href");
		
		if ($('#lightbox').length > 0) {
			
			$('#imgbox').html('<img src="' + image_href + '" /><p><i class="icon-remove icon-white"></i></p>');
		   	
			$('#lightbox').slideDown(500);
		}
		
		else { 
			var lightbox = 
			'<div id="lightbox" style="display:none;">' +
				'<div id="imgbox"><img src="' + image_href +'" />' + 
					'<p><i class="icon-remove icon-white"></i></p>' +
				'</div>' +	
			'</div>';
				
			$('body').append(lightbox);
			$('#lightbox').slideDown(500);
		}
		
	});
	

	$('#lightbox').live('click', function() { 
		$('#lightbox').hide(200);
	});
	
	$('.updateApprovalLinkPr').bind('click', function(e){
		e.preventDefault();
		$('.loader_box_overlay').show();
		$.get('approvals.php?act=update_pr', {}, function(data){
			$('.loader_box_overlay').hide();
			if(data.status=='error'){
				alert(data.msg);
			}else{
				window.location=window.location;
			}
		}, 'json').fail(function(){
			$('.loader_box_overlay').hide();
			alert('Error occurred during process.');
		});
	});
	
	// change overlay height
	updateLoaderOverlayHeight();
	
	$('table.sortable_tbl').tablesorter();
	
	$('#_ExportToExcel').on('click', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var urlParams = [];
		queryString._export=$.base64.encode('1');
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
        $('#_ExportToExcelChart').on('click', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var urlParams = [];
		queryString._exportchart=$.base64.encode('1');
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
         $('#_ExportToExcelChartWithoutDetail').on('click', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var urlParams = [];
		queryString._exportDetail=$.base64.encode('1');
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
	
});



