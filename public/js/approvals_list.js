$(function(){
 var __createReasonsBlock = function($obj){
    var data = $('body').data('ApprovalRejectReasons');
    var options = [];
    
    $.each(data, function(k, v){
        options.push('<option value="'+k+'">'+v+'</option>');
    });
    if (options.length>0) {
        var selectBx = $('<select id="ApprovalRejectReason'+ $obj.val() +'" name="data[ApprovalLink][reason]['+ $obj.val() +'][]" multiple="multiple"></select>').html(options.join(''));
        $($obj.closest('li')).append(selectBx);
        selectBx.find('option[value=1]').attr('selected', true);
        selectBx.multiselect().multiselectfilter();
    }
 }
 // toggle accept or reject checkboxes
 $('._approval_reject').bind('click', function(){
    var $this = $(this);
    if($this.is(':checked')===true){
        $('._approval_accept[value='+$this.val()+']').attr('checked', false).parent('span').removeClass('checked');
        //$('.loader_box_overlay').show(); 
        if (!$('body').data('ApprovalRejectReasons') || $('body').data('ApprovalRejectReasons') == 'undefined') {
            var reasonsXhr = $.ajax({url: 'approvals.php?act=list_reasons', dataType : 'json'});
            reasonsXhr.done(function(data){
                $('.loader_box_overlay').hide();
                $('body').data('ApprovalRejectReasons', data);
                __createReasonsBlock($this);
            });
            reasonsXhr.fail(function(){
               // $('.loader_box_overlay').hide();
                alert('error in processing.');
            });
        }else{
           // $('.loader_box_overlay').hide();
            __createReasonsBlock($this);
        }
        
    }else{
         $('select').remove('#ApprovalRejectReason'+ $(this).val());
    }
 });
 
 $('._approval_accept').bind('click', function(){
    if($(this).is(':checked')== true){
        $('._approval_reject[value='+$(this).val()+']').attr('checked', false).parent('span').removeClass('checked');
        
        // remove reject reasons box
        $('select').remove('#ApprovalRejectReason'+ $(this).val());
    }
 });
 
 // action for approvals
 $('a.doAct').bind('click', function(e){
    e.preventDefault();
    var actType = $.trim($(this).attr('rel'));
    if(actType=='approval_phase_1' || actType=='approval_phase_2'){
        var accpCheckedCount = $('._approval_accept:checked').length;
        var rejctCheckedCount = $('._approval_reject:checked').length;
        if(accpCheckedCount>0 || rejctCheckedCount>0){
         $('#ApprovalWhatDo').val(actType);
         __submitApprovalActForm();
     }else{
        alert('select atleast 1 campain to procees');
     }
    } else if(actType=='approval_forward'){
        var linkCount = $('._approval_id:checked').length;
        if(linkCount>0){
            $('#ApprovalWhatDo').val(actType);
            __submitApprovalActForm();
        }else{
            alert('Please select atleast one link to process2.');
        }
    } else if(actType=='approval_delete'){
        var linkCount = $('._approval_id:checked').length;
        if(linkCount>0){
            if (confirm('Do you want to delete selected links?')) {
                $('#ApprovalWhatDo').val(actType);
                __submitApprovalActForm();    
            }
        }else{
            alert('Please select atleast one link to process1.');
        }
    }
 });
 // check / uncheck all
 $('#checkAllApproval').bind('click', function(){
    $('input._approval_id').attr('checked', $(this).is(':checked'));
    if ($(this).is(':checked')) {
        $('input._approval_id').parent('span').addClass('checked');
    }else{
        $('input._approval_id').parent('span').removeClass('checked');
    }
 });
 
 //   check / uncheck all Approval
 
 $('#checkAllApprovalAccepted').bind('click', function(){
    $('input._approval_accept').attr('checked', $(this).is(':checked'));
    if ($(this).is(':checked')) {
        $('input._approval_accept').parent('span').addClass('checked');
    }else{
        $('input._approval_accept').parent('span').removeClass('checked');
    }
 });
 
  //   check / uncheck all Rejected
 
 $('#checkAllApprovalReceted').bind('click', function(){
    $('input._approval_reject').attr('checked', $(this).is(':checked'));
    if ($(this).is(':checked')) {
        $('input._approval_reject').parent('span').addClass('checked');
    }else{
        $('input._approval_reject').parent('span').removeClass('checked');
    }
 });
 
 $('._submitLink').on('click', function(){
      $(this)._submitLink();
   });
 
 
 // domain or ip history
 $('.approvals_list_tbl tr[domain]').on('dblclick', function(e){
   var $domain = $(this).attr('domain');
   if($domain.length>0){
      getDomainHistory($domain);
   }
 })
 
 $('.domain-ip').on('click', function(e){
   var $ip = ($(this).text()).trim();
   var ipValidateRegX = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
   if(ipValidateRegX.test($ip)){
    //  getIpHistory($ip);
   }
 })
 
 $('#RowPerPage').on('multiselectcreate', function(event, ui){
   $('#RowPerPage').multiselect('destroy');
 });
 
 $('#AdminCommentAdd').on('show', function(){
      var $this= $(this);
      $.get('approvals.php?act=admin_comment', {'id': approvalId}, function(data){
            $this.find('.modal-body').html(data);
            $this.find('.modal-header h3 span').html(' for domain - '+ domainname);
         });
   });
 
 $('#admin-add-comment').on('click', function(){
      var $form = $(this).closest('div.modal-dialog').find('form');
      var $target = $($form.attr('data-target'));
      if($form){
         var commentTxt = $form.find('#narration_admin').val();
         var _appRow = $form.find('#_ApprovalId').val();
         $.ajax({
         type: $form.attr('method'),
         url: $form.attr('action'),
         data: $form.serialize(),
         success: function(data, status) {
            $target.modal('hide');
            if (data) {
               alert(data);   
            }else{
               $('#_row_'+ _appRow+ ' span:first').html(commentTxt);
               //window.location = window.location;
            }
         }
         });
      }
   });
 
 
});
// submit approval action form
 function __submitApprovalActForm(){
    var $form = $('#ApprovalListForm');
    $('.loader_box_overlay').show();
        var $postForm = $.post( $form.attr('action'), $form.serialize(), function(data) {
        if(data.status=='success'){
         window.location = window.location;
        }else{
            alert(data.msg);
        }
    }, "json");
    $postForm.done(function() {
        $('.loader_box_overlay').hide();
    }).fail(function(){
        $('.loader_box_overlay').hide();
        alert('error in form processing.');
    });
 }
 
 
 