function getDaPrIp(domain){
    if(domain!=''){ 
        $('.loader_box_overlay').show();
        $.ajax({						
            type: 'GET',
            url:  'approvals.php?act=calc_da_pr_ip',
            data: "domain="+domain,
            dataType:"JSON",
            success: function(msg){
                $('.loader_box_overlay').hide(); 
                if(msg != "" && msg.ip){
                    $('#PaymentIp').val(msg.ip);
                    $('#PaymentPr').val(msg.pr);
                    $('#PaymentDa').val(msg.da);
                }else{
                    $('.modal').showModalBox();
                }
            }		
        })
    }
}
function getBenDetail(bencode){

    if(bencode!=''){ 
        $('.loader_box_overlay').show();
        $.ajax({						
            type: 'GET',
            url:  'approvals.php?act=get_ben_detail',
            data: "bencode="+bencode,
            dataType:"JSON",
            success: function(msg){
                $('.loader_box_overlay').hide(); 
                if(msg != "" && msg.bencode){
                    $('#Paymentbenname').val(msg.benname);
					$('#Paymentbenaccount').val(msg.benaccount);
					$('#Paymentifsccode').val(msg.ifsccode);
					$('#Paymentaddress').val(msg.address);
					$('#Paymentcity').val(msg.city);
					$('#Paymentstate').val(msg.state);
					$('#Paymentzip_code').val(msg.zip_code);
					 
                    
                }else{
                    alert('No record exist!');
		$('#Paymentbencode').focus();
		return false
                }
            }		
        })
    }
	else
	{
		alert('Please enter the value of Beneficiary Code');
		$('#Paymentbencode').focus();
		return false
		
		
	}
}
function getTdsDetail(bencode,paymentamount,adddate,pyid){

    if(bencode!=''){ 
        $('.loader_box_overlay').show();
        $.ajax({						
            type: 'POST',
            url:  'approvals.php?act=get_tds_detail',
            data: {bencode:bencode,paymentamount:paymentamount,adddate:adddate,pyid:pyid},
            dataType:"JSON",
            success: function(msg){
                $('.loader_box_overlay').hide(); 
                if(msg != "" && msg.grossamount){
                    $('#PaymentTdsamount').val(msg.tdsamount);
					$('#PaymentGrossamount').val(msg.grossamount);              
                }else{
                    $('.modal').showModalBox();
                }
            }		
        })
    }
	else
	{
		alert('Please enter the value of Beneficiary Code');
		$('#Paymentbencode').focus();
		return false
		
		
	}
}
function checkAndActSubmitLinkForm(form, e) {
    e.preventDefault();
    var _paymentId = $(form).find('input#PaymentId').val();
    $('.loader_box_overlay').show();
	
    var submitNow = function(){
        $.ajax({						
            type: 'POST',
            url:  $(form).attr('action'),
            data: $(form).serialize(),
            dataType:"JSON",
            success: function(msg){
                $('.loader_box_overlay').hide();
                if (msg.status=='success') {
                    window.location = 'submitted_links.php';
                }else{
                    alert(msg.msg);
                }
            }		
        }).fail(function(){
            $('.loader_box_overlay').hide();
            alert('Error occurred during process.');
        });
    }
    if (_paymentId) {
        submitNow();
    }else{
        $.ajax({						
            type: 'POST',
            url:  'approvals.php?act=submit_link_ip_check',
            data: {'ip': $('#PaymentIp').val()},
            dataType:"JSON",
            success: function(msg){
                if (msg.status=='success') {
                    submitNow();
                } else if(msg.status=='error' && msg.type=='confirm'){
                    $('.loader_box_overlay').hide();
                    if (confirm(msg.msg)) {
                        $('.loader_box_overlay').show();
                        submitNow();
                    }
                }else{
                    $('.loader_box_overlay').hide();
                    alert(msg.msg);
                }
            }		
        });
    }
    
}


jQuery(document).ready(function($){
    var SubmitLinkFormValidate = jQuery('#SubmitLinkForm').validate({errorElement : 'span', ignore: ':hidden:not("select")'});
    
    // calculate da, pr, ip
    $('#PaymentCalcDaPr').bind('click', function(){
        if(SubmitLinkFormValidate.element('#PaymentDomain')){
          getDaPrIp($('#PaymentDomain').val());
        }
    });
	
	
    
    // submit link for payment
    $('#SubmitLinkForm').bind('submit', function(e){
      if(SubmitLinkFormValidate.valid()){
        e.preventDefault();
        if ($('#PaymentCurrency').val() == '') {
            alert('Please select currency');
        }else{
            checkAndActSubmitLinkForm(this, e);
        }
      }
    });
    
    // domain history
    $('.DomainIpHistory').bind('click', function(e){
      e.preventDefault();
       if(SubmitLinkFormValidate.element('#PaymentDomain')){
            if ($(this).attr('id')=='DomainHistory') {
                getDomainHistory($('#PaymentDomain').val());
            } else if ($(this).attr('id')=='IpHistory') {
                if ($('#PaymentIp').val()=='') {
                    alert('Please calculate ip for this domain.');
                }else{
                    getIpHistory($('#PaymentIp').val());
                }
            }
          
        }
    });
    
    // datepicker init for form
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    $('#PaymentStartDate').on('changeDate', function(ev){
       // var endDat = new Date($('#PaymentEndDate').val());
        //if (ev.date.valueOf() > endDat.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setFullYear(newDate.getFullYear() + 1);
            $('#PaymentEndDate').datepicker('setValue', newDate);
        //}
        $('#PaymentStartDate').datepicker('hide');
    }).data('datepicker');
    $('#PaymentEndDate').on('changeDate', function(ev){
        $('#PaymentEndDate').datepicker('hide');
    }).data('datepicker');
	
	$("#uploadfile").on("click", function() {
    var file_data = $("#sortpicture").prop("files")[0];  
	var file_val = $("#sortpicture").val();

if(file_val==""){
	
	alert("Please select the file");
	return false;
}
	
    var form_data = new FormData();
    form_data.append("file", file_data);
  
    $.ajax({
        url: "submitted_links.php?act=upload_file",
        dataType: 'JSON',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(dataval){
			
			if(dataval.msg!='')
			{
			 alert(dataval.msg); 	
				
			}
			else
			{
				$("#Paymentinvoice_file").val(dataval.filename);
				$("#filelink").html(dataval.filelink);
				$("#sortpicture").val('');
				
			}
           return false;
        }
    });
});
  });