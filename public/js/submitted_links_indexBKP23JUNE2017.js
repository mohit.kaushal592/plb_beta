// submit Payment action form
 function __submitPaymentActForm(){
    var $form = $('#PaymentListForm');
    $('.loader_box_overlay').show();
        var $postForm = $.post( $form.attr('action'), $form.serialize(), function(data) {
        if(data.status=='success'){
         window.location = window.location;
        }else{
            alert(data.msg);
        }
    }, "json");
    $postForm.done(function() {
        $('.loader_box_overlay').hide();
    }).fail(function(){
        $('.loader_box_overlay').hide();
        alert('error in form processing.');
    });
 }
 
 function checkAllInIt() {
   // check / uncheck all
   $('#checkAllPayment').bind('click', function(){
      $('input._payment_id').attr('checked', $(this).is(':checked'));
      if ($(this).is(':checked')) {
          $('input._payment_id').parent('span').addClass('checked');
      }else{
          $('input._payment_id').parent('span').removeClass('checked');
      }
   });
   
   //   check / uncheck all Approval
 
 $('#checkAllPaymentAccepted').bind('click', function(){
    $('input._payment_accept').attr('checked', $(this).is(':checked'));
    if ($(this).is(':checked')) {
        $('input._payment_accept').parent('span').addClass('checked');
    }else{
        $('input._payment_accept').parent('span').removeClass('checked');
    }
 });
 
  //   check / uncheck all Rejected
 
 $('#checkAllPaymentReceted').bind('click', function(){
    $('input._payment_reject').attr('checked', $(this).is(':checked'));
    if ($(this).is(':checked')) {
        $('input._payment_reject').parent('span').addClass('checked');
    }else{
        $('input._payment_reject').parent('span').removeClass('checked');
    }
 });
 }
 
 function toggleAcceptRejectCheckbox() {
   // toggle accept or reject checkboxes
   $('._payment_reject').bind('click', function(){
      var $this = $(this);
      if($this.is(':checked')===true){
          $('._payment_accept[value='+$this.val()+']').attr('checked', false).parent('span').removeClass('checked');
      }
   });
   
   $('._payment_accept').bind('click', function(){
      if($(this).is(':checked')== true){
          $('._payment_reject[value='+$(this).val()+']').attr('checked', false).parent('span').removeClass('checked');
      }
   });
 }
 
 function filterSubmit(){ 
   $('#exportList').val('');
   $('#FilterForm').trigger('submit');
 }
 
$(function(){
checkAllInIt();
toggleAcceptRejectCheckbox(); 
 // action for approvals
 $('a.doAct').bind('click', function(e){
    e.preventDefault();
    var actType = $.trim($(this).attr('rel'));
    if(actType=='link_payment'){
        var accpCheckedCount = $('._payment_accept:checked').length;
        var rejctCheckedCount = $('._payment_reject:checked').length;
        if(accpCheckedCount>0 || rejctCheckedCount>0){
         $('#PaymentWhatDo').val(actType);
         __submitPaymentActForm();
     }else{
        alert('select atleast 1 link to process');
     }
    } else if(actType=='link_delete'){
        var linkCount = $('._payment_id:checked').length;
        if(linkCount>0){
            if (confirm('Do you want to delete selected links?')) {
                $('#PaymentWhatDo').val(actType);
                __submitPaymentActForm();    
            }
        }else{
            alert('Please select atleast one link to delete.');
        }
    }
 });
 
 checkAllInIt();
 
 $('.exportList').on('click', function(){
   $('#exportList').val(1);
   $('#FilterForm').trigger('submit');
 });
 
 
 
})