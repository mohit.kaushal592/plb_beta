var previousPoint = null, previousLabel = null;
$.fn.UseTooltip = function () { 
    $(this).bind("plothover", function (event, pos, item) {
	if (item) { 
	    if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
		previousPoint = item.dataIndex;
		previousLabel = item.series.label;
		$("#tooltip").remove();

		var x = item.datapoint[0];
		var y = item.datapoint[1];

		var color = item.series.color;

		showTooltip(item.pageX,
		item.pageY,
		color,
		"<strong>" + item.series.label + "</strong><br>" + item.series.xaxis.ticks[x].label + " : <strong>" + y + "</strong>");
	    }
	} else {
	    $("#tooltip").remove();
	    previousPoint = null;
	}
    });
};

function showTooltip(x, y, color, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
	position: 'absolute',
	display: 'none',
	top: y - 40,
	left: x - 120,
	border: '2px solid ' + color,
	padding: '3px',
	'font-size': '9px',
	'border-radius': '5px',
	'background-color': '#fff',
	'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
	'color': '#000000',
	opacity: 0.9
    }).appendTo("body").fadeIn(200);
}


function renderPieGraph(data, where){
    $.plot(where, data,
    {
      series: {
	  pie: {
	      show: true,
	      radius: 3/4,
	      label: {
		  show: true,
		  radius: 3/4,
		  formatter: function(label, series){
		      return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+parseFloat(series.percent).toFixed(2)+'%</div>';
		  },
		  background: {
		      opacity: 0.5,
		      color: '#000'
		  }
	      }
	  },
	legend: {
	    show: false
	}
      }
    });
  }
  
function renderBarGraph($options) {
	var data = $options.data;
	var where = $options.where;
	var axisLebl = $options.axisLabel;
	var datas = [];
	$.each(data, function(k, v){
		datas.push({
			label: v.label,
			data: v.data,
			bars: {
			    show: true, 
			    barWidth: 1, 
			    lineWidth: 1,
			    order : k
			}
		}); 
	});
	var options = {
		legend: {
			labelBoxBorderColor: "none",
			position: "top"
		},
		series: {
			shadowSize: 1,
			bars: {
			    show: true
			}
		},
		grid: {
		    hoverable: true,
		    borderWidth: 1,
		}
	};
	if (axisLebl) {
		if (axisLebl.x && axisLebl.x.length>0) {
			options.xaxis = {
			    axisLabel: axisLebl.x,
			    axisLabelUseCanvas: true,
			    axisLabelFontSizePixels: 12,
			    axisLabelFontFamily: 'Verdana, Arial',
			}
		}
		
		if (axisLebl.y && axisLebl.y.length>0) {
			options.yaxis = {
			    axisLabel: axisLebl.y,
			    axisLabelUseCanvas: true,
			    axisLabelFontSizePixels: 12,
			    axisLabelFontFamily: 'Verdana, Arial',
			    axisLabelPadding: 3,
			    tickFormatter: function (v, axis) {
				return v + ($options.y_ticks && $options.y_ticks.tickPostFix ? $options.y_ticks.tickPostFix : '');
			    }
			}
		}
	}
	if ($options.x_ticks && $options.x_ticks.label) {
	    options.xaxis.ticks = $options.x_ticks.label;
	}
	if ($options.y_ticks && $options.y_ticks.label) {
	    options.yaxis.ticks = $options.y_ticks.label;
	}
	$.plot(where, datas, options);
	
	if ($options.x_ticks && $options.x_ticks.hide==true) {
	    where.find('.tickLabels .xAxis').hide();
	}
	
	if ($options.tooltip) {
	    where.UseTooltip();
	}
	
}