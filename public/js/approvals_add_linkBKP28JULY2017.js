function getDaPrIp(domain){
    if(domain!=''){ 
        $('.loader_box_overlay').show();
        $.get('approvals.php?act=calc_da_pr_ip',
            {'domain':domain},
            function(msg){
              $('.loader_box_overlay').hide(); 
              if(msg != "" && msg.ip){
                  $('#ApprovalIp').val(msg.ip);
                  $('#ApprovalPr').val(msg.pr);
                  $('#ApprovalDa').val(msg.da);
              }else{
                  $('#pop-up-box').showModalBox();
              }
          }, 'json').fail(function(){
            $('.loader_box_overlay').hide();
            alert('error in request processing.');
        });
    }
}

function checkAndSubmitApprovalForm(form, e) {
    e.preventDefault();
    $('.loader_box_overlay').show();
    
    $.post('approvals.php?act=save_add_link',
        $(form).serialize(),
        function(msg){
            $('.loader_box_overlay').hide();
            alert(msg.status);
            if(msg.status=='success') {
                alert(msg.msg, 'Success');
                $(form).reset();
                $('input[type=radio]').closest('span.checked').removeClass('checked');
            }else{
                alert(msg.msg);
            }
      }, 'json').fail(function(){
        $('.loader_box_overlay').hide();
        alert('error in request processing.');
    });
    
}


jQuery(document).ready(function($){
    var AddLinkFormValidate = jQuery('#AddLinkForm').validate({errorElement : 'span', ignore: ':hidden:not("select")'});
    
    // calculate da, pr, ip
    $('#ApprovalCalcDaPr').bind('click', function(){
        if(AddLinkFormValidate.element('#ApprovalDomain')){
          getDaPrIp($('#ApprovalDomain').val());
        }
    });
    
    // check campaign and link duplicacy, submit add link form
    $('#AddLinkForm').bind('submit', function(e){
      if(AddLinkFormValidate.valid()){
        e.preventDefault();
        if(!$('input:radio[id^=ApprovalContentWriter]').is(':checked')){
            $('span.content-writer').show();
        }else{
            $('span.content-writer').hide();
            checkAndSubmitApprovalForm(this, e);
        }
      }
    });
    
    // domain history
    $('.DomainIpHistory').bind('click', function(e){
      e.preventDefault();
       if(AddLinkFormValidate.element('#ApprovalDomain')){
            if ($(this).attr('id')=='DomainHistory') {
                getDomainHistory($('#ApprovalDomain').val());
            } else if ($(this).attr('id')=='IpHistory') {
                if ($('#ApprovalIp').val()=='') {
                    alert('Please calculate ip for this domain.');
                }else{
                    getIpHistory($('#ApprovalIp').val());
                }
            }
          
        }
    });
    
  });
   /*         * ** Start: Added by Ajit: 15-DESC-2015  ***************** */
  //////Set up Currency////////
  
  function checkCurrency(curr)
  {
     
      if(curr=='No Payment')
      {          
          $("#PaymentAmount").val('0');
          $("#PaymentAmount").attr("readonly", "readonly");
      }
      else
      {
         if( $("#PaymentAmount").val()=='0')
         {
              $("#PaymentAmount").val('');
             
         }
          $("#PaymentAmount").removeAttr("readonly"); 
        
      }    
          
     /*         * ** End: Added by Ajit: 15-DESC-2015  ***************** */      
      
      
      
  }
  