// submit Payment action form
function __submitClientsActForm($rtype){
      var $form = $('#ClientsListForm');
      $('.loader_box_overlay').show();
      var $postForm = $.post( $form.attr('action'), $form.serialize(), function(data) {
         if(data.status=='success'){
          window.location = window.location;
         }else{
             alert(data.msg);
         }
     }, "json");
     $postForm.done(function() {
         $('.loader_box_overlay').hide();
     }).fail(function(){
         $('.loader_box_overlay').hide();
         alert('error in form processing.');
     });   
}
function toggleAcceptRejectCheckbox(chkType) {
      // toggle accept or reject checkboxes
      $('._reject_'+chkType).bind('click', function(){
         var $this = $(this);
         if($this.is(':checked')===true){
             $('._accept_'+chkType+'[value='+$this.val()+']').attr('checked', false).parent('span').removeClass('checked');
         }
      });
      
      $('._accept_'+chkType).bind('click', function(){
         if($(this).is(':checked')== true){
             $('._reject_'+chkType+'[value='+$(this).val()+']').attr('checked', false).parent('span').removeClass('checked');
         }
      });
   }
   
$(function(){
   toggleAcceptRejectCheckbox('client');
   toggleAcceptRejectCheckbox('client_sec');
   
   // action for approvals
   $('a.doAct').bind('click', function(e){
      e.preventDefault();
      var actType = $.trim($(this).attr('rel'));
      if(actType=='update_client'){
          var accpCheckedCount = $('._accept_client:checked').length;
          var rejctCheckedCount = $('._reject_client:checked').length;
          if(accpCheckedCount>0 || rejctCheckedCount>0){
           $('#ClientsWhatDo').val(actType);
           __submitClientsActForm();
         }else{
            alert('select atleast 1 client to process');
         }
      } else if(actType=='update_client_approval'){
          var accpCheckedCount = $('._accept_client_sec:checked').length;
          var rejctCheckedCount = $('._reject_client_sec:checked').length;
          if(accpCheckedCount>0 || rejctCheckedCount>0){
           $('#ClientsWhatDo').val(actType);
           __submitClientsActForm();
         }else{
            alert('select atleast 1 client to process');
         }
      }
   });
   
   $('#edit-campaign-submit').bind('click', function(){
       var $form = $(this).closest('div.modal-dialog').find('form');
      var $target = $($form.attr('data-target'));
      var submitForm = true;
      $form.find('span.error').hide();
      $form.find('input[required]').each(function(k, el){
         if($(el).val().trim()==''){
            $(el).closest('p').find('span.error').show();
            submitForm=false;
         }
      });
      if($form && submitForm){ 
         $.ajax({
         type: $form.attr('method'),
         url: $form.attr('action'),
         data: $form.serialize(),
         success: function(data, status) {
            $target.modal('hide');
            if (data) {
               alert(data);   
            }else{
               window.location = window.location;
            }
         }
         });
      }
       
    });
   
   $('#CampaignEdit').on('show', function(){
      var $this= $(this);
      $.get('clients.php?act=edit', {'cid': _cid_}, function(data){
            $this.find('.modal-body').html(data);
            $this.find('.modal-header span').html(_cname_);
         });
   });
   
 })

$(function(){
    $('#RowPerPage').on('multiselectcreate', function(event, ui){
       $('#RowPerPage').multiselect('destroy');
    })
    $('#RowPerPage').on('change', function(){
		var curUrl = $.parseUrl();
		var queryString = curUrl.query;
		var _rp = $('#RowPerPage').val();
		var urlParams = [];
		queryString._rp=_rp;
                queryString.p=1;
		$.each(queryString, function(k,v){
			if(v.length>0){
				urlParams.push(k+'='+v);
			}
		});
		if(urlParams.length>=1){
			window.location = '?'+urlParams.join('&');
		}
	});
});