<?php
require_once("../includes/initialize.php");
 if (!$session->is_logged_in()) { redirect_to("login.php"); }

   // function updateResponseStatus($id,$status_code){ 
   //    $Payment = new Payment;
   //    $Payment->save(array('Payment'=>array('id'=>$id, 'response_status'=>$status_code)));
   // }
   // function updateIndexStatus($id,$status_code){ 
   //    $Payment = new Payment;
   //    $Payment->save(array('Payment'=>array('id'=>$id, 'index_status'=>$status_code)));
   // }
   // function submitted_links_upateDomain(){ 
   //    $arr_domain_wise_id = $_POST['domain_wise_id']; 
   //    $status_type = $_POST['status_type'];   
   //    $responseUpdateCount = 0;
   //    $indexUpdateCount = 0;  
   //    //$json_domain_wise_id = json_encode($arr_domain_wise_id);
     
   //    $url = 'https://api.adlift.com/marketing/plb-update/'; 
   //    $data = json_encode(array(
   //       'ids' => $arr_domain_wise_id,
   //       'response_status' => ($status_type == 'r_status')?'True':'False',
   //       'index_status' => ($status_type == 'i_status')?'True':'False',
   //    )); 
   //    //echo "<pre>"; print_r($data); die;
   //    $ch = curl_init($url);  
   //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   //    curl_setopt($ch, CURLOPT_POST, true);
   //    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));  
   //    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
   //    $response = curl_exec($ch);
   //    if ($response === false) {
   //       $error = curl_error($ch); 
   //       echo json_encode(['status' => 'error', 'message' => $error]);
   //       exit();
   //    }else{  
   //       echo json_encode(['status' => 'success', 'message' => $response]);
   //    }
   //    curl_close($ch); 
   //    exit;
   // }
   
   function submitted_links_updateDomain() {
      $arr_domain_wise_id = json_decode($_POST['domain_wise_id'],true);
      $status_type = $_POST['status_type'];
      $responseUpdateCount = 0;
      $indexUpdateCount = 0; 
      $url = 'https://api.adlift.com/marketing/plb-update/';
      $data = json_encode(array(
          'ids' => $arr_domain_wise_id,
          'response_status' => (in_array('r_status',$status_type))? 'True' : 'False',
          'index_status' => (in_array('i_status',$status_type)) ? 'True' : 'False',
      ));
      //echo $data; die;
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      $response = curl_exec($ch);
  
      if ($response === false) {
          $error = curl_error($ch);
          curl_close($ch);
          echo json_encode(['status' => 'error', 'message' => $error]);
          exit();
      }
  
      $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
  
      if ($http_code == 200) {
          echo json_encode(['status' => 'success', 'message' => json_decode($response, true)]);
      } else {
          echo json_encode(['status' => 'error', 'message' => 'API request failed with response: ' . $response]);
      } 
      exit;
  }
  
   function submitted_links_upateDomain_old(){ 
      $arr_domain_wise_id = $_POST['domain_wise_id']; 
      $status_type = $_POST['status_type']; 
      $filter_type = $_SESSION['response_status_value'];  
      $responseUpdateCount = 0;
      $indexUpdateCount = 0;
      if(count($filter_type)>=1){  
         $json_domain_wise_id = json_encode($arr_domain_wise_id);
         //echo $json_domain_wise_id; 
         $url = 'https://api.adlift.com/marketing/plb-update/'; 
         $data = array(
            'ids' => $json_domain_wise_id,
            'response_status' => (in_array('r_status',$filter_type))?true:false,
            'index_status' => (in_array('i_status',$filter_type))?true:false,
         ); 
         $ch = curl_init($url);  
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_POST, true);
         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));  
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         $response = curl_exec($ch);
         if ($response === false) {
            $error = curl_error($ch); 
            echo json_encode(['status' => 'error', 'message' => $error]);
            exit();
         }else{  
            echo json_encode(['status' => 'success', 'message' => $response]);
         }
         curl_close($ch); 
             
         // if(in_array("r_status", $filter_type)){
         //    //$headers = '';
         //    $headers = get_headers($domain); 
         //    //echo "<pre>"; print_r($headers); //die;
         //    if(isset($headers)){
         //       $status_line = $headers[0];
         //       preg_match('/\d{3}/', $status_line, $matches);
         //       $status_code = $matches[0];  
         //    }else{
         //       $status_code = 404;
         //    } 
         //    //echo $domain.'==='.$status_code;
         //    try{ 
         //       updateResponseStatus($id,$status_code);
         //       $responseUpdateCount++;
         //    }catch(Error $e){
         //       http_response_code(500);
         //       echo json_encode(['status' => 'error', 'message' => $e->getMessage()]);
         //       exit();
         //    }  
         // }
         
         //http_response_code(200);
         //echo json_encode(['status' => 'success', 'message' => "Total ($responseUpdateCount) response status and ($indexUpdateCount) index status url updated."]);
      }
      exit;
   }
   function submitted_links_index(){
      global $viewData;
      $viewData->setTitle('Submitted Links Listing');
      $filters = __paymentFilterVars();  
   }
   function submitted_links_data(){
      global $viewData;
      $Payment = new Payment;
      $filters = __paymentFilterVars();
      $isExport = base64_decode($_REQUEST['_export'])==1? true : false;
	  $isExportBank = base64_decode($_REQUEST['_exportBank'])==1? true : false;
      $paymentFilter = array('contain'=>array('User'), 'order'=>array('Payment.id DESC'));
      if(!empty($filters['Payment'])){ 
         $paymentFilter['where'] = !empty($paymentFilter['where']) ? $paymentFilter['where'].' AND '. $filters['Payment'] : $filters['Payment']; 
      }
      //echo "<pre>"; print_r($filters); die;
      if($isExportBank===true){  
         $submittedLinks = $Payment->find_all($paymentFilter, $isExportBank==false ? true : false); 
	   }
	   else
	   {   
		   $submittedLinks = $Payment->find_all($paymentFilter, $isExport==false ? true : false);  
		   $getUpdateDomain = $Payment->find_all($paymentFilter);   
	   }
      $viewData->set('submittedLinks', $submittedLinks);
      //echo "<pre>"; print_r($submittedLinks); die;
      foreach($getUpdateDomain as $dataValue){
         $getDomainWiseIDArr[] = $dataValue['Payment']['id'];
      }

      if($isExport===true){
		  include_once "views/submitted_links_export.php";
        exit;
      }
	   elseif($isExportBank===true){		  
		  include_once "views/submitted_links_export_bank.php";
         exit;
      }else{
		   ob_start();
         include_once "views/submitted_links_data.php";
         $_html = ob_get_contents();
         //echo "<pre>"; print_r($_html); die('1___');
         ob_end_clean();
         echo json_encode(array('html'=>$_html, 'pagination'=>$viewData->get('pageLinks'), 'totalRecords'=> $viewData->get('totalRecords')));
         
      }
      exit;      
   }
   
   function submitted_links_filter_form(){
      global $viewData;
      if($_POST['filterVars']){
         $viewData->set('filterVars', $_POST['filterVars']);
      }
      $Campaign = new Campaign;
      $User = new User;
       $viewData->set('campaignsList', $Campaign->getList());
        $viewData->set('usersList', $User->getList());
      include_once "views/submitted_links_filter_form.php";
      exit;
   }
      
   function submitted_links_do_action(){
      switch($_REQUEST['whatDo']){
       case 'link_delete':
        __action_link_delete($_REQUEST['data']);
        break;
       case 'link_payment':
        __action_link_payment($_REQUEST['data']);
        break;
      }
      exit;
   }
   
   function submitted_links_webmaster_list_json(){
      global $viewData;
      $Payment = new Payment;
      $options=array('group'=>'client_mail,Payment_id', 'fields'=>array('id','client_mail'));
      if(!empty($_GET['q'])){
         $options['where'] = "Payment.client_mail LIKE '%".$_GET['q']."%'";
         $options['limit'] = !empty($_GET['page_limit']) and (int)$_GET['page_limit']>0 ? '0, '.$_GET['page_limit'] : '0, 10';
         
         $clientsEmailFromPayment = $Payment->find_list($options);
         
         $clientEmailList = array();
         if(!empty($clientsEmailFromPayment)){
            foreach($clientsEmailFromPayment as $id=>$email){
               $clientEmailList[$email]=$email;
            }
         }
         echo json_encode(select2DataFormat($clientEmailList));
      }
      exit;
   }

   function submitted_links_edit(){
      global $viewData;
      $viewData->setTitle('Edit Submitted Link');
      $Payment = new Payment;
      $paymentData = $Payment->find_all(array('where'=>"Payment.id = '{$_GET['lid']}' and Payment.status ='Underprocess' "));
      if(!empty($paymentData)){
         $paymentData = array_shift($paymentData);
         $viewData->set('payment', $paymentData);
      }else{
         redirect_to('submitted_links.php');
      }
   }
   function submitted_links_view(){
      global $viewData;
      $viewData->setTitle('View Submitted Link');
      $Payment = new Payment;
      $paymentData = $Payment->find_all(array('where'=>"Payment.id = '{$_GET['lid']}'  "));
      $paymentData = array_shift($paymentData);
         $viewData->set('payment', $paymentData);
   }
   function submitted_links_upload_file(){
	 
	    if ( 0 < $_FILES['invoice_file_val']['error'] ) {
       
		 $msg = array('msg'=>'Error: ' . $_FILES['file']['error']);
    }
    else {
		  $invoice_file=$_FILES['file']['name'];
		 $existpath=WWW_ROOT.DS.'uploads'.DS.'invoice'.DS.$invoice_file;
		 $ext = pathinfo($invoice_file, PATHINFO_EXTENSION);
		 $filetype=array('doc','docx','pdf','jpg','gif','jpeg','png');
		 if(in_array(strtolower($ext),$filetype))
		 {
		$filename=str_replace(".".$ext,"",$invoice_file);
		$i=0;
		 while(file_exists($existpath))
		 {
			 $filenametemp=$filename.$i;
			 $invoice_file=$filenametemp.'.'.$ext;
			 $existpath=WWW_ROOT.DS.'uploads'.DS.'invoice'.DS.$invoice_file; 
			$i++; 
		 }
		 
        move_uploaded_file($_FILES['file']['tmp_name'], WWW_ROOT.DS.'uploads'.DS.'invoice'.DS.$invoice_file);
		$filelink='<a href="http://beta.adlift.com/plb/uploads/invoice/'.$invoice_file.'" target="_blank">View File</a><br/>File is uploaded ';
		$msg = array('filename'=>$invoice_file,'msg'=>'','filelink'=>$filelink);
		 }
		 else
		 {
		$msg = array('msg'=>'Please upload valid file ');
		 }
    }
	echo json_encode($msg);
        exit;
	   
   }
   function submitted_links_edit_save(){
      
         
      global $database, $session;
      
      $msg = array('msg'=>'Link could not updated. Please try again later.', 'status'=>'error');
      if(!empty($_POST['data'])){
         
		  if($_POST['data']['Payment']['currency']!='INR') 
         { 
      //  if(!(__isPaypalEmailExistAndActive($_POST['data']['Payment']['paypal_email'], $msg))){
      //   echo json_encode($msg);
        
      //   exit;
      //  }
		 } 
       $ApprovalLink = new ApprovalLink;
       $Payment = new Payment;
       $Paypal = new Paypal;
       
       $data = $_POST['data'];
      //  echo "<pre>";
      //  print_r($data);
      //  die;
	   if($data['Payment']['invoice_file']=="") 
	   {
		   unset($data['Payment']['invoice_file']);
	   }
       else{
		  
		   unlink(WWW_ROOT.DS.'uploads'.DS.'invoice'.DS.$_POST['invoice_val']);
		 
	   }	   
	   
       $anchorTxt = explode("\n", $data['Payment']['anchor_text']);
       $data['Payment']['anchor_text'] = implode('[', $anchorTxt);
       $data['Payment']['start_date'] = date('Y-m-d', strtotime($data['Payment']['start_date']));
       $data['Payment']['end_date'] = strtotime($data['Payment']['end_date'])<strtotime($data['Payment']['start_date']) ? date('Y-m-d', strtotime($data['Payment']['start_date'])) : date('Y-m-d', strtotime($data['Payment']['end_date']));
	   
	   /*         * ** Start: Added by Ajit : 15-DESC-2014  ***************** */
        $data['Payment']['bencode'] = $data['Payment']['bencode'];
        $data['Payment']['benname'] = $data['Payment']['benname'];
        $data['Payment']['benaccount'] = $data['Payment']['benaccount'];
        $data['Payment']['ifsccode'] = $data['Payment']['ifsccode'];
        $data['Payment']['address'] = $data['Payment']['address'];
        $data['Payment']['city'] = $data['Payment']['city'];
        $data['Payment']['state'] = $data['Payment']['state'];
        $data['Payment']['zip_code'] = $data['Payment']['zip_code'];
		$data['Payment']['tdsamount'] = $data['Payment']['tdsamount'];
		$data['Payment']['grossamount'] = $data['Payment']['grossamount'];
		

        /*         * ** End: Added by Ajit: 15-DESC-2015  ***************** */
		if($data['Payment']['bencode']<>"")
		{	
		$Beneficiary = new Beneficiary;
	    $beneficiarydata = $Beneficiary->find_all(array('contain' => array('Beneficiary'), 'where' => "Beneficiary.bencode='" . $data['Payment']['bencode']."'"));
		
		if(count($beneficiarydata)>0)
		{
		  $beneficiarydata=$beneficiarydata[0];	
		  $paymentdata = $Payment->find_all(array('contain' => array('Payment'), 'where' => " id='".$data['Payment']['id']."'"));
		 
		 if($paymentdata[0]['Payment']['grossamount']=="" || $paymentdata[0]['Payment']['grossamount']=="0")
		 {
		   $tdsamount= $beneficiarydata['Beneficiary']['tdsamount'] + $data['Payment']['tdsamount'] ;
		  $grossamount=$beneficiarydata['Beneficiary']['grossamount'] +$data['Payment']['amount'] ;
		 }
         else
		 {
			 $tdsamount= $beneficiarydata['Beneficiary']['tdsamount'] + $data['Payment']['tdsamount'] - $paymentdata[0]['Payment']['tdsamount'];
		  $grossamount=$beneficiarydata['Beneficiary']['grossamount'] +$data['Payment']['amount'] - $paymentdata[0]['Payment']['amount']; 
			 
		 }			 
		 $Beneficiary->save(array('Beneficiary' => array('id' => $beneficiarydata['Beneficiary']['id'],'bencode' => $data['Payment']['bencode'], 'benname' => $data['Payment']['benname'],'benaccount' => $data['Payment']['benaccount'],'ifsccode' => $data['Payment']['ifsccode'],'address' => $data['Payment']['address'],'city' => $data['Payment']['city'],'state' => $data['Payment']['state'],'zip_code' => $data['Payment']['zip_code'],'grossamount' => $grossamount,'tdsamount' => $tdsamount)));		
			
		}
		else
		{
		 $tdsamount=$data['Payment']['tdsamount'];
		  $grossamount=$data['Payment']['amount'];	
		  if($data['Payment']['bencode']<>"")
		  {
		 $Beneficiary->save(array('Beneficiary' => array('bencode' => $data['Payment']['bencode'], 'benname' => $data['Payment']['benname'],'benaccount' => $data['Payment']['benaccount'],'ifsccode' => $data['Payment']['ifsccode'],'address' => $data['Payment']['address'],'city' => $data['Payment']['city'],'state' => $data['Payment']['state'],'zip_code' => $data['Payment']['zip_code'],'grossamount' => $grossamount,'tdsamount' => $tdsamount, 'postdate' => date('Y-m-d'))));	
		  }
			
		}
        }
	   
      
	   
       $link = $ApprovalLink->find_all(array('contain'=>array('Approval'=>array('fields'=>array('id', 'domain_id'))), 'fields'=>array('id'), 'where'=>"ApprovalLink.id='". $data['Payment']['child_table_id']."'"));
      
       if($Payment->save($data)){
         $link = array_shift($link);
        // add domain id in paypal
        $paypal = $Paypal->find_all(array('where'=>"Paypal.name ='".$data['Payment']['paypal_email']."'"));
        if(!empty($paypal)){
         $paypal = array_shift($paypal);
         $_domainStr = array_filter(explode(',', $paypal['Paypal']['domain_id_string']));
         if(!in_array($link['Approval']['domain_id'], $_domainStr)){
          $_domainStr[] = $link['Approval']['domain_id'];
          $_paypalData['Paypal']['domain_id_string'] = join(',', $_domainStr);
          $_paypalData['Paypal']['id'] = $paypal['Paypal']['id'];
          $_paypalData['Paypal']['updated_on'] = date('Y-m-d H:i:s');
          $Paypal->save($_paypalData);
         }
        }else{
         $_paypalData['Paypal'] = array('name'=>$data['Payment']['paypal_email'], 'domain_id_string'=>$link['Approval']['domain_id'], 'status'=>1, 'added_on'=>date('Y-m-d H:i:s'), 'updated_on'=>date('Y-m-d H:i:s'));
         $Paypal->save($_paypalData);
        }
        
      
       
       }
        $msg = array('msg'=>'Link has been updated.', 'status'=>'success');
      }
      echo json_encode($msg);
      exit;
    
   }
   
   
   function __action_link_delete($data=array()){
      global $session, $database;
      $msg = array('msg'=>'Sorry, link could not be deleted. Please try again later.', 'status'=>'error');
      if(!empty($data)){
         $linkIds = $data['Payment']['id'];
         if(!empty($linkIds)){
            $Payment = new Payment;
            $ApprovalLink = new ApprovalLink;
			 $Beneficiary = new Beneficiary;
			
			foreach($linkIds as  $paymentId)
			{
				
				
			    $paymentdata = $Payment->find_all(array('contain' => array('Payment'), 'where' => " Payment.id='".$paymentId."'"));
				$paymentdata=$paymentdata[0];
				$amount=$paymentdata['Payment']['amount'];
				$tdsamount=$paymentdata['Payment']['tdsamount'];
				$bencode=$paymentdata['Payment']['bencode'];
				$grossamount=$paymentdata['Payment']['grossamount'];
				$status=$paymentdata['Payment']['status'];
				if($grossamount<>"" && $status!='Rejected')
				{	
				$beneficiarydata = $Beneficiary->find_all(array('contain' => array('Beneficiary'), 'where' => "Beneficiary.bencode='" . $bencode."'"));
	            $beneficiarydata=$beneficiarydata[0];
				
				$benid=$beneficiarydata['Beneficiary']['id'];
				$amountben=$beneficiarydata['Beneficiary']['grossamount'];
				$tdsamountben=$beneficiarydata['Beneficiary']['tdsamount'];
				$amountfinel= $amountben- $amount;
				$tdsamountfinel= $tdsamountben- $tdsamount;
				$Beneficiary->save(array('Beneficiary'=>array('id'=>$benid, 'grossamount'=>$amountfinel, 'tdsamount'=>$tdsamountfinel)));
				}	
				
				
				
			}
			
			
			
			
            $deleteConds = array("id IN (". join(',', $linkIds). ") ");
            if($session->read('User.user_type')!= 'superadmin'){
               $deleteConds[] = "user_id='". $session->read('User.id'). "' ";
               $deleteConds[] = "status='Underprocess' ";
            }
            
            $paymentList = $Payment->find_list(array('where'=>join(' AND ', $deleteConds), 'fields'=>array('id','child_table_id')));
            if(!empty($paymentList)){
               foreach($paymentList as $paymentId=>$approvalLinkId){
                  if($Payment->delete(" id=$paymentId ")){
                     $ApprovalLink->save(array('ApprovalLink'=>array('id'=>$approvalLinkId, 'final_link_submit_status'=>0)));
                     $msg = array('msg'=>'link has been deleted.', 'status'=>'success');
                  }   
               }
            }
         }
      }
      echo json_encode($msg);
      exit;
   }
   
   function __action_link_payment($data=array()){
      global $session, $database;
      $msg = array('msg'=>'Sorry, link status could not be changed. Please try again later.', 'status'=>'error');
      if(!empty($data)){
         $Payment = new Payment;
		 $Beneficiary = new Beneficiary;
         $linkAcceptIds = $data['Payment']['accept'];
         $linkRejectIds = $data['Payment']['reject'];
         if(!empty($linkAcceptIds)){
            foreach($linkAcceptIds as $paymentId){
               if($Payment->save(array('Payment'=>array('id'=>$paymentId, 'status'=>'Paid', 'payment_date'=>date('Y-m-d'))))){
                  __sendPaymentDoneMail($paymentId);
                  $msg = array('msg'=>'link status has been changed.', 'status'=>'success');
               }   
            }
         }
         if(!empty($linkRejectIds)){
            foreach($linkRejectIds as $paymentId){
				$paymentdata = $Payment->find_all(array('contain' => array('Payment'), 'where' => "Payment.id='".$paymentId."'"));
				$paymentdata=$paymentdata[0];
				$amount=$paymentdata['Payment']['amount'];
				$tdsamount=$paymentdata['Payment']['tdsamount'];
				$bencode=$paymentdata['Payment']['bencode'];
				$grossamount=$paymentdata['Payment']['grossamount'];
				if($grossamount<>"")
				{	
				$beneficiarydata = $Beneficiary->find_all(array('contain' => array('Beneficiary'), 'where' => "Beneficiary.bencode='" . $bencode."'"));
	            $beneficiarydata=$beneficiarydata[0];
				
				$benid=$beneficiarydata['Beneficiary']['id'];
				$amountben=$beneficiarydata['Beneficiary']['grossamount'];
				$tdsamountben=$beneficiarydata['Beneficiary']['tdsamount'];
				$amountfinel= $amountben- $amount;
				$tdsamountfinel= $tdsamountben- $tdsamount;
				$Beneficiary->save(array('Beneficiary'=>array('id'=>$benid, 'grossamount'=>$amountfinel, 'tdsamount'=>$tdsamountfinel)));
				}
				
               if($Payment->save(array('Payment'=>array('id'=>$paymentId, 'status'=>'Rejected')))){
                  __sendPaymentRejectMail($paymentId);
                  $msg = array('msg'=>'link status has been changed.', 'status'=>'success');
               }   
            }
         }
      }
      echo json_encode($msg);
      exit;
   }
   
   function __sendPaymentDoneMail($paymentId=null){
      global $session;
      if($paymentId){
         $Payment = new Payment;
         $link = $Payment->find_by_id($paymentId);
         if(!empty($link)){
         
            $mail = new PHPMail();
   
            $mail->setFrom($session->read('User.email_id'));
            $mail->setTo($link['Payment']['approached_by']);
            $username = array_shift(explode('@', $link['Payment']['approached_by']));
            $mail->setSubject("PLB - Payment done for submitted link.");
            $message = "Hello {$username},  <br />
                              <p>Payment done by client for submiited links, Please find details below- <br />
                              <b>Domain: </b> {$link['Payment']['domain']} <br />
                              <b>URL: </b> {$link['Payment']['url']} <br />
                              <b>Campaign: </b> {$link['Payment']['campaign_name']} <br />
                              <b>Ammount Paid: </b> ". currency_format($link['Payment']['amount'], $link['Payment']['currency']). " <br />
                              </p>
                              <p>
                              <b>Thanks</b> <br />
                              Adlift PLB
                              </p>
                              ";
            $mail->setBody($message);
            $mail->sendMail();
         }
      }
   }
   
   function __sendPaymentRejectMail($paymentId=null){
      global $session;
      if($paymentId){
         $Payment = new Payment;
         $link = $Payment->find_by_id($paymentId);
         if(!empty($link)){
         
            $mail = new PHPMail();
   
            $mail->setFrom($session->read('User.email_id'));
            $mail->setTo($link['Payment']['approached_by']);
            $username = array_shift(explode('@', $link['Payment']['approached_by']));
            $mail->setSubject("PLB - Payment rejected for submitted link.");
            $message = "Hello {$username},  <br />
                              <p>Payment rejected by client for submiited links, Please find details below- <br />
                              <b>Domain: </b> {$link['Payment']['domain']} <br />
                              <b>URL: </b> {$link['Payment']['url']} <br />
                              <b>Campaign: </b> {$link['Payment']['campaign_name']} <br />
                              <b>Ammount Paid: </b> ". currency_format($link['Payment']['amount'], $link['Payment']['currency']). " <br />
                              </p>
                              <p>
                              <b>Thanks</b> <br />
                              Adlift PLB
                              </p>
                              ";
            $mail->setBody($message);
            $mail->sendMail();
         }
      }
   }
      
   
   function __paymentFilterVars(){
      global $viewData;
      $filter = array();
      $filterVars = array();
      if(!empty($_POST['_uid'])){
       $users = base64_decode($_POST['_uid']);
       $filterVars['users'] = explode(',', $users);
       $filter['Payment'][] = ' Payment.user_id IN ('.$users.') ';
      }
      if(!empty($_POST['_camp'])){
       $campigns = base64_decode($_POST['_camp']);
       $filterVars['campaigns'] = explode(',', $campigns);
       $filter['Payment'][] = ' Payment.campaign_id IN ('.$campigns.') ';
      }
      
      if(!empty($_POST['_st'])){
         $paymentStatus = array('0'=>'Rejected','1'=>'Paid','-1'=>'Underprocess');
       $status = base64_decode($_POST['_st']);
       $filterVars['status'] = explode(',', $status);
       $stArr = array();
       foreach($filterVars['status'] as $_sts){
         $stArr[] = $paymentStatus[$_sts];
       }
       
       $filter['Payment'][] = " Payment.status IN ('".join("','", $stArr)."') ";
      }
      if(!empty($_POST['_pst'])){
         
       $payment_type = base64_decode($_POST['_pst']);
       $filterVars['payment_type'] = $payment_type;
      
       if($payment_type=='NEFT')
	   {
       $filter['Payment'][] = " Payment.payment_type='".$payment_type."' and Payment.bencode!='' ";
	   }
	   else
	   {
		  $filter['Payment'][] = " Payment.payment_type='".$payment_type."' ";   
		   
	   }
      }
      if(!empty($_POST['_rs'])){ 
         $response_status = base64_decode($_POST['_rs']);
         $filterVars['response_status'] = $response_status; 
         $filter['Payment'][] = " Payment.response_status=".$response_status;
      }
      if(!empty($_POST['_is'])){ 
         $index_status = base64_decode($_POST['_is']);
         $filterVars['index_status'] = $index_status; 
         $filter['Payment'][] = " Payment.index_status='".$index_status."'";
      }
      if(!empty($_POST['_d_frm']) && !empty($_POST['_d_to'])){ 
         $from_date = base64_decode($_POST['_d_frm']);
         $to_date = base64_decode($_POST['_d_to']);
         $from_date_data = explode('/',$from_date);
         $to_date_data = explode('/',$to_date);
         $update_from_date = $from_date_data[2].'-'.$from_date_data[0].'-01';
         $update_to_date = $to_date_data[2].'-'.$to_date_data[0].'-30';
         //echo $update_to_date; die('__1__');
         $filterVars['invoiceDateFrom'] = base64_decode($_POST['_d_frm']); 
         $filterVars['invoiceDateTo'] = base64_decode($_POST['_d_to']); 
         $filter['Payment'][] = " Payment.invoice_month >= '$update_from_date' AND Payment.invoice_month <= '$update_to_date'";
      }
      if(!empty($_POST['_ru'])){ 
         $response_status = base64_decode($_POST['_ru']);
         $response_status_value = explode(',', $response_status);
         $_SESSION['response_status_value'] = $response_status_value;
         $filterVars['response_update'] = $response_status_value; 
         //$filter['Payment'][] = " Payment.index_status='".$response_status."'";
      }else{ 
         unset($_SESSION['response_status_value']);
      }
	  if(!empty($_POST['_bc'])){
         
       $bencode = base64_decode($_POST['_bc']);
       $filterVars['bencode'] = $bencode;
      
       
       $filter['Payment'][] = " Payment.bencode like '%".$bencode."%' ";
      }
      if(!empty($_POST['_m'])){
       $m = base64_decode($_POST['_m']);
       $filterVars['month'] = explode(',', $m);
       $filter['Payment'][] = ' Payment.month IN ('.$m.') ';
      }
      //Start: Added by Jitendra: 28-Nov-2014
      if(!empty($_POST['_pm'])){
       $pm = base64_decode($_POST['_pm']);
       $filterVars['payment_month'] = explode(',', $pm);
       $filter['Payment'][] = ' Payment.payment_month IN ('.$pm.') ';
      }
      //End: Added by Jitendra: 28-Nov-2014
      if(!empty($_POST['_y'])){
       $y = base64_decode($_POST['_y']);
       $filterVars['year'] = explode(',', $y);
       $filter['Payment'][] = ' Payment.year IN ('.$y.') ';
      }
      //Start: Added by Jitendra: 28-Nov-2014
      if(!empty($_POST['_py'])){
       $py = base64_decode($_POST['_py']);
       $filterVars['payment_year'] = explode(',', $py);
       $filter['Payment'][] = ' Payment.payment_year IN ('.$py.') ';
      }
      if(!empty($_POST['_fc'])){
         $fc = base64_decode($_POST['_fc']);
         $filterVars['filter_category'] = explode(',', $fc);

         $update_fc = str_replace(',',"','", $fc);
         $update_fc = "$update_fc";
         
         $filter['Payment'][] = " Payment.category IN ('$update_fc') ";
        }
      //End: Added by Jitendra: 28-Nov-2014
      if(!empty($_POST['_ce'])){
       $ce = base64_decode($_POST['_ce']);
       $filterVars['client_mail'] = explode(',', $ce);
       $filter['Payment'][] = " Payment.client_mail IN ('".join("','", $filterVars['client_mail'])."') ";
      }
      
      if(!empty($_POST['_pe'])){
       $pe = base64_decode($_POST['_pe']);
       $filterVars['paypal_email'] = explode(',', $pe);
       $filter['Payment'][] = " Payment.paypal_email IN ('".join("','", $filterVars['paypal_email'])."') ";
      }
      
      $operatorsCollection = array('eq'=>'=', 'lt'=>'<', 'lte'=>'<=', 'gt'=>'>', 'gte'=>'>=');
      if(!empty($_POST['_prVl'])){
       $pr = base64_decode($_POST['_prVl']);
       $prOp = base64_decode($_POST['_prOp']);
       $filterVars['pr_value'] = $pr;
       $filterVars['pr_op'] = $prOp;
       $filter['Payment'][] = " Payment.pr_value ".$operatorsCollection[$prOp]." '".$pr."' ";
      }
      
      if(!empty($_POST['_daVl'])){
       $da = base64_decode($_POST['_daVl']);
       $daOp = base64_decode($_POST['_daOp']);
       $filterVars['da_value'] = $da;
       $filterVars['da_op'] = $daOp;
       $filter['Payment'][] = " Payment.da_value ".$operatorsCollection[$daOp]." '".$da."' ";
      }
      
      if(!empty($_POST['_dom'])){
       $domain = trim(base64_decode($_POST['_dom']));
       $filterVars['domain'] = $domain;
	    $domainurl = $domain;
       $domain = rtrim($domain,"/");	
       $domain = str_replace(array("https://","http://","www."),"",$domain);	
       $domain = preg_replace('/^\./', '', get_domain_name($domain));
       $filter['Payment'][] = " (Payment.domain LIKE 'http://".$domain."%' OR Payment.domain LIKE 'http://www.".$domain."%' or  Payment.url LIKE '%".$domainurl."%') ";
      }
      
      if(!empty($_POST['_dt'])){
         list($dateFrom, $dateTo) = explode('_', base64_decode($_POST['_dt']));
         if(!empty($dateFrom) and !empty($dateTo)){
          $filterVars['dateFrom'] = $dateFrom;
          $filterVars['dateTo'] = $dateTo;
          $filter['Payment'][] = " DATE_FORMAT(Payment.payment_date, '%Y-%m-%d') BETWEEN '".date('Y-m-d', strtotime($dateFrom))."' AND '".date('Y-m-d', strtotime($dateTo))."' "; 
         } else if(!empty($dateFrom) and empty($dateTo)){
          $filterVars['dateFrom'] = $dateFrom;
          $filter['Payment'][] = " DATE_FORMAT(Payment.payment_date, '%Y-%m-%d') = '".date('Y-m-d', strtotime($dateFrom))."' "; 
         }
      }
       //Start: Added by Ajit : 23 june 2017 
	    if(!empty($_POST['_dtstb'])){
         list($dateFromSubmted, $dateToSubmted) = explode('_', base64_decode($_POST['_dtstb']));
         if(!empty($dateFromSubmted) and !empty($dateToSubmted)){
          $filterVars['dateFromSubmted'] = $dateFromSubmted;
          $filterVars['dateToSubmted'] = $dateToSubmted;
          $filter['Payment'][] = " DATE_FORMAT(Payment.added_on, '%Y-%m-%d') BETWEEN '".date('Y-m-d', strtotime($dateFromSubmted))."' AND '".date('Y-m-d', strtotime($dateToSubmted))."' "; 
         } else if(!empty($dateFromSubmted) and empty($dateToSubmted)){
          $filterVars['dateFromSubmted'] = $dateFrom;
          $filter['PaymentSubmted'][] = " DATE_FORMAT(Payment.added_on, '%Y-%m-%d') = '".date('Y-m-d', strtotime($dateFromSubmted))."' "; 
         }
      }
	  //End: Added by Ajit : 23 june 2017 
      $filters['Payment'] = $filter['Payment'] ? implode(' AND ', $filter['Payment']) : '';
      $viewData->set('filterVars', $filterVars);
      return $filters;
   }
   
   // function __isPaypalEmailExistAndActive($email=null, &$msg){ 
   //    if(!empty($email)){
   //     $Paypal = new Paypal;
   //     $paypalCount = $Paypal->count(array('where'=>"Paypal.name='".$email."' AND status=0"));
   //     if($paypalCount>0){
   //      $msg = array('msg'=>'Paypal email black listed.', 'status'=>'error');
   //      return false;
   //     }else{
   //      return true;
   //     }
   //    }else {
   //     $msg = array('msg'=>'Invalid paypal email.', 'status'=>'error');
   //    }
   //    return false;
   // }
 
   function __isPaypalEmailOrUrlExistAndActive($input = null, &$msg) {
      if (!empty($input)) {
          $Paypal = new Paypal;
  
          // Check if input is an email
          if (filter_var($input, FILTER_VALIDATE_EMAIL)) {
              $paypalCount = $Paypal->count(array('where' => "Paypal.name='" . $input . "' AND status=0"));
              if ($paypalCount > 0) {
                  $msg = array('msg' => 'Paypal email is blacklisted.', 'status' => 'error');
                  return false;
              } else {
                  return true;
              }
          }
          // Check if input is a URL
          elseif (filter_var($input, FILTER_VALIDATE_URL)) {
              $paypalCount = $Paypal->count(array('where' => "Paypal.url='" . $input . "' AND status=0"));
              if ($paypalCount > 0) {
                  $msg = array('msg' => 'Paypal URL is blacklisted.', 'status' => 'error');
                  return false;
              } else {
                  return true;
              }
          } else {
              $msg = array('msg' => 'Invalid Paypal email or URL.', 'status' => 'error');
          }
      } else {
          $msg = array('msg' => 'Input cannot be empty.', 'status' => 'error');
      }
      return false;
  }
 // auto call function related to page if exists
$action = isset($_GET['act']) ? $_GET['act'] : 'index';
if(function_exists('submitted_links_'.$action)){ 
 call_user_func('submitted_links_'.$action);
}
// include default template
$useLayout = isset($_GET['act']) ? 'submitted_links_'.$_GET['act'].'.php' : 'submitted_links_index.php';
include "views/default.php";
 ?>